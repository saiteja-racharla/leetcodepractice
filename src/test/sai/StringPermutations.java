//This program works even when repitition is present in string
package test.sai;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StringPermutations {
	public static void main(String[] args) {
		List<List<Character>> result=permutate("AABV".toCharArray());
		System.out.println(result);
	}
	public static List<List<Character>> permutate(char[] input){
		HashMap<Character, Integer> hm=new HashMap<Character, Integer>();
		for(char ch:input){
			if(hm.containsKey(ch)){
				int count=(int)hm.get(ch);
				hm.put(ch, count+1);
			}
			else{
				hm.put(ch, 1);
			}
		}
		char[] str=new char[hm.size()];
		int[] countArr=new int[hm.size()];
		int index=0;
		for(Map.Entry<Character,Integer> entry: hm.entrySet()){
			str[index]=entry.getKey();
			countArr[index]=entry.getValue();
			index++;
		}
		char result[]=new char[input.length];
		List<List<Character>> overAllResult=new ArrayList<>();
		List<Character> tempResult=new ArrayList<>();
		permutateUtil(overAllResult,tempResult,str, countArr, result, 0);
		return overAllResult;
	}
	public static void permutateUtil(List<List<Character>> overAllResult, List<Character> tempResult, char[] str, int[] countArr, char result[],int level){
		if(level==result.length){
			List<Character> listC = new ArrayList<Character>();
		    for (char c : result) {
		        listC.add(c);
		    }
		    overAllResult.add(listC);
			return;
		}
		for(int i=0;i<str.length;i++){
			if(countArr[i]==0){
				continue;
			}
			result[level]=str[i];
			countArr[i]--;
			permutateUtil(overAllResult, tempResult, str, countArr, result, level+1);
			countArr[i]++;
		}
	}
}
