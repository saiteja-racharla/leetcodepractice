/*
 * Given a positive integer, return its corresponding column title as appear in an Excel sheet.

For example:
 1 -> A
    2 -> B
    3 -> C
    ...
    26 -> Z
    27 -> AA
    28 -> AB 
    Example 1:

Input: 1
Output: "A"
Example 2:

Input: 28
Output: "AB"
Example 3:

Input: 701
Output: "ZY"
 */

package test.sai;

public class ExcelSheetColumnTitle {

	public static void main(String[] args) {
		int i=18229;
		StringBuilder sb=new StringBuilder();
		while(i>0){
			i--;
			sb.insert(0, (char)(i%26+'A'));
			i=i/26;
			
		}
		System.out.println(sb.toString());
	}

}
