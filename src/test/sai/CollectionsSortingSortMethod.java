package test.sai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

class Student1 implements Comparable{
	int age;
	int rollNo;
	String name;
	Student1(int age,int rollNo,String name){
		this.age=age;
		this.rollNo=rollNo;
		this.name=name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getRollNo() {
		return rollNo;
	}
	public void setRollNo(int rollNo) {
		this.rollNo = rollNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public int compareTo(Object o) {
		int compareAge=((Student1)o).getAge();
		return this.age-compareAge;
	}
	public String toString() {
        return "[ rollno=" + rollNo + ", name=" + name + ", age=" + age + "]";
    }
}

public class CollectionsSortingSortMethod {

	public static void main(String[] args) {
		ArrayList<Student1> al=new ArrayList<Student1>();
		al.add(new Student1(12,1,"sai"));
		al.add(new Student1(13,2,"teja"));
		al.add(new Student1(3,3,"ram"));
		al.add(new Student1(2,4,"raj"));
		Collections.sort(al);
		for(Student1 str: al){
			System.out.println(str);
	   }
	}

}
