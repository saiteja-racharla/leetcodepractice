package test.sai;

public class ZigZagString {

	public static void main(String[] args) {
		System.out.println("The first two solutions are most efficient solutions");
		String convertedZigzagString = convert("PAYPALISHIRINGANDAMAZONISHIRING",6);
		String convertedZigzagString1 = convert1("PAYPALISHIRINGANDAMAZONISHIRING",6);
		System.out.println(convertedZigzagString);
		System.out.println(convertedZigzagString1);
		String convertedZigzagString2 = convert2("PAYPALISHIRINGANDAMAZONISHIRING",6);
		System.out.println(convertedZigzagString2);
	}
	public static String convert(String s, int numRows){
		if (numRows == 1)
			return s;
	 
		StringBuilder sb = new StringBuilder();
		// step
		int step = 2 * numRows - 2;
	 
		for (int i = 0; i < numRows; i++) {
			//first & last rows
			if (i == 0 || i == numRows - 1) {
				for (int j = i; j < s.length(); j = j + step) {
					sb.append(s.charAt(j));
				}
			//middle rows	
			} else {
				int j = i;
				boolean flag = true;
				int step1 = 2 * (numRows - 1 - i);
				int step2 = step - step1;
	 
				while (j < s.length()) {
					sb.append(s.charAt(j));
					if (flag)
						j = j + step1;
					else
						j = j + step2;
					flag = !flag;
				}
			}
		}
	 
		return sb.toString();
	}
	public static String convert1(String s, int numRows){
		int step=2*numRows-2;
		boolean flag=true;
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<numRows;i++){
			if(i==0 || i==numRows-1){
				for(int j=i;j<s.length();j=j+step){
					sb.append(s.charAt(j));
				}
			}
			else{
				int j=i;
				int step1=2*(numRows-1-i);
				int step2=step-step1;
				while(j<s.length()){
					sb.append(s.charAt(j));
					if(flag)
						j=j+step1;
					else
						j=j+step2;
					flag=!flag;
				}
			}
		}
		return sb.toString();
	}
	public static String convert2(String s, int numRows){
		StringBuilder sb[]=new StringBuilder[numRows];
		for(int i=0;i<numRows;i++){
			sb[i]=new StringBuilder();
		}
		int indexOfString=0;
		while(indexOfString<s.length()){
			for(int j=0;j<numRows && indexOfString<s.length();j++){
				sb[j].append(s.charAt(indexOfString));
				indexOfString++;
			}
			for(int j=numRows-2;j>=1 && indexOfString<s.length();j--){
				sb[j].append(s.charAt(indexOfString));
				indexOfString++;
			}
		}
		for(int i=1;i<numRows;i++){
			sb[0].append(sb[i]);
		}
		return sb[0].toString();
	}
}
