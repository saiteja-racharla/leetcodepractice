/*
 * This program does intersection and union of 2 sorted arrays
 * Time complexity of both union and intersection is O(m+n)
 * Another approach that is useful when difference between sizes of two given 
 * arrays is significant.
The idea is to iterate through the shorter array and do a binary search for every element 
of short array in big array (note that arrays are sorted). Time complexity of this 
solution is O(min(mLogn, nLogm)). This solution works better than the above approach 
when ratio of larger length to smaller is more than logarithmic order.
 */
package test.sai;

public class UnionAndIntersectionOfSortedArrays {
	static void printUnion(int[] arr1,int[] arr2,int m,int n){
		int i = 0, j = 0;
		  while (i < m && j < n)
		  {
		    if (arr1[i] < arr2[j])
		      System.out.print(arr1[i++]+" ");
		    else if (arr2[j] < arr1[i])
		      System.out.print(arr2[j++]+" ");
		    else
		    {
		      System.out.print(arr2[j++]+" ");
		      i++;
		    }
		  }
		 
		  /* Print remaining elements of the larger array */
		  while(i < m)
		   System.out.print(arr1[i++]+" ");
		  while(j < n)
		   System.out.print(arr2[j++]+" ");
	}
	static void printIntersection(int arr1[], int arr2[], int m, int n)
	{
		  int i = 0, j = 0;
		  while (i < m && j < n)
		  {
		    if (arr1[i] < arr2[j])
		      i++;
		    else if (arr2[j] < arr1[i])
		      j++;
		    else /* if arr1[i] == arr2[j] */
		    {
		    	System.out.print(arr2[j++]+" ");
		      i++;
		    }
		  }
	}
	public static void main(String[] args) {
		int arr1[] = {1, 2, 4, 5, 6};
		int arr2[] = {2, 3, 5, 7};
		int m = arr1.length;
		int n = arr2.length;
		printUnion(arr1, arr2, m, n);
		System.out.println();
		printIntersection(arr1, arr2, m, n);

	}

}
