package test.sai;

public class ReverseLinkedList {

	public static void main(String[] args) {
		reverseNodeOperations rev=new reverseNodeOperations();
		rev.insert(1);
		rev.insert(3);
		rev.insert(7);
		rev.insert(6);
		rev.insert(5);
		rev.insert(4);
		rev.insert(12);
		rev.insert(11);
		rev.insert(10);
		rev.insert(9);
		rev.insert(8);
		
		rev.display();
		rev.reverse();
	}

}

class reverseNodeOperations{
	public reverseNode head;
	public reverseNode temp;
	public reverseNode parent;
	public void insert(int data){
		reverseNode node=new reverseNode(data);
		if(head==null){
			head=node;
			temp=head;
		}
		else{
			temp.next=node;
			temp=temp.next;
		}
	}
	public void display(){
		temp=head;
		while(temp!=null){
			System.out.print(temp.data+" ");
			temp=temp.next;
		}
		System.out.println();
	}
	public void reverse(){
		reverseNode next=null;
		reverseNode current=head;
		reverseNode prev=null;
		while(current!=null){
			next=current.next;
			current.next=prev;
			prev=current;
			current=next;
		}
		head=prev;
		temp=head;
		while(temp!=null){
			System.out.print(temp.data+" ");
			temp=temp.next;
		}
	}
}

class reverseNode{
	int data;
	reverseNode next;
	public reverseNode(int data){
		this.data=data;
		this.next=null;
	}
}
