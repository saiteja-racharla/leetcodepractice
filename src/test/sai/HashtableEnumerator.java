package test.sai;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

public class HashtableEnumerator {

	public static void main(String[] args) {
		Hashtable<Integer,String> ht=new Hashtable<Integer,String>();
		ht.put(1, "sai");
		ht.put(2, "teja");
		ht.put(3, "ram");
		//display keys
		System.out.println("Displaying keys");
		Enumeration<Integer> e=ht.keys();
		while(e.hasMoreElements()){
			System.out.println(e.nextElement());
		}
		//display values
		System.out.println("Displaying values");
		Enumeration<String> en=ht.elements();
		while(en.hasMoreElements()){
			System.out.println(en.nextElement());
		}
		//Displaying by iterator
		Set<Integer> set=ht.keySet();
		System.out.println("Displaying by iterator");
		Iterator<Integer> it=set.iterator();
		while(it.hasNext()){
			Integer i=it.next();
			System.out.println("key:"+i+"value:"+ht.get(i));
		}
	}

}
