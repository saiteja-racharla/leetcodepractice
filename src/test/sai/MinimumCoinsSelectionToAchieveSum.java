/*Given a total and coins of certain denomination with infinite supply, what is the 
 * minimum number of coins it takes to form this total.
 * Program not done yet
 */
package test.sai;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

public class MinimumCoinsSelectionToAchieveSum {
	public static int maximumNumberOfCoins(int[] coins, int total){
		int temp[][]=new int[coins.length+1][total+1];
		int totalNumberOfCoins=0;
		for(int i=1;i<=coins.length;i++){
			for(int j=1;j<=total;j++){
				if(j>=coins[i]){
					temp[i][j]=Math.min(temp[i-1][j],1+temp[i][j-coins[i]]);
				}
				else{
					temp[i][i]=temp[i-1][j];
				}
			}
		}
		return temp[coins.length][total];
	}
	public static void main(String[] args) {
		int coins[]={1,5,6,8};
		int total=11;
		System.out.println(maximumNumberOfCoins(coins,total));
	}
}
