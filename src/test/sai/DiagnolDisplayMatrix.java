/*
 * this program Prints the Matrix Diagonally
 */
package test.sai;

public class DiagnolDisplayMatrix {

	public static void main(String[] args) {
		int[][] arr={{1,2,3,4},{5,6,7,8}};
		int i=0,j=0;
		for(int line=0;line<arr.length-1;line++){
			i=line;
			j=0;
			while(i>=0 && j>=0 && i<=line && j<=line){
				System.out.print(arr[i][j]+"\t");
				i--;
				j++;
			}
			System.out.println();
		}
		for(int line=arr.length-1;line>=0;line--){
			i=arr.length-1;
			j=arr.length-line-1;
			while(i>=0 && j>=0 && j<arr[0].length){
				System.out.print(arr[i][j]+"\t");
				i--;
				j++;
			}
			System.out.println();
		}
	}

}
