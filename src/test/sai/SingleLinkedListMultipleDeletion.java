package test.sai;
class Node{
	public int data;
	public Node next;
	public Node(int data){
		this.data=data;
		this.next=null;
	}
}

class LinkedList{
	public Node front;
	public Node rear;
	LinkedList(){
		front=null;
	}
	public void insertElement(int data){
		if(front==null){
			Node node=new Node(data);
			node.next=front;
			front=node;
			rear=front;
		}
		else{
			Node node=new Node(data);
			rear.next=node;
			rear=node;
		}
	}
	public void display(){
		Node temp=front;
		while(temp!=null){
			System.out.print(temp.data+"  ");
			temp=temp.next;
		}
	}
	public void delete(int data){
		Node temp=front;
		Node previous=front;
		while(temp!=rear){
			if(front.data==data && temp.data==data){
				front=front.next;
				temp=front;
				previous=front;
			}
			else{
				if(temp.data==data){
					previous.next=temp.next;
					
				}
				else{
					previous=temp;
				}
				temp=temp.next;
			}
		}
	}
}
public class SingleLinkedListMultipleDeletion {

	public static void main(String[] args) {
		LinkedList sll=new LinkedList();
		sll.insertElement(1);
		sll.insertElement(1);
		sll.insertElement(2);
		sll.insertElement(3);
		sll.insertElement(4);
		sll.insertElement(1);
		sll.insertElement(5);
		sll.insertElement(7);
		//sll.insertElement(2);
		//sll.insertElement(10);
		System.out.println("Before deleting");
		sll.display();
		sll.delete(1);
		System.out.println("After deleting");
		sll.display();

	}

}
