/*
 *  This program converts infix expression to postfix expression
 *  Enter Infix Expression
	input  - a+b*c
	output - abc*+
 */
package test.sai;

import java.util.Scanner;

public class InfixToPostfix {
	static String infixString;
	static char infix[];
	static char ch,temp;
	static int length=0,top=0;
	static char post[]=new char[20];
	static char s[]=new char[20];
	public static void main(String[] args) {
		System.out.println("Enter Infix Expression");
		Scanner sc=new Scanner(System.in);
		infixString=sc.nextLine();
		infix=infixString.toCharArray();
		s[top++]='#';
		for(int i=0;i<infix.length;i++){
			ch=infix[i];
			if(Character.isLetter(ch))
				post[length++]=ch;
			else{
				if(ch=='+' || ch=='-' || ch=='*' || ch=='/' || ch=='^' || ch=='(' || ch==')'){
					switch(ch){
					case '-':
					case '+':check();
							push(ch);
							break;
					case '*':
					case '/':check();
							push(ch);
							break;
					case '(':push(ch);
							break;
					case ')':do{
								temp=pop();
								post[length++]=temp;
							}while(temp!='(');
							length=length-1;
							break;
					}
				}
			}
		}
		while(s[top]!='#')
			post[length++]=pop();
		for(int j=0;j<length-1;j++)
			System.out.print(post[j]);
		if(sc!=null)
			sc.close();
	}
	static void check(){
		while(priority(ch)<=priority(s[top-1]))
			post[length++]=pop();
	}
	static void push(char ch){
		s[top++]=ch;
	}
	static char pop(){		
		top--;
		char poppedElement=s[top];
		return poppedElement;
	}
	static int priority(char ch){
		if(ch=='+' || ch=='-')
			return 2;
		else if(ch=='*' || ch=='/')
			return 3;
		else if(ch=='^')
			return 4;
		else 
			return 0;
	}
}
