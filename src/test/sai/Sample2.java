package test.sai;

import java.util.*;

public class Sample2 {

	public static void main(String[] args) {
		List<List<Integer>> foregroundList=new ArrayList<List<Integer>>();
		List<List<Integer>> backgroundList=new ArrayList<List<Integer>>();
		List<Integer> a1=new ArrayList<Integer>();
		a1.add(1);
		a1.add(3);
		List<Integer> a2=new ArrayList<Integer>();
		a2.add(2);
		a2.add(5);
		List<Integer> a3=new ArrayList<Integer>();
		a3.add(3);
		a3.add(7);
		List<Integer> a4=new ArrayList<Integer>();
		a4.add(4);
		a4.add(10);
		foregroundList.add(a1);
		foregroundList.add(a2);
		foregroundList.add(a3);
		foregroundList.add(a4);
		List<Integer> a5=new ArrayList<Integer>();
		a5.add(1);
		a5.add(2);
		List<Integer> a6=new ArrayList<Integer>();
		a6.add(2);
		a6.add(3);
		List<Integer> a7=new ArrayList<Integer>();
		a7.add(3);
		a7.add(4);
		List<Integer> a8=new ArrayList<Integer>();
		a8.add(4);
		a8.add(5);
		backgroundList.add(a5);
		backgroundList.add(a6);
		backgroundList.add(a7);
		backgroundList.add(a8);
		Iterator<List<Integer>> it=foregroundList.iterator();
		HashMap<Integer,Integer> fore=new HashMap<Integer,Integer>();
		HashMap<Integer,Integer> back=new HashMap<Integer,Integer>();
		while(it.hasNext()){
			List<Integer> temp=it.next();
			fore.put(temp.get(0), temp.get(1));
		}
		it=backgroundList.iterator();
		while(it.hasNext()){
			List<Integer> temp=it.next();
			back.put(temp.get(0), temp.get(1));
		}
		System.out.println(fore);
		System.out.println(back);
		List<List<Integer>> result=new ArrayList<List<Integer>>();
		List<List<Integer>> result1=new ArrayList<List<Integer>>();
		int max=0;
		int deviceCapacity=10;
		for (Map.Entry<Integer, Integer> entry : fore.entrySet()) {
		    for(Map.Entry<Integer, Integer> entry1:back.entrySet()){
		    	int val1=entry.getValue();
		    	int val2=entry1.getValue();
		    	if(val1+val2==deviceCapacity){
		    		List<Integer> temp=new ArrayList<Integer>();
		    		temp.add(entry.getKey());
		    		temp.add(entry1.getKey());
		    		result.add(temp);
		    	}
		    	else{
		    		if(max<val1+val2 && val1+val2<deviceCapacity){
		    			max=val1+val2;
		    			List<Integer> temp=new ArrayList<Integer>();
			    		temp.add(entry.getKey());
			    		temp.add(entry1.getKey());
			    		if(result1.size()>0){
			    			result1.remove(result1.size()-1);
			    			result1.add(temp);
			    		}
			    		else{
			    			result1.add(temp);
			    		}
		    		}
		    	}
		    	
		    }
		}
		System.out.println(result);
		System.out.println(result1);
	}

}
