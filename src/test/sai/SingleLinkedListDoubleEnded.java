package test.sai;

class NodeDoubleEnded{
	int data;
	NodeDoubleEnded next;
	public NodeDoubleEnded(int data) {
		this.data=data;
		this.next=null;
	}
}

class LinkedListDoubleEnded{
	NodeDoubleEnded front;
	NodeDoubleEnded rear;
	public LinkedListDoubleEnded() {
		front=null;
		rear=null;
	}
	public void insertFirst(int data){
		if(front==null){
			NodeDoubleEnded node=new NodeDoubleEnded(data);
			node.next=front;
			front=node;
			rear=front;
		}
		else{
			NodeDoubleEnded node=new NodeDoubleEnded(data);
			rear.next=node;
			rear=node;
		}
	}
}

public class SingleLinkedListDoubleEnded {

	public static void main(String[] args) {
		LinkedListDoubleEnded slld=new LinkedListDoubleEnded();
		slld.insertFirst(1);
		slld.insertFirst(2);
		slld.insertFirst(3);

	}

}
