package test.sai;

import java.util.Scanner;

public class TimeConversionTo24Hours {
	public static void main(String[] args) {
    	Scanner in=new Scanner(System.in);
        String time = in.nextLine();
        if(time.substring(8, 10).equals("PM") && !time.substring(0, 2).equals("12")){
            int hours=Integer.parseInt(time.substring(0, 2));
     	   hours+=12;
     	   System.out.println(hours+time.substring(2,8));
        }
        else
     	   System.out.println(time.substring(0,8));
        in.close();
    }
}
