/*This program is to print the output in the following format
				1
			2	3	2
		3	4	5	4	3
	4	5	6	7	6	5	4
5	6	7	8	9	8	7	6	5*/
package test.sai;

public class NumberStars1 {

	public static void main(String[] args) {
		int rows=5,i,j,elem=0,count=0;
		for(i=1;i<=rows;i++){
			for(j=1;j<=rows-i;j++){
				System.out.print("\t");
			}
			count=i;
			for(j=1;j<=(i*2)-1;j++){
				elem++;
				if(elem>=i){
					System.out.print(count+"\t");
					count--;
				}
				else{
					System.out.print(count+"\t");
					count++;
				}
			}
			elem=0;
			System.out.println();
		}

	}

}
