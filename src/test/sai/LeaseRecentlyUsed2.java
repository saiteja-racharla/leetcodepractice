/*
 * 
 */
package test.sai;

import java.util.Hashtable;

public class LeaseRecentlyUsed2 {
	public static void main(String[] args) {
		LRUCache cache=new LRUCache(2);
		cache.put(1, 1);
		cache.put(2, 2);
		System.out.println(cache.get(1));       // returns 1
		cache.put(3, 3);    // evicts key 2
		System.out.println(cache.get(2));       // returns -1 (not found)
		cache.put(4, 4);    // evicts key 1
		System.out.println(cache.get(1));       // returns -1 (not found)
		System.out.println(cache.get(3));       // returns 3
		System.out.println(cache.get(4));       // returns 4
	}
}

class LRUCache{
	Hashtable<Integer, DlinkedNodeLRU>  cache = new Hashtable<Integer, DlinkedNodeLRU>();
	int capacity;
	int count;
	DlinkedNodeLRU head,tail;
	public LRUCache(int capacity) {
		this.count=0;
		this.capacity=capacity;
        head=new DlinkedNodeLRU();
        head.pre=null;
        tail=new DlinkedNodeLRU();
        tail.post=null;
        head.post=tail;
        tail.pre=head;
    }
    
	/*
	 * Add after head
	 */
	public void addNode(DlinkedNodeLRU node){
		node.pre=head;
		node.post=head.post;
		head.post.pre=node;
		head.post=node;
	}
	
	/*
	 * Remove the node from the linked list
	 */
	private void removeNode(DlinkedNodeLRU node){
		DlinkedNodeLRU pre=node.pre;
		DlinkedNodeLRU post=node.post;
		pre.post = post;
		post.pre = pre;
	}
	
	/*
	 * Move certain node to the head
	 */
	private void moveToHead(DlinkedNodeLRU node){
		this.removeNode(node);
		this.addNode(node);
	}
	
	/*
	 * Remove the last element
	 */
	private DlinkedNodeLRU popTail(){
		DlinkedNodeLRU tailPre=tail.pre;
		removeNode(tailPre);
		return tailPre;
	}
	
    public int get(int key) {
        DlinkedNodeLRU node=cache.get(key);
        if(node==null){
        	return -1;
        }
        this.moveToHead(node);
        return node.value;
        
    }
    
    public void put(int key, int value) {
        DlinkedNodeLRU node=cache.get(key);
        if(node==null){
        	node=new DlinkedNodeLRU();
        	node.key=key;
        	node.value=value;
        	cache.put(key, node);
        	this.addNode(node);
        	count=count+1;
        	if(count>capacity){
        		DlinkedNodeLRU tail=this.popTail();
        		cache.remove(tail.key);
        		count=count-1;
        	}
        }
        else{
        	node.value=value;
        	this.moveToHead(node);
        }
        
    }
}

class DlinkedNodeLRU{
	int key;
	int value;
	DlinkedNodeLRU pre;
	DlinkedNodeLRU post;
}
