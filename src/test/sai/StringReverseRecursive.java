/*
 * this program reverses a string using recursive technique
 */

package test.sai;

public class StringReverseRecursive {

	public static void main(String[] args) {
		String ar="sai";
		String rev=reverseRecursively(ar);
		System.out.println(rev);
	}
	public static String reverseRecursively(String str) {
		if (str.length() < 2) {
            return str;
        }
        return reverseRecursively(str.substring(1)) + str.charAt(0);
    }
}
