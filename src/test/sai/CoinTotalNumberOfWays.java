/*
 * This program gives us coins of certain denominations and a total sum. we have to 
 * find number of ways total can be formed from coins assuming infinity supply of coins.
 * This program is solved using dynamic programming appraoch. See tushar roy videos.
 *https://www.youtube.com/watch?v=_fgjrs570YE&list=PLrmLmBdmIlpsHaNTPP_jHHDx_os9ItYXr&index=10
 *input-{1,2,3,4,10},10
 *output-24
 *it also displays the ways of getting it.
 */
package test.sai;

import java.util.ArrayList;
import java.util.List;

public class CoinTotalNumberOfWays {
	public static int numberOfSolutions(int total, int coins[]){
        int temp[][] = new int[coins.length+1][total+1];
        for(int i=0; i <= coins.length; i++){
            temp[i][0] = 1;
        }
        for(int i=1; i <= coins.length; i++){
            for(int j=1; j <= total ; j++){
                if(coins[i-1] > j){
                    temp[i][j] = temp[i-1][j];
                }
                else{
                    temp[i][j] = temp[i][j-coins[i-1]] + temp[i-1][j];
                }
            }
        }
        return temp[coins.length][total];
    }
	public static void printCoinChangingSolution(int total,int coins[]){
        List<Integer> result = new ArrayList<>();
        printActualSolution(result, total, coins, 0);
    }
    
    private static void printActualSolution(List<Integer> result,int total,int coins[],int pos){
        if(total == 0){
            for(int r : result){
                System.out.print(r + " ");
            }
            System.out.print("\n");
        }
        for(int i=pos; i < coins.length; i++){
            if(total >= coins[i]){
                result.add(coins[i]);
                printActualSolution(result,total-coins[i],coins,i);
                result.remove(result.size()-1);
            }
        }
    }
	public static void main(String[] args) {
		int total = 5;
        int coins[] = {1,2,5};
        System.out.println(numberOfSolutions(total, coins));
        printCoinChangingSolution(total, coins);
	}
}
