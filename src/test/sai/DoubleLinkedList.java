/* This Program covers all functions of double linkedlist
 Insertion
 Deletion
 Insert Last
 Delete Last
 Insert After
 Delete
 Display Forward
 Display backward
 */

package test.sai;

class Node2{
	int data;
	Node2 previous;
	Node2 next;
	public Node2(int data){
		this.data=data;
		this.previous=null;
		this.next=null;
	}
}

class DoubleLinkedList1{
	public Node2 front;
	public Node2 rear;
	DoubleLinkedList1(){
		front=null;
		rear=null;
	}
	public void insertFirst(int data){
		Node2 node=new Node2(data);
		if(front==null){
			front=node;
			rear=front;
		}
		else{
			front.previous=node;
			node.next=front;
			front=node;
		}
	}
	public void insertLast(int data){
		Node2 node=new Node2(data);
		if(front==null){
			front=node;
			rear=front;
		}
		else{
			rear.next=node;
			node.previous=rear;
			rear=node;
		}
	}
	public void displayFromFront(){
		Node2 temp=front;
		while(temp!=null){
			System.out.print(temp.data+" ");
			temp=temp.next;
		}
		System.out.println();
	}
	public void displayFromBack(){
		Node2 temp=rear;
		while(temp!=null){
			System.out.print(temp.data+" ");
			temp=temp.previous;
		}
		System.out.println();
	}
	public void deleteFront(){
		System.out.println("Element deleted from Front"+front.data);
		front=front.next;
		front.previous=null;
	}
	public void deleteLast(){
		System.out.println("Element deleted from Last"+rear.data);
		rear=rear.previous;
		rear.next=null;
	}
	public void deleteItem(int data){
		Node2 temp=front;
		Node2 previous=front;
		if(front==null){
			System.out.println("No Elements");
		}
		else{
			while(temp!=null){
				if(temp.data==data){
					if(temp==front){
						temp=temp.next;
						temp.previous=null;
						front=temp;
					}
					else if(temp==rear){
						rear=previous;
						rear.next=null;
						break;
					}
					else{
						previous.next=temp.next;
						temp=temp.next;
						temp.previous=previous;
					}
					continue;
				}
				previous=temp;
				temp=temp.next;
			}
		}
	}
	public void insertAfter(int insertNode,int data){
		Node2 temp=front;
		while(temp!=null){
			Node2 newNode=new Node2(data);
			if(temp.data==insertNode){
				if(temp==rear){
					temp.next=newNode;
					newNode.next=null;
					newNode.previous=temp;
					rear=newNode;
					break;
				}
				else{
					newNode.next=temp.next;
					newNode.previous=temp;
					temp.next=newNode;
					temp=temp.next;
					temp=temp.next;
					temp.previous=newNode;
					continue;
				}
			}
			temp=temp.next;
		}
	}
}

public class DoubleLinkedList {

	public static void main(String[] args) {
		DoubleLinkedList1 dl=new DoubleLinkedList1();
		dl.insertFirst(7);
		dl.insertFirst(8);
		dl.insertFirst(8);
		dl.insertFirst(8);
		dl.insertFirst(9);
		dl.insertFirst(10);
		dl.insertLast(4);
		dl.insertLast(5);
		dl.insertLast(6);
		dl.insertLast(8);
		dl.insertLast(8);
		System.out.println("Display from Front");
		dl.displayFromFront();
		System.out.println("Display from back");
		dl.displayFromBack();
		dl.deleteFront();
		dl.deleteLast();
		System.out.println("Display from Front");
		dl.displayFromFront();
		System.out.println("Display from back");
		dl.displayFromBack();
		dl.deleteItem(8);
		System.out.println("Display from Front after deleting item");
		dl.displayFromFront();
		dl.insertAfter(7, 8);
		dl.insertAfter(6,8);
		System.out.println("Display from Front after insertion");
		dl.displayFromFront();

		dl.insertAfter(8, 1);
		System.out.println("Display from Front after insertion");
		dl.displayFromFront();
	}

}
