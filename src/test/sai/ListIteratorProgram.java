package test.sai;

import java.util.ArrayList;
import java.util.ListIterator;

public class ListIteratorProgram {
	public static void main(String[] args) {
		ArrayList<Integer> al=new ArrayList<Integer>();
		al.add(1);
		al.add(34);
		al.add(64);
		al.add(12);
		al.add(11);
		ListIterator<Integer> it=al.listIterator(al.size());
		while(it.hasPrevious()){
			System.out.println(it.previous());
		}
	}
}
