/**
 * this program is used to check if array is in sorted or not using recursion
 */
package test.sai;

public class IsArrayInSortedOrder {
	public static void main(String[] args){
		int arr[]={2,1};
		System.out.println(isArrayInSortedOrder(arr, 0));
	}
	public static int isArrayInSortedOrder(int[] arr,int index){
		if(index>=arr.length-1){
			return 1;
		}
		if(arr[index]<arr[index+1])
			return isArrayInSortedOrder(arr, index+1);
		else
			return 0;
	}
}
