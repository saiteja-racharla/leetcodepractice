package test.sai;

import java.util.Scanner;

public class SmallNumber {

	public static void main(String[] args) {
		Scanner scanner=new Scanner(System.in);
		int totalNum=scanner.nextInt();
		int array[]=new int[100];
		int i,small;
		for(i=0;i<totalNum;i++){
			System.out.println("Enter number");
			array[i]=scanner.nextInt();
		}
		small=array[0];
		for(i=1;i<totalNum;i++){
			if(array[i]<small)
				small=array[i];
		}
		System.out.println("Small Number: "+small);
		scanner.close();
	}

}
