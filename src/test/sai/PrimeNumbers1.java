package test.sai;

import java.util.Scanner;

public class PrimeNumbers1 {

	public static void main(String[] args) {
		Scanner scanner=new Scanner(System.in);
		int num=scanner.nextInt();
		int i=0,count=0;
		if(num==2)
			System.out.println(num);
		else if(num>3){
			for(i=2;i<=num;i++){
				for(int j=1;j<num;j++){
					if(i%j==0)
						count++;
				}
				if(count==2)
					System.out.println(i+"\t");
				count=0;
			}
		}
		scanner.close();
	}

}
