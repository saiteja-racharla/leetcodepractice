package test.sai;

public class CheckArraySortedNonRecursion {

	public static void main(String[] args) {
		int a[]={1,2,3,4,5,-1,-2};
		int b[]={3,2,1,4,-1};
		System.out.println(checkArraySortedNonRecursion(a, a.length));
		System.out.println(checkArraySortedNonRecursion(b, b.length));
	}
	public static String checkArraySortedNonRecursion(int array[],int index){
		String returnArray="Array is Sorted";
		while(index!=1){
			if(array[index-1]<array[index-2]){
				returnArray= "Array is not sorted";
				break;
			}
			else{
				index=index-1;
			}
		}
		return returnArray;
	}
}
