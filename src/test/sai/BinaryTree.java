package test.sai;

import org.w3c.dom.Node;

class BinaryNode{
	int data;
	BinaryNode leftChild;
	BinaryNode rightChild;
	public BinaryNode(int data) {
		super();
		this.data = data;
		this.leftChild = null;
		this.rightChild = null;
	}
}

class BinaryNodeOperations{
	BinaryNode root;
	public BinaryNodeOperations() {
		this.root = null;
	}
	public void insertOperation(int data){
		BinaryNode bn=new BinaryNode(data);
		if(root==null)
			root=bn;
		else{
			BinaryNode current=root;
			BinaryNode parent;
			while(true){
				parent=current;
				if(current.data>data){//left child
					current=current.leftChild;
					if(current==null){
						parent.leftChild=bn;
						return;
					}
				}
				else{	//right child
					current=current.rightChild;
					if(current==null){
						parent.rightChild=bn;
						return;
					}
				}
			}
		}
	}
	
	public int max(int left,int right){
		return (left>right)?left:right;
	}
	
	public int findHeight(BinaryNode data){
		if(data==null)
			return -1;
		int leftHeight=findHeight(data.leftChild);
		int rightHeight=findHeight(data.rightChild);
		return max(leftHeight,rightHeight)+1;
	}
	
	public BinaryNode find(int data){
		BinaryNode temp=root;
		while(temp.data!=data){
			if(temp.data>data){
				temp=temp.leftChild;
			}
			else{
				temp=temp.rightChild;
			}
			if(temp==null)
				return null;
		}
		return temp;
	}
	
	public int height(){
		return findHeight(root);
	}
	
	public void inOrderTraverse(BinaryNode temp){
		if(temp!=null){
			inOrderTraverse(temp.leftChild);
			System.out.print(temp.data+" ");
			inOrderTraverse(temp.rightChild);
		}
	}
	public void preOrderTraverse(BinaryNode temp){
		if(temp!=null){
			System.out.print(temp.data+" ");
			preOrderTraverse(temp.leftChild);
			preOrderTraverse(temp.rightChild);
		}
	}
	public void postOrderTraverse(BinaryNode temp){
		if(temp!=null){
			postOrderTraverse(temp.leftChild);
			postOrderTraverse(temp.rightChild);
			System.out.print(temp.data+" ");
		}
	}
	public void traverse(){
		System.out.println("Inorder");
		inOrderTraverse(root);
		System.out.println("Preorder");
		preOrderTraverse(root);
		System.out.println("Postorder");
		postOrderTraverse(root);
	}
	
	public BinaryNode minimum(){
		BinaryNode temp=root;
		while(temp.leftChild!=null){
			temp=temp.leftChild;
		}
		return temp;
	}
	
	public BinaryNode maximum(){
		BinaryNode temp=root;
		while(temp.rightChild!=null){
			temp=temp.rightChild;
		}
		return temp;
	}
	/*public boolean delete(int item){
		BinaryNode current=root;
		BinaryNode parent=root;
		boolean isLeftChild=false;
		while(current.data!=item){
			parent=current;
			if(current.data>item){
				current=current.leftChild;
				isLeftChild=true;
			}
			else{
				current=current.rightChild;
				isLeftChild=false;
			}
			if(current==null)
				return false;
		}
		if(current.leftChild==null && current.rightChild==null){
			if(isLeftChild)
				parent.leftChild=null;
			else
				parent.rightChild=null;
		}
		else if(current.rightChild==null){
			if(current==root){
				root=current.leftChild;
			}
			else if(isLeftChild){
				parent.leftChild=current.leftChild;
			}
			else{
				parent.rightChild=current.leftChild;
			}
		}
		else if(current.leftChild==null){
			if(current==root)
				root=current.rightChild;
			else if(isLeftChild){
				parent.leftChild = current.rightChild;
			}
			else{
				parent.rightChild=current.rightChild;
			}
			
		}
		else{
			BinaryNode successor=getSuccessor(current);
			if(current == root)
				root = successor;
			else if(isLeftChild)
				parent.leftChild = successor;
			else
				parent.rightChild = successor;
			successor.leftChild = current.leftChild;
		}
	}
	public BinaryNode getSuccessor(BinaryNode delNode){
		BinaryNode successorParent=delNode;
		BinaryNode successor=delNode;
		BinaryNode current=delNode.rightChild;
		while(current!=null){
			successorParent=successor;
			successor=current;
			current=current.leftChild;
		}
		if(successor!=delNode.rightChild){
			successorParent.leftChild=successor.rightChild;
			successor.rightChild=delNode.rightChild;
		}
		return successor;
	}*/
}

public class BinaryTree {
	public static void main(String[] args) {
		BinaryNodeOperations bnt=new BinaryNodeOperations();
		bnt.insertOperation(35);
		bnt.insertOperation(25);
		bnt.insertOperation(15);
		bnt.insertOperation(30);
		bnt.insertOperation(7);
		bnt.insertOperation(20);
		bnt.insertOperation(28);
		bnt.insertOperation(31);
		bnt.insertOperation(45);
		bnt.insertOperation(40);
		bnt.insertOperation(50);
		bnt.insertOperation(38);
		bnt.insertOperation(42);
		bnt.insertOperation(49);
		bnt.insertOperation(51);
		bnt.insertOperation(52);
		/*bnt.insertOperation(80);
		bnt.insertOperation(52);
		bnt.insertOperation(71);
		bnt.insertOperation(63);
		bnt.insertOperation(67);*/
		BinaryNode result= bnt.find(42);
		if(result!=null)
			System.out.println("Found");
		else
			System.out.println("Not found");
		System.out.println("Traversing");
		bnt.traverse();
		System.out.println("Minimum value is "+bnt.minimum().data);
		System.out.println("Minimum value is "+bnt.maximum().data);
		System.out.println(bnt.height());
		//bnt.delete(35);
	}

}
