package test.sai;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Flatten2dVector {

	public static void main(String[] args) {
		List<List<Integer>> ls=new ArrayList<List<Integer>>();
		List<Integer> ls1=new ArrayList<Integer>();
		List<Integer> ls2=new ArrayList<Integer>();
		List<Integer> ls3=new ArrayList<Integer>();
		ls1.add(1);
		ls1.add(2);
		ls2.add(3);
		ls3.add(4);
		ls3.add(5);
		ls3.add(6);
		ls.add(ls1);
		ls.add(ls2);
		ls.add(ls3);
		Vector2D vec=new Vector2D(ls);
		while(vec.hasNext()){
			System.out.print(vec.next()+"\t");
		}
	}

}

class Vector2D implements Iterator<Integer> {
	Iterator<List<Integer>> it=null;
	Iterator<Integer> row=null;
	int index=0;
    public Vector2D(List<List<Integer>> vec2d) {
    	if(vec2d == null || vec2d.size() == 0) return;
        it=vec2d.iterator();
        row=it.hasNext()?it.next().iterator():null;
    }

    @Override
    public Integer next() {
        int next=row.next();
        return next;
    }

    @Override
    public boolean hasNext() {
    	if(row.hasNext()==false && it.hasNext()==true) row=it.next().iterator();
    	return row!=null && row.hasNext();
        
    }
}
