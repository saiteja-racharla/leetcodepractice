package test.sai;

import java.util.Arrays;
import java.util.Scanner;

public class Anagrams {

	public static void main(String[] args) {
		/*Scanner scanner=new Scanner(System.in);
		System.out.println("First String:");
		String firstString=scanner.nextLine();
		System.out.println("Second String");
		String secondString=scanner.nextLine();
		String firstCopy=firstString.replaceAll("\\s", "");
		String secondCopy=secondString.replaceAll("\\s", "");
		if(firstCopy.length()!=secondCopy.length())
			System.out.println("Not anagrams");
		else{
			char first[]=firstCopy.toLowerCase().toCharArray();
			char second[]=secondCopy.toLowerCase().toCharArray();
			Arrays.sort(first);
			Arrays.sort(second);
			boolean status = Arrays.equals(first, second);
			if(status==true)
				System.out.println("Anagrams");
			else
				System.out.println("Not Anagrams");
		}*/
		Scanner scanner=new Scanner(System.in);
		System.out.println("First String:");
		String firstString=scanner.nextLine();
		System.out.println("Second String");
		String secondString=scanner.nextLine();
		String s=firstString.replaceAll("\\s", "");
		String t=secondString.replaceAll("\\s", "");
		int[] arr=new int[27];
        for(int i=0;i<s.length();i++){
			arr[s.charAt(i)-'a']++;
		}
		for(int i=0;i<t.length();i++){
			arr[t.charAt(i)-'a']--;
		}
		int count=0;
		boolean status=true;
		for(int i:arr){
			if(i!=0)
				status=false;
		}
		if(status)
			System.out.println("Anagrams");
		else
			System.out.println("Not anagrams");
		scanner.close();

	}

}
