/*
 * This program is for string pattern matching. * and ? program. Program not working in java
 * So have copy pasted from geeksforgeeks
 * // A C program to match wild card characters
		#include <stdio.h>
		#include <stdbool.h>
		 
		// The main function that checks if two given strings
		// match. The first string may contain wildcard characters
		bool match(char *first, char * second)
		{
		    // If we reach at the end of both strings, we are done
		    if (*first == '\0' && *second == '\0')
		        return true;
		 
		    // Make sure that the characters after '*' are present
		    // in second string. This function assumes that the first
		    // string will not contain two consecutive '*'
		    if (*first == '*' && *(first+1) != '\0' && *second == '\0')
		        return false;
		 
		    // If the first string contains '?', or current characters
		    // of both strings match
		    if (*first == '?' || *first == *second)
		        return match(first+1, second+1);
		 
		    // If there is *, then there are two possibilities
		    // a) We consider current character of second string
		    // b) We ignore current character of second string.
		    if (*first == '*')
		        return match(first+1, second) || match(first, second+1);
		    return false;
		}
		 
		// A function to run test cases
		void test(char *first, char *second)
		{  match(first, second)? puts("Yes"): puts("No"); }
		 
		// Driver program to test above functions
		int main()
		{
		    test("g*ks", "geeks"); // Yes
		    test("ge?ks*", "geeksforgeeks"); // Yes
		    test("g*k", "gee");  // No because 'k' is not in second
		    test("*pqrs", "pqrst"); // No because 't' is not in first
		    test("abc*bcd", "abcdhghgbcd"); // Yes
		    test("abc*c?d", "abcd"); // No because second must have 2
		                             // instances of 'c'
		    test("*c*d", "abcd"); // Yes
		    test("*?c*d", "abcd"); // Yes
		    return 0;
		}
 */
package test.sai;

public class WildcardCharactersStringPatternMatching {
	static boolean match(String first, String second){
		return false;
	}
	static String test(String first, String second){
		if(match(first, second))
			return "Yes";
		else
			return "No";
	}
	public static void main(String[] args) {
		
		System.out.println(isMatch("g*ks", "geeks")); // Yes
		System.out.println(isMatch("ge?ks*", "geeksforgeeks")); // Yes
		System.out.println(isMatch("g*k", "gee"));  // No because 'k' is not in second
		System.out.println(isMatch("*pqrs", "pqrst")); // No because 't' is not in first
		System.out.println(isMatch("abc*bcd", "abcdhghgbcd")); // Yes
		System.out.println(isMatch("abc*c?d", "abcd")); // No because second must have 2
	                             // instances of 'c'
		System.out.println(isMatch("*c*d", "abcd")); // Yes
		System.out.println(isMatch("*?c*d", "abcd")); // Yes
	}
	
	/*public static boolean isMatch(String pattern, String str){
		int s = 0, p = 0, match = 0, starIdx = -1;            
        while (s < str.length()){
            // advancing both pointers
            if (p < pattern.length()  && (pattern.charAt(p) == '?' || str.charAt(s) == pattern.charAt(p))){
                s++;
                p++;
            }
            // * found, only advancing pattern pointer
            else if (p < pattern.length() && pattern.charAt(p) == '*'){
                starIdx = p;
                match = s;
                p++;
            }
           // last pattern pointer was *, advancing string pointer
            else if (starIdx != -1){
                p = starIdx + 1;
                match++;
                s = match;
            }
           //current pattern pointer is not star, last patter pointer was not *
          //characters do not match
            else return false;
        }
        
        //check for remaining characters in pattern
        while (p < pattern.length() && pattern.charAt(p) == '*')
            p++;
        
        return p == pattern.length();
	}*/
	public static boolean isMatch(String p, String s){
		char str[]=s.toCharArray();
        char pattern[]=p.toCharArray();
        /*
        * Replace multiple * with single star
        */
        int writeIndex=0;
        boolean isFirst=true;
		for(int i=0;i<pattern.length;i++){
            if(pattern[i]=='*'){
                if(isFirst){
                    pattern[writeIndex++]=pattern[i];
                    isFirst=false;
                }
            }
            else{
                pattern[writeIndex++]=pattern[i];
                isFirst=true;
            }
        }
		/*for(int i=0;i<writeIndex;i++){
			System.out.print(pattern[i]);
		}*/
		boolean output[][]=new boolean[str.length+1][writeIndex+1];
        if(writeIndex>0 && pattern[0]=='*'){
            output[0][1]=true;
        }
        output[0][0]=true;
        for(int i=1;i<output.length;i++){
            for(int j=1;j<output[0].length;j++){
                if(pattern[j-1]=='?' || str[i-1]==pattern[j-1]){
                    output[i][j]=output[i-1][j-1];
                }
                else if(pattern[j-1]=='*'){
                    output[i][j]=output[i-1][j]||output[i][j-1];
                }
            }
        }
		return output[str.length][writeIndex];
	}
}
