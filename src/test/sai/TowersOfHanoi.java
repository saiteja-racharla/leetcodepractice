package test.sai;

public class TowersOfHanoi {
	public static void main(String[] args){
		solve(3,'A','C','B');
	}
	
	public static void solve(int n, char from, char to, char aux){
		if(n==1){
			System.out.println(from+"->"+to);
			return;
		}
		solve(n-1,from,aux,to);
		System.out.println(from+"->"+to);
		solve(n-1,aux,to,from);
	}
}
