package test.sai;

public class CountBitsInRange {

	public static void main(String[] args) {
		int num=5;
		int[] f = new int[num + 1];
	    for (int i=1; i<=num; i++) 
	    	f[i] = f[i >> 1] + (i & 1);
	    for(int i=0;i<=num;i++){
	    	System.out.print(f[i]+"\t");
	    }
	}

}
