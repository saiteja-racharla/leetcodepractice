package test.sai;

public class SingleElementInSortedArray {
	public static void main(String[] args) {
		int nums[]={1,1,2,2,3,3,4,8,8};
		int low=0;
        int high=nums.length-1;
        while(low<high){
        	if(low==high)
        		break;
            int mid=(low+high)/2;
            if(mid%2==0){
                if(nums[mid]!=nums[mid+1])
                    high=mid;
                else
                    low=mid;
            }
            else{
                if(nums[mid]!=nums[mid+1])
                    low=mid+1;
                else
                    high=mid-1;
            }
        }
        System.out.println(nums[low]);
	}
}
