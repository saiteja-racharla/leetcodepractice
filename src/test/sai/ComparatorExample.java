package test.sai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
class Student{
	int age;
	String name;
	Student(String name,int age){
		this.age=age;
		this.name=name;
	}
}

class NameComparator implements Comparator{
	@Override
	public int compare(Object o1, Object o2) {
		Student s1=(Student)o1;
		Student s2=(Student)o2;
		return s1.name.compareTo(s2.name);
	}
}

public class ComparatorExample {

	public static void main(String[] args) {
		List al=new ArrayList();
	//  ArrayList al=new ArrayList();	even this will work.
		al.add(new Student("sai",12));
		al.add(new Student("abhi",12));
		al.add(new Student("jaggu",12));
		Collections.sort(al, new NameComparator());
		Iterator it=al.iterator();
		while(it.hasNext()){
			System.out.println(((Student)it.next()).name);
		}
	}

}
