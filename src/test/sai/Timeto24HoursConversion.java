package test.sai;

import java.util.Scanner;

public class Timeto24HoursConversion {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String time = in.next();
		String AMPM=time.substring(8);
		String formattedNumber;
		int hours=0;
		if(AMPM.equals("PM")){
			if(Integer.parseInt(time.substring(0,2))>12){
				hours=Integer.parseInt(time.substring(0, 2));
				hours=hours+12;
			}
		}
		if(time.substring(0,2).equals("12") && AMPM.equals("AM")){
			hours=Integer.parseInt(time.substring(0, 2));
			hours=hours-12;
		}
		if(hours<10)
			formattedNumber = String.format("%02d", hours);
		else
			formattedNumber = hours+"";
		System.out.printf("%s:%s:%s",formattedNumber,time.substring(3,5),time.substring(6,8));

	}

}
