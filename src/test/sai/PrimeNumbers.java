package test.sai;

public class PrimeNumbers {

	public static void main(String[] args) {
		int a[]=new int[100];
		int i,k;
		for(i=0;i<100;i++){
			a[i]=i+1;
		}
		System.out.println("Read Numbers are:");
		for(i=0;i<100;i++){
			System.out.print(a[i]+"\t");
		}
		for(i=1;i<100;i++){
			if(a[i]==0){
				continue;
			}
			else{
				for(k=i+1;k<100;k++){
					if(a[k]==0)
						continue;
					else{
						if(a[k]%a[i]==0){
							a[k]=0;
						}
					}
				}
			}
		}
		System.out.println("Prime numbers are:");
		for(i=1;i<100;i++){
			if(a[i]==0)
				continue;
			else
				System.out.print(a[i]+"\t");
		}

	}

}
