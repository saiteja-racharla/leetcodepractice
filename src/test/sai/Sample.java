package test.sai;

public class Sample {
	public static void main(String[] args) {
		Sample s=new Sample();
		int N=4;
		int trust[][]= {
				
		};
		int judge=s.findJudge(N, trust);
		System.out.println(judge);
	}
	public int findJudge(int N, int[][] trust) {
        int judge=-1;
        int[] trusted=new int[N+1];
        for(int[] t:trust){
            trusted[t[1]]++;
            judge=t[1];
            trusted[t[0]]--;
        }
        for(int i=1;i<trusted.length;i++){
            if(trusted[i]==N){
                return i;
            }
        }
        return -1;
    }
}