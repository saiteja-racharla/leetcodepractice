/* Print output in the follwing format
 *********
 *******
  *****
   ***
    *
 */
package test.sai;

public class ReversePyramid {

	public static void main(String[] args) {
		int rows=5;
		int j;
		for(int i=rows;i>=0;i--){
			for(j=0;j<rows-i;j++)
			{
				System.out.print(" ");
			}
			for(j=1;j<=(i*2)-1;j++){
				System.out.print("*");
			}
			System.out.println();
		}

	}

}
