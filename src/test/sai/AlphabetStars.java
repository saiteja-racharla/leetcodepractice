/*
 This program is to print the output in following format.
 ABCDEFGFEDCBA
 ABCDEF FEDCBA
 ABCDE   EDCBA
 ABCD     DCBA
 ABC       CBA
 AB         BA
 A			 A
 */
package test.sai;

public class AlphabetStars {

	public static void main(String[] args) {
		int count=0;
		int i,j,n=65,countSpace=0;
		int maxrows=7;
		for(i=1;i<=maxrows;i++){
			//Print first set of alphabets in a row in ascending order
			for(j=0;j<=maxrows-i;j++){
				System.out.print((char)n);
				n++;
			}
			// From Here 
			//Only for the first row
			count=i;
			if(j==7){
				n=n-2;
				count+=2;
				System.out.print((char)n);
			}
			//To Here
			//loop for maintaining spaces
			for(countSpace=1;countSpace<=(i*2)-3;countSpace++){
				System.out.print(" ");
			}
			//Print next set of alphabets in a row in descending order
			for(j=0;j<=maxrows-count;j++){
				n--;
				System.out.print((char)n);
			}
			System.out.println();
			n=65;
		}
	}
}
