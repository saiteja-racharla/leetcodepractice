package test.sai;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class BreadthFirstSearch {
	static VertexState[] vertextlist=new VertexState[10];
	static int[][] edges=new int[10][10];
	static int numberOfVertices;
	static Queue q=new LinkedList();
	enum VertexState{
		White, Gray, Black 
	}
	
	public static void BFS(){
		for(int i=0;i<numberOfVertices;i++){
			vertextlist[i]=VertexState.White;
		}
		q.add(0);
		System.out.println(1);
		vertextlist[0]=VertexState.Gray;
		while(!q.isEmpty()){
			int ele=(int)q.remove();
			for(int i=0;i<numberOfVertices;i++){
				if(isEdge(ele,i) && vertextlist[i]==VertexState.White){
					System.out.println(i+1);
					q.add(i);
					vertextlist[i]=VertexState.Gray;
				}
			}
		}
			
	}
	
	public static boolean isEdge(int i,int j){
		if(edges[i][j]==1)
			return true;
		else 
			return false;
	}
	
	public static void main(String[] args) {
		System.out.println("Enter Number of vertices");
		Scanner sc=new Scanner(System.in);
		numberOfVertices=sc.nextInt();
		for(int i=0;i<numberOfVertices;i++){
			for(int j=0;j<numberOfVertices;j++){
				System.out.println("Enter 1 if Edge between "+(i+1)+"and "+(j+1));
				edges[i][j]=sc.nextInt();
			}
		}
		BFS();

	}

}
