/* Print the output in folliwing format.Also called as pascal triangle
 						1		
					1		1		
				1		2		1		
			1		3		3		1		
		1		4		6		4		1		
	1		5		10		10		5		1
 */
package test.sai;

import java.util.Arrays;
import java.util.List;

public class PascalTriangle {

	public static void main(String[] args) {
		int n=6;
		int[][] arr=new int[n][n];
        for (int i = 0; i < n; i++)
        {
        	for(int k=1;k<=n-i;k++){		// this loop is only for neat arragement.
        		System.out.print("\t");		// you can remove this if there is time constraint.
        	}
			for (int j = 0; j <= i; j++)
			{
			if (i == j || j == 0)
				arr[i][j] = 1;
			else
				arr[i][j] = arr[i-1][j-1] + arr[i-1][j];
			System.out.print(arr[i][j]+"\t\t");
			}
			System.out.print("\n");
			getRow(5);
        }
	}
	public static List<Integer> getRow(int rowIndex) {
        //DP solution
        Integer[] a=new Integer[rowIndex+1];
        for(int i=0;i<rowIndex+1;i++) a[i]=0;
        a[0]=1;
        for(int i=1;i<rowIndex+1;i++){
            for(int j=i;j>=1;j--){
                a[j]=a[j]+a[j-1];
            }
        }
        return Arrays.asList(a);
    }
}
