/*
 * Given two strings of numbers, how do you add them 
 * without converting the string into integers.
 * we do addition, as if we normally perform addition on paper.
 * Time Complexity is O(m+n).
 * m and n are total number of characters in first and second string
 * 
 * first commented approach is shortest way to do. second is just elaborated version.
 */
package test.sai;

public class StringNumbersAddition {
	public static void main(String[] args) {
		/*String num1="2";
		String num2="43";
		StringBuilder sb = new StringBuilder();
        int carry = 0;
        for(int i = num1.length() - 1, j = num2.length() - 1; i >= 0 || j >= 0 || carry == 1; i--, j--){
            int x = i < 0 ? 0 : num1.charAt(i) - '0';
            int y = j < 0 ? 0 : num2.charAt(j) - '0';
            sb.append((x + y + carry) % 10);
            carry = (x + y + carry) / 10;
        }
        System.out.println(sb.reverse().toString());*/
		String num1="2";
		String num2="43";
		int i = num1.length() - 1;
        int j = num2.length() - 1;
        int carry = 0;
        char[] num1Array = num1.toCharArray();
        char[] num2Array = num2.toCharArray();
        StringBuilder sb = new StringBuilder();
        while (i >= 0 || j >= 0 || carry == 1) {
            int a = i >= 0 ? (num1Array[i--] - '0') : 0;//if we dont do -'0', we get some
            int b = j >= 0 ? (num2Array[j--] - '0') : 0;//wrong character into variable a.
            int sum = a + b + carry;
            sb.insert(0, sum % 10);
            carry = sum / 10;
        }
        System.out.println(sb.toString());
	}
}
