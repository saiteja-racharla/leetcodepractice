/*
 * Find a subsequence in given array in which the subsequence's elements are 
 * in sorted order, lowest to highest, and in which the subsequence is as long as possible.
 * input: {3,4,-1,0,6,2,3}
 * output:4
 * sequence is{-1,0,2,3} --- output is shown in reverse order{3,2,0,-1}
 * Time complexity is O(n^2).
 * Space complexity is O(n)
 */
package test.sai;

public class LongestIncreasingSubsequence {
	public static int longestSubsequenceWithActualSolution(int arr[]){
		int T[] = new int[arr.length];
		int actualSolution[] = new int[arr.length];
        for(int i=0; i < arr.length; i++){
            T[i] = 1;
            actualSolution[i] = i;
        }
        for(int i=1; i < arr.length; i++){
            for(int j=0; j < i; j++){
            	if(arr[i]>arr[j]){
            		if(T[j]+1>T[i]){
            			T[i]=T[j]+1;
            			actualSolution[i] = j;
            		}
            	}
            }
        }
        //find the index of max number in T 
        int maxIndex = 0;
        for(int i=0; i < T.length; i++){
            if(T[i] > T[maxIndex]){
                maxIndex = i;
            }
        }
        int t = maxIndex;
        int newT = maxIndex;
        do{
            t = newT;
            System.out.print(arr[t] + " ");
            newT = actualSolution[t];
        }while(t != newT);
        System.out.println();
		return T[maxIndex];
	}
	public static void main(String[] args) {
		int arr[] = {3,4,-1,0,6,2,3};
		System.out.println(longestSubsequenceWithActualSolution(arr));
	}
}
