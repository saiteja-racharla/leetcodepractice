/*
 * Given two strings s and t, determine if they are isomorphic.
Two strings are isomorphic if the characters in s can be replaced to get t.
All occurrences of a character must be replaced with another character while preserving 
the order of characters. No two characters may map to the same character but a character 
may map to itself.

For example,
Given "egg", "add", return true.
Given "foo", "bar", return false.
Given "paper", "title", return true.

Note:
You may assume both s and t have the same length.

Time complexity using 1st approach is very less.
 */

package test.sai;

import java.util.Arrays;
import java.util.HashMap;

public class Isomorphic {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(checkIsomorphic1("aab","xxy"));
		System.out.println(checkIsomorphic1("aab","xyz"));
		System.out.println(checkIsomorphic2("ab","aa"));
	}
	
	public static boolean checkIsomorphic1(String s,String t){
		if(s.length()!=t.length())
			return false;
		int[] as = new int[256];
        int[] at = new int[256];
        for(int i=0;i<s.length();i++){
            if(as[s.charAt(i)]!=at[t.charAt(i)]) 
            	return false;
            as[s.charAt(i)]=at[t.charAt(i)] = i+1;
        }
		return true;
	}
	public static boolean checkIsomorphic2(String s,String t){
		if(s.length()!=t.length())
			return false;
		HashMap<Character,Character> hm=new HashMap<Character,Character>();
		for(int i=0;i<s.length();i++){
			if(hm.containsKey(s.charAt(i))){
				if(hm.get(s.charAt(i)).equals(t.charAt(i)))
					continue;
				else
					return false;
			}
			else{
				if(hm.containsValue(t.charAt(i)))
					return false;
				else 
					hm.put(s.charAt(i), t.charAt(i));
			}
        }
		return true;
	}

}
