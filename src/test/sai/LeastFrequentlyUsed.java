/** Least Frequently Used
 * LFU Cache
 * Design and implement a data structure for Least Frequently Used (LFU) cache. It should support the following operations: get and put.
get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
put(key, value) - Set or insert the value if the key is not already present. When the cache reaches its capacity, it should invalidate the least frequently used item before inserting a new item. For the purpose of this problem, when there is a tie (i.e., two or more keys that have the same frequency), the least recently used key would be evicted.
Note that the number of times an item is used is the number of calls to the get and put functions for that item since it was inserted. This number is set to zero when the item is removed.
Follow up:
Could you do both operations in O(1) time complexity?
LFUCache cache = new LFUCache( 2 --capacity  );

cache.put(1, 1);
cache.put(2, 2);
cache.get(1);       // returns 1
cache.put(3, 3);    // evicts key 2
cache.get(2);       // returns -1 (not found)
cache.get(3);       // returns 3.
cache.put(4, 4);    // evicts key 1.
cache.get(1);       // returns -1 (not found)
cache.get(3);       // returns 3
cache.get(4);       // returns 4
 */

package test.sai;

import java.util.HashMap;

public class LeastFrequentlyUsed {

	public static void main(String[] args) {
		LFUCache cache = new LFUCache( 2 /* capacity */ );

		/*
		 * cache.put(1, 1); cache.put(2, 2); System.out.println(cache.get(1)); //
		 * returns 1 cache.put(3, 3); // evicts key 2 System.out.println(cache.get(2));
		 * // returns -1 (not found) System.out.println(cache.get(3)); // returns 3.
		 * cache.put(4, 4); // evicts key 1. System.out.println(cache.get(1)); //
		 * returns -1 (not found) System.out.println(cache.get(3)); // returns 3
		 * System.out.println(cache.get(4)); // returns 4
		 */
		cache.put(3, 1);
		cache.put(2, 1);
		cache.put(2, 3);
		cache.put(2, 4);
		cache.put(4, 4);
		System.out.println(cache.get(2));
	}

}
class LFUCache{
	int capacity=0, currentSize=0, minimum=0;
	HashMap<Integer, DLinkedListNodeLFU> nodeMap;
	HashMap<Integer, DLinkedListNodeLFUHelper> countMap;
	public LFUCache(int capacity) {
		this.capacity=capacity;
		nodeMap=new HashMap<Integer, DLinkedListNodeLFU>();
		countMap=new HashMap<Integer, DLinkedListNodeLFUHelper>();
    }
    
    public int get(int key) {
    	DLinkedListNodeLFU node=nodeMap.get(key);
    	if(node==null)
    		return -1;
    	DLinkedListNodeLFUHelper oldList=countMap.get(node.frequency);
    	oldList.remove(node);
    	if(node.frequency == minimum && oldList.size==0)
    		minimum++;
    	node.frequency++;
    	DLinkedListNodeLFUHelper newList=countMap.getOrDefault(node.frequency, new DLinkedListNodeLFUHelper());
    	newList.add(node);
    	countMap.put(node.frequency, newList);
    	return node.value;
    }
    
    public void put(int key, int value) {
    	if(capacity==0)
    		return;
    	DLinkedListNodeLFU node;
    	if(!nodeMap.containsKey(key)) {
    		node=new DLinkedListNodeLFU(key, value);
    		nodeMap.put(key, node);
    		if(currentSize == capacity) {
    			//we should remove now
    			DLinkedListNodeLFUHelper oldList = countMap.get(minimum);
    			nodeMap.remove(oldList.removeLast().key);
    			currentSize--;
    		}
    		currentSize++;
    		minimum=1;
    		DLinkedListNodeLFUHelper newList=countMap.getOrDefault(node.frequency, new DLinkedListNodeLFUHelper());
    		newList.add(node);
    		countMap.put(node.frequency, newList);
    	} else {
    		node=nodeMap.get(key);
    		node.value=value;
    		DLinkedListNodeLFUHelper oldList=countMap.get(node.frequency);
    		oldList.remove(node);
    		if (node.frequency == minimum && oldList.size == 0) 
    			minimum++; 
    		node.frequency++;
    		DLinkedListNodeLFUHelper newList=countMap.getOrDefault(node.frequency, new DLinkedListNodeLFUHelper());
    		newList.add(node);
    		countMap.put(node.frequency, newList);
    	}
    }
}
class DLinkedListNodeLFU{
	int key,value,frequency;
	DLinkedListNodeLFU prev;
	DLinkedListNodeLFU next;
	public DLinkedListNodeLFU(int key, int value) {
		this.key=key;
		this.value=value;
		this.frequency=1;
	}
	
	public DLinkedListNodeLFU() {
		
	}
}

class DLinkedListNodeLFUHelper{
	DLinkedListNodeLFU head;
	DLinkedListNodeLFU tail;
	int size;
	public DLinkedListNodeLFUHelper() {
		head=new DLinkedListNodeLFU(0,0);
		tail=new DLinkedListNodeLFU(0,0);
		head.next=tail;
		tail.prev=head;
	}
	public void add(DLinkedListNodeLFU node) {
		head.next.prev=node;
		node.next=head.next;
		node.prev=head;
		head.next=node;
		size++;
	}
	public void remove(DLinkedListNodeLFU node) {
		node.prev.next=node.next;
		node.next.prev=node.prev;
		size--;
	}
	public DLinkedListNodeLFU removeLast() {
		if(size>0) {
			DLinkedListNodeLFU node=tail.prev;
			remove(node);
			return node;
		} else {
			return null;
		}
	}
	
}