package test.sai;

import java.util.HashMap;
import java.util.Scanner;

public class Panagram {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		String inputSentence=sc.nextLine().replaceAll("\\s", "").toLowerCase();
		boolean status = isPanagram(inputSentence);
		if(status==true)
			System.out.println("pangram");
		else
			System.out.println("not pangram");
		sc.close();
	}
	public static boolean isPanagram(String inputSentence){
		if(inputSentence.length()<26)
			return false;
		else{
			for(char ch='a';ch<='z';ch++){
				if(inputSentence.indexOf(ch)<0)
					return false;
			}
		}
		return true;
	}
}
