package test.sai;

import java.lang.reflect.Array;
import java.util.Arrays;

public class GenerateStringsOfNBits {
	static int a[]=new int[3];
	public static void main(String[] args){
		generateStringsNBits(3);
	}
	public static void generateStringsNBits(int n){
		if(n<=0){
			System.out.println(Arrays.toString(a));
			return;
		}
		a[n-1]=0;
		generateStringsNBits(n-1);
		a[n-1]=1;
		generateStringsNBits(n-1);
	}
}
