package test.sai;
import java.util.*;

public class checkStringCanBeMadePalindrome {

	public static void main(String[] args) {
		System.out.println(canMakePalindrome("madams"));
	}
	public static boolean canMakePalindrome(String s) {
        Set<Character> oddLetters = new HashSet<>();
        for ( char c : s.toCharArray() ) {
            if ( ! oddLetters.remove(c) ) {
                oddLetters.add(c);
            }
        }
        return oddLetters.size() <= 1;
    }

}
