package test.sai;
/*
 Last In First Out(LIFO)-----STACK---------------
class Node{
	public int data;
	public Node next;
	Node(int data){
		this.data=data;
		this.next=null;
	}
}

class LinkedList{
	public Node first;
	LinkedList(){
		first=null;
	}
	public void insertElement(int data){
		Node link=new Node(data);
		link.next=first;
		first=link;
	}
	public void display(){
		Node temp=first;
		while(temp!=null){
			System.out.print(temp.data);
			temp=temp.next;
		}
	}
	public Node delete(){
		Node temp=first;
		first=first.next;
		return temp;
	}
}

public class SingleLinkedList {
	public static void main(String[] args) {
		LinkedList sll=new LinkedList();
		sll.insertElement(1);
		sll.insertElement(2);
		sll.insertElement(3);
		System.out.println("Before deleting");
		sll.display();
		Node del=sll.delete();
		System.out.println("Deleted Node"+del.data);
		System.out.println("After deleting");
		sll.display();
	}
}
*/


//FIrst in first out(FIFO)-------QUEUE-----------

class NodeSLL{
	public int data;
	public NodeSLL next;
	public NodeSLL(int data){
		this.data=data;
		this.next=null;
	}
}

class LinkedListSLL{
	public NodeSLL front;
	public NodeSLL rear;
	LinkedListSLL(){
		front=null;
	}
	public void insertElement(int data){
		if(front==null){
			NodeSLL node=new NodeSLL(data);
			node.next=front;
			front=node;
			rear=front;
		}
		else{
			NodeSLL node=new NodeSLL(data);
			rear.next=node;
			rear=node;
		}
	}
	public void display(){
		NodeSLL temp=front;
		while(temp!=null){
			System.out.print(temp.data+" ");
			temp=temp.next;
		}
	}
	public NodeSLL delete(){
		NodeSLL temp=front;
		front=front.next;
		return temp;
	}
}

public class SingleLinkedList{
	public static void main(String[] args) {
		LinkedListSLL sll=new LinkedListSLL();
		System.out.println("************QUEUE**********");
		sll.insertElement(1);
		sll.insertElement(1);
		sll.insertElement(2);
		sll.insertElement(3);
		sll.insertElement(4);
		sll.insertElement(1);
		sll.insertElement(5);
		sll.insertElement(7);
		System.out.println("Before deleting");
		sll.display();
		NodeSLL deletedNode=sll.delete();
		System.out.println("Deleted Node"+deletedNode.data);
		System.out.println("After deleting");
		sll.display();
	}
}