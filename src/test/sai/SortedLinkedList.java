package test.sai;

class Node1{
	int data;
	Node1 next;
	public Node1(int data){
		this.data=data;
		this.next=null;
	}
}

class LinkedList1{
	Node1 front;
	Node1 rear;
	public LinkedList1(){
		front=null;
		rear=null;
	}
	public void insertElement(int data){
		Node1 node=new Node1(data);
		Node1 previous=null;
		if(front==null){
			front=node;
			rear=front;
		}
		else{
			Node1 temp=front;
			previous=null;
			if(data>temp.data){
				while(temp!=null && data>temp.data){
					previous=temp;
					temp=temp.next;
					
				}
				previous.next=node;
				node.next=temp;
			}
			else{
				front=node;
				front.next=temp;
			}
			
			
		}
	}
	public void display(){
		Node1 temp=front;
		while(temp!=null){
			System.out.print(temp.data+" ");
			temp=temp.next;
		}
	}
}

public class SortedLinkedList {

	public static void main(String[] args) {
		LinkedList1 l=new LinkedList1();
		l.insertElement(3);
		l.insertElement(5);
		System.out.println("Before sorting");
		l.display();
		System.out.println("After sorting");
		l.insertElement(2);
		l.insertElement(1);
		l.insertElement(6);
		l.display();

	}

}
