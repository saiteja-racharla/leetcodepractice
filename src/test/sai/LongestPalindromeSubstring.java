/*
 * This program is to find longest palindromic substring. It can be solved using dynamic
 * programming also. The time complexity of the Dynamic Programming based solution is 
 * O(n^2) and it requires O(n^2) extra space. 
 * We can find the longest palindrome substring in (n^2) time with O(1) extra space. 
 * The idea is to generate all even length and odd length palindromes and keep track 
 * of the longest palindrome seen so far.
 */
package test.sai;

public class LongestPalindromeSubstring {
	/*static void printSubStr(String str, int low, int high)
	{
	    for( int i = low; i <= high; ++i )
	        System.out.print(str.charAt(i));
	}
	 
	// This function prints the longest palindrome substring (LPS)
	// of str[]. It also returns the length of the longest palindrome
	static int longestPalSubstr(String str)
	{
	    int maxLength = 1;  // The result (length of LPS)
	 
	    int start = 0;
	    int len = str.length();
	 
	    int low, high;
	 
	    // One by one consider every character as center point of 
	    // even and length palindromes
	    for (int i = 1; i < len; ++i)
	    {
	        // Find the longest even length palindrome with center points
	        // as i-1 and i.  
	        low = i - 1;
	        high = i;
	        while (low >= 0 && high < len && str.charAt(low) == str.charAt(high))
	        {
	            if (high - low + 1 > maxLength)
	            {
	                start = low;
	                maxLength = high - low + 1;
	            }
	            --low;
	            ++high;
	        }
	 
	        // Find the longest odd length palindrome with center 
	        // point as i
	        low = i - 1;
	        high = i + 1;
	        while (low >= 0 && high < len && str.charAt(low) == str.charAt(high))
	        {
	            if (high - low + 1 > maxLength)
	            {
	                start = low;
	                maxLength = high - low + 1;
	            }
	            --low;
	            ++high;
	        }
	    }
	 
	    System.out.println("Longest palindrome substring is: ");
	    printSubStr(str, start, start + maxLength - 1);
	 
	    return maxLength;
	}*/
	public static String longestPalSubstring(String s){
		int low=0,high=0,maxLength=1,start=0,end=0;
		for(int i=1;i<s.length();i++){
			low=i-1;
			high=i+1;
			while(low>=0 && high<s.length() && s.charAt(low)==s.charAt(high)){
				if(high-low+1>maxLength){
					start=low;
					maxLength=high-low+1;
				}
				low--;
				high++;
			}
			low=i-1;
			high=i;
			while(low>=0 && high<s.length() && s.charAt(low)==s.charAt(high)){
				if(high-low+1>maxLength){
					start=low;
					maxLength=high-low+1;
				}
				low--;
				high++;
			}
		}
		return s.substring(start, start+maxLength);
	}
	public static int longestPalSubstrLength(char []str){
        int T[][] = new int[str.length][str.length];
        for(int i=0; i < str.length; i++){
            T[i][i] = 1;
        }
        for(int l = 2; l <= str.length; l++){
            for(int i = 0; i < str.length-l + 1; i++){
                int j = i + l - 1;
                if(l == 2 && str[i] == str[j]){
                    T[i][j] = 2;
                }else if(str[i] == str[j]){
                    T[i][j] = T[i + 1][j-1] + 2;
                }else{
                    T[i][j] = Math.max(T[i + 1][j], T[i][j - 1]);
                }
            }
        }
        return T[0][str.length-1];
    }
	public static void main(String[] args) {
		String str = "bb";
	    System.out.println(longestPalSubstrLength( str.toCharArray() ) );
	    System.out.println(longestPalSubstring( str ) );
	}

}
