package test.sai;

public class EnumExample {

	public static void main(String[] args) {
		Customer[] customers=new Customer[3];
		customers[0]=new Customer("sai",Gender.male);
		customers[1]=new Customer("teja",Gender.male);
		customers[2]=new Customer("sai",Gender.male);
		for(Customer customer:customers){
			System.out.print(customer.name);
			System.out.println(customer.gender);
		}
	}
}

enum Gender{
	male,female
}

class Customer{
	String name;
	Gender gender;
	public Customer(String name, Gender gender) {
		super();
		this.name = name;
		this.gender = gender;
	}
}
