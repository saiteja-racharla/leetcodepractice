/*
 * Given a collection of intervals, merge all overlapping intervals.

For example,
Given [1,3],[2,6],[8,10],[15,18],
return [1,6],[8,10],[15,18].

Time complexity-O(nlogn)
 */
package test.sai;

import java.util.ArrayList;
import java.util.List;

class Interval {
	int start;
	int end;
	Interval() { 
		start = 0; 
		end = 0; 
	}
	Interval(int s, int e) { 
		start = s; 
		end = e; 
	}
}

public class MergeIntervals {
	public static void main(String[] args) {
		List<Interval> intervals=new ArrayList<Interval>();
		intervals.add(new Interval(1, 3));
		intervals.add(new Interval(2, 6));
		intervals.add(new Interval(8, 10));
		intervals.add(new Interval(12, 15));
		List<Interval> mergedIntervals = merge(intervals);
		for(Interval interval:mergedIntervals){
			System.out.println(interval.start+"\t"+interval.end);
		}
	}
	public static List<Interval> merge(List<Interval> intervals) {
		if (intervals.size() <= 1)
	        return intervals;
		intervals.sort((i1, i2) -> Integer.compare(i1.start, i2.start));
		List<Interval> result=new ArrayList<Interval>();
		int start=intervals.get(0).start;
		int end=intervals.get(0).end;
		for(Interval interval:intervals){
			if(interval.start<=end){
				end=Math.max(end, interval.end);
			}
			else{
				result.add(new Interval(start,end));
				start=interval.start;
				end=interval.end;
			}
		}
		result.add(new Interval(start, end));
		return result;
        
    }

}
