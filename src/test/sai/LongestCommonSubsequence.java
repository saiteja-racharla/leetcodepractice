/*This program calculates the longest common sub sequence
 * input: abcdaf
 * 		  abcaf
 * output: 4
 * https://leetcode.com/problems/longest-common-subsequence/description/
 * Given two strings text1 and text2, return the length of their longest common subsequence.
A subsequence of a string is a new string generated from the original string with some characters(can be none) deleted without changing the relative order of the remaining characters. (eg, "ace" is a subsequence of "abcde" while "aec" is not). A common subsequence of two strings is a subsequence that is common to both strings.
If there is no common subsequence, return 0.
Example 1:

Input: text1 = "abcde", text2 = "ace" 
Output: 3  
Explanation: The longest common subsequence is "ace" and its length is 3.
Example 2:

Input: text1 = "abc", text2 = "abc"
Output: 3
Explanation: The longest common subsequence is "abc" and its length is 3.
Example 3:

Input: text1 = "abc", text2 = "def"
Output: 0
Explanation: There is no such common subsequence, so the result is 0.
 */
package test.sai;

public class LongestCommonSubsequence {
	public static int longestCommonSubsequence(char[] str1,char[] str2){
		int temp[][]=new int[str1.length][str2.length];
		int max=0;
		for(int i=1;i<temp.length;i++){
			for(int j=1;j<temp[0].length;j++){
				if(str1[i-1]==str2[j-1])
					temp[i][j]=temp[i-1][j-1]+1;
				else
					temp[i][j]=Math.max(temp[i][j-1],temp[i-1][j]);
				if(temp[i][j]>max)
					max=temp[i][j];
			}
		}
		return max;
		
	}
	public static void main(String[] args) {
		String str1="abcdaf";
		String str2="abcaf";
		System.out.println("Maximum common subsequence is "+longestCommonSubsequence(str1.toCharArray(),str2.toCharArray()));
	}
}
