/*
 * Maze problem. Find path from top left to bottom right
 */
package test.sai;

public class MazeProblem {
	final static int N = 4;
	public static void printSolutionMaze(int[][] sol){
		for(int i=0;i<sol.length;i++){
			for(int j=0;j<sol[0].length;j++){
				System.out.print(sol[i][j]+" ");
			}
			System.out.println();
		}
	}
	public static boolean isSafe(int[][] maze, int x, int y){
		return (x >= 0 && x < N && y >= 0 &&
                y < N && maze[x][y] == 1);
	}
	public static boolean solveMaze(int[][] maze){
		int sol[][]=new int[maze.length][maze[0].length];
		for(int i=0;i<maze.length;i++){
			for(int j=0;j<maze[0].length;j++){
				sol[i][i]=0;
			}
		}	
		if(solveMazeUtil(maze,0,0,sol)==false){
			return false;
		}
		printSolutionMaze(sol);
		return true;
	}
	public static boolean solveMazeUtil(int[][] maze,int x,int y, int[][] sol){
		if(x==N-1 && y==N-1){
			sol[x][y]=1;
			return true;
		}
		if (isSafe(maze, x, y) == true)
        {
			sol[x][y] = 1;
			if(solveMazeUtil(maze,x+1,y,sol))
				return true;
			if(solveMazeUtil(maze, x, y+1, sol))
				return true;
			sol[x][y]=0;
			return false;
        }
		
		return false;
	}
	public static void main(String[] args) {
		int maze[][]= {{1, 0, 0, 0},
	            {1, 1, 0, 1},
	            {0, 1, 0, 0},
	            {1, 1, 1, 1}
	            };
		System.out.println(solveMaze(maze)?"Path found":"Not found");

	}

}
