package test.sai;

class BankAccount{
	private double totalBalance;
	double interestRate;
	public BankAccount() {
		totalBalance=0;
		interestRate=2;
	}
	public void deposit(int amount){
		totalBalance+=amount;
	}
	public void withdraw(int amount){
		totalBalance-=amount;
	}
	public double getBalance(){
		return totalBalance;
	}
	public void addInterest(){
		totalBalance = totalBalance+(interestRate*0.01*totalBalance);
	}
}

public class CalculateInterest {
	public static void main(String[] args) {
		BankAccount bankAccount=new BankAccount();
		bankAccount.deposit(800);
		bankAccount.deposit(200);
		System.out.println("Total Balance in account is "+bankAccount.getBalance());
		bankAccount.addInterest();
		System.out.println("Total Balance in account is "+bankAccount.getBalance());
		bankAccount.withdraw( 20 );
		System.out.println("Total Balance in account is "+bankAccount.getBalance());
		bankAccount.interestRate = 10;
		bankAccount.addInterest();
		double balance = bankAccount.getBalance();
		System.out.println("Total Balance in account is "+balance);
	}
}
