/*
 THis program is to print the output in the following format
 			1
		2		3
	4		4		6
7		8		9		10
 */

package test.sai;

public class NumberStars {

	public static void main(String[] args) {
		int number=1;
		int maxrows=4;
		for(int i=1;i<=4;i++){
			for(int k=1;k<=maxrows-1;k++){
				System.out.print(" ");
			}
			for(int j=1;j<=i;j++){
				System.out.print(number+"  ");
				number++;
			}
			System.out.println();
			maxrows--;
		}
		/*int i,j,n,f;
		for(n=1;n<=6;n++)
		{
			for(j=1;j<=6-n;j++)
			{
				System.out.print(" ");
			}
			for(i=1;i<=n;i++)
			{
				System.out.print("* ");
			}
			System.out.print("\n");
		}*/
	}

}
