package test.sai;

public class CheckArraySortedRecursion {
	public static void main(String[] args) {
		int a[]={1,2,3,4,5};
		int b[]={3,2,1,4,-1};
		System.out.println(checkArraySortedRecursion(a, a.length));
		System.out.println(checkArraySortedRecursion(b, b.length));
	}
	public static String checkArraySortedRecursion(int array[],int index){
		if(index==1){
			return "Array is Sorted";
		}
		return array[index-1]<array[index-2]?"Array is not sorted":checkArraySortedRecursion(array, index-1);
	}
}
