package test.sai;

import java.util.Scanner;

public class DepthFirstSearch {
	static VertexState[] vertextlist=new VertexState[10];
	static int[][] edges=new int[10][10];
	static int numberOfVertices;
	enum VertexState{
		White, Gray, Black 
	}
	public static void DFS(){
		for(int i=0;i<numberOfVertices;i++){
			vertextlist[i]=VertexState.White;
		}
		runDFS(1);
	}
	public static void runDFS(int u){
		System.out.println(u+1);
		vertextlist[u] = VertexState.Gray;
        for (int v = 0; v < numberOfVertices; v++)
              if (isEdge(u, v) && vertextlist[v] == VertexState.White)
                    runDFS(v);
        vertextlist[u] = VertexState.Black;
	}
	
	public static boolean isEdge(int i,int j){
		if(edges[i][j]==1)
			return true;
		else 
			return false;
	}
	
	public static void main(String[] args) {
		System.out.println("Enter Number of vertices");
		Scanner sc=new Scanner(System.in);
		numberOfVertices=sc.nextInt();
		for(int i=0;i<numberOfVertices;i++){
			for(int j=0;j<numberOfVertices;j++){
				System.out.println("Enter 1 if Edge between "+(i+1)+"and "+(j+1));
				edges[i][j]=sc.nextInt();
			}
		}
		DFS();
	}
	

}
