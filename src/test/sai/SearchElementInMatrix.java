/*
 * Find an element in given matrix , matrix is such that each row and each column is sorted
 * Solution:
To Solve this , we start from top right.
1. If element is greater move down
2. If element is lesser move left
3. If the element matches then print row and column indexes

This algorithm is called Saddleback search. 
Search element in  matrix. Matrix is in sorted order.
 */
package test.sai;

public class SearchElementInMatrix {
	static int[][] matrix= {
	  { 11, 21, 31, 41, 51 },
	  { 12, 22, 32, 42, 52 },
	  { 13, 23, 33, 43, 53 } ,
	  { 14, 24, 34, 44, 54 },
	  { 15, 25, 35, 45, 55 }
	 };

	public boolean find(int[][] martix , int num) {
		if(matrix.length==0)
			return false;
		int rLen=matrix.length;
		int cLen=matrix[0].length ;
		int row=0;
		int col=cLen-1;
		// Start from top Right
		while(row<rLen && col>=0)
		{
			//Move Down
			if(num > matrix[row][col]) 
				row++;
		
			//Move Left
			else if(num < matrix[row][col])
				col--;
		    
			//Element Found
			else
			{
					System.out.println("Found at : ( "+ row+ " , "+ col + "  ) ");
					return true;
			}
		}
		return false;
	}

	public static void main(String[] args) {
		SearchElementInMatrix fnum=new SearchElementInMatrix();
		boolean returnValue = fnum.find(matrix, 25);
		if(returnValue)
			System.out.println("true");
		else
			System.out.println("False");
	}
}
