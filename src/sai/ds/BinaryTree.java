package sai.ds;

class BinaryTreeNode{
	int data;
	BinaryTreeNode leftChild;
	BinaryTreeNode rightChild;
	public BinaryTreeNode(int data) {
		this.data=data;
		leftChild=null;
		rightChild=null;
	}
}

class BinaryTreeOperations{
	BinaryTreeNode root;
	public BinaryTreeOperations() {
		root=null;
	}
	public void insert(int data){
		BinaryTreeNode bn=new BinaryTreeNode(data);
		BinaryTreeNode current,parent=null;
		if(root==null)
			root=bn;
		else{
			current=root;
			while(current!=null){
				parent=current;
				if(data<current.data){
					current=current.leftChild;
				}
				else {
					current=current.rightChild;
				}
			}
			if(data<parent.data){
				parent.leftChild=bn;
			}
			else{
				parent.rightChild=bn;
			}
		}
	}
	
	public int max(int left,int right){
		return (left>right)?left:right;
	}
	
	public int printHeight(BinaryTreeNode node){
		if(node==null)
			return -1;
		int leftHeight=printHeight(node.leftChild);
		int rightHeight=printHeight(node.rightChild);
		return max(leftHeight,rightHeight)+1;
	}
	
	public int height(){
		return printHeight(root);
	}
	
	public boolean findElemenet(int data){
		BinaryTreeNode current=root;
		boolean found=false;
		while(current!=null){
			if(data==current.data)
				found=true;
			if(data<current.data){
				current=current.leftChild;
			}
			else{
				current=current.rightChild;
			}
		}
		return found;
	}
	
	public void printInOrder(){
		System.out.print("\nInoder: ");
		inOrder(root);
	}
	public void inOrder(BinaryTreeNode bnt){
		if(bnt!=null){
			inOrder(bnt.leftChild);
			System.out.print(bnt.data+" ");
			inOrder(bnt.rightChild);
		}
	}
	public void printPreOrder(){
		System.out.print("\nPre order: ");
		preOrder(root);
	}
	public void preOrder(BinaryTreeNode bnt){
		if(bnt!=null){
			System.out.print(bnt.data+" ");
			preOrder(bnt.leftChild);
			preOrder(bnt.rightChild);
		}
	}
	public void printPostOrder(){
		System.out.print("\nPost oder: ");
		postOrder(root);
	}
	public void postOrder(BinaryTreeNode bnt){
		if(bnt!=null){
			postOrder(bnt.leftChild);
			postOrder(bnt.rightChild);
			System.out.print(bnt.data+" ");
		}
	}
}

public class BinaryTree {

	public static void main(String[] args) {
		BinaryTreeOperations bt=new BinaryTreeOperations();
		bt.insert(35);
		bt.insert(25);
		bt.insert(15);
		bt.insert(7);
		bt.insert(20);
		bt.insert(30);
		bt.insert(28);
		bt.insert(31);
		bt.insert(45);
		bt.insert(40);
		bt.insert(50);
		bt.insert(38);
		bt.insert(42);
		bt.insert(49);
		bt.insert(51);
		bt.insert(52);
		bt.printInOrder();
		bt.printPreOrder();
		bt.printPostOrder();
		System.out.println("\nheight: "+bt.height());
		System.out.println("Element 7 "+bt.findElemenet(6));
	}

}
