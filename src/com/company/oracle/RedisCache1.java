package com.company.oracle;

import java.util.HashMap;
import java.util.Hashtable;

public class RedisCache1 {
	public static void main(String[] args) throws InterruptedException {
		LRUCache1 cache=new LRUCache1();
		cache.set("1", "1");
		cache.set("1", "2");
		cache.set("2", "2");
		cache.set("3", "3");
		cache.set("4", "4");
		cache.listPush("5", "1");
		cache.listPush("5", "2");
		cache.listPush("5", "3");
		cache.listPush("5", "2");
		cache.listPush("5", "2");
		int x=cache.listRemove("5", "1", 2);
		System.out.println(x);
		cache.expire("3", 3);
		cache.expire("2", 5);
		Thread.sleep(3100);
		System.out.println(cache.get("3"));
	}
}
class LRUCache1{
	Hashtable<String, Node>  cache = new Hashtable<String, Node>();
	HashMap<String,Long> expireMap = new HashMap<>();
	public void set(String key, String value) {
		Node node=cache.get(key);
        if(node==null){
    		Node tempnode=new Node(value);
    		tempnode.isList=false;
        	cache.put(key, tempnode);
        }
        else{
        	cache.get(key).value=value;
        }
    }
	
	public void listPush(String key, String value) {
		Node node=cache.get(key);
    	Node tempnode=new Node(value);
    	tempnode.isList=true;
        if(node==null) {
        	cache.put(key, tempnode);
        }
        else{
        	tempnode.next=node;
        	node.prev=tempnode;
        	cache.put(key, tempnode);
        }
    }
	public Node get(String key) {
    	Node node=cache.get(key);
    	if(node==null) {
    		return null;
    	} else  {
    		if(expireMap.containsKey(key) && expireMap.get(key)<System.currentTimeMillis()) {
    			expireMap.remove(key);
    			cache.remove(key);
    			return null;
    		}
    		return node;
    	}
    }
	public int size() {
    	return cache.size();
    }
	public int listRemove(String key, String value, int count) {
		Node node=cache.get(key);
    	if(node==null) {
    		return -1;
    	}
    	if(!node.isList) {
    		return -1;
    	}
    	return removeValue(node, key, value, count);
    }
	private int removeValue(Node tempList, String key, String value, int count) {
		if(count==0) {
			Node iter=new Node("-1");
			Node pseudo=iter;
			iter.next=tempList;
			int tc=count;
			while(iter.next!=null) {
				if(iter.next.value.equals(value))
					iter.next=iter.next.next;
				else
					iter = iter.next;
			}
			cache.put(key, pseudo.next);
			return tc;
		} else if(count>0) {
			Node iter=new Node("-1");
			Node pseudo=iter;
			iter.next=tempList;
			int tc=count;
			while(iter.next!=null && tc>0) {
				if(iter.next.value.equals(value)) {
					iter.next=iter.next.next;
					tc--;
				}
				else
					iter = iter.next;
			}
			cache.put(key, pseudo.next);
			return tc;
		} else {
			Node iter=new Node("-1");
			Node pseudo=iter;
			iter.next=tempList;
			int tc=count;
			while(iter.next!=null) {
				iter=iter.next;
			}
			while(iter.prev!=null && tc>0) {
				if(iter.prev.value.equals(value)) {
					iter.prev=iter.prev.prev;
					tc--;
				}
				else
					iter = iter.prev;
			}
			cache.put(key, pseudo.next);
			return tc;
		}
	}
	public boolean expire(String key, int time) {
    	if(cache.containsKey(key)) {
    		if(!expireMap.containsKey(key)) {
    			expireMap.put(key, System.currentTimeMillis()+(time*1000));
    		} else if(expireMap.containsKey(key) && time>0) {
    			expireMap.put(key, System.currentTimeMillis()+(time*1000));
    		} else {
    			expireMap.remove(key);
    		}
    	}
    	return true;
    }
}

class Node {
	String value;
	Node next,prev;
	boolean isList;
	public Node(String value) {
		this.value=value;
	}
	public String toString() {
		return value;
	}
}