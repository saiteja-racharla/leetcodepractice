package com.company.oracle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class TupleImpl {
	public static void main(String[] args) {
		List<Integer> list1=Arrays.asList(1,2,3);
		List<String> list2=Arrays.asList("a","b","c");
		Zipper<Integer, String> zipObj = new Zipper<>(list1,list2);
		zipObj.zip();
		Iterator<Tuple<Integer,String>> iterator=zipObj.iterator();
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
	}
	public static class Zipper<A,B> implements Iterable<Tuple<A,B>> {
		List<A> a;
		List<B> b;
		public Zipper(List<A> a, List<B> b) {
			this.a = a;
			this.b = b;
		}

		@Override
		public Iterator<Tuple<A, B>> iterator() {
			// TODO Auto-generated method stub
			return zip();
		}

		private Iterator<Tuple<A, B>> zip() {
			Iterator<A> it1=a.iterator();
			Iterator<B> it2=b.iterator();
			List<Tuple<A,B>> result=new ArrayList<>();
			while(it1.hasNext() && it2.hasNext()) {
				result.add(new Tuple<A,B>(it1.next(),it2.next()));
			}
			return result.iterator();
		}
	}
	public static class Tuple<A,B> {
		private A a;
		private B b;
		public Tuple(A a, B b) {
			this.a = a;
			this.b = b;
		}
		public String toString() {
			StringBuilder sb=new StringBuilder();
			if(null==a) {
				sb.append("null");
			} else {
				sb.append(a.toString());
			}
			sb.append(",");
			if(null==b) {
				sb.append("null");
			} else {
				sb.append(b.toString());
			}
			return sb.toString();
		}
	}
}
