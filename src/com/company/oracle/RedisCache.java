package com.company.oracle;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

public class RedisCache {

	public static void main(String[] args) throws InterruptedException {
		LRUCache cache=new LRUCache(100, 100, 10);
		new Timer().scheduleAtFixedRate(new TimerTask(){
		    @Override
		    public void run(){
		    	System.out.println("\ncache elements\n");
		    	cache.print();
		    	cache.cleanup();
		    }
		},0,1000);
		cache.set("1", "1");
		cache.set("1", "2");
		cache.set("2", "2");
		cache.set("3", "3");
		cache.set("4", "4");
		cache.listPush("5", "1");
		cache.listPush("5", "2");
		cache.listPush("5", "3");
		cache.listPush("5", "2");
		cache.listPush("5", "2");
		int x=cache.listRemove("5", "1", 2);
		cache.expire("3", 3);
		cache.expire("2", 5);
		Thread.sleep(3000);
	}

}
class LRUCache{
	Hashtable<String, Object>  cache = new Hashtable<String, Object>();
	HashMap<String,Long> expireMap = new HashMap<>();
	long timeToLive;
	public LRUCache(long timeToLive, final long crunchifyTimerInterval, int capacity) {
//		this.timeToLive = timeToLive * 1000;
//        if (this.timeToLive > 0 && crunchifyTimerInterval > 0) {
//            Thread t = new Thread(new Runnable() {
//                public void run() {
//                    while (true) {
//                        try {
//                            Thread.sleep(crunchifyTimerInterval * 1000);
//                        } catch (InterruptedException ex) {
//                        }
//                        while(true) {
//                        	cleanup();
//                        	try {
//								Thread.sleep(1000);
//							} catch (InterruptedException e) {
//								// TODO Auto-generated catch block
//								e.printStackTrace();
//							}
//                        }
//                    }
//                }
//            });
//            t.setDaemon(true);
//            t.start();
//        }
    }
	
	public void print() {
		for(Object key: cache.keySet()) {
			System.out.println(key+"\t"+cache.get(key));
		}
	}
	
    public Object get(String key) {
    	Object node=cache.get(key);
    	if(node==null) {
    		return -1;
    	} else if(node instanceof CustomNode) {
    		CustomNode custom=(CustomNode) node;
    		custom.lastAccessed = System.currentTimeMillis();
    		return custom;
    	} else {
    		LinkedList<CustomNode> tempList=(LinkedList<CustomNode>) node;
    		tempList.get(0).lastAccessed=System.currentTimeMillis();
    		return tempList;
    	}
        
    }
    
    public int size() {
    	return cache.size();
    }
    
    public void set(String key, String value) {
        Object node=cache.get(key);
        CustomNode tempnode=new CustomNode(value);
        if(node==null){
        	cache.put(key, tempnode);
        }
        else{
        	cache.put(key, tempnode);
        }
    }
    
    public void listPush(String key, String value) {
    	Object node=cache.get(key);
    	LinkedList<CustomNode> tempList=new LinkedList<CustomNode>();
    	CustomNode tempnode=new CustomNode(value);
        if(node==null){
        	tempList.addFirst(tempnode);
        	cache.put(key, tempList);
        }
        else{
        	tempList = (LinkedList<CustomNode>) node;
        	tempList.addFirst(tempnode);
        }
    }
    
    public int listRemove(String key, String value, int count) {
    	Object node=cache.get(key);
    	if(node==null) {
    		return -1;
    	}
    	if(node instanceof CustomNode) {
    		return -1;
    	}
    	LinkedList<CustomNode> tempList = (LinkedList<CustomNode>) node;
    	return removeValue(tempList, value, count);
    }
    
    private int removeValue(LinkedList<CustomNode> tempList, String value, int count) {
    	if(count==0) {
    		Iterator<CustomNode> itr = tempList.iterator();
    		while (itr.hasNext()) {
        		CustomNode c=itr.next();
    			if(c.value==value)
 				   itr.remove();
    		}
    		return -1;
    	} else if(count>0) {
    		LinkedList<CustomNode> temp=new LinkedList<>();
    		Iterator<CustomNode> itr = tempList.iterator();
    		int tc=count;
    		while (itr.hasNext()) {
    			if(tc==0)
    				break;
        		CustomNode c=itr.next();
    			if(c.value==value) {
    				temp.add(c);
    				tc--;
    			}
    		}
    		tempList.removeAll(temp);
    		return tc;
    	} else {
    		LinkedList<CustomNode> temp=new LinkedList<>();
    		Iterator<CustomNode> itr = tempList.descendingIterator();
    		int tc=-count;
    		while (itr.hasNext()) {
    			if(tc==0)
    				break;
        		CustomNode c=itr.next();
    			if(c.value==value) {
    				temp.add(c);
    				tc--;
    			}
    		}
    		tempList.removeAll(temp);
    		return tc;
    	}
    }
    
    public boolean expire(String key, int time) {
    	if(cache.containsKey(key)) {
    		if(!expireMap.containsKey(key)) {
    			expireMap.put(key, System.currentTimeMillis()+(time*1000));
    		} else if(expireMap.containsKey(key) && time>0) {
    			expireMap.put(key, System.currentTimeMillis()+(time*1000));
    		} else {
    			expireMap.remove(key);
    		}
    	}
    	return true;
    }
    
    public void cleanup() {
        long now = System.currentTimeMillis();
        ArrayList<String> deleteKey = new ArrayList<>();
 
        synchronized (cache) {
        	for(String key: expireMap.keySet()) {
        		Long time=expireMap.get(key);
        		if(now > time) {
        			deleteKey.add(key);
        		}
        	}
        }
        for (String key : deleteKey) {
            synchronized (cache) {
                cache.remove(key);
                expireMap.remove(key);
            }
            Thread.yield();
        }
    }
}

class CustomNode {
	String value;
	long timeToExpire;
	long lastAccessed;
	public CustomNode(String value) {
		this.value=value;
		this.lastAccessed = System.currentTimeMillis();
	}
	public String toString() {
		return value;
	}
}