package com.company.oracle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MergeSortRemoveDuplicates {

	public static void main(String[] args) {
		int[] arr= {2,2,1,2,3,1,3,6,7,5,4,5,5,5,6,4,3,2,2,2,1};
		merge(arr,0,arr.length-1);
		System.out.println(Arrays.toString(merge(arr,0,arr.length-1)));
	}

	public static int[] merge(int[] arr,int low,int high) {
		if(low>high)
			return new int[]{};
		else if(low==high)
			return new int[] {arr[low]};
		int mid=low+(high-low)/2;
		int[] arr1=merge(arr,low,mid);
		int[] arr2=merge(arr,mid+1,high);
		int[] newarr=mergeSorted(arr1,arr2);
		return newarr;
	}
	public static int[] mergeSorted(int[] left,int[] right) {
		List<Integer> arr=new ArrayList<>();
		int n1=left.length,n2=right.length;
		int i=0,j=0;
		while (i < n1 && j < n2) {
			if(left[i]==right[j]) {
				i++;
				continue;
			}
        	if (left[i] < right[j]) { 
                arr.add(left[i]); 
                i++; 
            } 
            else { 
            	arr.add(right[j]); 
                j++; 
            } 
        } 
  

        while (i < n1) { 
        	if(i>0 && left[i]==left[i-1]) {
        		i++;
        		continue;
        	}
            arr.add(left[i]); 
            i++; 
        } 
  

        while (j < n2) { 
        	if(j>0 && right[j]==right[j-1]) {
        		j++;
        		continue;
        	}
            arr.add(right[j]); 
            j++; 
        }
		return arr.stream().mapToInt(item->item).toArray();
	}
}
