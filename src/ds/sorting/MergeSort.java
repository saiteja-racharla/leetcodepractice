package ds.sorting;

public class MergeSort {
	static void divide(int receivedA[]){
		int length=receivedA.length;
		int mid=length/2;
		if(length<2)
			return;
		int low[]=new int[mid];
		int high[];
		if(length%2==0){
			high=new int[mid];
		}
		else{
			high=new int[mid+1];
		}
		for(int i=0;i<mid;i++){
			low[i]=receivedA[i];
		}
		for(int j=mid;j<length;j++){
			high[j-mid]=receivedA[j];
		}
		divide(low);
		divide(high);
		merge(low,high,receivedA);
		for(int i:receivedA){
			System.out.print(i+" ");
		}
		System.out.println("After merge for loop");
	}
	static void merge(int left[],int right[],int combined[]){
		int i=0,j=0,k=0;
		while(i<left.length && j<right.length){
			if(left[i]<right[j]){
				combined[k]=left[i];
				i++;
				k++;
			}
			else{
				combined[k]=right[j];
				j++;
				k++;
			}
		}
		while(i<left.length){
			combined[k]=left[i];
			i++;
			k++;
		}
		while(j<right.length){
			combined[k]=right[j];j++;k++;
		}
	}
	public static void main(String[] args) {
		int a[]={8,7,6,5,4,3,2,1};
		divide(a);
		for(int i:a){
			System.out.print(i+" ");
		}
	}

}
