package ds.sorting;

public class QuickSort {
	static void quickSort(int a[],int startIndex, int endIndex){
		if(startIndex<endIndex){
			int partitionIndex=partition(a,startIndex,endIndex);
			quickSort(a,startIndex,partitionIndex-1);
			quickSort(a,partitionIndex+1,endIndex);
		}
	}
	static int partition(int a[],int startIndex, int endIndex){
		int partitionIndex=0;
		int pivot=a[endIndex];
		partitionIndex=startIndex;
		for(int i=startIndex;i<=endIndex-1;i++){
			if(a[i]<=pivot){
				int temp=a[i];
				a[i]=a[partitionIndex];
				a[partitionIndex]=temp;
				partitionIndex+=1;
			}
		}
		int temp=a[partitionIndex];
		a[partitionIndex]=a[endIndex];
		a[endIndex]=temp;
		return partitionIndex;
	}
	
	public static void main(String[] args) {
		int a[]={8,7,6,5,4,3,2,1};
		quickSort(a,0,a.length-1);
		for(int i:a){
			System.out.print(i+" ");
		}
	}

}
