//Bubble Sort
//max element goes to the end of array for each loop.
package ds.sorting;

public class BubbleSort {

	public static void main(String[] args) {
		int a[]={8,7,6,5,4,3,2,1};
		int flag=0;
		for(int i=0;i<a.length;i++){
			flag=0;
			for(int j=0;j<a.length-i-1;j++){	//When sorted array is passed, O(n) is
				if(a[j]>a[j+1]){				//time complexity. THe outer for loop
					int temp=a[j+1];			//executes only once.
					a[j+1]=a[j];
					a[j]=temp;
					flag=1;
				}
			}
			if(flag==0) break;
		}
		for(int i=0;i<a.length;i++)
			System.out.print(a[i]+"\t");
	}

}
