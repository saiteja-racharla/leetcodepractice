package ds.sorting;

public class InsertionSort {
	static int a[]={8,7,6,5,4,3,2,1};
	public static void main(String[] args) {
		int hole=0;
		for(int i=1;i<a.length;i++){
			int value=a[i];
			hole=i;
			while(hole>0 && a[hole-1]>value){	//This loop doesn't execute when array is 
				a[hole]=a[hole-1];			//in sorted order. So time complexity in best
				hole=hole-1;			//case is O(n).
			}
			a[hole]=value;
		}
		for(int i:a){
			System.out.print(i+" ");
		}
	}
}
