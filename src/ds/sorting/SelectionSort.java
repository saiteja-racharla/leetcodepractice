package ds.sorting;
//Smallest element comes to first position for every loop
public class SelectionSort {

	public static void main(String[] args) {
		int a[]={8,7,6,5,4,3,2,1};
		int min=0;
		for(int i=0;i<a.length-1;i++){
			min=i;
			for(int j=i+1;j<a.length;j++){
				if(a[j]<a[min]){
					min=j;
				}
			}
			int temp=a[i];
			a[i]=a[min];
			a[min]=temp;
		}
		for(int i:a){
			System.out.print(i+" ");
		}

	}

}
