/*The algorithm for this program is correct. But the program is not outputting properly. Please 
 * have at the link below
 * program from http://www.geeksforgeeks.org/serialize-deserialize-binary-tree/
How much extra space is required in above solution?
If there are n keys, then the above solution requires n+1 markers which may be better than 
simple solution (storing keys twice) in situations where keys are big or keys have big data 
items associated with them.

Can we optimize it further?
The above solution can be optimized in many ways. If we take a closer look at above serialized 
trees, we can observer that all leaf nodes require two markers. One simple optimization is to 
store a separate bit with every node to indicate that the node is internal or external. 
This way we don�t have to store two markers with every leaf node as leaves can be identified by 
extra bit. We still need marker for internal nodes with one child. For example in the following 
diagram � is used to indicate an internal node set bit, and �/� is used as NULL marker. 

Please note that there are always more leaf nodes than internal nodes in a Binary Tree (Number 
of leaf nodes is number of internal nodes plus 1, so this optimization makes sense.
How to serialize n-ary tree?
In an n-ary tree, there is no designated left or right child. We can store an 
�end of children� marker with every node. The following diagram shows serialization where �)� 
is used as end of children marker. We will soon be covering implementation for n-ary tree. 
The diagram is taken from here.
serializeNaryTree*/
package ds;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class SerializeAndDeserializeBinaryTree {
	static FileInputStream in = null;
	static FileOutputStream out = null;
	static{
		try {
			in = new FileInputStream("D:\\Programs\\Java Programs\\SamplePrograms\\src\\ds\\SerializeAndDeserializeText.txt");
			out = new FileOutputStream("D:\\Programs\\Java Programs\\SamplePrograms\\src\\ds\\SerializeAndDeserializeText.txt");
		    int c;
		    while ((c = in.read()) != -1) {
	    		out.write(c);
		    }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	static void serialize(BinarySearchTreeNode root){
		if(root==null)
		{
			try {
				out.write(-1);
			}
			catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return;
		}
		try{
			out.write(root.data);
			serialize(root.left);
			serialize(root.right);
		}
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	static void deSerialize(BinarySearchTreeNode root){
		int data=0;
		try {
			data = in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(data==-1 || data==255){	//data=255 is checked because -1 is not being inserted into file. -1 is inserted as 255
			return;
		}
		root = new BinarySearchTreeNode(data);
		deSerialize(root.left);
		deSerialize(root.right);
	}
	static void printBinaryTree(BinarySearchTreeNode root){
		if(root==null)
			return;
		System.out.println(root.data);
		printBinaryTree(root.left);
		printBinaryTree(root.right);
	}
	public static void main(String[] args) throws IOException {
		BinarySearchTreeNode root=new BinarySearchTreeNode(20);
		BinarySearchTreeNode root1=null;
		root.left               = new BinarySearchTreeNode(8);
	    root.right              = new BinarySearchTreeNode(22);
	    root.left.left         = new BinarySearchTreeNode(4);
	    root.left.right        = new BinarySearchTreeNode(12);
	    root.left.right.left  = new BinarySearchTreeNode(10);
	    root.left.right.right = new BinarySearchTreeNode(14);
		serialize(root);
		deSerialize(root1);
		if (in != null) {
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (out != null) {
			try {
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("After retrieval from file. The tree in preorder traversal is");
		printBinaryTree(root1);
	}
}
