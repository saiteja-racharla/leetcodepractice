/**
 * https://leetcode.com/problems/longest-substring-with-at-most-k-distinct-characters/
 * Leetcode-340
 */
package ds.strings;

import java.util.HashMap;

public class LongestSubStringWithAtmostKDistinctCharacters {

	public static void main(String[] args) {
		System.out.println(lengthOfLongestSubstringKDistinct("eceba",2));
		System.out.println(lengthOfLongestSubstringKDistinct("aa",1));
		System.out.println(lengthOfLongestSubstringKDistinct("ab",1));
	}
	public static int lengthOfLongestSubstringKDistinct(String s, int k) {
		int[] count=new int[256];//256 ascii characters in the world
    	int i=0;
    	int num=0;
    	int result=0;
    	for(int j=0;j<s.length();j++)
    	{
    		if(count[s.charAt(j)]==0) {
    			num++;
    		}
			count[s.charAt(j)]++;
    		while(num>k && i<s.length())
    		{
    			count[s.charAt(i)]--;
    			if(count[s.charAt(i)]==0) 
    				num--;
    			i++;
    		}
    		
    		result=Math.max(result, j-i+1);
    	}
    	return result;
    }

}
