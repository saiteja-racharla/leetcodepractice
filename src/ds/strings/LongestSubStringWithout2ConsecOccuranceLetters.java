/** Micrsoft Online Assessment
 * Given a string s containing only a and b, find longest substring of s such that s does not contain more than two contiguous occurrences of a and b.

Example 1:
Input: "aabbaaaaabb"
Output: "aabbaa"

Example 2:
Input: "aabbaabbaabbaa"
Output: "aabbaabbaabbaa"

We traverse through the string. If the curr is equal we increment count. If count is two, we store string from starting until there. 
when we encounter count as 3, we initialize start pointer to that point before it.
aabbaaa
when we are at index-7, we have count as 3, so we initialize start from index 6 
 */
package ds.strings;

public class LongestSubStringWithout2ConsecOccuranceLetters {

	public static void main(String[] args) {
		System.out.println(validLongestSubstring("aabbaaaaabb"));
		System.out.println(validLongestSubstring("aabbaabbaabbaa"));
	}
	public static String validLongestSubstring(String s) {
		if(s.length()<3)
			return s;
		int end=1, start=0, count=1, max=0, cur=0;
		String output = null;
		char c=s.charAt(0);
		while(end<s.length()) {
			if(s.charAt(end)==c) {
				count++;
				if(count==2) {
					if(end-start+1>max) {
						max=end-start+1;
						cur=start;
					}
				} else {
					start=end-1;
				}
			} else {
				c=s.charAt(end);
				count=1;
				if(end-start+1>max) {
					max=end-start+1;
					cur=start;
				}
			}
			end++;
		}
		output=s.substring(cur, cur+max);
		return output;
	}
}
