package ds.strings;

import java.util.ArrayList;
import java.util.List;

public class RemoveComments {

	public static void main(String[] args) {
		RemoveComments r=new RemoveComments();
		String[] source= {"struct Node{", "    /*/ declare members;/**/", "    int size;", "    /**/int val;", "};"};
		System.out.println(r.removeComments(source));
	}
	public List<String> removeComments(String[] source) {
		List<String> result=new ArrayList<>();
        boolean blockComment=false;
        StringBuffer sb=null;
        for(String line:source){
            if(!blockComment)
                sb=new StringBuffer();
            for(int i=0;i<line.length();i++){
                if(!blockComment && i<line.length()-1 && line.charAt(i)=='/' && line.charAt(i+1)=='*'){
                    blockComment=true;
                } else if(blockComment && i<line.length()-1 && line.charAt(i)=='*' && line.charAt(i+1)=='/') {
                    blockComment=false;
                    i+=1;
                    continue;
                }
                if(!blockComment && i<line.length()-1 && line.substring(i,i+2).equals("//")){
                    break;
                }
                if(blockComment)
                    continue;
                else{
                    sb.append(line.charAt(i));
                }
            }
            if(sb.length()!=0 && !blockComment)
                result.add(sb.toString());
        }
        return result;
    }
}
