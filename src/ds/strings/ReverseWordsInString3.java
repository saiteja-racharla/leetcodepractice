/*
 * Leetcode - 557
 * Given a string, you need to reverse the order of characters in each word within a sentence 
 * while still preserving whitespace and initial word order.

Example 1:
Input: "Let's take LeetCode contest"
Output: "s'teL ekat edoCteeL tsetnoc"
Note: In the string, each word is separated by single space and there will not be any extra 
space in the string.
 */
package ds.strings;

public class ReverseWordsInString3 {

	public static void main(String[] args) {
		System.out.println(reverseWords("Let's take LeetCode contest"));
	}
	
	public static String reverseWords(String s) {
        char[] str=s.toCharArray();
        int i=0,j=0;
        while(j<str.length){
            if(str[j]==' '){
                reverse(str,i,j-1);
                i=j+1;
            }
            j++;
        }
        reverse(str,i,j-1);
        return String.valueOf(str);
    }
    public static void reverse(char[] str,int i,int j){
        while(i<j){
            char temp = str[i];
            str[i] = str[j];
            str[j] = temp;
            i++;
            j--;
        }
    }

}
