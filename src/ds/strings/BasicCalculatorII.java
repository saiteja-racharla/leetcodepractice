package ds.strings;

import java.util.Stack;

public class BasicCalculatorII {

	public static void main(String[] args) {
		BasicCalculatorII basic=new BasicCalculatorII();
		System.out.println(basic.calculate("3+2+5*2"));
	}
	public int calculate(String s) {
		int prev=0,sum=0;
        char prevOp='+';
        int num=0;
        for(int i=0;i<s.length();i++){
            char c=s.charAt(i);
            if(Character.isDigit(c)){
                num=(num*10)+c-'0';
            }
            if(i==s.length()-1 || !Character.isDigit(c) && c!=' '){
                switch(prevOp){
                    case '+':
                        sum=sum+prev;
                        prev=num;
                        break;
                    case '-':
                        sum=sum+prev;
                        prev=-num;
                        break;
                    case '*':
                        prev=prev*num;
                        break;
                    case '/':
                        prev=prev/num;
                        break;
                }
                prevOp=c;
                num=0;
            }
        }
        sum=sum+prev;
        return sum;
    }

}
