/*Convert a number given in string format to integer
 * 4 cases
 * 1. Empty String
 * 2. Empty Spaces before string
 * 3. Handling Signs
 * 4. Overflow
 */
package ds.strings;

public class StringToInteger {
	public static void main(String[] args) {
		StringToInteger s=new StringToInteger();
		int returnedNumber = s.returnInteger("552223111233");
		System.out.println(returnedNumber);
	}
	public int returnInteger(String s){
		int sign=1,total=0,index=0,digit=0;
		//case 1
		if(s.isEmpty())
			return 0;
		
		//case 2
		while(s.charAt(index)==' ' && index<s.length())
			index++;
		
		//case 3
		if(s.charAt(index)=='-' || s.charAt(index)=='+'){
			sign=sign*s.charAt(index)=='-'?-1:1;
			index++;
		}
		
		//case 4
		while(index<s.length()){
			digit = s.charAt(index)-'0';
			if(digit<0 || digit>9)
				break;
			//Handling overflow
			if(Integer.MAX_VALUE/10<total || Integer.MAX_VALUE/10==total && Integer.MAX_VALUE%10<digit)
				return sign == 1 ? Integer.MAX_VALUE : Integer.MIN_VALUE;
			total=total*10+digit;
			index++;
		}
		return total*sign;
	}
}
