/**
 * Given a string s, partition s such that every substring of the partition is a palindrome.

Return all possible palindrome partitioning of s.

Example:

Input: "aab"
Output:
[
  ["aa","b"],
  ["a","a","b"]
]
https://leetcode.com/problems/palindrome-partitioning/description/

 */
package ds.strings;

import java.util.*;
public class PalindromePartitioning {
	static List<List<String>> result;
	static ArrayList<String> temp;
	public static void main(String[] args) {
		String string="ababa";
		temp=new ArrayList<String>();
		result=new ArrayList<List<String>>();
		backtrack(string,0);
		System.out.println(result);
	}
	public static void backtrack(String s, int index){
		if(temp.size()>0 //the initial str could be palindrome
	            && index>=s.length()){
	                List<String> r = (ArrayList<String>) temp.clone();
	                result.add(r);
	        }
		for(int i=index;i<s.length();i++){
			if(isPalindrome(s, index, i)){
				if(index==i)
					temp.add(Character.toString(s.charAt(i)));
				else
					temp.add(s.substring(index,i+1));
				backtrack(s, i+1);
				temp.remove(temp.size()-1);
			}
		}
	}
	public static boolean isPalindrome(String s, int left, int right){
		if(left==right) return true;
		while(left<right){
			if(s.charAt(left)!=s.charAt(right)) return false;
			left++;right--;
		}
		return true;
	}
}
