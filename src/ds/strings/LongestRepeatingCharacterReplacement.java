/**
 * Given a string s that consists of only uppercase English letters, you can perform at most k operations on that string.
In one operation, you can choose any character of the string and change it to any other uppercase English character.
Find the length of the longest sub-string containing all repeating letters you can get after performing the above operations.
Note:
Both the string's length and k will not exceed 104.
 */
package ds.strings;

public class LongestRepeatingCharacterReplacement {

	public static void main(String[] args) {
		LongestRepeatingCharacterReplacement l=new LongestRepeatingCharacterReplacement();
		//System.out.println(l.characterReplacement("ABABAABAB", 1));
		//System.out.println(l.characterReplacement("AAABBAA", 1));
		System.out.println(l.characterReplacement("AAAAABBABBBBBBAAAAAA", 1));
	}
	public int characterReplacement(String s, int k) {
        int[] count=new int[26];
        int length=s.length();
        int maxLength=0;
        int maxCount=0;
        int start=0;
        for(int end=0;end<length;end++) {
        	count[s.charAt(end)-'A']+=1;
        	maxCount = Math.max(maxCount, count[s.charAt(end)-'A']);
        	while(end-start+1-maxCount>k) {			//check in the sliding window if there are more than k replacement characters. If so, we advance the start pointer
        		count[s.charAt(start)-'A']--;
        		start++;
        	}
        	maxLength = Math.max(maxLength, end-start+1);
        }
        return maxLength;
    }
}
