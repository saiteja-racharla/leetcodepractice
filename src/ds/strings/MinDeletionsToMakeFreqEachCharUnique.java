/** Microsoft Online Assessment
 * Min Deletions to Make Frequency of Each Letter Unique
 * Given a string s consisting of n lowercase letters, you have to delete the minimum number of characters from s so that every letter in s appears a unique number of times. We only care about the occurrences of letters that appear at least once in result.

Example 1:
Input: "eeeeffff"
Output: 1
Explanation: We can delete one occurence of 'e' or one occurence of 'f'. Then one letter will occur four times and the other three times.

Example 2:
Input: "aabbffddeaee"
Output: 6
Explanation:For example, we can delete all occurences of 'e' and 'f' and one occurence of 'd' to obtain the word "aabbda".
Note that both 'e' and 'f' will occur zero times in the new word, but that's fine, since we only care about the letter that appear at least once.

Example 3:
Input: "llll"
Output: 0
Explanation: There is no need to delete any character.

Example 4:
Input: "example"
Output: 4
 */
package ds.strings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MinDeletionsToMakeFreqEachCharUnique {

	public static void main(String[] args) {
		String s1="eeeeffff";
		String s2="aabbffddeaee";
		String s3="llll";
		String s4="example";
		System.out.println(String.format("Mininum number of deletions in %s is %d", s1,minDeletions(s1)));
		System.out.println(String.format("Mininum number of deletions in %s is %d", s2,minDeletions(s2)));
		System.out.println(String.format("Mininum number of deletions in %s is %d", s3,minDeletions(s3)));
		System.out.println(String.format("Mininum number of deletions in %s is %d", s4,minDeletions(s4)));
	}
	public static int minDeletions(String s) {
		HashMap<Character, Integer> hm=new HashMap<Character, Integer>();
		/* Store the frequency of each character */
		for(int i=0;i<s.length();i++) {
			hm.put(s.charAt(i), hm.getOrDefault(s.charAt(i), 0)+1);
		}
		/* Move the frequencies to a list*/
		List<Integer> frequencies=new ArrayList<>(hm.values());
		Set<Integer> countSet=new HashSet<Integer>();
		/* Iterate through the frequencies. If frequency is not present, we can add it(no deletions). 
		 * If frequency is present, we reduce frequency and check if it is present. If it is present, we reduce the number and check
		 * until we don't find frequency in map. In this process, we count number of deletions*/
		int deletions = 0;
		for(int n:frequencies) {
			if(countSet.contains(n)) {
				while(n>0 && countSet.contains(n)) {
					n--;
					deletions++;
				}
				if(n!=0) {
					countSet.add(n);
				}
			} else {
				countSet.add(n);
			}
		}
		return deletions;
	}
}
