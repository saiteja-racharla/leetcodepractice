/*
 * Length of longest substring without repeating characters
 */
package ds.strings;

import java.util.HashMap;

public class LengthOfLongestSubstring {
	public static void main(String[] args) {
		String s="pwwkew";
		int maxCount=0;
		HashMap<Character,Integer> hm=new HashMap<Character,Integer>();
		int i=0,j=0;
		for(j=0;j<s.length();j++){
			if(hm.containsKey(s.charAt(j))){
			    i=Math.max(hm.get(s.charAt(j)),i);
			}
			hm.put(s.charAt(j),j+1);
			maxCount=Math.max(maxCount,j-i+1);
		}
		System.out.println(maxCount);
	}
}
