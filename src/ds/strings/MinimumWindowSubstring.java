package ds.strings;

public class MinimumWindowSubstring {
	public static void main(String[] args) {
		String S="ADOBECODEBANC";
		String T="ABC";
		System.out.println(minWindow(S,T));
		System.out.println(minWindowShort(S,T));
	}
	public static String minWindow(String S, String T) {
		if (S.isEmpty() || T.isEmpty())
        {
            return "";
        }
        int count = T.length();
        int require[] = new int[256];
        boolean chSet[] = new boolean[256];;
        for (int i = 0; i < count; ++i)
        {
            require[T.charAt(i)-'A']++;
            chSet[T.charAt(i)-'A'] = true;
        }
        int i = -1;
        int j = 0;
        int minLen = Integer.MAX_VALUE;
        int minIdx = 0;
        while (i < S.length() && j < S.length())
        {
            if (count>0)
            {
                i++;
                if(i<S.length()){
	                require[S.charAt(i)-'A']--;
	                if (chSet[S.charAt(i)-'A'] && require[S.charAt(i)-'A'] >= 0)
	                {
	                    count--;
	                }
                }
            }
            else
            {
                if (minLen > i - j + 1)
                {
                    minLen = i - j + 1;
                    minIdx = j;
                }
                require[S.charAt(j)-'A']++;
                if (chSet[S.charAt(j)-'A'] && require[S.charAt(j)-'A'] > 0)
                {
                    count++;
                }
                j++;
            }
        }
        if (minLen == Integer.MAX_VALUE)
        {
            return "";
        }
        return S.substring(minIdx, minIdx+minLen);
    }
	public static String minWindowShort(String s, String t) {
		int map[] = new int[256];
        for(char c: t.toCharArray()) 
        	map[c]++;
        int counter=t.length(), left=0, right=0, d=Integer.MAX_VALUE, head=0;
        while(right<s.length()){
        	char ch=s.charAt(right);
            if(map[ch]>0) { 
            	counter--; //in t
            }
        	map[ch]--;
            while(counter==0){ //valid
                if(right-left+1<d) { 
                	d=right-left+1;
                	head=left;
                }
                char leftChar=s.charAt(left);
                map[leftChar]++;
                if(map[leftChar]>0) {
                	counter++;  //make it invalid
                }
                left++;
            }
            right++;
        }
        return d==Integer.MAX_VALUE? "":s.substring(head, head+d);
	}
}
