/*
 * Given an input string, reverse the string word by word.

For example,
Given s = "the sky is blue",
return "blue is sky the".
	What constitutes a word?
		A sequence of non-space characters constitutes a word.
	Could the input string contain leading or trailing spaces?
		Yes. However, your reversed string should not contain leading or trailing spaces.
	How about multiple spaces between two words?
		Reduce them to a single space in the reversed string.
 */
package ds.strings;

public class ReverseWordsInString {
	public static void main(String[] args) {
		ReverseWordsInAString s=new ReverseWordsInAString();
		System.out.println(s.reverseWords("the sky   is blue    "));
	}
}
class ReverseWordsInAString {
    public String reverseWords(String s) {
        char[] a=s.toCharArray();
        a = reverse(a,0,a.length-1);
        a=reverseWords(a,a.length);
        return cleanSpaces(a,a.length);
    }
    public char[] reverse(char[] a,int low,int high){
        while(low<high){
            char temp=a[low];
            a[low++]=a[high];
            a[high--]=temp;
        }
        return a;
    }
    public char[] reverseWords(char[] a,int n){
    	int i=0,j=0;
    	while(i<n){
    		while(i<j || (i<n && a[i]==' '))
    			i++;
    		while(j<i || j<n && a[j]!=' ')
    			j++;
        	a=reverse(a,i,j-1);
    	}
    	return a;
    }
    String cleanSpaces(char[] a, int n) {
        int i = 0, j = 0;
          
        while (j < n) {
          while (j < n && a[j] == ' ') j++;             // skip spaces
          while (j < n && a[j] != ' ') a[i++] = a[j++]; // keep non spaces
          while (j < n && a[j] == ' ') j++;             // skip spaces
          if (j < n) a[i++] = ' ';                      // keep only one space
        }
      
        return new String(a).substring(0, i);
      }
}
