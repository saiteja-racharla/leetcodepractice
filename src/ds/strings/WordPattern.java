package ds.strings;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class WordPattern {

	public static void main(String[] args) {
		WordPattern w=new WordPattern();
		System.out.println(w.wordPattern("abba", "dog cat cat dog"));

	}
	public boolean wordPattern(String pattern, String str) {
		String[] words=str.split(" ");
		if(pattern.length()!=words.length)
			return false;
		Map temp=new HashMap();
		for(int i=0;i<words.length;i++){
			//some autoboxing stuff. that is the reason for comparing using objects.equals
			if(!Objects.equals(temp.put(pattern.charAt(i), i),temp.put(words[i], i)))
				return false;
		}
        return true;
    }

}
