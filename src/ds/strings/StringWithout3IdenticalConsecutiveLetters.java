/** Microsoft Online Assessment
 * Given a string S of N lowercase letters, return a string with no instance of three identical consecutive letters obtained from S by deleting 
 * the minimum possible number of letters
 * If S="eedaaad", function should return "eedaad". 
 * If S="xxxtxxx", function should return "xxtxx"
 */
package ds.strings;

public class StringWithout3IdenticalConsecutiveLetters {

	public static void main(String[] args) {
		System.out.println(getLongestSubstring("eedaaad"));
		System.out.println(getLongestSubstring("eedaaaaaaddddddeeecdde"));
		System.out.println(getLongestSubstring("xxxtxxx"));
	}
	private static String getLongestSubstring(String s) {
		StringBuffer sb=new StringBuffer();
		if(s.length()<3)
			return s;
		char cur=s.charAt(0);
		sb.append(cur);
		int count=1;
		for(int i=1;i<s.length();i++) {
			if(s.charAt(i)==cur) {
				count++;
				if(count==3) {
					count--;
				} else {
					sb.append(s.charAt(i));
				}
			} else {
				cur=s.charAt(i);
				count=1;
				sb.append(s.charAt(i));
			}
		}
		if(count==3)
			return sb.substring(0,sb.length()-2);
		return sb.toString();
	}

}
