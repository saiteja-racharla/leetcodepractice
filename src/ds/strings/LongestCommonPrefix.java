package ds.strings;

public class LongestCommonPrefix {
	public static void main(String[] args) {
		LongestCommonPrefix l=new LongestCommonPrefix();
		String[] arr={"geeksforgeeks", "geeks", "geek", "geezer"};
		System.out.println(l.longestCommonPrefix(arr));
		System.out.println(l.prefixSolution2(arr));
	}
	public String longestCommonPrefix(String[] strs) {
        return prefix(strs,0,strs.length-1);
    }
    public String prefix(String strs[],int low,int high){
        if(low==high){
            return strs[low];
        }
        if(high>low){
            int mid=(low+high)/2;
            String str1=prefix(strs,low,mid);
            String str2=prefix(strs,mid+1,high);
            return prefixUtil(str1,str2);
        }
		return null;
    }
    public String prefixUtil(String str1,String str2){
        String result = "";
        for(int i=0,j=0;i<=str1.length()-1&&j<=str2.length()-1;i++,j++){
            if (str1.charAt(i) != str2.charAt(j))
                break;
            result=result+str1.charAt(i);
        }
        return result;
    }
    /*
     * Get commons string between first and second. Store in some x variable.
     * Common between x and third string, store in x
     * Common between x and fourth string, store in x and so on
     * 
     */
    public String prefixSolution2(String[] strs){
    	String pre=strs[0];
    	int i=1;
    	while(i<strs.length){
    		while(strs[i].indexOf(pre)!=0)
    			pre=pre.substring(0,pre.length()-1);
    		i++;
    	}
    	return pre;
    }
}
