package ds.strings;

public class RegularExpressionPatternMatching {

	public static void main(String[] args) {
		System.out.println(isMatch("abc","a*b*"));
	}
	public static boolean isMatch(String str, String pattern) {
		char[] s=str.toCharArray();
		char[] p=pattern.toCharArray();
		boolean output[][]=new boolean[s.length+1][p.length+1];
		output[0][0]=true;
		//looks for patterns a*b* or a*b*c*
		for(int i=1;i<=p.length;i++){
			if(p[i-1]=='*'){
				output[0][i]=output[0][i-2];
			}
		}
		for(int i=1;i<output.length;i++){
			for(int j=1;j<output[0].length;j++){
				if(p[j-1]=='.' || s[i-1]==p[j-1]){
					output[i][j]=output[i-1][j-1];
				}
				else if(p[j-1]=='*'){
					output[i][j]=output[i][j-2];
					if(p[j-2]=='.' || s[i-1]==p[j-2]){
						output[i][j]=output[i][j]||output[i-1][j];
					}
				}
				else{
					output[i][j]= false;
				}
			}
		}
    	return output[s.length][p.length];
    }	
}
