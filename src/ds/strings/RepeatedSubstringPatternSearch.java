/*
 * Given a non-empty string check if it can be constructed by taking a substring of it 
 * and appending multiple copies of the substring together. 
 * You may assume the given string consists of lowercase English letters only and its 
 * length will not exceed 10000.
 */
package ds.strings;

public class RepeatedSubstringPatternSearch {

	public static void main(String[] args) {
		String str="ababababab";
		RepeatedSubstringPatternSearch rp=new RepeatedSubstringPatternSearch();
		System.out.println("Is there any repeated pattern? "+rp.isRepeatedPatternString(str));
		System.out.println("Answer by KWP Approach ?"+rp.isRepeatedPatternString1(str));
	}
	public boolean isRepeatedPatternString(String str){
		String str1=str+str;
		String str2=str1.substring(1,str1.length()-1);
		if(str2.contains(str)) return true;
		return false;
	}
	public boolean isRepeatedPatternString1(String str){
		int temp[]=createTemporaryArray(str);
		int val=temp[str.length()-1];
		return (val>0 && str.length()%(str.length()-val)==0);
	}
	public int[] createTemporaryArray(String str){
		int j=0;
		int temp[]=new int[str.length()];
		for(int i=1;i<str.length();){
			if(str.charAt(i)==str.charAt(j)){
				temp[i]=j+1;
				i++;
				j++;
			}
			else{
				if(j!=0){
					j=temp[j-1];
				}
				else{
					temp[i]=0;
					i++;
				}
			}
		}
		return temp;
	}
}
