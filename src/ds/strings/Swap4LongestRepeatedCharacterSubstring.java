/**
 * Given a string text, we are allowed to swap two of the characters in the string. Find the length of the longest substring with repeated characters.

 

Example 1:

Input: text = "ababa"
Output: 3
Explanation: We can swap the first 'b' with the last 'a', or the last 'b' with the first 'a'. Then, the longest repeated character substring is "aaa", which its length is 3.
Example 2:

Input: text = "aaabaaa"
Output: 6
Explanation: Swap 'b' with the last 'a' (or the first 'a'), and we get longest repeated character substring "aaaaaa", which its length is 6.
Example 3:

Input: text = "aaabbaaa"
Output: 4
Example 4:

Input: text = "aaaaa"
Output: 5
Explanation: No need to swap, longest repeated character substring is "aaaaa", length is 5.
Example 5:

Input: text = "abcdef"
Output: 1
 */
package ds.strings;

import java.util.ArrayList;
import java.util.List;

public class Swap4LongestRepeatedCharacterSubstring {

	public static void main(String[] args) {
		Swap4LongestRepeatedCharacterSubstring s=new Swap4LongestRepeatedCharacterSubstring();
		System.out.println(s.maxRepOpt1("aabaaab"));

	}
	public int maxRepOpt1(String text) {
		List<Integer>[] memo = new ArrayList[26];
        for (int i = 0; i < 26; i++) {
            memo[i] = new ArrayList<>();
        }
        int n = text.length();
        for (int i = 0; i < n; i++) {
            memo[text.charAt(i) - 'a'].add(i);
        }
        int res = 1;
        for (int i = 0; i < 26; i++) {
            int cnt = 1;
            int cnt1 = 0;
            int mx = 0;
            for (int j = 1; j < memo[i].size(); j++) {
                if (memo[i].get(j) == memo[i].get(j - 1) + 1) {
                    cnt++;
                } else {
                    cnt1 = memo[i].get(j) == memo[i].get(j - 1) + 2 ? cnt : 0;
                    cnt = 1;
                }
                mx = Math.max(mx, cnt + cnt1);
            }
            res = Math.max(res, mx + (memo[i].size() > mx ? 1 : 0));
        }
        return res;
   }
}
