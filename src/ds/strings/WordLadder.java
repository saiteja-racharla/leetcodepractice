/**
 * Given two words (beginWord and endWord), and a dictionary's word list, find the length of shortest transformation sequence from beginWord to endWord, such that:

Only one letter can be changed at a time.
Each transformed word must exist in the word list. Note that beginWord is not a transformed word.
Note:

Return 0 if there is no such transformation sequence.
All words have the same length.
All words contain only lowercase alphabetic characters.
You may assume no duplicates in the word list.
You may assume beginWord and endWord are non-empty and are not the same.
Example 1:

Input:
beginWord = "hit",
endWord = "cog",
wordList = ["hot","dot","dog","lot","log","cog"]

Output: 5

Explanation: As one shortest transformation is "hit" -> "hot" -> "dot" -> "dog" -> "cog",
return its length 5.
 */
package ds.strings;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class WordLadder {

	public static void main(String[] args) {
		WordLadder wl=new WordLadder();
		List<String> wordList=Arrays.asList(new String[] {"hot","dot","dog","lot","log","cog"});
		String beginWord="hit";
		String endWord="cog";
		System.out.println(wl.ladderLength(beginWord, endWord, wordList));
	}
	public int ladderLength(String beginWord, String endWord, List<String> wordList) {
        HashSet<String> wordset=new HashSet<String>(wordList);
        HashSet<String> reached=new HashSet<String>();
        reached.add(beginWord);
        wordset.remove(beginWord);
        int level=1;
        while(!reached.isEmpty()){
            HashSet<String> reachedNext=new HashSet<String>();
            for(String s:reached){
                for(int i=0;i<s.length();i++)
                {
                    char[] c=s.toCharArray();
                    for(char j='a';j<='z';j++){
                        c[i]=j;
                        String temp=new String(c);
                        if(wordset.contains(temp)){
                            wordset.remove(temp);
                            reachedNext.add(temp);
                            if(temp.equals(endWord)) return level+1;
                        }
                    }
                }
            }
            reached=reachedNext;
            level++;
        }
        return 0;
    }
}
