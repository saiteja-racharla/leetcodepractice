/**
 * Equations are given in the format A / B = k, where A and B are variables represented as 
 * strings, and k is a real number (floating point number). Given some queries, return the answers. 
 * If the answer does not exist, return -1.0.
 * Example:
 * Given a / b = 2.0, b / c = 3.0.  
 * queries are: a / c = ?, b / a = ?, a / e = ?, a / a = ?, x / x = ? . 
 * return [6.0, 0.5, -1.0, 1.0, -1.0 ].
 * 
 * equations = [ ["a", "b"], ["b", "c"] ],
 * values = [2.0, 3.0],
 * queries = [ ["a", "c"], ["b", "a"], ["a", "e"], ["a", "a"], ["x", "x"] ]. 
 */
package ds.strings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class EvaluateDivision {
	public static void main(String[] args) {
		String[][] equations = { {"a", "b"}, {"b", "c"} };
		double[] values={2.0, 3.0};
		String[][] queries={{"a", "c"}, {"b", "a"}, {"a", "e"}, {"a", "a"}, {"x", "x"}};
		EvaluateDivisionSolution evaluateDivision=new EvaluateDivisionSolution();
		double[] result=evaluateDivision.calcEquation(equations, values, queries);
		for(double res:result){
			System.out.print(res+" \t");
		}
	}
}

class EvaluateDivisionSolution {
    public double[] calcEquation(String[][] equations, double[] values, String[][] queries) {
        double[] result=new double[queries.length];
        HashMap<String, ArrayList<String>> pairs=new HashMap<String, ArrayList<String>>();
        HashMap<String, ArrayList<Double>> valuePairs=new HashMap<String, ArrayList<Double>>();
        for(int i=0;i<equations.length;i++){
        	String[] equation=equations[i];
        	if(!pairs.containsKey(equation[0])){
        		pairs.put(equation[0], new ArrayList<String>());
        		valuePairs.put(equation[0], new ArrayList<Double>());
        	}
        	if(!pairs.containsKey(equation[1])){
        		pairs.put(equation[1], new ArrayList<String>());
        		valuePairs.put(equation[1], new ArrayList<Double>());
        	}
        	pairs.get(equation[0]).add(equation[1]);
        	pairs.get(equation[1]).add(equation[0]);
        	valuePairs.get(equation[0]).add(values[i]);
        	valuePairs.get(equation[1]).add(1/values[i]);
        	
        }
        for(int i=0;i<queries.length;i++){
        	String query[]=queries[i];
        	result[i]=dfs(query[0],query[1],pairs,valuePairs,new HashSet<String>(), 1.0);
        	if(result[i]==0.0) result[i]=-1.0;
        }
        return result;
    }
    public double dfs(String start, String end, HashMap<String, ArrayList<String>> pairs, HashMap<String, ArrayList<Double>> valuePairs, HashSet<String> set, Double value){
    	if(set.contains(start)) return 0.0;		//already traversed, then return 0
    	if(!pairs.containsKey(start)) return 0.0;	//some unknown value like x,y,z
    	if(start.equals(end)) return value;		//start equals end, return value
    	set.add(start);
    	ArrayList<String> pairsList=pairs.get(start);
    	ArrayList<Double> valuePairsList=valuePairs.get(start);
    	double temp = 0.0;
    	for(int i=0;i<pairsList.size();i++){
    		temp=dfs(pairsList.get(i),end,pairs,valuePairs,set,value*valuePairsList.get(i));
    		if(temp!=0.0)
    			break;
    	}
    	set.remove(start);
    	return temp;
    }
}