/**
 * Given a string text, you want to use the characters of text to form as many instances of the word "balloon" as possible.
You can use each character in text at most once. Return the maximum number of instances that can be formed.
Example 1:
Input: text = "nlaebolko"
Output: 1

Example 2:
Input: text = "loonbalxballpoon"
Output: 2

Example 3:
Input: text = "leetcode"
Output: 0
 */
package ds.strings;

public class MaximumNumberOfBalloons {

	public static void main(String[] args) {
		MaximumNumberOfBalloons m=new MaximumNumberOfBalloons();
		/*
		 * System.out.println(m.maxNumberOfBalloons("nlaebolko"));
		 * System.out.println(m.maxNumberOfBalloons("loonbalxballpoon"));
		 * System.out.println(m.maxNumberOfBalloons("leetcode"));
		 */
		System.out.println(m.maxNumberOfBalloons("balon"));
	}
	public int maxNumberOfBalloons(String text) {
		int[] a=new int[26];
		for(char c:text.toCharArray()) {
			a[c-'a']++;
		}
		int min=a[0];
		min=Math.min(min, a[1]);	//character b
		min=Math.min(min, a[11]/2);	//character l
		min=Math.min(min, a[13]);	//character n
		min=Math.min(min, a[14]/2);	//character o
        return min;
    }

}
