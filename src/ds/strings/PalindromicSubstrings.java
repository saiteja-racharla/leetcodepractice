/*
 * Given a string, your task is to count how many palindromic substrings in this string.

The substrings with different start indexes or end indexes are counted as different 
substrings even they consist of same characters.
 */
package ds.strings;

public class PalindromicSubstrings {
	static int count=0;
	public static void main(String[] args) {
		String s="aaa";
		for(int i=0;i<s.length();i++){
			extendPalindrome(s,i,i);
			extendPalindrome(s,i,i+1);
		}
		System.out.println("Number of palindromes in given string-"+s+" is "+count);
	}
	public static void extendPalindrome(String s, int low, int high){
		while(low>=0 && high<s.length() && s.charAt(low)==s.charAt(high)){
			count++;
			low--;
			high++;
		}
	}
}
