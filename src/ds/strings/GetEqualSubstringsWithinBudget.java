/** Get Equal Substrings Within Budget
 * You are given two strings s and t of the same length. You want to change s to t. Changing the i-th character of s to i-th character of t costs |s[i] - t[i]| that is, the absolute difference between the ASCII values of the characters.
You are also given an integer maxCost.
Return the maximum length of a substring of s that can be changed to be the same as the corresponding substring of twith a cost less than or equal to maxCost.
If there is no substring from s that can be changed to its corresponding substring from t, return 0.
Example 1:
Input: s = "abcd", t = "bcdf", maxCost = 3
Output: 3
Explanation: "abc" of s can change to "bcd". That costs 3, so the maximum length is 3.

Example 2:
Input: s = "abcd", t = "cdef", maxCost = 3
Output: 1
Explanation: Each character in s costs 2 to change to charactor in t, so the maximum length is 1.

Example 3:
Input: s = "abcd", t = "acde", maxCost = 0
Output: 1
Explanation: You can't make any change, so the maximum length is 1.
 */
package ds.strings;

public class GetEqualSubstringsWithinBudget {
	public static void main(String[] args) {
		GetEqualSubstringsWithinBudget g=new GetEqualSubstringsWithinBudget();
		String s = "abcd", t = "bcdf";
		int maxCost = 3;
		System.out.println(g.equalSubstring(s, t, maxCost));
		s = "abcd"; t = "cdef"; maxCost = 3;
		System.out.println(g.equalSubstring(s, t, maxCost));
		s = "abcd"; t = "acde"; maxCost = 0;
		System.out.println(g.equalSubstring(s, t, maxCost));
		s = "abcdghimnopqrs"; t="bdefhijmopqrst"; maxCost=3;
		System.out.println(g.equalSubstring(s, t, maxCost));
		s = "krrgw"; t="zjxss"; maxCost=19;
		System.out.println(g.equalSubstring(s, t, maxCost));

	}
	public int equalSubstring(String s, String t, int maxCost) {
		int start=0,end=0,currValue=0;
		int maxLengthOfString=Integer.MIN_VALUE;
		while(end<s.length() && start<s.length()) {
			currValue=Math.abs((t.charAt(end)-'a')-(s.charAt(end)-'a'))+currValue;
			if(currValue>maxCost) {
				maxLengthOfString=Math.max(maxLengthOfString, end-1-start);
				currValue=currValue-Math.abs((t.charAt(start)-'a')-(s.charAt(start)-'a'))-Math.abs((t.charAt(end)-'a')-(s.charAt(end)-'a'));
				start++;
			} else if(currValue==maxCost) {
				maxLengthOfString=Math.max(maxLengthOfString, end-start+1);
				end++;
			} else {
				if(currValue<=maxCost) {
					maxLengthOfString=Math.max(maxLengthOfString, end-start+1);
				}
				end++;
			}
		}
		maxLengthOfString=Math.max(maxLengthOfString, end-start);
        return maxLengthOfString;
    }
}
