package ds.strings;

public class SplitStringBalancedStrings {

	public static void main(String[] args) {
		SplitStringBalancedStrings s=new SplitStringBalancedStrings();
		System.out.println(s.balancedStringSplit("RLRRLLRLRL"));
		System.out.println(s.balancedStringSplit("RLLLLRRRLR"));
		System.out.println(s.balancedStringSplit("LLLLRRRR"));
	}
	public int balancedStringSplit(String s) {
		int count=0,sum=0;
		for(int i=0;i<s.length();i++) {
			if(s.charAt(i)=='L')
				sum++;
			else
				sum--;
			if(sum==0)
				count++;
		}
        return count;
    }
}
