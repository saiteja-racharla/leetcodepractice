/**
 * This program generates parentheses
 * https://www.youtube.com/watch?v=JKXs7a4RMFU
 */
package ds.strings;

import java.util.ArrayList;
import java.util.Currency;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class GenerateParentheses {

	public static void main(String[] args) {
		List<String> result=generateParenthesis(3);
		for(String s:result){
			System.out.println(s);
		}
		
	}
	public static List<String> generateParenthesis(int n) {
		GenerateParenthesesNode root=new GenerateParenthesesNode("",0,0);
		List<String> result=new ArrayList<String>();
		Queue<GenerateParenthesesNode> q=new LinkedList<GenerateParenthesesNode>();
		q.add(root);
		while(!q.isEmpty()){
			GenerateParenthesesNode curr=q.poll();
			if(curr.left==n && curr.right==n){
				result.add(curr.str);
				continue;
			}
			if(curr.left<n){
				GenerateParenthesesNode temp=new GenerateParenthesesNode(curr.str+"(",curr.left+1,curr.right);
				q.add(temp);
			}
			if(curr.right<curr.left){
				GenerateParenthesesNode temp=new GenerateParenthesesNode(curr.str+")",curr.left,curr.right+1);
				q.add(temp);
			}
		}
		return result;
    }
}

class GenerateParenthesesNode{
	String str;
	int left, right;
	GenerateParenthesesNode(String s, int left,int right){
		this.str=s;
		this.left=left;
		this.right=right;
	}
}
