/*Search for a string/pattern in given text
 * This program has brute forch apprach as well as KMP Search
 * KMP Stands for Knuth - Morris - Pratt
 * Brute force solves in O(mn)-worst case time complexity
 * m-length of text, n- length of pattern
 * KMP Search solves in O(m+n)- time complexity
 * Space complexity for KMP Search is O(n).
 * 
 * Leetcode time complexity--
 * L==needle.length() n=haystack.length()
 * Time complexity-O((N-L)L) Space complexity-O(1)
 */
package ds.strings;

public class SubStringSearch {
	public static boolean stringSearchBruteForceApproach(String pattern, String text){
		int i=0;
		int j=0;
		int k=0;
		while(i<text.length() && j<pattern.length()){
			if(text.charAt(i)==pattern.charAt(j)){
				i++;
				j++;
			}
			else{
				j=0;
				k++;
				i=k;
			}
		}
		if(j==pattern.length())
			return true;
		return false;
	}
	public static int strStr(String haystack, String needle) {
        if(haystack.length()<needle.length()) return -1;
        if(needle.length()==0) return 0;
        for(int i=0;;i++){
            for(int j=0;;j++){
                if(j==needle.length()) return i;
                if(i+j==haystack.length()) return -1;
                if(haystack.charAt(i+j)!=needle.charAt(j)) break;
            }
        }
    }
	public static int[] createTemporaryArray(String pattern){
		int index=0;
		int i=0;
		int[] temporaryArray=new int[pattern.length()];
		for(i=1;i<pattern.length();){
			if(pattern.charAt(index)==pattern.charAt(i)){
				temporaryArray[i]=index+1;
				index++;
				i++;
			}
			else{
				if(index!=0){
					index = temporaryArray[index-1];
				}
				else{
					temporaryArray[i]=0;
					i++;
				}
			}
		}
		return temporaryArray;
	}
	public static boolean stringSearchKMPApproach(String pattern, String text){
		int i=0;
		int j=0;
		int[] temporaryArray=createTemporaryArray(pattern);
		while(i<pattern.length() && j<text.length()){
			if(pattern.charAt(i)==text.charAt(j)){
				i++;
				j++;
			}
			else{
				if(i!=0){
					i = temporaryArray[i-1];
				}
				else{
					j++;
				}
			}
		}
		//If the pattern is present in text, its present at index(j-pattern.length) 
		if(i==pattern.length())
			return true;
		return false;
	}
	public static void main(String[] args) {
		String text = "abcxabcdxabcdabcy";
        String pattern = "abcdabcy";
        System.out.println("Brute-force search");
        System.out.println(stringSearchBruteForceApproach(pattern,text)?"Found":"Not found");
        System.out.println("KMP Approach");
        System.out.println(stringSearchKMPApproach(pattern,text)?"Found":"Not found");
	}
}
