/*
 * Palindrom partitioning II
 * Given a string s, partition s such that every substring of the partition is a palindrome.

Return the minimum cuts needed for a palindrome partitioning of s.

Example:

Input: "aab"
Output: 1
Explanation: The palindrome partitioning ["aa","b"] could be produced using 1 cut.
 */
package ds.strings;

public class PalindromePartitioningII {
	public static void main(String[] args) {
		String s="abcbm";
		solution1(s);
		solution2(s);
	}
	//Tushar roy solution. this solution has produced "Time Limit Exceeded Exception"
	public static void solution1(String s){
		int[][] res=new int[s.length()][s.length()];
		for(int i=0;i<s.length();i++){
			for(int j=0;j<=s.length()-i-1;j++){
				int k=j+i;
				if(isPalindrome(s, j, k)){
					res[j][k]=0;
				}
				else{
					int min=0;
					boolean firstTimeLoop=true;
					for(int l=j;l<k;l++){
						if(firstTimeLoop){
							min=res[j][l]+res[l+1][k];
							firstTimeLoop=!firstTimeLoop;
						}
						else
							min=Math.min(min, res[j][l]+res[l+1][k]);
					}
					res[j][k]=1+min;
				}
			}
		}
		System.out.println(res[0][s.length()-1]);
	}
	public static boolean isPalindrome(String s, int left, int right){
		if(left==right) return true;
		while(left<right){
			if(s.charAt(left)!=s.charAt(right)) return false;
			left++;right--;
		}
		return true;
	}
	/*https://discuss.leetcode.com/topic/32575/easiest-java-dp-solution-97-36/2*/
	public static void solution2(String s){
		char[] c = s.toCharArray();
	    int n = c.length;
	    int[] cut = new int[n];
	    boolean[][] pal = new boolean[n][n];
	    
	    for(int i = 0; i < n; i++) {
	        int min = i;
	        for(int j = 0; j <= i; j++) {
	            if(c[j] == c[i] && (j + 1 > i - 1 || pal[j + 1][i - 1])) {
	                pal[j][i] = true;  
	                min = j == 0 ? 0 : Math.min(min, cut[j - 1] + 1);
	            }
	        }
	        cut[i] = min;
	    }
        System.out.println(cut[s.length() - 1]);
	}
}
