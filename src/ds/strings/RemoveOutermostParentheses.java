/** Leetcode: 1021
 * https://leetcode.com/problems/remove-outermost-parentheses/description/
 * A valid parentheses string is either empty (""), "(" + A + ")", or A + B, where A and B are valid parentheses strings, and + represents string concatenation.  For example, "", "()", "(())()", and "(()(()))" are all valid parentheses strings.

A valid parentheses string S is primitive if it is nonempty, and there does not exist a way to split it into S = A+B, with A and B nonempty valid parentheses strings.

Given a valid parentheses string S, consider its primitive decomposition: S = P_1 + P_2 + ... + P_k, where P_i are primitive valid parentheses strings.

Return S after removing the outermost parentheses of every primitive string in the primitive decomposition of S.

Input: "(()())(())"
Output: "()()()"
Explanation: 
The input string is "(()())(())", with primitive decomposition "(()())" + "(())".
After removing outer parentheses of each part, this is "()()" + "()" = "()()()".

Input: "(()())(())(()(()))"
Output: "()()()()(())"
Explanation: 
The input string is "(()())(())(()(()))", with primitive decomposition "(()())" + "(())" + "(()(()))".
After removing outer parentheses of each part, this is "()()" + "()" + "()(())" = "()()()()(())".

Input: "()()"
Output: ""
Explanation: 
The input string is "()()", with primitive decomposition "()" + "()".
After removing outer parentheses of each part, this is "" + "" = "".

 */
package ds.strings;

public class RemoveOutermostParentheses {

	public static void main(String[] args) {
		String S="(()())(())(()(()))";
		System.out.println(removeOuterParentheses(S));
	}
	public static String removeOuterParentheses(String S) {
		int start=0;
		int open=0,close=0;
		StringBuffer sb=new StringBuffer();
		for(int i=0;i<S.length();i++) {
			if(S.charAt(i)=='(')
				open++;
			if(S.charAt(i)==')')
				close++;
			if(open==close) {
				sb.append(S.substring(start+1, i));
				start=i+1;
			}
		}
        return sb.toString();
    }

}
