package ds.strings;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AutoCompleteSystem {

	Trie root;
    String curr_sent="";
	public static void main(String[] args) {
		AutoCompleteSystem s=new AutoCompleteSystem();
		String[] sentences= {"i love you","island","iroman","i love leetcode"};
		int[] times= {5,3,2,2};
		s.searchSystem(sentences, times);
		System.out.println(s.input('i'));
		System.out.println(s.input(' '));
		System.out.println(s.input('a'));
		System.out.println(s.input('#'));
	}
    public void searchSystem(String[] sentences, int[] times) {
    	root=new Trie();
        insertInToTrie(root,sentences,times);
    }
    
    public List<String> input(char c) {
        List<String> result=new ArrayList<>();
        if(c=='#'){
            insert(root,curr_sent,1);
            curr_sent="";
        } else{
            curr_sent+=c;
            List<Node> list=lookup(root,curr_sent);
            Collections.sort(list,(a,b)->(a.times==b.times)?a.sentence.compareTo(b.sentence):b.times-a.times);
            for(int i=0;i<Math.min(3,list.size());i++)
                result.add(list.get(i).sentence);
        }
        return result;
    }
    
    public List<Node> lookup(Trie root,String s){
        Trie pCrawl=root;
        List<Node> result = new ArrayList<>();
        for(int i=0;i<s.length();i++){
            if(s.charAt(i)==' '){
                if(pCrawl.children[26]==null){
                    return new ArrayList<>();
                }
                pCrawl=pCrawl.children[26];
            } else{
                if(pCrawl.children[s.charAt(i)-'a']==null){
                    return new ArrayList<>();
                }
                pCrawl=pCrawl.children[s.charAt(i)-'a'];
            }
        }
        dfs(pCrawl,s,result);
        return result;
    }
    
    public void dfs(Trie pCrawl,String s,List<Node> list){
        if(pCrawl.times>0) 
            list.add(new Node(s,pCrawl.times));
        for(char i='a';i<='z';i++){
            if(pCrawl.children[i-'a']!=null){
                dfs(pCrawl.children[i-'a'],s+i,list);
            }
        }
        if(pCrawl.children[26]!=null)
            dfs(pCrawl.children[26],s+' ',list);
    }
    
    public void insertInToTrie(Trie root,String[] sentences, int[] times){
        for(int i=0;i<sentences.length;i++){
            insert(root,sentences[i],times[i]);
        }
    }
    public void insert(Trie root,String sentence,int times){
        Trie pCrawl=root;
        for(char ch:sentence.toCharArray()){
            if(ch==' '){
                if(pCrawl.children[26]==null)
                    pCrawl.children[26]=new Trie();
                pCrawl=pCrawl.children[26];
            } else{
                if(pCrawl.children[ch-'a']==null)
                    pCrawl.children[ch-'a']=new Trie();
                pCrawl=pCrawl.children[ch-'a'];
            }
        }
        pCrawl.times+=times;
    }
}
class Trie{
    int times;
    Trie[] children=new Trie[27];
}
class Node{
    String sentence;
    int times;
    Node(String sentence,int times){
        this.sentence=sentence;
        this.times=times;
    }
}