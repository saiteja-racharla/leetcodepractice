/**
 * Given two strings A and B of lowercase letters, 
 * return true if and only if we can swap two letters in A so that the result equals B.
 */
package ds.strings;

public class BuddyStrings {

	public static void main(String[] args) {
		String A="abccccd";
		String B="abcccdc";
		System.out.println(A+" "+B +" are buddy strings:"+buddyStrings(A, B));
	}
	public static boolean buddyStrings(String A, String B) {
		if(A.length()!=B.length())
			return false;
		int firstDiff = -1;
		int secondDiff = -1;
		int diff=0;
		int[] count=new int[26];
		boolean canSwitch=false;
		for(int i=0;i<A.length();i++) {
			if(A.charAt(i)==B.charAt(i)) {
				if(++count[A.charAt(i)-'a']>=2) 
					canSwitch=true;
			}
			else {
				if(firstDiff==-1) {
					firstDiff=i;
				}
				else if(secondDiff==-1){
					secondDiff=i;
				}
				diff++;
			}
		}
		if(diff==0 && canSwitch) {
			return true;
		}
		else if(diff==2 && A.charAt(firstDiff)==B.charAt(secondDiff) && A.charAt(secondDiff)==B.charAt(firstDiff)) {
			return true;
		}
		else
			return false;
    }

}
