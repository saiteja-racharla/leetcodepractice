package ds.strings;

import java.util.ArrayList;
import java.util.List;

import test.sai.checkStringCanBeMadePalindrome;

public class RemoveInvalidParentheses {

	public static void main(String[] args) {
		RemoveInvalidParentheses r=new RemoveInvalidParentheses();
		//System.out.println(r.removeInvalidParentheses("()())()())"));
		//System.out.println(r.removeInvalidParentheses("(a)())()"));
		System.out.println(r.removeInvalidParentheses(")("));
	}

	public List<String> removeInvalidParentheses(String s) {
	    List<String> ans = new ArrayList<>();
	    remove(s, ans, 0, 0, new char[]{'(', ')'});
	    return ans;
	}
	
	public void remove(String s, List<String> ans, int last_i, int last_j,  char[] par) {
	    for(int stack=0,i=last_i;i<s.length();i++) {
	    	if(s.charAt(i)==par[0])
	    		stack++;
	    	if(s.charAt(i)==par[1])
	    		stack--;
	    	if(stack>=0)
	    		continue;
	    	//if stack<0, which means one extra ')' is encountered. We have to remove it 
	    	//and process rest of the string. We can remove any ')' before the encoutered ')'
	    	for(int j=last_j;j<=i;j++) {
	    		//the brace we are going to remove should be ')'(second in par[]) and it should not be 
	    		//equal to previous brace.
	    		if(s.charAt(j)==par[1] && (j==last_j || s.charAt(j-1)!=par[1]))
	    			remove(s.substring(0,j)+s.substring(j+1,s.length()),ans,i,j,par);
	    	}
	    	return;
	    }
	    //What if s = �(()(()� in which we need remove �(�?...We reverse string and use same logic
	    String reversed=new StringBuilder(s).reverse().toString();
	    if(par[0]=='(')
	    	remove(reversed,ans,0,0,new char[] {')','('});
	    else
	    	ans.add(reversed);
	}
}
