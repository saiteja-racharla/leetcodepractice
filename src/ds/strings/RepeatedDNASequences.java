package ds.strings;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RepeatedDNASequences {
	public static void main(String[] args) {
		String s="AAAAACCCCCAAAAACCCCCCAAAAAGGGTTT";
		RepeatedDNASequences r=new RepeatedDNASequences();
		List<String> repeated=r.findRepeatedDnaSequences(s);
		
	}
	public List<String> findRepeatedDnaSequences(String s) {
		Set<String> seen = new HashSet<String>();
		Set<String> repeated = new HashSet<String>();
        for(int i=0;i+9<s.length();i++){
        	String ten = s.substring(i, i + 10);
        	if(!seen.add(ten))
        		repeated.add(ten);
        }
		return new ArrayList(repeated);
    }
}
