/** Microsoft Online Assessment
 * Min Moves to Obtain String Without 3 Identical Consecutive Letters
 * You are given a string S consisting of N letters 'a' and/or 'b'. In one move, you can swap one letter for the other('a' for 'b' or 'b' for 'a')
 * Write a function that, given such a String S, returns the minimum number of moves required to obtain a String consisting no instances of 
 * 3 identical consecutive letters
 * 
 * there are fixed pattern we need to consider:
3 consecutive : baaab , replace the middle a
4 consecutive : baaaab, replace the third a
5 consecutive : baaaaab, replace the third a

for more than 5, replace the third char and it can be reduced as the above three conditions
6 consecutive : baaaaaab --> baabaaab ->trans to baaab with 1 replacement -> trans to bb with 2 replancement
10 consecutive : baaaaaaaaaab --> baabaaaaaaab ->trans to baaaaaaab with 1 replacement -> trans to baaaab with 2 replacement -> trans to baab with 3 replacement, done.
 */
package ds.strings;

public class MinMovesObtainStringWithout3IdentConsecLetters {
	public static void main(String[] args) {
		System.out.println(solution("baaab"));
		System.out.println(solution("baaaab"));
		System.out.println(solution("baaaaab"));
		System.out.println(solution("baaaaaab"));
	}
	public static int solution(String s){
		int res = 0;
		for(int i = 0 ; i < s.length() ;){
			int next = i+1;
			while(next < s.length() && s.charAt(i) == s.charAt(next))
				next++;
			res += (next - i)/3;
			i = next;
		}
		return res;
	}
}
