//this program is not working
package ds.strings;

import java.util.ArrayList;
import java.util.List;

public class RestoreIPAddresses {
	public static void main(String[] args) {
		RestoreIPAddresses r=new RestoreIPAddresses();
		r.restoreIpAddresses("25525511135").size();
	}
	public List<String> restoreIpAddresses(String s) {
	    List<String> solutions = new ArrayList<String>();
	    restoreIp(s, solutions, 0, "", 0);
	    return solutions;
	}

	private void restoreIp(String ip, List<String> solutions, int idx, String restored, int count) {
	    if (count > 4) return;
	    if (count == 4 && idx == ip.length()) solutions.add(restored);
	    
	    for (int i=1; i<4; i++) {
	        if (idx+i > ip.length()) break;
	        String s = ip.substring(idx,idx+i);
	        if ((s.startsWith("0") && s.length()>1) || (i==3 && Integer.parseInt(s) >= 256)) continue;
	        restoreIp(ip, solutions, idx+i, restored+s+(count==3?"" : "."), count+1);
	    }
	}
}
