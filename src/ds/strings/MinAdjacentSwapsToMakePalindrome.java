/** Microsoft Online Assessment
 * Given a string, what is the minimum number of adjacent swaps required to convert a string into a palindrome. If not possible, return -1.
 * Example 1:
Input: "mamad"
Output: 3

Example 2:
Input: "asflkj"
Output: -1

Example 3:
Input: "aabb"
Output: 2

Example 4:
Input: "ntiin"
Output: 1
Explanation: swap 't' with 'i' => "nitin"
 */
package ds.strings;

public class MinAdjacentSwapsToMakePalindrome {
	public static void main(String[] args) {
		System.out.println(String.format("Minimum swaps needed to make String-'%s' palindrome is %d","mamad",minSwapPalindrome("mamad")));
		System.out.println(String.format("Minimum swaps needed to make String-'%s' palindrome is %d","asflkj",minSwapPalindrome("asflkj")));
		System.out.println(String.format("Minimum swaps needed to make String-'%s' palindrome is %d","aabb",minSwapPalindrome("aabb")));
	}

	public static int minSwapPalindrome(String s) {
		if (s == null)
			throw new IllegalArgumentException();
		if (!canFormPalindrome(s))
			return -1;

		int n = s.length(), swaps = 0;
		StringBuilder sb = new StringBuilder(s);

		for (int i = 0; i < n / 2; i++) {
			boolean found = false;
			for (int j = n - i - 1; j > i; j--) {
				// System.out.println(i + ":" + j);
				if (sb.charAt(j) == sb.charAt(i)) {
					found = true;
					for (int k = j; k < n - i - 1; k++) {
						swap(sb, k, k + 1);
						swaps++;
					}
					break;
				}
			}

			if (!found && n % 2 != 0) {
				for (int k = i; k < n / 2; k++) {
					swap(sb, k, k + 1);
					swaps++;
				}
			}
		}

		return swaps;
	}

	private static void swap(StringBuilder sb, int i, int j) {
		char c = sb.charAt(i);
		sb.setCharAt(i, sb.charAt(j));
		sb.setCharAt(j, c);
	}

	// only for lowercase letter
	private static boolean canFormPalindrome(String s) {
		int[] counts = new int[26];
		for (char c : s.toCharArray())
			counts[c - 'a']++;

		boolean hasOdd = false;
		for (int count : counts) {
			if (count % 2 == 0)
				continue;
			else {
				if (hasOdd)
					return false;
				hasOdd = true;
			}
		}

		return true;
	}
}
