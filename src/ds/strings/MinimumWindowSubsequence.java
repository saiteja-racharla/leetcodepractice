/**
 * Given strings S and T, find the minimum (contiguous) substring W of S, so that T is a subsequence of W.
If there is no such window in S that covers all characters in T, return the empty string "". If there are multiple such minimum-length windows, return the one with the left-most starting index.
Example 1:
Input: 
S = "abcdebdde", T = "bde"
Output: "bcde"
Explanation: 
"bcde" is the answer because it occurs before "bdde" which has the same length.
"deb" is not a smaller window because the elements of T in the window must occur in order.
 */
package ds.strings;

public class MinimumWindowSubsequence {

	public static void main(String[] args) {
		MinimumWindowSubsequence m=new MinimumWindowSubsequence();
		String S="abcdebdde";
		String T="bde";
		System.out.println(m.minWindow(S, T));
	}
	 public String minWindow(String S, String T) {
	        char[] s = S.toCharArray(), t = T.toCharArray();
	        int sindex = 0, tindex = 0, slen = s.length, tlen = t.length, start = -1, len = slen;
	        while(sindex<slen){
	            if(s[sindex]==t[tindex]){
	                tindex++;
	                if(tindex==tlen){
	                    tindex--;
	                    int endindex=sindex+1;
	                    while(tindex>=0){
	                        while(s[sindex]!=t[tindex])
	                            sindex--;
	                        sindex--;
	                        tindex--;
	                    }
	                    sindex++;
	                    tindex++;
	                    if(endindex-sindex<len){
	                        len=endindex-sindex;
	                        start=sindex;
	                    }
	                }
	            }
	            sindex++;
	        }
	        return start == -1? "" : S.substring(start, start + len);
	    }
}
