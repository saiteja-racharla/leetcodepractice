package ds.strings;

public class StringCompression {
	public static void main(String[] args) {
		char[] a={'a','a','a','b','b','b'};
		int length=compress(a);
		System.out.println(length);
	}
	public static int compress(char[] chars) {
		int anchor=0,write=0;
		for(int read=0;read<chars.length;read++){
			if (read + 1 == chars.length || chars[read + 1] != chars[read]) {
				chars[write++] = chars[anchor];
				if (read > anchor) {
					for(char c:String.valueOf(read-anchor+1).toCharArray()){
                        chars[write++] = c;
                    }
                }
				anchor = read + 1;
			}
		}
		return write;
    }
}
