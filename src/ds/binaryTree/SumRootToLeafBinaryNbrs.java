/** Leetcode:1022
 * https://leetcode.com/problems/sum-of-root-to-leaf-binary-numbers/description/
 * Given a binary tree, each node has value 0 or 1.  Each root-to-leaf path represents a binary number starting with the most significant bit.  For example, if the path is 0 -> 1 -> 1 -> 0 -> 1, then this could represent 01101 in binary, which is 13.

For all leaves in the tree, consider the numbers represented by the path from the root to that leaf.

Return the sum of these numbers.
 */
package ds.binaryTree;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SumRootToLeafBinaryNbrs {
	static List<Integer> list=new ArrayList<Integer>();
	static int sum=0;
	public static void main(String[] args) {
		BinaryTreeNode root=new BinaryTreeNode(1);
		root.left=new BinaryTreeNode(0);
		root.left.left=new BinaryTreeNode(0);
		root.left.right=new BinaryTreeNode(1);
		root.left.left.left=new BinaryTreeNode(1);
		root.left.left.right=new BinaryTreeNode(0);
		root.left.right.left=new BinaryTreeNode(1);
		root.right=new BinaryTreeNode(1);
		root.right.left=new BinaryTreeNode(0);
		root.right.right=new BinaryTreeNode(1);
		System.out.println(sumRootToLeaf(root));
	}
	public static int sumRootToLeaf(BinaryTreeNode root) {
		
		return sumRootToLeafUtil(root,sum);
    }
	public static int sumRootToLeafUtil(BinaryTreeNode root,int sum) {
		sum=sum*2+root.data;
		if(root.left==null && root.right==null) {
			return sum;
		}
		int leftSum=root.left!=null?sumRootToLeafUtil(root.left,sum):0;
		int rightSum=root.right!=null?sumRootToLeafUtil(root.right,sum):0;
		return leftSum+rightSum;
    }
}
