package ds.binaryTree;

public class BinaryTreeMaximumPathSum {
	static int maxValue;
	public static void main(String[] args) {
		BinaryTreeNode root=new BinaryTreeNode(-10);
		root.left=new BinaryTreeNode(9);
		root.right=new BinaryTreeNode(20);
		root.right.left=new BinaryTreeNode(15);
		root.right.right=new BinaryTreeNode(7);
		/*
		 * BinaryTreeNode root=new BinaryTreeNode(2); root.left=new BinaryTreeNode(-1);
		 */
		System.out.println("Maximum path sum is "+maxPathSum(root));
	}
	public static int maxPathSum(BinaryTreeNode root){
		maxValue = Integer.MIN_VALUE;
        maxPathDown(root);
        return maxValue;
	}
	public static int maxPathDown(BinaryTreeNode node){
		if(node==null) return 0;
		int left=Math.max(0,maxPathDown(node.left));
		int right=Math.max(0,maxPathDown(node.right));
		maxValue = Math.max(maxValue, left + right + node.data);
	    return Math.max(left, right) + node.data;
	}
	
}
