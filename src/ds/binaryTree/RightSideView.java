/**
 * Given a binary tree, imagine yourself standing on the right side of it, return the values of the
 *  nodes you can see ordered from top to bottom.
 * 
 *	Example:
 *	Input: [1,2,3,null,5,null,4]
	Output: [1, 3, 4]
	Explanation:

   1            <---
 /   \
2     3         <---
 \     \
  5     4       <---
 */
package ds.binaryTree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class RightSideView {

	public static void main(String[] args) {
		BinaryTreeNode root=new BinaryTreeNode(1);
		root.left=new BinaryTreeNode(2);
		root.right=new BinaryTreeNode(3);
		root.left.right=new BinaryTreeNode(5);
		root.right.right=new BinaryTreeNode(4);
		root.right.right.left=new BinaryTreeNode(6);
		root.right.right.left.right=new BinaryTreeNode(7);
		root.right.right.left.right.right=new BinaryTreeNode(8);
		List<Integer> result=printRightSideView(root);
		System.out.println(result);
		printRightSideViewRecursion(root,result,0);
		System.out.println(result);
	}
	public static List<Integer> printRightSideView(BinaryTreeNode root){
		List<Integer> result=new ArrayList<Integer>();
		Queue<BinaryTreeNode> queue=new LinkedList<BinaryTreeNode>();
		queue.add(root);
		int currNumElementsRow=0;
		int nextNumElementsRow=0;
		currNumElementsRow=1;
		while(!queue.isEmpty()){
			result.add(queue.peek().data);
			for(int i=0;i<currNumElementsRow;i++){
				BinaryTreeNode temp=queue.poll();
				if(temp.right!=null){
					queue.add(temp.right);
					nextNumElementsRow++;
				}
				if(temp.left!=null){
					queue.add(temp.left);
					nextNumElementsRow++;
				}
			}
			currNumElementsRow=nextNumElementsRow;
			nextNumElementsRow=0;
		}
		return result;
	}
	
	public static void printRightSideViewRecursion(BinaryTreeNode root,List<Integer> result,int depth){
		if(root==null) return;
		if(depth==result.size())
			result.add(root.data);
		printRightSideViewRecursion(root.right, result, depth+1);
		printRightSideViewRecursion(root.left, result, depth+1);
	}
}
