package ds.binaryTree;

import java.util.Queue;
import java.util.Stack;

import java.util.LinkedList;

public class MaximumLevelSumBinaryTree {

	public static void main(String[] args) {
		BinaryTreeNode root=new BinaryTreeNode(1);
		root.left=new BinaryTreeNode(7);
		root.right=new BinaryTreeNode(0);
		root.left.left=new BinaryTreeNode(7);
		root.left.right=new BinaryTreeNode(-8);
		root.right.left=new BinaryTreeNode(11);
		root.right.right=new BinaryTreeNode(12);
		root.left.left.left=new BinaryTreeNode(23);
		System.out.println(maxLevelSum(root));

	}
	public static int maxLevelSum(BinaryTreeNode root) {
		Queue<BinaryTreeNode> q=new LinkedList<BinaryTreeNode>();
		int maxCount=0;
		int numberOfNodes=0;
		int level=0,maxLevel=0;
		q.add(root);
		numberOfNodes=1;
		level=1;
		while(!q.isEmpty()) {
			int currCount=0;
			numberOfNodes=q.size();
			while(numberOfNodes-->0) {
				BinaryTreeNode pop=q.poll();
				currCount=currCount+pop.data;
				if(pop.left!=null) {
					q.add(pop.left);
				}
				if(pop.right!=null) {
					q.add(pop.right);
				}
			}
			if(currCount>maxCount) {
				maxCount=currCount;
				maxLevel=level;
			}
			level++;
		}
		return maxLevel;
    }

}
