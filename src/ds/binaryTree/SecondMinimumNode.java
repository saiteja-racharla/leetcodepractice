package ds.binaryTree;

public class SecondMinimumNode {
	public static void main(String[] args) {
		
	}
	public int findSecondMinimumValue(BinaryTreeNode root) {
		if(root==null)
			return -1;
		if(root.left==null && root.right==null)
			return -1;
		int left=root.left.data;
		int right=root.right.data;
		if(root.data==left)
			left= findSecondMinimumValue(root.left);
		if(root.data==right)
			right= findSecondMinimumValue(root.right);
		if(left==-1 && right==-1)
			return Math.max(left, right);
		else if(left!=-1)
			return left;
		else
			return right;
    }
}
