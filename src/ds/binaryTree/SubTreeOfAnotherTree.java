/**
 * Given two non-empty binary trees s and t, check whether tree t has exactly the same structure and node values with a subtree of s. A subtree of s is a tree consists of a node in s and all of this node's descendants. The tree s could also be considered as a subtree of itself.
 * https://leetcode.com/problems/subtree-of-another-tree/description/
 */

package ds.binaryTree;

public class SubTreeOfAnotherTree {
	static boolean isSubTree=true;
	public static void main(String[] args) {
//		BinarySearchTreeNode root1=new BinarySearchTreeNode(3);
//		root1.left=new BinarySearchTreeNode(4);
//		root1.right=new BinarySearchTreeNode(5);
//		root1.left.left=new BinarySearchTreeNode(1);
//		root1.left.right=new BinarySearchTreeNode(2);
//		
//		BinarySearchTreeNode root2=new BinarySearchTreeNode(4);
//		root2.left=new BinarySearchTreeNode(1);
//		root2.right=new BinarySearchTreeNode(2);
		
		BinaryTreeNode root1=new BinaryTreeNode(1);
		//root1.left=new BinarySearchTreeNode(1);
		
		BinaryTreeNode root2=new BinaryTreeNode(1);
		root1.left=new BinaryTreeNode(1);
		System.out.println(isSubtree(root1, root2));
	}
	
	
	public static boolean isSubtree(BinaryTreeNode s, BinaryTreeNode t) {
		if(s==null)
			return false;
		if(checkIfBothTreeSame(s, t)) 
				return true;
		return isSubtree(s.left, t) || isSubtree(s.right, t);
    }
	
	public static boolean checkIfBothTreeSame(BinaryTreeNode root1,BinaryTreeNode root2) {
		if(root1==null && root2==null) {
			return true;
		}
		if(root1==null || root2==null) {
			return false;
		}
		if(root1.data!=root2.data) {
			return false;
		}
		return checkIfBothTreeSame(root1.left,root2.left) && checkIfBothTreeSame(root1.right,root2.right);
	}
}
