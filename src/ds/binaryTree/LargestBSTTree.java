package ds.binaryTree;

public class LargestBSTTree {

	public static void main(String[] args) {
		LargestBSTTree bst=new LargestBSTTree();
		BinaryTreeNode bt=new BinaryTreeNode(10);
		bt.left=new BinaryTreeNode(5);
		bt.right=new BinaryTreeNode(15);
		bt.left.left=new BinaryTreeNode(1);
		bt.left.right=new BinaryTreeNode(8);
		bt.right.right=new BinaryTreeNode(7);
		System.out.println(bst.largestBSTSubTree(bt));
	}
	public int largestBSTSubTree(BinaryTreeNode root) {
		int[] result=largestBSTSubTreeUtil(root);
		return result[2];
	}
	public int[] largestBSTSubTreeUtil(BinaryTreeNode root) {
		if(root==null)
			return new int[] {Integer.MAX_VALUE,Integer.MIN_VALUE,0};
		int left[]=largestBSTSubTreeUtil(root.left);
		int right[]=largestBSTSubTreeUtil(root.right);
		//if root value is greater than largest value in left subtree and less than 
		//the least value in right subtree
		if(root.data>left[1] && root.data<right[0]) {
			return new int[] {Math.min(root.data, left[0]), Math.max(root.data, right[1]),left[2]+right[2]+1};
		} else {
			return new int[] {Integer.MIN_VALUE, Integer.MAX_VALUE, Math.max(left[2], right[2])};
		}
	}

}
