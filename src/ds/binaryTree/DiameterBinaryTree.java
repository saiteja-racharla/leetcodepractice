package ds.binaryTree;

public class DiameterBinaryTree {
	public static void main(String[] args) {
		BinaryTreeNode root=new BinaryTreeNode(1);
		root.right=new BinaryTreeNode(3);
		root.left=new BinaryTreeNode(2);
		root.left.left=new BinaryTreeNode(4);
		root.left.right=new BinaryTreeNode(5);
		System.out.println("diameter of a tree is "+diameterOfBinaryTree(root));
	}
	public static int diameterOfBinaryTree(BinaryTreeNode root) {
        if(root==null)
            return 0;
        int leftHeight=height(root.left);
        int rightHeight = height(root.right);
        
        int leftDiameter=diameterOfBinaryTree(root.left);
        int rightDiameter=diameterOfBinaryTree(root.right);
        System.out.println("left diameter "+leftDiameter+"right diameter "+rightDiameter);
        return Math.max(leftHeight+rightHeight, Math.max(leftDiameter,rightDiameter));
    }
    public static int height(BinaryTreeNode node){
        if(node==null)
            return 0;
        int leftHeight = height(node.left);
        int rightHeight = height(node.right);
        return Math.max(leftHeight,rightHeight)+1;
    }
}
