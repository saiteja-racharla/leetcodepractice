/*
 * Given a binary tree and a number, return true if the tree has a root-to-leaf path such that 
 * adding up all the values along the path equals the given number. Return false if no such path 
 * can be found.
 */
package ds;
public class RootToLeafPathSum {
	static boolean haspathSum(BinarySearchTreeNode node, int sum){
		 if (node == null)
	            return (sum == 0);
		 else{
			boolean ans = false;
			int subsum = sum - node.data;
            if (subsum == 0 && node.left == null && node.right == null)
                return true;
            if (node.left != null)
                ans = ans || haspathSum(node.left, subsum);
            if (node.right != null)
                ans = ans || haspathSum(node.right, subsum);
            return ans;
		 }
	}
	public static void main(String[] args) {
		int sum = 21;
        
        /* Constructed binary tree is
              10
             /  \
           8     2
          / \   /
         3   5 2
        */
        BinarySearchTreeNode root = new BinarySearchTreeNode(10);
        root = new BinarySearchTreeNode(10);
        root.left = new BinarySearchTreeNode(8);
        root.right = new BinarySearchTreeNode(2);
        root.left.left = new BinarySearchTreeNode(3);
        root.left.right = new BinarySearchTreeNode(5);
        root.right.left = new BinarySearchTreeNode(2);
  
        if (haspathSum(root, sum))
            System.out.println("There is a root to leaf path with sum " + sum);
        else
            System.out.println("There is no root to leaf path with sum " + sum);
	}
}
