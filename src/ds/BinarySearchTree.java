/*THis program does following operations
Insert nodes by recursion and non recursion
Find min and max by recursion and non recursion
FInd height by recursion
pre,post,inorder traversals.
Boundary traversal
*/

package ds;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;


class BinarySearchTreeNodeOperations{
	BinarySearchTreeNode root;
	Queue<BinarySearchTreeNode> q=new LinkedList<BinarySearchTreeNode>();
	public void insertNonRecursion(int data){
		BinarySearchTreeNode temp=new BinarySearchTreeNode(data);
		if(root==null){
			root=temp;
		}
		else{
			BinarySearchTreeNode current=root;
			BinarySearchTreeNode parent=current;
			while(true){
				if(data<current.data){
					current=current.left;
					if(current==null){
						parent.left=temp;
						return;
					}
				}
				else{
					current=current.right;
					if(current==null){
						parent.right=temp;
						return;
					}
				}
				parent=current;
			}
		}
	}
	public void insertRec(int data){
		BinarySearchTreeNode temp=new BinarySearchTreeNode(data);
		BinarySearchTreeNode current=root;
		if(current==null)
			root=temp;
		else{
			root=insertRecursion(current,temp);
		}
	}
	public BinarySearchTreeNode insertRecursion(BinarySearchTreeNode currentNode,BinarySearchTreeNode newNode){
		if(currentNode==null)
			return newNode;
		if(newNode.data<currentNode.data){
			currentNode.left=insertRecursion(currentNode.left,newNode);
		}
		else{
			currentNode.right=insertRecursion(currentNode.right,newNode);
		}
		return currentNode;
	}
	
	public void inOrderNonRecursion(){
		BinarySearchTreeNode temp=root;
		Stack s=new Stack();
		while(temp!=null){
			s.push(temp);
			temp=temp.left;
		}
		while(s.size()>0){
			BinarySearchTreeNode printNode=(BinarySearchTreeNode)s.pop();
			System.out.print(printNode.data+" ");
			if(printNode.right!=null){
				printNode=printNode.right;
				while(printNode!=null){
					s.push(printNode);
					printNode=printNode.left;
				}
			}
		}
	}

	public void preOrderNonRecursion(){
		BinarySearchTreeNode temp=root;
		Stack s=new Stack();
		s.push(temp);
		while(s.size()>0){
			BinarySearchTreeNode poppedElement=(BinarySearchTreeNode)s.pop();
			System.out.print(poppedElement.data+" ");
			if(poppedElement.right!=null)
				s.push(poppedElement.right);
			if(poppedElement.left!=null)
				s.push(poppedElement.left);
		}
	}
	
	public void postOrderNonRecursion(){
		BinarySearchTreeNode node=root;
		Stack<BinarySearchTreeNode> S = new Stack<BinarySearchTreeNode>();
        
        // Check for empty tree
        if (node == null) {
            return;
        }
        S.push(node);
        BinarySearchTreeNode prev = null;
        while (!S.isEmpty()) {
        	BinarySearchTreeNode current = S.peek();
 
            /* go down the tree in search of a leaf an if so process it and pop
            stack otherwise move down */
            if (prev == null || prev.left == current || prev.right == current) {
                if (current.left != null) {
                    S.push(current.left);
                } else if (current.right != null) {
                    S.push(current.right);
                } else {
                    S.pop();
                    System.out.print(current.data+" ");
                }
 
                /* go up the tree from left node, if the child is right 
                push it onto stack otherwise process parent and pop stack */
            } else if (current.left == prev) {
                if (current.right != null) {
                    S.push(current.right);
                } else {
                    S.pop();
                    System.out.print(current.data+" ");
                }
                 
                /* go up the tree from right node and after coming back
                 from right node process parent and pop stack */
            } else if (current.right == prev) {
                S.pop();
                System.out.print(current.data+" ");
            }
 
            prev = current;
        }
	}
	
	public void pre(){
		preOrder(root);
	}
	public void preOrder(BinarySearchTreeNode node){
		if(node==null)
			return;
		System.out.print(node.data+" ");
		preOrder(node.left);
		preOrder(node.right);
	}
	public void in(){
		inOrder(root);
	}
	public void inOrder(BinarySearchTreeNode node){
		if(node==null)
			return;
		inOrder(node.left);
		System.out.print(node.data+" ");
		inOrder(node.right);
	}
	public void post(){
		postOrder(root);
	}
	public void postOrder(BinarySearchTreeNode node){
		if(node==null)
			return;
		postOrder(node.left);
		postOrder(node.right);
		System.out.print(node.data+" ");
	}
	public void height(){
		int height=findHeight(root);
		System.out.println("Height is"+height);
	}
	public int findHeight(BinarySearchTreeNode node){
		if(node==null)
			return -1;
		int leftHeight=findHeight(node.left);
		int rightHeight=findHeight(node.right);
		return max(leftHeight,rightHeight)+1;
	}
	public int findHeightNonRecursion(){
		BinarySearchTreeNode node=root;
		int height=0;
		 if (node == null) {
			 return 0;
	        }
		 Queue<BinarySearchTreeNode> q = new LinkedList();
		 
        // Enqueue Root and initialize height
        q.add(node);
        while (1 == 1) {
            // nodeCount (queue size) indicates number of nodes
        	// at current lelvel.
	        int nodeCount = q.size();
	        if (nodeCount == 0) {
	        	return height;
	        }
	    	height++;
	        // Dequeue all nodes of current level and Enqueue all
	    	// nodes of next level
			while (nodeCount > 0) {
				BinarySearchTreeNode newnode = q.peek();
			    q.remove();
			    if (newnode.left != null) {
			        q.add(newnode.left);
			    }
			    if (newnode.right != null) {
			        q.add(newnode.right);
			    }
			    nodeCount--;
			}
        }
	}
	public int max(int left,int right){
		return (left>right)?left:right;
	}
	public void findMinNonRec(){
		minimumElementNonRec(root);
	}
	public void minimumElementNonRec(BinarySearchTreeNode node){
		while(node.left!=null){
			node=node.left;
		}
		System.out.println("Minimum "+node.data);
	}
	public void findMaxNonRec(){
		maximumElementNonRec(root);
	}
	public void maximumElementNonRec(BinarySearchTreeNode node){
		while(node.right!=null){
			node=node.right;
		}
		System.out.println("Maximum "+node.data);
	}
	public void findMinRecurive(){
		int min=minimumRecursive(root);
		System.out.println("Minimum in recursive manner"+min);
	}
	public int minimumRecursive(BinarySearchTreeNode node){
		if(node.left==null)
			return node.data;
		int minimum=minimumRecursive(node.left);
		return minimum;
		
	}
	public void findMaxRecurive(){
		int max=maximumRecursive(root);
		System.out.println("Maximum in recursive manner"+max);
	}
	public int maximumRecursive(BinarySearchTreeNode node){
		if(node.right==null)
			return node.data;
		int maximum=maximumRecursive(node.right);
		return maximum;
	}
	public void levelOrder(){
		BinarySearchTreeNode node=root;
		if(node==null)
			return;
		q.add(node);
		while(!q.isEmpty()){
			BinarySearchTreeNode bst=(BinarySearchTreeNode)q.peek();
			System.out.print(bst.data+" ");
			if(bst.left!=null)
				q.add(bst.left);
			if(bst.right!=null)
				q.add(bst.right);
			q.remove();
		}
	}
	public void delete(int data){
		boolean isLeftChild=false;
		BinarySearchTreeNode current=root;
		BinarySearchTreeNode parent=current;
		if(data==0)
			return;
		if(current.data==data)
			return ;
		while(current.data!=data){
			parent=current;
			if(data<current.data){
				current=current.left;
				isLeftChild=true;
			}
			else{
				current=current.right;
				isLeftChild=false;
			}	
		}
		if(current.left==null && current.right==null){
			if(isLeftChild){
				parent.left=null;
			}
			else{
				parent.right=null;
			}
		}
		else if(current.left==null){
			if(isLeftChild){
				parent.left=current.right;
			}
			else{
				parent.right=current.right;
			}
		}
		else if(current.right==null){
			if(isLeftChild){
				parent.left=current.left;
			}
			else{
				parent.right=current.left;
			}
		}
		else{
			BinarySearchTreeNode temp=findMinNode(current.right);
			int deletedNodeData=temp.data;
			delete(temp.data);
			current.data=deletedNodeData;
		}
	}
	public BinarySearchTreeNode findMinNode(BinarySearchTreeNode current){
		while(current.left!=null)
			current=current.left;
		return current;
	}
	public int findSuccessor(int data){
		return successor(root,data).data;
	}
	public BinarySearchTreeNode successor(BinarySearchTreeNode node,int data){
		BinarySearchTreeNode current=node;
		BinarySearchTreeNode successor=null;
		while(current.data!=data){
			if(data<current.data)
				current=current.left;
			else
				current=current.right;
		}
		if(current.right!=null){
			successor=findMinNode(current);
		}
		else{
			BinarySearchTreeNode ancestor=root;
			while(current!=ancestor){
				if(current.data<ancestor.data){
					successor=ancestor;
					ancestor=ancestor.left;
				}
				else{
					ancestor=ancestor.right;
				}
			}
		}
		return successor;
	}
	public int findPredecessor(int data){
		return predecessor(root,data).data;
	}
	public BinarySearchTreeNode predecessor(BinarySearchTreeNode node,int data){
		BinarySearchTreeNode current=node;
		BinarySearchTreeNode predecessor=null;
		while(current.data!=data){
			if(data<current.data)
				current=current.left;
			else
				current=current.right;
		}
		if(current.left!=null){
			current=current.left;
			while(current.right!=null)
				current=current.right;
			predecessor=current;
		}
		else{
			BinarySearchTreeNode ancestor=root;
			while(current!=ancestor){
				if(current.data>ancestor.data){
					predecessor=ancestor;
					ancestor=ancestor.right;
				}
				else{
					ancestor=ancestor.left;
				}
			}
		}
		return predecessor;
	}
	public void findEachLevel(){
		Queue<BinarySearchTreeNode> q1=new LinkedList<BinarySearchTreeNode>();
		q1.add(root);
		while(q1.size()>0){
			System.out.println(q1.size());
			Queue<BinarySearchTreeNode> temp=new LinkedList<BinarySearchTreeNode>();
			while(q1.isEmpty()!=true){
				BinarySearchTreeNode tempNode=q1.remove();
				if(tempNode.left!=null)
					temp.add(tempNode.left);
				if(tempNode.right!=null)
					temp.add(tempNode.right);
			}
			while(temp.size()>0){
				q1.add(temp.remove());
			}
		}
	}
	public void findElement(int data){
		findElementUtil(root,data);
	}
	public void findElementUtil(BinarySearchTreeNode node,int val){
		if(node.data==val){
			System.out.println("0");
		}
		int found=0;
		while(node!=null){
			if(val>node.data){
				node=node.right;
			}
			else if(val<node.data){
				node=node.left;
			}
			else{
				found=1;
				break;
			}
		}
		if(found==1){
			System.out.println("Found Element");
		}
		else
			System.out.println("Element not found");
	}
	/*Start of boundary traversal*/
	public void boundary(){
		BinarySearchTreeNode current=root;
		if(current!=null){
			System.out.print(root.data+"\t");
			printBoundaryLeft(root.left);
			printLeaves(root.left);
			printLeaves(root.right);
			printBoundaryRight(root.right);
		}
	}
	public void printBoundaryLeft(BinarySearchTreeNode root){
		if(root!=null){
			if(root.left!=null){
				System.out.print(root.data+"\t");
				printBoundaryLeft(root.left);
			}
			else if(root.right!=null){
				System.out.print(root.data+"\t");
				printBoundaryLeft(root.right);
			}
		}
	}
	public void printLeaves(BinarySearchTreeNode root){
		if(root!=null){
			printLeaves(root.left);
			if(root.left==null && root.right==null){
				System.out.print(root.data+"\t");
			}
			printLeaves(root.right);
		}
	}
	public void printBoundaryRight(BinarySearchTreeNode root){
		if(root!=null){
			if(root.right!=null){
				printBoundaryRight(root.right);
				System.out.print(root.data+"\t");
			}
			else if(root.left!=null){
				printBoundaryRight(root.left);
				System.out.print(root.data+"\t");
			}
			
		}
	}
	/*End of boundary traversal*/
}

public class BinarySearchTree {
	public static void main(String[] args) {
		BinarySearchTreeNodeOperations bs=new BinarySearchTreeNodeOperations();
		/* To Check nodes at each level
		bs.insertRec(10);
		bs.insertRec(5);
		bs.insertRec(15);
		bs.insertRec(3);
		bs.insertRec(6);
		bs.insertRec(14);
		bs.insertRec(16);
		bs.insertRec(1);
		bs.insertRec(4);*/
		bs.insertRec(25);
		bs.insertRec(20);
		bs.insertRec(30);
		bs.insertRec(15);
		bs.insertRec(22);
		bs.insertRec(27);
		bs.insertRec(35);
		bs.insertRec(13);
		bs.insertRec(17);
		bs.insertRec(21);
		bs.insertRec(23);
		bs.insertRec(26);
		bs.insertRec(28);
		bs.insertRec(33);
		bs.insertRec(37);
		bs.insertNonRecursion(40);
		bs.insertNonRecursion(38);
		bs.insertNonRecursion(41);
		bs.insertRec(12);
		System.out.println("Inorder");
		bs.in();
		System.out.println("Inorder non recursion");
		bs.inOrderNonRecursion();
		System.out.println("preorder");
		bs.pre();
		System.out.println("Pre Order Non recursion");
		bs.preOrderNonRecursion();
		System.out.println("postorder");
		bs.post();
		System.out.println("Post Order non recursion");
		bs.postOrderNonRecursion();
		bs.height();
		System.out.println("Height by non recursion"+bs.findHeightNonRecursion());
		bs.findMinNonRec();
		bs.findMaxNonRec();
		bs.findMinRecurive();
		bs.findMaxRecurive();
		bs.levelOrder();
		bs.delete(35);
		System.out.println("List after deletion");
		bs.in();
		System.out.println("Successor of 23 is"+bs.findSuccessor(23));
		System.out.println("Predecessor of 33 is"+bs.findPredecessor(33));
		bs.findEachLevel();
		System.out.println("Find element");
		bs.findElement(100);
		System.out.println("Boundary Traversal");
		bs.boundary();
	}
}
