package ds;

public class BinarySearchTreeNode {
	public int data;
	BinarySearchTreeNode left;
	BinarySearchTreeNode right;
	public BinarySearchTreeNode(int data) {
		this.data=data;
		this.left=null;
		this.right=null;
	}
}
