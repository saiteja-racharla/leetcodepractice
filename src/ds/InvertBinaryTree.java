/*This program inverses a binary tree as follow
 * 
 * Time complexity is not found in the solution from where this code was taken.
 *   4
   /   \
  2     7
 / \   / \
1   3 6   9

convert it to

     4
   /   \
  7     2
 / \   / \
9   6 3   1
 */
package ds;

import java.util.Stack;

public class InvertBinaryTree {
	static BinarySearchTreeNode invertTree(BinarySearchTreeNode root){
		if(root==null)
			return null;
		Stack<BinarySearchTreeNode> st=new Stack<BinarySearchTreeNode>();
		st.push(root);
		while(!st.isEmpty()){
			BinarySearchTreeNode node=st.pop();
			BinarySearchTreeNode left = node.left;
			node.left=node.right;
			node.right=left;
			if(node.left!=null){
				st.push(node.left);
			}
			if(node.right!=null){
				st.push(node.right);
			}
		}
		return root;
	}
	public static void main(String[] args) {
		BinarySearchTreeNode root=new BinarySearchTreeNode(4);
		root.left=new BinarySearchTreeNode(2);
		root.left.left=new BinarySearchTreeNode(1);
		root.left.right=new BinarySearchTreeNode(3);
		root.right=new BinarySearchTreeNode(7);
		root.right.left=new BinarySearchTreeNode(6);
		root.right.right=new BinarySearchTreeNode(9);
		BinarySearchTreeNode newRoot = invertTree(root);
		printTree(newRoot);
	}
	static void printTree(BinarySearchTreeNode node){
		if(node==null)
			return;
		System.out.print(node.data +" ");
		printTree(node.left);
		printTree(node.right);
	}
}
