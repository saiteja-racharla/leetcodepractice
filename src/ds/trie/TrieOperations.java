package ds.trie;

class TrieOperationsImplementation {
	TrieNode root=null;
	public TrieOperationsImplementation(){
		root=new TrieNode();
	}
	public void insert(String key){
		int level;
		int length=key.length();
		int index;
		TrieNode pCrawl=root;
		for(level=0;level<length;level++){
			index=key.charAt(level)-'a';
			if(pCrawl.children[index]==null)
				pCrawl.children[index]=new TrieNode();
			pCrawl = pCrawl.children[index];
		}
		pCrawl.isEndOfWord = true;
	}
	public boolean search(String key){
		int level;
		int length = key.length();
        int index;
        TrieNode pCrawl = root;
        for (level = 0; level < length; level++)
        {
            index = key.charAt(level) - 'a';
      
            if (pCrawl.children[index] == null)
                return false;
      
            pCrawl = pCrawl.children[index];
        }
      
        return (pCrawl != null && pCrawl.isEndOfWord);
	}
	public void delete(String key){
		TrieNode pCrawl = root;
		if(pCrawl==null || key==null){
			System.out.println("Null trie or null key");
			return;
		}
		deleteHelper(key,root,key.length(),0);
		return;
	}
	public boolean deleteHelper(String key, TrieNode currentNode,int length,int level){
		TrieNode pCrawl = currentNode;
		boolean deletedSelf = false;
		if(pCrawl==null){
			System.out.println("Key does not exists");
			return deletedSelf;
		}
		if(level==length){
			if(hasNoChildren(pCrawl)){
				pCrawl=null;
				deletedSelf=true;
			}
			else{
				pCrawl.isEndOfWord=false;
				deletedSelf=false;
			}
		}
		else{
			TrieNode childNode = pCrawl.children[getIndex(key.charAt(level))];
			boolean childDeleted = deleteHelper(key, childNode, length, level + 1);
			if (childDeleted){
				currentNode.children[getIndex(key.charAt(level))] = null;
				if (currentNode.isEndOfWord)
				{
					deletedSelf = false;
				}
				else if (!hasNoChildren(currentNode))
				{
					deletedSelf = false;
				}
				else 
				{
					currentNode = null;
					deletedSelf = true;
				}
			}
			else{
				deletedSelf = false;
			}
		}
		return deletedSelf;
	}
	private int getIndex(char ch)
	{
		return ch - 'a';
	}
	private boolean hasNoChildren(TrieNode currentNode)
	{
		for (int i = 0; i < currentNode.children.length; i++)
		{
			if ((currentNode.children[i]) != null)
				return false;
		}
		return true;
	}
	public boolean searchRegularExpression(String word) {
        return searchRecur(word,root,0);
    }
    
    public boolean searchRecur(String word, TrieNode parent,int level){
        if(level==word.length()){
            if(parent.isEndOfWord==true)
                return true;
            return false;
        }
        TrieNode children[]=parent.children;
        if(word.charAt(level)=='.'){
            for(int i=0;i<26;i++){
            	TrieNode n = children[i];
            	if(n!=null){
            		boolean b = searchRecur(word,n,level+1);
	                if(b)
	                    return true;
            	}
            }
            return false;
        }
        TrieNode pCrawl=parent.children[word.charAt(level)-'a'];
        if(pCrawl==null)
            return false;
        return searchRecur(word,pCrawl,level+1);
    }
}

public class TrieOperations{
	public static void main(String[] args){
		TrieOperationsImplementation ts=new TrieOperationsImplementation();
		 String keys[] = {"the", "a", "there", "answer", "any",
                 "by", "bye", "their"};
		 String output[] = {"Not present in trie", "Present in trie"};
		 int i;
		 for (i = 0; i < keys.length ; i++)
            ts.insert(keys[i]);
		// Search for different keys
        if(ts.search("the") == true)
            System.out.println("the --- " + output[1]);
        else System.out.println("the --- " + output[0]);
         
        if(ts.search("these") == true)
            System.out.println("these --- " + output[1]);
        else System.out.println("these --- " + output[0]);
         
        if(ts.search("their") == true)
            System.out.println("their --- " + output[1]);
        else System.out.println("their --- " + output[0]);
        System.out.println("Searching by regular expressions");
        if(ts.searchRegularExpression(".heir") == true)
            System.out.println("their --- " + output[1]);
        else System.out.println("their --- " + output[0]);
         
        if(ts.search("thaw") == true)
            System.out.println("thaw --- " + output[1]);
        else System.out.println("thaw --- " + output[0]);
        ts.delete("the");
        if(ts.search("their") == true)
            System.out.println("their --- " + output[1]);
        else System.out.println("their --- " + output[0]);
        if(ts.search("the") == true)
            System.out.println("the --- " + output[1]);
        else System.out.println("the --- " + output[0]);
	}
}
