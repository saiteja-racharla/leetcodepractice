package ds.trie;

import java.util.ArrayList;
import java.util.List;

public class WordSearchII {
	public static void main(String[] args) {
		String[] words = {"oath","pea","eat","rain"};
		char board[][]={
				{'o','a','a','n'},
				{'e','t','a','e'},
				{'i','h','k','r'},
				{'i','f','l','v'}
		};
		List<String> result=findWords(board, words);
		for(String res:result){
			System.out.print(res+"\t");
		}
	}
	
	public static List<String> findWords(char[][] board, String[] words) {
		List<String> result=new ArrayList<String>();
		TrieNodeWordSearchII root=insertToTrie(words);
		for(int i=0;i<board.length;i++){
			for(int j=0;j<board[0].length;j++){
				dfs(board,i,j,result,root);
			}
		}
		return result;
	}
	
	public static void dfs(char[][] board,int i, int j,List<String> result,TrieNodeWordSearchII root){
		char c = board[i][j];
		if(c=='#' || root.children[c-'a']==null) return;
		root=root.children[c-'a'];
		if(root.word!=null){
			result.add(root.word);
			root.word=null;
		}
		board[i][j] = '#';
		if(i>0) 
			dfs(board,i-1,j,result,root);
		if(j>0) 
			dfs(board,i,j-1,result,root);
		if(i<board.length-1) 
			dfs(board,i+1,j,result,root);
		if(j<board[0].length-1) 
			dfs(board,i,j+1,result,root);
		board[i][j]=c;
	}
	public static TrieNodeWordSearchII insertToTrie(String[] words){
		TrieNodeWordSearchII root=new TrieNodeWordSearchII();
		for(String word:words){
			int level;
			int length=word.length();
			int index;
			TrieNodeWordSearchII pCrawl=root;
			for(level=0;level<length;level++){
				index=word.charAt(level)-'a';
				if(pCrawl.children[index]==null)
					pCrawl.children[index]=new TrieNodeWordSearchII();
				pCrawl = pCrawl.children[index];
			}
			pCrawl.word = word;
		}
		return root;
	}
}

class TrieNodeWordSearchII {
	final int ALPHABET_SIZE=26;
	TrieNodeWordSearchII children[]=new TrieNodeWordSearchII[ALPHABET_SIZE];
	String word;
	TrieNodeWordSearchII(){
		for(int i=0;i<ALPHABET_SIZE;i++){
			children[i]=null;
		}
	}
}
