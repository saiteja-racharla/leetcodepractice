package ds.trie;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class RemoveSubFoldersFromFileSystem {

	public static void main(String[] args) {
		RemoveSubFoldersFromFileSystem p=new RemoveSubFoldersFromFileSystem();
		String[] folder= {"/a","/a/b","/c/d","/c/d/e","/c/f"};
		System.out.println(p.removeSubfolders(folder));
	}
	public List<String> removeSubfolders(String[] folder) {
		RemoveSubFoldersTrie root = new RemoveSubFoldersTrie();
        for(int i=0;i<folder.length;i++){
        	RemoveSubFoldersTrie pCrawl = root;
            for(char c:folder[i].toCharArray()){
                int index=(c=='/')?26:c-'a';
                if(pCrawl.sub[index]==null)
                    pCrawl.sub[index]=new RemoveSubFoldersTrie();
                pCrawl=pCrawl.sub[index];
            }
            pCrawl.index=i;
        }
        return bfs(root, folder);
    }
    private List<String> bfs(RemoveSubFoldersTrie t, String[] folder) {
        List<String> ans = new ArrayList<>();
        Queue<RemoveSubFoldersTrie> q = new LinkedList<>();
        q.offer(t);
        while (!q.isEmpty()) { // BFS search.
            t = q.poll();
            if (t.index >= 0) { // found a parent folder, but there might be more.
                ans.add(folder[t.index]);
            }
            for (int i = 0; i < 27; ++i)
                if (t.sub[i] != null && !(i == 26 && t.index >= 0)) // not yet found all parent folders in current trie branch.
                    q.offer(t.sub[i]);
        }
        return ans;
    }

}
class RemoveSubFoldersTrie {
	RemoveSubFoldersTrie[] sub = new RemoveSubFoldersTrie[27];
    int index = -1;
}