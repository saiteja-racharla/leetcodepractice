/**
 * https://leetcode.com/problems/campus-bikes/description/
 * initiate a priority queue of bike and worker pairs. The heap order should be Distance ASC, WorkerIndex ASC, Bike ASC
Loop through all workers and bikes, calculate their distance, and then throw it to the queue.
Initiate a set to keep track of the bikes that have been assigned.
initiate a result array and fill it with -1. (unassigned)
poll every possible pair from the priority queue and check if the person already got his bike or the bike has been assigned.
early exist on every people got their bike.

 * Solution:
 * https://leetcode.com/problems/campus-bikes/discuss/305603/Java-Fully-Explained
 */
package ds.priorityqueue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Set;

public class CampusBikes {

	public static void main(String[] args) {
		CampusBikes c=new CampusBikes();
		int[][] workers= {
				{0,0},
				{1,1},
				{2,0}
		};
		int[][] bikes= {
				{1,0},
				{2,2},
				{2,1}
		};
		c.assignBikes1(workers, bikes);
	}
	public int[] assignBikes(int[][] workers, int[][] bikes) {
		int n=workers.length;
		//Order by distance asc,workersindex asc,bikeIndex asc
		PriorityQueue<int[]> queue=new PriorityQueue<int[]>((a,b)-> {
			int comp=Integer.compare(a[0], b[0]);
			if(comp==0) {
				if(a[1]==b[1]) {
					return Integer.compare(a[2], b[2]);
				}
				return Integer.compare(a[1], b[1]);
			}
			return comp;
		});
		//loop through every possible pairs of bikes and people
		//calculate their distance and push in to queue
		for(int i=0;i<workers.length;i++) {
			int[] worker=workers[i];
			for(int j=0;j<bikes.length;j++) {
				int[] bike=bikes[j];
				int distance=Math.abs(bike[0]-worker[0])+Math.abs(bike[1]-worker[1]);
				queue.add(new int[] {distance, i,j});
			}
		}
		
		int[] res=new int[n];
		Arrays.fill(res, -1);
		Set<Integer> bikeAssigned=new HashSet<>();
		while(bikeAssigned.size()<n) {
			int[] workerAndBikePair=queue.poll();
			if(res[workerAndBikePair[1]]==-1 && bikeAssigned.contains(workerAndBikePair[2])) {
				res[workerAndBikePair[1]]=workerAndBikePair[2];
				bikeAssigned.add(workerAndBikePair[2]);
			}
		}
		
		return res;
    }
	//the same above code can also be solved without priority queue. Given constraints are 0 <= workers[i][j], bikes[i][j] < 1000
	// and 1 <= workers.length <= bikes.length <= 1000
	//So the maximum manhattan distance with be (1000-0)+(1000-0) worker at [0,0] and bike at [1000,1000]
	public int[] assignBikes1(int[][] workers, int[][] bikes) {
		int n=workers.length;
		//Order by distance asc,workersindex asc,bikeIndex asc
		List<int[]>[] distances=new List[2001];
		//loop through every possible pairs of bikes and people
		//calculate their distance and push in to queue
		for(int i=0;i<workers.length;i++) {
			int[] worker=workers[i];
			for(int j=0;j<bikes.length;j++) {
				int[] bike=bikes[j];
				int dist=Math.abs(bike[0]-worker[0])+Math.abs(bike[1]-worker[1]);
				if(distances[dist]==null) {
					distances[dist]=new ArrayList<>();
				}
				distances[dist].add(new int[] {i,j});
			}
		}
		
		int[] res=new int[n];
		Arrays.fill(res, -1);
		Set<Integer> bikeAssigned=new HashSet<>();
		for(int i=0;i<distances.length && bikeAssigned.size()<workers.length;i++) {
			if(distances[i]==null) {
				continue;
			}
			for(int[] workerAndBikePair:distances[i]) {
				if(res[workerAndBikePair[0]]==-1 && !bikeAssigned.contains(workerAndBikePair[1])) {
					res[workerAndBikePair[0]]=workerAndBikePair[1];
					bikeAssigned.add(workerAndBikePair[1]);
				}
			}
			
		}
		return res;
    }
}
