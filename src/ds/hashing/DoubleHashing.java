package ds.hashing;

import java.util.Scanner;

public class DoubleHashing {
	static int choice;
	static int size=11;
	static int[] arr=new int[size];
	public static void main(String[] args) {
		int data;
		for(int i=0;i<size;i++){
			arr[i]=-1;
		}
		Scanner sc=new Scanner(System.in);
		insert(10);
		insert(20);
		insert(30);
		insert(40);
		insert(50);
		insert(60);
		insert(70);
		insert(80);
		insert(90);
		insert(100);
		/*insert(10);
		insert(31);
		insert(18);
		insert(12);
		insert(30);
		insert(1);
		insert(19);
		insert(36);
		insert(41);
		insert(15);
		insert(25);*/
		
		do{
			System.out.println("Enter choice");
			choice=sc.nextInt();
			switch(choice){
				case 1: System.out.println("Enter number");
						data=sc.nextInt();
						insert(data);
						break;
				case 2: System.out.println("Enter number to delete");
						data=sc.nextInt();
						delete(data);
						break;
				case 3: display();break;
				case 4:System.exit(0);
				default:
			}
			
		}while(choice!=4);
		if(sc!=null)
			sc.close();
	}
	public static void insert(int data){
		int hashIndex=hash(data);
		int stepIndex=hashStep(hashIndex);
		while(arr[hashIndex]>0){
			hashIndex+=stepIndex;
			hashIndex%=size;
		}
		arr[hashIndex]=data;
	}
	public static void display(){
		for(int i=0;i<size;i++){
			System.out.print(arr[i]+" ");
		}
	}
	public static int hash(int key){
		return key%size;
	}
	public static int hashStep(int key){
		return 5-key%5;
	}
	public static void delete(int data){
		int hashIndex=hash(data);
		int stepIndex=hashStep(hashIndex);
		while(arr[hashIndex]!=data){
			hashIndex+=stepIndex;
			hashIndex%=size;
		}
		arr[hashIndex]=-1;
	}
}
