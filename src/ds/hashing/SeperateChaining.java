package ds.hashing;

class Node{
	int data;
	Node next;
}

class SeperateChainingOperations{
	Node[] arr;
	int size;
	SeperateChainingOperations(int s){
		size=s;
		arr=new Node[s];
	}
	public void insert(int data){
		int hashIndex=hash(data);
		if(arr[hashIndex].data==-1){
			arr[hashIndex].data=data;
			arr[hashIndex].next=null;
		}
		else{
			Node temp=arr[hashIndex];
			while(temp.next!=null){
				temp=temp.next;
			}
			Node newNode=new Node();
			newNode.data=data;
			newNode.next=null;
			temp.next=newNode;
		}
	}
	public static int hash(int key){
		return key%10;
	}
	public void initialize(){
		for(int i=0;i<size;i++){
			arr[i]=new Node();
			arr[i].data=-1;
			arr[i].next=null;
		}
	}
	public void display(){
		for(int i=0;i<size;i++){
			if(arr[i].next!=null){
				while(arr[i]!=null){
					System.out.print(arr[i].data+"-->");
					arr[i]=arr[i].next;
				}
			}
			else
				System.out.println(arr[i].data);
		}
	}
}

public class SeperateChaining {
	public static void main(String[] args) {
		SeperateChainingOperations s=new SeperateChainingOperations(10);
		s.initialize();
		s.insert(12);
		s.insert(22);
		s.insert(32);
		s.insert(43);
		s.insert(54);
		s.insert(62);
		s.insert(71);
		s.insert(72);
		s.display();
	}

}
