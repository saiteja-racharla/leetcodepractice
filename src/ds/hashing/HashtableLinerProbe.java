package ds.hashing;

import java.util.Scanner;

public class HashtableLinerProbe{
	static int choice;
	static int size=10;
	static int[] arr=new int[size];
	public static void main(String[] args) {
		int data;
		for(int i=0;i<size;i++){
			arr[i]=-1;
		}
		Scanner sc=new Scanner(System.in);
		do{
			System.out.println("Enter choice");
			choice=sc.nextInt();
			switch(choice){
				case 1: System.out.println("Enter number");
						data=sc.nextInt();
						insert(data);
						break;
				case 2: System.out.println("Enter number to delete");
						data=sc.nextInt();
						delete(data);
						break;
				case 3: display();break;
				case 4:System.exit(0);
				default:
			}
			
		}while(choice!=4);
		if(sc!=null)
			sc.close();
	}
	public static void insert(int data){
		int hashIndex=hash(data);
		while(arr[hashIndex]>0){
			hashIndex+=1;
			hashIndex%=size;
		}
		arr[hashIndex]=data;
	}
	public static void display(){
		for(int i=0;i<size;i++){
			System.out.print(arr[i]+" ");
		}
	}
	public static int hash(int key){
		return key%size;
	}
	public static void delete(int data){
		int hashIndex=hash(data);
		while(arr[hashIndex]!=data){
			hashIndex+=1;
			hashIndex%=size;
		}
		arr[hashIndex]=-1;
	}
}