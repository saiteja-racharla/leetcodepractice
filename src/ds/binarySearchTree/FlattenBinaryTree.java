package ds.binarySearchTree;

public class FlattenBinaryTree {
	BinarySearchTreeNode prev=null;
	public static void main(String[] args) {
		
	}
	public void flattenRecursive(BinarySearchTreeNode root) {
        if(root==null)
        	return;
        flattenRecursive(root.right);
        flattenRecursive(root.left);
        root.right=prev;
        root.left=null;
        prev=root;
    }
	public void flattenNonRecursive(BinarySearchTreeNode root){
		BinarySearchTreeNode rightNode=null;
		BinarySearchTreeNode now=root;
		while(now!=null){
			rightNode=now;
			if(rightNode.left!=null){
				rightNode=now.left;
				while(rightNode.right!=null)
					rightNode=rightNode.right;
				rightNode.right=now.right;
				now.right=now.left;
				now.left=null;
			}
			now=now.right;
		}
	}
}
