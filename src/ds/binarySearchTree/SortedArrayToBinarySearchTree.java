package ds.binarySearchTree;

public class SortedArrayToBinarySearchTree {

	public static void main(String[] args) {
		SortedArrayToBinarySearchTree s=new SortedArrayToBinarySearchTree();
		int nums[]={1,2,3,4,5,6,7};
		BinarySearchTreeNodeOperations bst=new BinarySearchTreeNodeOperations();
		bst.preOrder(s.sortedArrayToBST(nums));
	}
	public BinarySearchTreeNode sortedArrayToBST(int[] nums) {
		if(nums.length==0)
			return null;
        return sortedArrayToBSTUtil(nums,0,nums.length-1);
    }
    public BinarySearchTreeNode sortedArrayToBSTUtil(int[] nums,int start,int end){
    	if(start==end)
    		return new BinarySearchTreeNode(nums[start]);
    	if(start>end)
            return null;
    	int mid=(start+end)/2;
    	BinarySearchTreeNode root=new BinarySearchTreeNode(nums[mid]);
    	root.left=sortedArrayToBSTUtil(nums,start,mid-1);
    	root.right=sortedArrayToBSTUtil(nums, mid+1, end);
    	return root;
    }
}
