/*
 * Inorder and postorder traversal of tree are given.
 * Construct the BST and return root
 */
package ds.binarySearchTree;

public class ConstructingBinarySearchTree2 {
	public static void main(String[] args) {
//		int[] inOrder={15,20,23,25,28,30,32};
//		int[] postOrder={15,23,20,28,32,30,25};
		int[] inOrder={12};
		int[] postOrder={12};
		ConstructingBinarySearchTree2 cs=new ConstructingBinarySearchTree2();
		BinarySearchTreeNode root=cs.createBST(inOrder, postOrder);
		cs.printInOrder(root);
	}
	public BinarySearchTreeNode createBST(int[] inOrder,int[] postOrder){
		return createBSTUtil(inOrder,postOrder,0,inOrder.length-1,0,postOrder.length-1);
	}
	public BinarySearchTreeNode createBSTUtil(int[] inOrder, int[] postOrder, int inStart, int inEnd, int postStart, int postEnd){
		if(inStart>inEnd || postStart>postEnd)
			return null;
		int data=postOrder[postEnd];
		int offset=inEnd;
		BinarySearchTreeNode curr=new BinarySearchTreeNode(data);
		for(;offset>inStart;offset--){
			if(inOrder[offset]==data)
				break;
		}
		curr.left=createBSTUtil(inOrder,postOrder,inStart,offset-1,postStart,postStart+offset-1-inStart);
		curr.right=createBSTUtil(inOrder,postOrder,offset+1,inEnd,postStart+offset-inStart,postEnd-1);
		return curr;
	}
	public void printInOrder(BinarySearchTreeNode root){
		if(root==null){
			return;
		}
		printInOrder(root.left);
		System.out.print(root.data+"\t");
		printInOrder(root.right);
	}
	
}
