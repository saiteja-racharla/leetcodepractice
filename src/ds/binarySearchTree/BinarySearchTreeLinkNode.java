package ds.binarySearchTree;

public class BinarySearchTreeLinkNode {
	public int data;
	BinarySearchTreeLinkNode left;
	BinarySearchTreeLinkNode right;
	BinarySearchTreeLinkNode next;
	public BinarySearchTreeLinkNode(int data) {
		this.data=data;
		this.left=null;
		this.right=null;
		this.next=null;
	}
}
