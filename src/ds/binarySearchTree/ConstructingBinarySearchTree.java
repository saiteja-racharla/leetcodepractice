/*
 * Inorder and preorder traversal of tree is given. 
 * Construct the BST and return root
 */
package ds.binarySearchTree;

public class ConstructingBinarySearchTree {

	public static void main(String[] args) {
		ConstructingBinarySearchTree c=new ConstructingBinarySearchTree();
//		int[] inOrder={10,20,23,25,28,30,32,35};
//		int[] preOrder={25,20,10,23,30,28,32,35};
		int[] inOrder={10,20,23,25,30,35,40,41,43,44,45,47,48,49,50};
		int[] preOrder={30,20,10,25,23,40,35,45,43,41,44,48,47,49,50};
		c.printInOrder(c.createBST(inOrder, preOrder));
	}
	public BinarySearchTreeNode createBST(int[] inOrder, int[] preOrder){
		if(preOrder.length==0 || inOrder.length!=preOrder.length){
			return null;
		}
		return createBSTUtil(inOrder, preOrder, 0, inOrder.length-1, 0, preOrder.length-1);
	}
	public BinarySearchTreeNode createBSTUtil(int[] inOrder, int[] preOrder, int inStart, int inEnd, int preStart, int preEnd){
		if(preStart>preEnd || inStart>inEnd){
			return null;
		}
		int data=preOrder[preStart];
		BinarySearchTreeNode cur=new BinarySearchTreeNode(data);
		int offset = inStart;
		for(;offset<inEnd;offset++){
			if(inOrder[offset]==data)
				break;
		}
		cur.left=createBSTUtil(inOrder, preOrder, inStart, offset-1, preStart+1, preStart+offset-inStart);
		cur.right=createBSTUtil(inOrder, preOrder, offset+1, inEnd, preStart+offset-inStart+1, preEnd);
		return cur;
	}
	public void printInOrder(BinarySearchTreeNode root){
		if(root==null){
			return;
		}
		printInOrder(root.left);
		System.out.print(root.data+"\t");
		printInOrder(root.right);
	}
}
