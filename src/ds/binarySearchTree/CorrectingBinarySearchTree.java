package ds.binarySearchTree;

public class CorrectingBinarySearchTree {
	
	
	public static void main(String[] args) {
		BinarySearchTreeNode root=new BinarySearchTreeNode(6);
		root.left=new BinarySearchTreeNode(10);
		root.right=new BinarySearchTreeNode(2);
		root.left.left=new BinarySearchTreeNode(1);
		root.left.right=new BinarySearchTreeNode(3);
		root.right.right=new BinarySearchTreeNode(12);
		root.right.left=new BinarySearchTreeNode(7);
		System.out.println("Inorder Traversal of the original tree \n");
		util ut=new util();
		ut.printInorder(root);
	 
	    BinarySearchTreeNode tempNode = ut.correctBST(root);
	 
	    System.out.println("\nInorder Traversal of the fixed tree \n");
	    ut.printInorder(tempNode);
	}

}

class util{
	BinarySearchTreeNode first,last,middle,prev;
	util(){
		BinarySearchTreeNode first,last,middle,prev;
		first=null;
		last=null;
		middle=null;
		prev=null;
	}
	public void printInorder(BinarySearchTreeNode root){
		if(root==null)
			return;
		printInorder(root.left);
		System.out.print(root.data+"\t");
		printInorder(root.right);
	}

	public BinarySearchTreeNode correctBST(BinarySearchTreeNode root){
		correctBSTUtil(root);
		if( first!=null && last!=null ){
			int temp=first.data;
			first.data=last.data;
			last.data=temp;
		}
	    else if( first!=null && middle!=null ) {
	    	int temp=first.data;
			first.data=middle.data;
			middle.data=temp;
	    }
		return root;
	}
	
	public void correctBSTUtil(BinarySearchTreeNode root){
		if(root!=null){
			correctBSTUtil(root.left);
			if(prev!=null && root.data<prev.data){
				if(first==null){
					first=prev;
					middle=root;
				}
				else
	                last = root;
			}
			prev=root;
			correctBSTUtil(root.right);
		}
	}
}
