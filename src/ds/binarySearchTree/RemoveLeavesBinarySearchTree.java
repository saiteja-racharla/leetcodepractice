package ds.binarySearchTree;

import java.util.ArrayList;
import java.util.List;

public class RemoveLeavesBinarySearchTree {

	public static void main(String[] args) {
		BinarySearchTreeNode root=new BinarySearchTreeNode(1);
		root.left=new BinarySearchTreeNode(2);
		root.right=new BinarySearchTreeNode(3);
		root.left.left=new BinarySearchTreeNode(4);
		root.left.right=new BinarySearchTreeNode(5);
		System.out.println(findLeaves(root));
	}
	public static List<List<Integer>> findLeaves(BinarySearchTreeNode root) {
        List<List<Integer>> result=new ArrayList<List<Integer>>();
        List<Integer> temp=new ArrayList<>();
        while(root!=null) {
        	temp=new ArrayList<>();
        	root=removeLeaves(root, temp);
        	result.add(temp);
        }
        return result;
    }
	public static BinarySearchTreeNode removeLeaves(BinarySearchTreeNode root,List<Integer> temp) {
		if (root == null) return null;
		if(root.left==null && root.right==null) {
			temp.add(root.data);
			return null;
		}
		root.left=removeLeaves(root.left, temp);
		root.right=removeLeaves(root.right, temp);
		return root;
	}
}
