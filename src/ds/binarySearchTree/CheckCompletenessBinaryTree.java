package ds.binarySearchTree;

import java.util.LinkedList;
import java.util.Queue;

public class CheckCompletenessBinaryTree {

	public static void main(String[] args) {
		
	}
	public boolean isCompleteTree(BinarySearchTreeNode root) {
        Queue<BinarySearchTreeNode> q=new LinkedList<>();
        q.add(root);
        boolean leftNull=false;
        while(!q.isEmpty()){
        	BinarySearchTreeNode poll=q.poll();
        	if(poll==null){
                leftNull=true;
            }
            else{
                if(leftNull){
                    return false;
                }
                q.add(poll.left);
                q.add(poll.right);
        	}
        }
        return true;
    }
}
