/**
 * https://leetcode.com/problems/minimum-distance-between-bst-nodes/description/
 * Given a Binary Search Tree (BST) with the root node root, return the minimum difference between the values of any two different nodes in the tree.
 * Example :

Input: root = [4,2,6,1,3,null,null]
Output: 1
Explanation:
Note that root is a TreeNode object, not an array.

The given tree [4,2,6,1,3,null,null] is represented by the following diagram:

          4
        /   \
      2      6
     / \    
    1   3  

while the minimum difference in this tree is 1, it occurs between node 1 and node 2, also between node 3 and node 2.
Time complexity O(N). Space complexity O(h)
 */
package ds.binarySearchTree;

import java.util.Stack;

public class MinimumDistanceBetweenBSTNodes {
	static int pre=-1;
	static int minDifference=Integer.MAX_VALUE;
	public static void main(String[] args) {
		BinarySearchTreeNode root=new BinarySearchTreeNode(180);
		root.left=new BinarySearchTreeNode(50);
		root.right=new BinarySearchTreeNode(200);
		root.left.left=new BinarySearchTreeNode(1);
		root.left.right=new BinarySearchTreeNode(100);
		root.left.right.left=new BinarySearchTreeNode(80);
		root.left.right.right=new BinarySearchTreeNode(170);
		System.out.println(minDiffInBST(root));
		
	}
	public static int minDiffInBST(BinarySearchTreeNode root) {
		if(root.left!=null)
			minDiffInBST(root.left);
		if(pre>=0) {
            minDifference=Math.min(minDifference, root.data-pre);
		}
        pre=root.data;
		if(root.right!=null)
			minDiffInBST(root.right);
		return minDifference;
    }
}
