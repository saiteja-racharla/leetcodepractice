/*
 * Given a singly linked list where elements are sorted in ascending order, 
 * convert it to a height balanced BST.
 */
package ds.binarySearchTree;
import ds.linkedlist.*;

public class SortedListToBinarySearchTree {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public BinarySearchTreeNode sortedListToBST(LinkedListNode head) {
        return sortedListToBSTUtil(head,null);
    }
	public BinarySearchTreeNode sortedListToBSTUtil(LinkedListNode head, LinkedListNode tail){
		LinkedListNode slow=head,fast=head;
		if(head==tail) return null;
		while(fast!=tail && fast.next!=tail){
			slow=slow.next;
			fast=fast.next.next;
		}
		BinarySearchTreeNode root=new BinarySearchTreeNode(slow.data);
		root.left=sortedListToBSTUtil(head,slow);
		root.right=sortedListToBSTUtil(slow.next,tail);
		return root;
	}

}
