package ds.binarySearchTree;

public class KthSmallestElementInBST {
	static int k=5;
	static int count=0;
	static BinarySearchTreeNode root;
	public static void main(String[] args) {
		root=new BinarySearchTreeNode(22);
		root.left=new BinarySearchTreeNode(20);
		root.right=new BinarySearchTreeNode(26);
		root.left.left=new BinarySearchTreeNode(18);
		root.left.right=new BinarySearchTreeNode(21);
		root.right.left=new BinarySearchTreeNode(24);
		root.right.right=new BinarySearchTreeNode(28);
		root.left.left.right=new BinarySearchTreeNode(19);
		kthSmallestElement(root);
	}
	public static void kthSmallestElement(BinarySearchTreeNode root){
		if(root==null) return;
		kthSmallestElement(root.left);
		count++;
		if(count==k) System.out.println(root.data);
		kthSmallestElement(root.right);
		
	}

}
