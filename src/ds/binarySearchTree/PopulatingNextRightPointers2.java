package ds.binarySearchTree;

public class PopulatingNextRightPointers2 {
	public static void main(String[] args){
		BinarySearchTreeLinkNode root=new BinarySearchTreeLinkNode(1);
		root.left=new BinarySearchTreeLinkNode(2);
		PopulatingNextRightPointers2 b=new PopulatingNextRightPointers2();
		b.display(root);
	}
	public void display(BinarySearchTreeLinkNode root){
		if(root==null)
			return;
		System.out.println(root.data);
		display(root.left);
		display(root.right);
	}
	public void connect(BinarySearchTreeLinkNode root) {
        if(root==null)
            return;
        BinarySearchTreeLinkNode temp=root;
        while(temp.left!=null || temp.right!=null){
        	BinarySearchTreeLinkNode now=temp;
            while(now!=null){
                now.left.next=now.right;
                if(now.next!=null) {
                    if(now.next.left!=null)
                        now.right.next=now.next.left;
                    else
                        now.right.next=now.next.right;
                }
                now=now.next;
            }
            while(temp.left!=null || temp.right!=null){
            	BinarySearchTreeLinkNode dummy=temp;
                if(temp.left!=null)
                    temp=temp.left;
                else if(temp.right!=null)
                    temp=temp.right;
                if(temp==null)
                    temp=dummy.next;
            }
        }
    }
}
