/*
 * Given an integer n, generate all structurally unique BST's (binary search trees) 
 * that store values 1...n.

	For example,
	Given n = 3, your program should return all 5 unique BST's shown below.
 */
package ds.binarySearchTree;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GenerateBinarySearchTree {
	public static void main(String[] args){
		GenerateBinarySearchTree gst=new GenerateBinarySearchTree();
		List<BinarySearchTreeNode> l= gst.generateTrees(3);
		Iterator<BinarySearchTreeNode> it=l.iterator();
		while(it.hasNext()){
			BinarySearchTreeNode root=it.next();
			BinarySearchTreeNodeOperations bst=new BinarySearchTreeNodeOperations();
			System.out.println("\nprinting tree in preOrder");
			bst.preOrder(root);
		}
	}
	public List<BinarySearchTreeNode> generateTrees(int n) {
		return genTrees(1,n);
    }
	public List<BinarySearchTreeNode> genTrees (int start, int end){
		List<BinarySearchTreeNode> list = new ArrayList<BinarySearchTreeNode>();
		if(start>end)
        {
            list.add(null);
            return list;
        }
        
        if(start == end){
            list.add(new BinarySearchTreeNode(start));
            return list;
        }
        
        List<BinarySearchTreeNode> left,right;
        for(int i=start;i<=end;i++)
        {
            
            left = genTrees(start, i-1);
            right = genTrees(i+1,end);
            
            for(BinarySearchTreeNode lnode: left)
            {
                for(BinarySearchTreeNode rnode: right)
                {
                	BinarySearchTreeNode root = new BinarySearchTreeNode(i);
                    root.left = lnode;
                    root.right = rnode;
                    list.add(root);
                }
            }
                
        }
        
        return list;
	}
}
