package ds.matrix;

public class AddTwoBinaryNumbers {
	public static void main(String[] args) {
		String str1="11";
		String str2="1";
		AddTwoBinaryNumbers a=new AddTwoBinaryNumbers();
		String str3=a.addBinary(str1, str2);
		System.out.println("Addition of "+str1+" and "+str2+" is "+str3);
	}
	public String addBinary(String str1,String str2){
		StringBuilder sb=new StringBuilder();
		int l1=str1.length()-1;
		int l2=str2.length()-1;
		int carry=0;
		int sum=0;
		while(l1>=0 || l2>=0){
			sum=carry;
			if(l1>=0)
				sum=sum+str1.charAt(l1--)-'0';
			if(l2>=0)
				sum=sum+str2.charAt(l2--)-'0';
			sb.append(sum%2);
			carry=sum/2;
		}
		if(carry!=0)
			sb.append(carry);
		return sb.reverse().toString();
	}
}
