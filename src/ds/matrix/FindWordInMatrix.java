package ds.matrix;

public class FindWordInMatrix {
	public static void main(String[] args) {
		char[][] board={
				{'A','B','C','E'},
				{'S','F','C','S'},
				{'A','D','E','E'}
		};
		FindWordInMatrix fs=new FindWordInMatrix();
		String word="ABCCED";
		System.out.println("is word "+word +" exists in board??" +exist(board, word));
		word="SEE";
		System.out.println("is word "+word +" exists in board??" +exist(board, word));
		word="ABCB";
		System.out.println("is word "+word +" exists in board??" +exist(board, word));
	}
	public static boolean exist(char[][] board, String word) {
		if(board.length<0)
			return false;
		visited = new boolean[board.length][board[0].length];
		int index=0;
		for(int i=0;i<board.length;i++){
			for(int j=0;j<board[0].length;j++){
				if(board[i][j]==word.charAt(index)){
					if(existUtil(board,word,i,j,0))
						return true;
				}
			}
		}
		return false;
    }
	static boolean[][] visited;
	public static boolean existUtil(char[][] board, String word,int i,int j,int index){
		if(index==word.length())
			return true;
		if(i<0 || j<0 || i>=board.length || j>=board[0].length || visited[i][j]){
			return false;
		}
		if (board[i][j] != word.charAt(index)) return false;
		visited[i][j]=true;
		boolean exist=existUtil(board,word,i-1,j,index+1)|| existUtil(board,word,i,j+1,index+1)
				|| existUtil(board,word,i+1,j,index+1)|| existUtil(board,word,i,j-1,index+1);
		visited[i][j]=false;
		return exist;
	}
}
