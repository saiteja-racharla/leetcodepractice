/*
 * Given a m x n matrix, if an element is 0, set its entire row and column to 0. Do it in place.
 */
package ds.matrix;

public class MakeMatrixZeroes {
	public static void main(String[] args) {
		MakeMatrixZeroes m=new MakeMatrixZeroes();
		int[][] matrix={
				{2,5,7,0,5},
				{9,1,3,2,1},
				{3,0,5,17,1},
				{15,12,0,12,1},
				{2,3,4,5,0}
		};
		matrix = m.setZeroes(matrix);
		for(int i=0;i<matrix.length;i++){
            for(int j=0;j<matrix[0].length;j++){
            	System.out.print(matrix[i][j]+"\t");
            }
            System.out.println();
        }
	}
	public int[][] setZeroes(int[][] matrix) {
		int col0 = 1,rows = matrix.length, cols = matrix[0].length;
        for(int i=0;i<rows;i++){
            if (matrix[i][0] == 0) col0 = 0;
            for(int j=1;j<cols;j++){
                if(matrix[i][j]==0){
                    matrix[i][0]=0;
                    matrix[0][j]=0;
                }
            }
        }
        for (int i = rows - 1; i >= 0; i--) {
            for (int j = cols - 1; j >= 1; j--)
                if (matrix[i][0] == 0 || matrix[0][j] == 0)
                    matrix[i][j] = 0;
            if (col0 == 0) matrix[i][0] = 0;
        }
        return matrix;
    }
}
