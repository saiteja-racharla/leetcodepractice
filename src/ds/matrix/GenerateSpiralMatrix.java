/*
 * Given an integer n, generate a square matrix filled with elements from 1 to n2 in spiral order.

	For example,
	Given n = 3,
	
	You should return the following matrix:
	[
	 [ 1, 2, 3 ],
	 [ 8, 9, 4 ],
	 [ 7, 6, 5 ]
	]
 */

package ds.matrix;

public class GenerateSpiralMatrix {
	public static void main(String[] args) {
		int givenNumber=0;
		int n=givenNumber,m=givenNumber;
		int k=0,l=0,i;
		int number=1;
		int output[][]=new int[m][n];
		while(k<m && l<n){
			for(i=l;i<n;i++){
				output[k][i]=number;
				number++;
			}
			k++;
			for(i=k;i<m;i++){
				output[i][n-1]=number;
				number++;
			}
			n--;
			if(k<m){
				for(i=n-1;i>=l;i--){
					output[m-1][i]=number;
					number++;
				}
				m--;
			}
			if(l<n){
				for(i=m-1;i>=k;i--){
					output[i][l]=number;
					number++;
				}
				l++;
			}
		}
		for(int x=0;x<givenNumber;x++){
			for(int y=0;y<givenNumber;y++){
				System.out.print(output[x][y]+"\t");
			}
			System.out.println();
		}
	}
}
