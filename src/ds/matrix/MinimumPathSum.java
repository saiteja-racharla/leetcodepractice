package ds.matrix;

public class MinimumPathSum {

	public static void main(String[] args) {
		int[][] nums={
				{1,3,1},
				{1,5,1},
				{4,2,1}
		};
		MinimumPathSum ms=new MinimumPathSum();
		int sum = ms.minimumPathSum(nums);
		System.out.println("Minimum path from 0 to m*n is "+sum);
	}
	public int minimumPathSum(int[][] grid){
		int m=grid.length;
        int n=grid[0].length;
        int[][] temp=new int[grid.length][grid[0].length];
        temp[0][0]=grid[0][0];
        for(int i=1;i<m;i++){
            temp[i][0]=temp[i-1][0]+grid[i][0];
        }
        for(int i=1;i<n;i++){
            temp[0][i]=temp[0][i-1]+grid[0][i];
        }
        for(int i=1;i<m;i++){
            for(int j=1;j<n;j++){
                temp[i][j]=Math.min(temp[i-1][j],temp[i][j-1])+grid[i][j];
            }
        }
        return temp[m-1][n-1];
	}

}
