/**
 * A 3 x 3 magic square is a 3 x 3 grid filled with distinct numbers from 1 to 9 such that each row, column, and both diagonals all have the same sum.
Given an grid of integers, how many 3 x 3 "magic square" subgrids are there?  (Each subgrid is contiguous).
https://leetcode.com/problems/magic-squares-in-grid/description/
 */
package ds.matrix;

public class MagicSquaresInGrid {

	public static void main(String[] args) {
		MagicSquaresInGrid m=new MagicSquaresInGrid();
		int grid[][]= {{4,3,8,4},
				        {9,5,1,9},
				        {2,7,6,2}};
		System.out.println("Number of magic grids are "+m.numMagicSquaresInside(grid));
	}
	public int numMagicSquaresInside(int[][] grid) {
        int count=0;
        for(int i=1;i<grid.length-1;i++){
            for(int j=1;j<grid[0].length-1;j++){
                if(grid[i][j]!=5) continue;
                if(isMagic(i,j,grid))
                    count++;
            }
        }
        return count;
    }
    public boolean isMagic(int i,int j,int[][] grid){
        String s=""+grid[i-1][j-1]+grid[i-1][j]+grid[i-1][j+1]+grid[i][j+1]+grid[i+1][j+1]+grid[i+1][j]+grid[i+1][j-1]+grid[i][j-1];
        System.out.println(s);
        if("2761834927618349".contains(s) || "2943816729438167".contains(s)){
            return true;
        }
        return false;
    }
    /*
     * The center number should be 5. This can be extracted from below calculation.
     * Total numbers in the grid should be equal to 45(1+2+...9)
     * (if divided by 3 rows/3 columns comes to 15)
     * a1+a5+a9=15
     * a3+a5+a7=15
     * a4+a5+a6=15
     * a2+a5+a8=15
     * Solving the above equations will bring a5=5.
     * Since the middle value is 5, we have to form the grid with middle value as 5. 
     * 276
     * 951
     * 438
     * combining values in clockwise and anticlockwise gives us values 27618349 & 29438167
     */
}
