package ds;

public class LeastCommonAncestorBinaryTree
{
    BinarySearchTreeNode root;
    BinarySearchTreeNode findLCA(int n1, int n2)
    {
        return findLCA(root, n1, n2);
    }
    BinarySearchTreeNode findLCA(BinarySearchTreeNode node, int n1, int n2)
    {
        if (node == null)
            return null;
        if (node.data == n1 || node.data == n2)
            return node;
        BinarySearchTreeNode left_lca = findLCA(node.left, n1, n2);
        BinarySearchTreeNode right_lca = findLCA(node.right, n1, n2);
        if (left_lca!=null && right_lca!=null)
            return node;
        return (left_lca != null) ? left_lca : right_lca;
    }
 
    /* Driver program to test above functions */
    public static void main(String args[])
    {
    	LeastCommonAncestorBinaryTree tree = new LeastCommonAncestorBinaryTree();
        tree.root = new BinarySearchTreeNode(1);
        tree.root.left = new BinarySearchTreeNode(2);
        tree.root.right = new BinarySearchTreeNode(3);
        tree.root.left.left = new BinarySearchTreeNode(4);
        tree.root.left.right = new BinarySearchTreeNode(5);
        tree.root.right.left = new BinarySearchTreeNode(6);
        tree.root.right.right = new BinarySearchTreeNode(7);
        System.out.println("LCA(4, 5) = " +tree.findLCA(4, 5).data);
        System.out.println("LCA(4, 6) = " +tree.findLCA(4, 6).data);
        System.out.println("LCA(3, 4) = " +tree.findLCA(3, 4).data);
        System.out.println("LCA(2, 4) = " +tree.findLCA(2, 4).data);
    }
}
