/*
 * This program is to calculate vertical sum of a binary tree...not binary search tree.
 * Time complexity-O(n). 
 * This is not optimized solution(hashtable takes more space).Instead of hashtable, linkedlist 
 * can be used to reduce space. The Optimized solution can be found at
 * http://www.geeksforgeeks.org/vertical-sum-in-binary-tree-set-space-optimized/
 */
package ds;

import java.util.HashMap;
import java.util.Hashtable;

public class VerticalSumBinaryTree {
	static HashMap<Integer,Integer> hm1=new HashMap<>();
	public static void calculateVerticalSumBinaryTree(BinarySearchTreeNode root){
		HashMap<Integer,Integer> hm=new HashMap<Integer,Integer>();
		int c=0;
		if(root==null)
			System.out.println("null");
		calculateVerticalSumBinaryTreeUtil(root, c, hm);
		if(hm!=null)
			System.out.println(hm.entrySet());
	}
	public static void printHM1(){
		if(hm1!=null)
			System.out.println(hm1.entrySet());
	}
	public static void calculateVerticalSumBinaryTreeUtil(BinarySearchTreeNode root,int c, HashMap<Integer,Integer> hm){
		if(root==null)
			return;
		calculateVerticalSumBinaryTreeUtil(root.left,c-1,hm);
		int data = (hm.get(c)==null)?0:hm.get(c);
		hm.put(c, data+root.data);
		calculateVerticalSumBinaryTreeUtil(root.right, c+1, hm);
		
	}
	//My Own function. its also working
	public static void calculateVerticalSumBinaryTreeUtilTemp(BinarySearchTreeNode root,int c){
		if(root==null)
			return;
		if(root.left!=null)
			calculateVerticalSumBinaryTreeUtilTemp(root.left,c-1);
		if(root.right!=null)
			calculateVerticalSumBinaryTreeUtilTemp(root.right, c+1);
		if(hm1.containsKey(c)){
			int data = (hm1.get(c)==null)?0:hm1.get(c);
			hm1.put(c, data+root.data);
		}
		else
			hm1.put(c, root.data);
	}
	public static void main(String[] args) {
		BinarySearchTreeNode root = new BinarySearchTreeNode(1);
		root.left = new BinarySearchTreeNode(2);
		root.left.left = new BinarySearchTreeNode(4);
		root.left.right = new BinarySearchTreeNode(5);
		root.right = new BinarySearchTreeNode(3);
		root.right.left = new BinarySearchTreeNode(6);
		root.right.right = new BinarySearchTreeNode(7);
		calculateVerticalSumBinaryTree(root);
		calculateVerticalSumBinaryTreeUtilTemp(root,0);
		printHM1();
	}
}
