package ds;

class BinarySearchTreeOperations{
	BinarySearchTreeNode root;
	public void insertNonRecursion(int data){
		BinarySearchTreeNode temp=new BinarySearchTreeNode(data);
		if(root==null){
			root=temp;
		}
		else{
			BinarySearchTreeNode current=root;
			BinarySearchTreeNode parent=current;
			while(true){
				if(data<current.data){
					current=current.left;
					if(current==null){
						parent.left=temp;
						return;
					}
				}
				else{
					current=current.right;
					if(current==null){
						parent.right=temp;
						return;
					}
				}
				parent=current;
			}
		}
	}
	
	public void insert(int data){
		BinarySearchTreeNode temp=new BinarySearchTreeNode(data);
		if(root==null){
			root=temp;
		}
		else{
			BinarySearchTreeNode current=root;
			BinarySearchTreeNode parent=current;
			while(true){
				if(data<current.data){
					current=current.right;
					if(current==null){
						parent.left=temp;
						return;
					}
				}
				else{
					current=current.left;
					if(current==null){
						parent.right=temp;
						return;
					}
				}
				parent=current;
			}
		}
	}
	
	public void isBST(){
		boolean result=checkIsBST(root,Integer.MIN_VALUE, Integer.MAX_VALUE);
		if(result)
			System.out.println("BInary Search tree");
		else
			System.out.println("Not binary search tree");
	}
	
	public boolean checkIsBST(BinarySearchTreeNode node,int min,int max){
		if(node==null)
			return true;
		if(node.data>min && node.data<max && checkIsBST(node.left, min, node.data) && checkIsBST(node.right, node.data, max))
				return true;
		else
			return false;
	}
}

public class CheckBST {
	public static void main(String[] args) {
		BinarySearchTreeOperations bst=new BinarySearchTreeOperations();
		bst.insertNonRecursion(25);
		bst.insertNonRecursion(20);
		bst.insertNonRecursion(22);
		bst.insertNonRecursion(12);
		bst.insertNonRecursion(30);
		bst.insertNonRecursion(27);
		bst.insertNonRecursion(35);
		bst.insert(75);
		bst.isBST();
	}

}
