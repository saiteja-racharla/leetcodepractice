package ds;

class DoubleLinkedListNode{
	int data;
	DoubleLinkedListNode prev,next;
	public DoubleLinkedListNode(int data) {
		this.data=data;
		prev=null;
		next=null;
	}
}

class DoubleLinkedListNodeOperations{
	DoubleLinkedListNode head;
	public void insertAtBeginning(int data){
		DoubleLinkedListNode node=new DoubleLinkedListNode(data);
		if(head==null){
			head=node;
		}
		else{
			head.prev=node;
			node.next=head;
			head=node;
		}
	}
	public void insertAtEnd(int data){
		DoubleLinkedListNode node=new DoubleLinkedListNode(data);
		if(head==null){
			head=node;
		}
		else{
			DoubleLinkedListNode temp=head;
			while(temp.next!=null){
				temp=temp.next;
			}
			temp.next=node;
			node.prev=temp;
		}
	}
	public void printForward(){
		DoubleLinkedListNode temp=head;
		while(head!=null){
			System.out.print(head.data+" ");
			head=head.next;
		}
	}
	public void printReverse(){
		
	}
}

public class DoubleLinkedList {
	public static void main(String[] args) {
		DoubleLinkedListNodeOperations dl=new DoubleLinkedListNodeOperations();
		dl.insertAtBeginning(21);
		dl.insertAtBeginning(24);
		dl.insertAtBeginning(45);
		dl.insertAtBeginning(56);
		dl.insertAtBeginning(78);
		dl.insertAtBeginning(66);
		System.out.println("Printitng forward");
		dl.printForward();
		dl.insertAtEnd(67);
		dl.insertAtEnd(68);
		dl.insertAtEnd(69);
		dl.insertAtEnd(70);
		System.out.println("Printitng forward");
		dl.printForward();
	}

}
