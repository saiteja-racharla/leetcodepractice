/**
 * Path with Maximum Gold
 * In a gold mine grid of size m * n, each cell in this mine has an integer representing the amount of gold in that cell, 0 if it is empty.
Return the maximum amount of gold you can collect under the conditions:
Every time you are located in a cell you will collect all the gold in that cell.
From your position you can walk one step to the left, right, up or down.
You can't visit the same cell more than once.
Never visit a cell with 0 gold.
You can start and stop collecting gold from any position in the grid that has some gold.

Example 1:
Input: grid = [[0,6,0],[5,8,7],[0,9,0]]
Output: 24
Explanation:
[[0,6,0],
 [5,8,7],
 [0,9,0]]
Path to get the maximum gold, 9 -> 8 -> 7.

Example 2:
Input: grid = [[1,0,7],[2,0,6],[3,4,5],[0,3,0],[9,0,20]]
Output: 28
Explanation:
[[1,0,7],
 [2,0,6],
 [3,4,5],
 [0,3,0],
 [9,0,20]]
Path to get the maximum gold, 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7.
 */
package ds.arrays2d;

public class PathWithMaximumGold {
	public static void main(String[] args) {
		PathWithMaximumGold p=new PathWithMaximumGold();
		int[][] arr1= {
				{0,6,0},
				{5,8,7},
				{0,9,0}
		};
		int[][] arr2= {
				{1,0,7},
				{2,0,6},
				{3,4,5},
				{0,3,0},
				{9,0,20}
		};
		System.out.println(p.getMaximumGold(arr1));
		System.out.println(p.getMaximumGold(arr2));
	}
	public int getMaximumGold(int[][] grid) {
		int longest = 0;
		boolean visited[][]=new boolean[grid.length][grid[0].length];
		for(int i=0;i<grid.length;i++) {
			for(int j=0;j<grid[0].length;j++) {
				if(grid[i][j]>0) {
					longest=Math.max(longest, processMaximumGoldUtil(grid, visited,i,j,0));
				}
			}
		}
        return longest;
    }
	int[][] dir= {{-1,0},{1,0},{0,-1},{0,1}};
	public int processMaximumGoldUtil(int[][] grid,boolean[][] visited,int i,int j,int sum) {
		if(i<0 || i>=grid.length || j<0 || j>=grid[0].length || visited[i][j] || grid[i][j]==0)
			return sum;
		sum=sum+grid[i][j];
		visited[i][j]=true;
		int loopMax=0;
		for(int k=0;k<dir.length;k++) {
			loopMax=Math.max(loopMax, processMaximumGoldUtil(grid, visited, i+dir[k][0], j+dir[k][1],sum));
		}
		visited[i][j]=false;
		sum=sum-grid[i][j];
		return loopMax;
		
	}
}
