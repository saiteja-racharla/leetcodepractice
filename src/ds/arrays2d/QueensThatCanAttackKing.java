/**
 * On an 8x8 chessboard, there can be multiple Black Queens and one White King.
Given an array of integer coordinates queens that represents the positions of the Black Queens, 
and a pair of coordinates king that represent the position of the 
White King, return the coordinates of all the queens (in any order) that can attack the King.
https://leetcode.com/problems/queens-that-can-attack-the-king/description/
 */
package ds.arrays2d;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QueensThatCanAttackKing {

	public static void main(String[] args) {
		QueensThatCanAttackKing q=new QueensThatCanAttackKing();
		int[][] queens= {
				{0,1},
				{1,0},
				{4,0},
				{0,4},
				{3,3},
				{2,4}
		};
		int[] king= {0,0};
		System.out.println(q.queensAttacktheKing(queens, king));
	}
	public List<List<Integer>> queensAttacktheKing(int[][] queens, int[] king) {
		List<List<Integer>> result = new ArrayList<>();
		boolean[][] seen=new boolean[8][8];
		for(int[] queen:queens) {
			seen[queen[0]][queen[1]]=true;
		}
		int[] dirs= {-1,0,1};
		for(int dx:dirs) {
			for(int dy:dirs) {
				if(dx==0 && dy==0) continue;
				int x=king[0],y=king[1];
				while(x+dx>=0 && x+dx<8 && y+dy>=0 && y+dy<8) {
					x=x+dx;
					y=y+dy;
					if(seen[x][y]) {
						result.add(Arrays.asList(x, y));
                        break;
					}
				}
			}
		}
		return result;
        
    }

}
