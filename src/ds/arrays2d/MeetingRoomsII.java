package ds.arrays2d;

import java.util.Arrays;

public class MeetingRoomsII {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public int minMeetingRooms(int[][] intervals) {
        int start[]=new int[intervals.length];
        int end[]=new int[intervals.length];
        for(int i=0;i<intervals.length;i++) {
        	start[i]=intervals[i][0];
        	end[i]=intervals[i][1];
        }
        Arrays.sort(start);
        Arrays.sort(end);
        int endPointer=0,rooms=0;
        for(int i=0;i<start.length;i++) {
        	if(start[i]<end[endPointer]) {
        		rooms++;
        	} else {
        		endPointer++;
        	}
        }
        return rooms;
    }
}
