/**
 * Given two lists of closed intervals, each list of intervals is pairwise disjoint and in sorted order.
Return the intersection of these two interval lists.
(Formally, a closed interval [a, b] (with a <= b) denotes the set of real numbers x with a <= x <= b.  
The intersection of two closed intervals is a set of real numbers that is either empty, or can be represented as a closed interval.  
For example, the intersection of [1, 3] and [2, 4] is [2, 3].)
https://leetcode.com/problems/interval-list-intersections/
 */
package ds.arrays2d;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class IntervalListIntersections {

	public static void main(String[] args) {
		IntervalListIntersections it=new IntervalListIntersections();
		int[][] A= {
				{0,2},{5,10},{13,23},{24,25}
		};
		int[][] B= {
				{1,5},{8,12},{15,24},{25,26}
		};
		int[][] result=it.intervalIntersection(A, B);
		System.out.println(Arrays.deepToString(result));
	}
	public int[][] intervalIntersection(int[][] A, int[][] B) {
		List<int[]> result=new ArrayList<>();
		int i=0,j=0;
		while(i<A.length && j<B.length) {
			int low=Math.max(A[i][0], B[j][0]);
			int high=Math.min(A[i][1], B[j][1]);
			if(low<=high) {
				result.add(new int[] {low,high});
			}
			if(A[i][1]<B[j][1])
				i++;
			else
				j++;
		}
		return result.toArray(new int[result.size()][]);
	}

}
