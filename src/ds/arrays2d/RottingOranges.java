package ds.arrays2d;

import java.util.LinkedList;
import java.util.Queue;

public class RottingOranges {
	int freshOranges=0;
	public static void main(String[] args) {
		RottingOranges rot=new RottingOranges();
		int[][] grid= {
				{2,1,1},
				{0,1,1},
				{1,0,1}
		};
		int[][] grid2= {
				{1}
		};
		int minutes=rot.orangesRotting(grid2);
		System.out.println(minutes);
	}
	public int orangesRotting(int[][] grid) {
		Queue<Coord> queue=new LinkedList<Coord>();
		for(int i=0;i<grid.length;i++) {
			for(int j=0;j<grid[0].length;j++) {
				if(grid[i][j]==1) {
					freshOranges++;
				}
				if(grid[i][j]==2) {
					if(queue.isEmpty())
						queue.add(new Coord(i, j));
				}
			}
		}
		//if there is not even single rotten orange, return 0;
		if(queue.isEmpty())
			return 0;
		int numMinutes=0;
		while(!queue.isEmpty() && freshOranges!=0) {
			numMinutes++;
			int loopcount=queue.size();
			for(int i=0;i<loopcount;i++) {
				Coord coord=queue.poll();
				rottenSurroundedOranges(grid, coord.x, coord.y,queue);
			}
		}
		if(freshOranges!=0) {
			return -1;
		}
        return numMinutes;
    }
	public void rottenSurroundedOranges(int[][] matrix,int i,int j,Queue<Coord> queue) {
		int[][] dirs= {
				{1,0},{-1,0},{0,1},{0,-1}
		};
		for(int[] dir:dirs) {
			int x=i+dir[0];
			int y=j+dir[1];
			if(x<0 || y<0 || x>=matrix.length || y>=matrix[0].length || matrix[x][y]!=1) {
				continue;
			}
			queue.add(new Coord(x, y));
			matrix[x][y]=2;
			freshOranges--;
		}
	}
}
class Coord{
	int x;
	int y;
	public Coord(int x,int y) {
		this.x=x;
		this.y=y;
	}
}
