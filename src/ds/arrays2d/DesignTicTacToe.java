/**
 * https://leetcode.com/problems/design-tic-tac-toe
 * Design a Tic-tac-toe game that is played between two players on a n x n grid.

You may assume the following rules:
A move is guaranteed to be valid and is placed on an empty block.
Once a winning condition is reached, no more moves is allowed.
A player who succeeds in placing n of their marks in a horizontal, vertical, or diagonal 
row wins the game.
 */
package ds.arrays2d;

public class DesignTicTacToe {

	public static void main(String[] args) {
		TicTacToe toe=new TicTacToe(3);
		toe.move(0, 0, 1);
		toe.move(0, 2, 2);
		toe.move(2, 2, 1);
		toe.move(1, 1, 2);
		toe.move(2, 0, 1);
		toe.move(1, 0, 2);
		toe.move(2, 1, 1);
	}

}
class TicTacToe {
	int[] rowCount;
	int[] colCount;
	int diagCount=0;
	int antiDiagCount=0;

    /** Initialize your data structure here. */
    public TicTacToe(int i) {
        rowCount=new int[i];
		colCount=new int[i];
    }
    
    /** Player {player} makes a move at ({row}, {col}).
        @param row The row of the board.
        @param col The column of the board.
        @param player The player, can be either 1 or 2.
        @return The current winning condition, can be either:
                0: No one wins.
                1: Player 1 wins.
                2: Player 2 wins. */
    public int move(int row, int col, int player) {
        int val=(player==1)?1:-1;
		rowCount[row]+=val;
		colCount[col]+=val;
		if(row==col) {
			diagCount+=val;
		}
		if(col==(colCount.length-row-1)) {
			antiDiagCount+=val;
		}
		int size = rowCount.length;
		if(Math.abs(rowCount[row])==size || Math.abs(colCount[col])==size || Math.abs(diagCount)==size || Math.abs(antiDiagCount)==size) {
			return player;
		}
		return 0;
    }
}

/**
 * Your TicTacToe object will be instantiated and called as such:
 * TicTacToe obj = new TicTacToe(n);
 * int param_1 = obj.move(row,col,player);
 */