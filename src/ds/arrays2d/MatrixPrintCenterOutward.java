//Print matrix from center to outward
package ds.arrays2d;

import java.util.ArrayList;
import java.util.List;

public class MatrixPrintCenterOutward {
	public static void main(String[] args) {
		int[][] matrix= {
				{7,  8,  9, 10},
				{6,  1,  2, 11},
				{5,  4,  3, 12},
				{16, 15, 14, 13}
		};
		System.out.println(printSpiral(matrix,4));
	}
	public static List<Integer> printSpiral(int[][] matrix, int size)
	{
		List<Integer> result=new ArrayList<>();
	    int x = 0; // current position; x
	    int y = 0; // current position; y
	    int d = 0; // current direction; 0=RIGHT, 1=DOWN, 2=LEFT, 3=UP
	    int c = 0; // counter
	    int s = 1; // chain size
	 
	    // starting point
	    x = ((int)Math.floor(size/2.0))-1;
	    y = ((int)Math.floor(size/2.0))-1;
	 
	    for (int k=1; k<=(size-1); k++)
	    {
	        for (int j=0; j<(k<(size-1)?2:3); j++)
	        {
	            for (int i=0; i<s; i++)
	            {
	            	result.add(matrix[x][y]);
	                c++;
	 
	                switch (d)
	                {
	                    case 0: y = y + 1; break;
	                    case 1: x = x + 1; break;
	                    case 2: y = y - 1; break;
	                    case 3: x = x - 1; break;
	                }
	            }
	            d = (d+1)%4;
	        }
	        s = s + 1;
	    }
	    return result;
	}
}
