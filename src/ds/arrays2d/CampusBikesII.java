/**
 * https://leetcode.com/problems/campus-bikes-ii/description/
 */
package ds.arrays2d;

public class CampusBikesII {
	int minimum=Integer.MAX_VALUE;
	public static void main(String[] args) {
		CampusBikesII c=new CampusBikesII();
		int[][] workers = {
				{0,0},{1,0},{2,0},{3,0},{4,0}
		};
		int[][] bikes = {
				{0,999},{1,999},{2,999},{3,999},{4,999},{5,999}
		};
		System.out.println(c.calculateMinimum(workers, bikes));

	}
	public int calculateMinimum(int[][] workers, int[][] bikes) {
		dfs(workers,bikes,0,0,new boolean[bikes.length]);
        return minimum;
    }
	public void dfs(int[][] workers,int[][] bikes,int index,int distance,boolean[] used) {
		if(index==workers.length) {
			minimum=Math.min(minimum, distance);
			return;
		}
		if(distance>minimum)
			return;
		for(int i=0;i<bikes.length;i++) {
			if(used[i])
				continue;
			used[i]=true;
			int sum=getDistance(workers[index],bikes[i]);
			dfs(workers,bikes,index+1,sum+distance,used);
			used[i]=false;
		}
	}
	int getDistance(int[] worker, int[] bike) {
        return Math.abs(worker[0] - bike[0]) + Math.abs(worker[1] - bike[1]);
    }
}
