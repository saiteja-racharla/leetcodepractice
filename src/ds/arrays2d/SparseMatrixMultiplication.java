/**
 * https://leetcode.com/problems/sparse-matrix-multiplication/
 * 
 */
package ds.arrays2d;

public class SparseMatrixMultiplication {

	public static void main(String[] args) {
		SparseMatrixMultiplication s=new SparseMatrixMultiplication();
		int[][] A= {
				{1,0,1},
				{-1,0,3}
		};
		int[][] B= {
				{7,0,0},
				{0,0,0},
				{0,0,1}
		};
		int[][] c=s.multiply(A, B);
		for(int i=0;i<c.length;i++) {
			for(int j=0;j<c[0].length;j++) {
				System.out.print(c[i][j]+"\t");
			}
			System.out.println();
		}
	}
	public int[][] multiply(int[][] A, int[][] B) {
        int m = A.length, n = A[0].length, nB = B[0].length;
        int[][] C = new int[m][nB];

        for(int i = 0; i < m; i++) {
            for(int k = 0; k < n; k++) {
                if (A[i][k] != 0) {
                    for (int j = 0; j < nB; j++) {
                        if (B[k][j] != 0) 
                        	C[i][j] += A[i][k] * B[k][j];
                    }
                }
            }
        }
        return C;   
    }
}
