/**
 * Given the following details of a matrix with n columns and 2 rows :
The matrix is a binary matrix, which means each element in the matrix can be 0 or 1.
The sum of elements of the 0-th(upper) row is given as upper.
The sum of elements of the 1-st(lower) row is given as lower.
The sum of elements in the i-th column(0-indexed) is colsum[i], where colsum is given as an integer array with length n.
Your task is to reconstruct the matrix with upper, lower and colsum.
Return it as a 2-D integer array.
If there are more than one valid solution, any of them will be accepted.
If no valid solution exists, return an empty 2-D array.
https://leetcode.com/problems/reconstruct-a-2-row-binary-matrix/description/
Leetcode-1253
 */
package ds.arrays2d;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ReconstructTwoRowBinaryMatrix {

	public static void main(String[] args) {
		ReconstructTwoRowBinaryMatrix r=new ReconstructTwoRowBinaryMatrix();
		int upper=2, lower=1;
		int[] colsum= {1,1,1};
		var res=r.reconstructMatrix(upper,lower,colsum); System.out.println(res);
		upper=2;lower=3;colsum= new int[]{2,2,1,1};
		res=r.reconstructMatrix(upper,lower,colsum); System.out.println(res); 
		upper=5;lower=5;colsum= new int[]{2,1,2,0,1,0,1,2,0,1};
		res=r.reconstructMatrix(upper,lower,colsum);
		System.out.println(res);

	}
	public List<List<Integer>> reconstructMatrix(int upper, int lower, int[] colsum) {
		List<List<Integer>>	result=new ArrayList<List<Integer>>();
		int[] firstRow=new int[colsum.length];
		int[] secondRow=new int[colsum.length];
		int count=0;
		for(int i=0;i<colsum.length;i++) {
			count=count+colsum[i];
		}
		if(count!=upper+lower)
			return result;
		for(int i=0;i<colsum.length;i++) {
			if(colsum[i]==2) {
				firstRow[i]=1;
				secondRow[i]=1;
				if(upper<0 || lower<0)
					return result;
			}
		}
		for(int i=0;i<colsum.length;i++) {
			if(colsum[i]==1) {
				if(upper>0) {
					firstRow[i]=1;
					upper--;
				} else if(lower>0) {
					secondRow[i]= 1;
					lower--;
				}
			}
		}
		result.add(Arrays.stream(firstRow).boxed().collect(Collectors.toList()));
		result.add(Arrays.stream(secondRow).boxed().collect(Collectors.toList()));
		return result;
	}
}
