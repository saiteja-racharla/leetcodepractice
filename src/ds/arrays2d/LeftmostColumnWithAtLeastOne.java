/**
 * A binary matrix means that all elements are 0 or 1. For each individual row of the matrix, this row is sorted in non-decreasing order.

Given a row-sorted binary matrix binaryMatrix, return leftmost column index(0-indexed) with at least a 1 in it. If such index doesn't exist, return -1.

You can't access the Binary Matrix directly.  You may only access the matrix using a BinaryMatrix interface:

BinaryMatrix.get(x, y) returns the element of the matrix at index (x, y) (0-indexed).
BinaryMatrix.dimensions() returns a list of 2 elements [m, n], which means the matrix is m * n.
Submissions making more than 1000 calls to BinaryMatrix.get will be judged Wrong Answer.  Also, any solutions that attempt to circumvent the judge will result in disqualification.

For custom testing purposes you're given the binary matrix mat as input in the following four examples. You will not have access the binary matrix directly.

Input: mat = [[0,0,0,1],[0,0,1,1],[0,1,1,1]]
Output: 1
 */
package ds.arrays2d;

import java.util.ArrayList;
import java.util.List;

public class LeftmostColumnWithAtLeastOne {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public int leftMostColumnWithOne(BinaryMatrix binaryMatrix) {
        List<Integer> dimensions=binaryMatrix.getdimensions();
        int rows=dimensions.get(0);
        int cols=dimensions.get(1);
        
        if(rows==0 && cols==0)
            return -1;
        int r=0,c=cols-1;
        int result=-1;
        while(r<=rows-1 && c>=0){
            if(binaryMatrix.get(r,c)==1){
                result=c;
                c--;
            } else{
                r++;
            }
        }
        return result;
    }
}
interface BinaryMatrix {
	public default int get(int x, int y) {
		return 0;
	}
	public default List<Integer> getdimensions() {
		return new ArrayList<>();
	}
};
