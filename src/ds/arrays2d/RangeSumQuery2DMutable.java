package ds.arrays2d;

public class RangeSumQuery2DMutable {
	static int[][] matrix= {
			{1,2,3},
			{4,5,6},
			{7,8,9}
	};
    static int[][] colsum=new int[matrix.length+1][matrix[0].length];
	public static void main(String[] args) {
		RangeSumQuery2DMutable d=new RangeSumQuery2DMutable(matrix);
		d.update(0, 1, 4);
		System.out.println(d.sumRegion(0, 0, 2, 2));
	}
	public RangeSumQuery2DMutable(int[][] matrix) {
        if(matrix==null || matrix.length==0 || matrix[0].length==0){
            return;
        }
        this.matrix=matrix;
        colsum=new int[matrix.length+1][matrix[0].length];
        for(int i=1;i<=matrix.length;i++){
            for(int j=0;j<matrix[0].length;j++){
                colsum[i][j]=colsum[i-1][j]+matrix[i-1][j];
            }
        }
    }
    
    public void update(int row, int col, int val) {
        for(int i=row+1;i<colsum.length;i++){
            colsum[i][col]=(colsum[i][col]-matrix[row][col])+val;
        }
        matrix[row][col]=val;
    }
    
    public int sumRegion(int row1, int col1, int row2, int col2) {
        int sum=0;
        for(int i=col2;i>=col1;i--){
            sum=sum+(colsum[row2+1][i]-colsum[row1][i]);
        }
        return sum;
    }
}
