package ds.arrays2d;

public class LongestIncreasingPathInMatrix {
	int[][] dirs= {
			{-1,0},{0,1},{1,0},{0,-1}	
	};
	public static void main(String[] args) {
		int[][] matrix= {
				{9,9,4},
				{6,6,8},
				{2,1,1}
		};
		LongestIncreasingPathInMatrix l=new LongestIncreasingPathInMatrix();
		int result=l.longestIncreasingPath(matrix);
		System.out.println(result);
	}
	public int longestIncreasingPath(int[][] matrix) {
		int cache[][]=new int[matrix.length][matrix[0].length];
		int max=1;
		for(int i=0;i<matrix.length;i++) {
			for(int j=0;j<matrix[0].length;j++) {
				int len=dfs(matrix,i,j, cache,matrix.length,matrix[0].length);
				max=Math.max(max, len);
			}
		}
		return max;  
    }
	public int dfs(int[][] matrix,int row,int col,int[][] cache,int rowlen, int collen) {
		if(cache[row][col] != 0) 
			return cache[row][col];
		int max=1;
		for(int[] dir:dirs) {
			int x=row+dir[0];
			int y=col+dir[1];
			if(x<0 || x>=rowlen || y<0 || y>=collen || matrix[x][y]<=matrix[row][col])
				continue;
			int len=1+dfs(matrix, x, y, cache, rowlen, collen);
			max = Math.max(max, len);
		}
		cache[row][col]=max;
		return max;
	}
}
