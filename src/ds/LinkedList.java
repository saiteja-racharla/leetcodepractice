package ds;

class LinkedListNode{
	int data;
	LinkedListNode next;
	LinkedListNode(int data){
		this.data=data;
		next=null;
	}
}

class LinkedListOperations{
	LinkedListNode head=null;
	public void insert(int data){
		LinkedListNode temp=new LinkedListNode(data);
		if(head==null){
			head=temp;
		}
		else{
			LinkedListNode current=head;
			while(current.next!=null){
				current=current.next;
			}
			current.next=temp;
		}
	}
	public void display(){
		LinkedListNode temp=head;
		while(temp!=null){
			System.out.print(temp.data+" ");
			temp=temp.next;
		}
	}
	public void insertAtBeginning(int data){
		LinkedListNode temp=new LinkedListNode(data);
		temp.next=head;
		head=temp;
	}
	public void insertAtPosition(int data,int position){
		LinkedListNode node=new LinkedListNode(data);
		LinkedListNode temp=head;
		if(position==1)
		{
			node.next=temp;
			head=node;
			return;
		}
		for(int i=0;i<position-2;i++){
			temp=temp.next;
		}
		node.next=temp.next;
		temp.next=node;
	}
	public void deleteAtPosition(int position){
		LinkedListNode temp=head;
		if(position==1){
			temp=temp.next;
			head=temp;
			return;
		}
		for(int i=0;i<position-2;i++){
			temp=temp.next;
		}
		LinkedListNode next=temp.next;
		temp.next=next.next;
		
	}
	public void reverseRecur(){
		reverseRecursive(head);
	}
	public void reverseRecursive(LinkedListNode node){
		if(node.next==null){
			head=node;
			return;
		}
		reverseRecursive(node.next);
		LinkedListNode temp=node.next;
		temp.next=node;
		node.next=null;
	}
	public void reverseNonRecur(){
		reverseNonRecursive(head);
	}
	public void reverseNonRecursive(LinkedListNode node){
		LinkedListNode current=node;
		LinkedListNode prev,next;
		prev=null;
		while(current!=null){
			next=current.next;
			current.next=prev;
			prev=current;
			current=next;
			
		}
		head=prev;
	}
	public void printForwardRec(){
		printForwardRecursion(head);
	}
	public void printForwardRecursion(LinkedListNode node){
		if(node==null){
			return;
		}
		System.out.print(node.data+" ");
		printForwardRecursion(node.next);
	}
	public void printBackwardRecur(){
		printBackwardRecursion(head);
	}
	public void printBackwardRecursion(LinkedListNode node){
		if(node==null){
			return;
		}
		printBackwardRecursion(node.next);
		System.out.print(node.data+" ");
	}
}

public class LinkedList {

	public static void main(String[] args) {
		LinkedListOperations ls=new LinkedListOperations();
		ls.insert(23);
		ls.insert(25);
		ls.insert(17);
		ls.insert(12);
		ls.insert(10);
		ls.insert(1);
		ls.insert(8);
		System.out.println("Before inserting at beginning");
		ls.display();
		ls.insertAtBeginning(3);
		System.out.println("After inserting at beginning");
		ls.display();
		ls.insertAtPosition(19, 5);
		ls.insertAtPosition(1001, 1);
		System.out.println("\n After inserting at 5th position");
		ls.display();
		ls.deleteAtPosition(4);
		System.out.println("After deleting at position");
		ls.display();
		ls.deleteAtPosition(1);
		System.out.println();
		ls.display();
		System.out.println("After reversing recursive");
		ls.reverseRecur();
		ls.display();
		System.out.println("After reversing Non Recursive");
		ls.reverseNonRecur();
		ls.display();
		System.out.println("Printing forward recursion");
		ls.printForwardRec();
		System.out.println("Printing backward recursion");
		ls.printBackwardRecur();
	}

}
