/*
 * This program implements Queue using stack
 */

package ds.queues;

import java.util.Stack;

public class QueueImplUsingStacks {
	static Stack<Integer> stack=new Stack<Integer>(); 
	public static void main(String[] args) {
		push(1);
		push(2);
		push(3);
		push(4);
		System.out.println(pop()+"\t"+pop()+"\t"+pop()+"\t"+pop()+"\t");
	}
	public static void push(int element){
		Stack<Integer> temp=new Stack<Integer>();
		while(!stack.isEmpty()){
			temp.push(stack.pop());
		}
		temp.push(element);
		while(!temp.isEmpty()){
			stack.push(temp.pop());
		}
	}
	public static int pop(){
		return stack.pop();
	}
	public static int top(){
		return stack.peek();
	}
	public static boolean isEmpty(){
		return stack.isEmpty();
	}
}
