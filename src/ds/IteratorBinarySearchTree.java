package ds;

import java.util.Stack;

class BSTIterator{
	Stack<BinarySearchTreeNode> myStack;
	BSTIterator(BinarySearchTreeNode root){
		myStack = new Stack<BinarySearchTreeNode> ();
		pushAll(root);
	}
	void pushAll(BinarySearchTreeNode root){
		while(root!=null){
			myStack.push(root);
			root = root.left;
		}
	}
	boolean hasNext(){
		return myStack.isEmpty();
	}
	int next(){
		BinarySearchTreeNode tempNode = (BinarySearchTreeNode) myStack.peek();
		myStack.pop();
		pushAll(tempNode.right);
		return tempNode.data;
	}
}

public class IteratorBinarySearchTree {
	public static void main(String[] args) {
		BinarySearchTreeNode root = new BinarySearchTreeNode(4);
	    root.left = new BinarySearchTreeNode(2);
	    root.right = new BinarySearchTreeNode(6);
	    root.left.left = new BinarySearchTreeNode(1);
	    root.left.right = new BinarySearchTreeNode(3);
	    root.right.left = new BinarySearchTreeNode(5);
	    root.right.right = new BinarySearchTreeNode(7);
	    BSTIterator iterator = new BSTIterator(root);
	    while(!iterator.hasNext()){
	    	System.out.print(iterator.next()+" ");
	    }
	}
}
