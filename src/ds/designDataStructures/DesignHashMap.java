/**
 * Design a HashMap without using any built-in hash table libraries.

To be specific, your design should include these functions:

put(key, value) : Insert a (key, value) pair into the HashMap. If the value already exists in the HashMap, update the value.
get(key): Returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key.
remove(key) : Remove the mapping for the value key if this map contains the mapping for the key.

MyHashMap hashMap = new MyHashMap();
hashMap.put(1, 1);          
hashMap.put(2, 2);         
hashMap.get(1);            // returns 1
hashMap.get(3);            // returns -1 (not found)
hashMap.put(2, 1);          // update the existing value
hashMap.get(2);            // returns 1 
hashMap.remove(2);          // remove the mapping for 2
hashMap.get(2);            // returns -1 (not found) 
 */
package ds.designDataStructures;

public class DesignHashMap {
	public static void main(String[] args) {
		MyHashMap hashMap=new MyHashMap();
		hashMap.remove(2);
		hashMap.put(3, 11);
		hashMap.put(4, 13);
		hashMap.put(15, 6);
		hashMap.put(6, 15);
		hashMap.put(8, 8);
		hashMap.put(11, 0);
		System.out.println(hashMap.get(11));
		hashMap.put(1, 10);
		hashMap.put(12, 14);
	}
}	
class MyHashMap {
	int[] arr;
    /** Initialize your data structure here. */
    public MyHashMap() {
    	arr=new int[100000];
    }
    
    /** value will always be non-negative. */
    public void put(int key, int value) {
        arr[key]=value+1;
    }
    
    /** Returns the value to which the specified key is mapped, or -1 if this map contains no mapping for the key */
    public int get(int key) {
        return arr[key]-1;
    }
    
    /** Removes the mapping of the specified value key if this map contains a mapping for the key */
    public void remove(int key) {
        arr[key]=0;
    }
}