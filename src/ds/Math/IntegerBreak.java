/**
 * Given a positive integer n, break it into the sum of at least two positive integers and maximize 
 * the product of those integers. Return the maximum product you can get.
	Example 1:
	Input: 2
	Output: 1
	Explanation: 2 = 1 + 1, 1 � 1 = 1.
	Example 2:
	
	Input: 10
	Output: 36
	Explanation: 10 = 3 + 3 + 4, 3 � 3 � 4 = 36.
 */
package ds.Math;

import java.util.ArrayList;
import java.util.List;

public class IntegerBreak {
	static int product=1;
	public static void main(String[] args) {
		System.out.println(integerBreak(4));
	}
	public static int integerBreak(int n) {
		if(n==2)
			return 1;
		else if(n==3)
			return 2;
		while(n>4){
			product=product*3;
			n=n-3;
		}
		product*=n;
        
        return product;
    }

}
