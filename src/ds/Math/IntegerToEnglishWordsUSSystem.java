/**
 * Convert a non-negative integer to its english words representation. Given input is guaranteed to be less than 231 - 1.

Example 1:

Input: 123
Output: "One Hundred Twenty Three"
Example 2:

Input: 12345
Output: "Twelve Thousand Three Hundred Forty Five"
Example 3:

Input: 1234567
Output: "One Million Two Hundred Thirty Four Thousand Five Hundred Sixty Seven"
Example 4:

Input: 1234567891
Output: "One Billion Two Hundred Thirty Four Million Five Hundred Sixty Seven Thousand Eight Hundred Ninety One"
 */
package ds.Math;

public class IntegerToEnglishWordsUSSystem {
	public static String[] LESS_THAN_20={"", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
	public static String[] LESS_THAN_100={"", "Ten", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
	public static String[] THOUSANDS={"", "Thousand", "Million", "Billion"};
	public static void main(String[] args) {
		int num=2111112957;
		System.out.println(numberToWords(num));
	}
	public static String numberToWords(int num) {
		int i=0;
		String words="";
		while(num>0){
			if(num%1000!=0){
				words=helper(num%1000)+" "+THOUSANDS[i]+" "+words;
			}
			num=num/1000;
			i++;
		}
        return words.trim();
    }
	public static String helper(int num){
		if(num<0)
			return "";
		else if(num<20){
			return LESS_THAN_20[num];
		}
		else if(num<100){
			return LESS_THAN_100[num/10]+" "+helper(num%10);
		}
		else{
			return LESS_THAN_20[num/100]+" hundred "+helper(num%100);
		}
	}
	

}
