package ds.Math;

import java.util.Arrays;

public class KnightProbabilityInChessBoard {
	int[][] moves={{2,-1},{2,1},{-2,1},{-2,-1},{1,2},{1,-2},{-1,2},{-1,-2}};
	public static void main(String[] args) {
		int N=4,moves=2,row=0,col=0;
		KnightProbabilityInChessBoard k=new KnightProbabilityInChessBoard();
		System.out.println(k.knightProbability(N, moves, row, col));
	}
	public double knightProbability(int N, int K, int r, int c) {
		double[][] dp1=new double[N][N];
        for(double[] row:dp1){
            Arrays.fill(row,1);
        }
        for(int l=0;l<K;l++){
            double[][] dp2=new double[N][N];
            for(int i=0;i<N;i++){
                for(int j=0;j<N;j++){
                    for(int[] move:moves){
                        int row=i+move[0];
                        int col=j+move[1];
                        if(isLegalMove(row,col,N))
                            dp2[i][j]=dp2[i][j]+dp1[row][col];
                    }
                }
            }
            dp1=dp2;
        }
        return dp1[r][c]/Math.pow(8,K);
    }
    private boolean isLegalMove(int row,int col,int len){
        return row>=0 && row<len && col>=0 && col<len;
    }
}
