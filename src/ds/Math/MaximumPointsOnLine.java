/**
 * How to determine if three points are on the same line?
The answer is to see if slopes of arbitrary two pairs are the same.
Second, let's see what the minimum time complexity can be.
Definitely, O(n^2). It's because you have to calculate all slopes between any two points.
Then let's go back to the solution of this problem.
In order to make this discussion simpler, let's pick a random point A as an example.
Given point A, we need to calculate all slopes between A and other points. There will be three cases:
Some other point is the same as point A.
Some other point has the same x coordinate as point A, which will result to a positive infinite slope.
General case. We can calculate slope.
We can store all slopes in a hash table. And we find which slope shows up mostly. Then add the number of same points to it. Then we know the maximum number of points on the same line for point A.
We can do the same thing to point B, point C...
Finally, just return the maximum result among point A, point B, point C...
 */
package ds.Math;

import java.util.HashMap;
import java.util.Map;

public class MaximumPointsOnLine {
	public static void main(String[] args) {
		Point p1=new Point(1,1);
		Point p2=new Point(3,2);
		Point p3=new Point(5,3);
		Point p4=new Point(4,1);
		Point p5=new Point(2,3);
		Point p6=new Point(1,4);
		Point points[]={p1,p2,p3,p4,p5,p6};
		System.out.println("Maximum points in a line "+maxPoints(points));
	}
	public static int maxPoints(Point[] points) {
		if(points.length==0) return 0;
		if(points.length<=2) return points.length;
		int result=0;
		for(int i=0;i<points.length;i++){
            HashMap<Integer, Map<Integer,Integer>> map = new HashMap<Integer, Map<Integer,Integer>>();
			int overlap=0;
			int max=0;
			for(int j=i+1;j<points.length;j++){
				
				int x=points[j].x-points[i].x;
    			int y=points[j].y-points[i].y;
				if(x==0&&y==0){
					overlap++;continue;
				}
				int gcd=generateGCD(x,y);
    			if (gcd!=0){
    				x/=gcd;
    				y/=gcd;
    			}
    			if (map.containsKey(x)){
    				if (map.get(x).containsKey(y)){
    					map.get(x).put(y, map.get(x).get(y)+1);
    				}else{
    					map.get(x).put(y, 1);
    				}   					
    			}else{
    				Map<Integer,Integer> m = new HashMap<Integer,Integer>();
    				m.put(y, 1);
    				map.put(x, m);
    			}
    			max=Math.max(max, map.get(x).get(y));
			}
			result=Math.max(result, max+overlap+1);
		}
		return result;
	}
	private static int generateGCD(int a,int b){
	    
    	if (b==0) return a;
    	else return generateGCD(b,a%b);
    	
    }
}

class Point{
	int x;
	int y;
	Point(){
		this.x=0;
		this.y=0;
	}
	
	Point(int x, int y){
		this.x=x;
		this.y=y;
	}
}
