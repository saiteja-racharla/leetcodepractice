/*
 * This program checks if the given string is valid number or not
 * we have to know the following cases to solve the problem
 * 	If we see [0-9] we reset the number flags.
		We can only see . if we didn't see e or ..
		We can only see e if we didn't see e but we did see a number. We reset numberAfterE flag.
		We can only see + and - in the beginning and after an e 
		any other character break the validation.
 */
package ds.Math;

public class ValidNumber {
	public static void main(String[] args) {
		ValidNumber v=new ValidNumber();
		System.out.println("Is valid number "+v.isValidNumber("1 "));
	}
	public boolean isValidNumber(String s){
		s=s.trim();
		boolean numberSeen=false,numberAfterE=false,eSeen=false,pointSeen=false;
		for(int i=0;i<s.length();i++){
			if(s.charAt(i)-'0'>=0 && s.charAt(i)-'0'<=9){
				numberAfterE=true;
				numberSeen=true;
			}
			else if(s.charAt(i)=='+' || s.charAt(i)=='-'){
				if(i>0 && s.charAt(i-1)!='e'){
					return false;
				}
			}
			else if(s.charAt(i)=='.'){
				if(pointSeen || eSeen){
					return false;
				}
				pointSeen=true;
			}
			else if(s.charAt(i)=='e'){
				if(eSeen || !numberSeen){
					return false;
				}
				eSeen=true;
				numberAfterE=false;
			}
			else
				return false;
		}
		return numberSeen && numberAfterE;
	}
}
