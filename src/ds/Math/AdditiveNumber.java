package ds.Math;

public class AdditiveNumber {

	public static void main(String[] args) {
		String num="0235813";
		System.out.println("is "+num+" an additive number??"+isAdditive(num));
	}
	
	public static boolean isAdditive(String num){
		int length=num.length();
		for(int i=1;i<=(length-1)/2;i++){
			if(num.charAt(0)=='0' && i>=2) 
				break;
			for(int j=i+1;length-j>=j-i && length-j>=i;j++){
				if(num.charAt(i)=='0' && j-i>=2)
					break;
				long num1 = Long.parseLong(num.substring(0, i)); // A
                long num2 = Long.parseLong(num.substring(i, j)); // B
                String substr = num.substring(j); // remaining string
                if(isAdditive(substr, num1, num2)) 
                	return true; // return true if passes isAdditive test
                // else continue; // continue for loop if does not pass isAdditive test
			}
		}
		return false;
	}
	
	// Recursively checks if a string is additive
	public static boolean isAdditive(String str, long num1, long num2) {
		if(str.equals("")) return true; // reaches the end of string means a yes
		long sum = num1+num2;
        String s = ((Long)sum).toString();
        if(!str.startsWith(s)) 
        	return false; // if string does not start with sum of num1 and num2, returns false
        return isAdditive(str.substring(s.length()), num2, sum); // recursively checks the remaining string
	}
}
