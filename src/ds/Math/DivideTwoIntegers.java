package ds.Math;

public class DivideTwoIntegers {

	public static void main(String[] args) {
		DivideTwoIntegers d=new DivideTwoIntegers();
		System.out.println(d.divide(-1010369383, -2147483648));
	}
	public int divide(int dividend, int divisor) {
		boolean sign=((dividend<0)^(divisor<0) )?true:false;
		if(divisor==0) return Integer.MAX_VALUE;
		if(dividend == Integer.MIN_VALUE){
            if(divisor == -1) return Integer.MAX_VALUE;
            else if(divisor == 1) return dividend;
            else if(divisor == Integer.MIN_VALUE) return 1;
        }
		long divd=Math.abs((long)dividend);
		long div=Math.abs(divisor);
		int res=0;
		while(divd>=div){
			long temp=div, multiple=1;
			while(divd>=(temp << 1)){
				temp<<=1;
				multiple<<=1;
			}
			divd-=temp;
			res+=multiple;
		}
		return sign?-res:res;
    }
}
