/** MedianFromDataStream
 * Median is the middle value in an ordered integer list. If the size of the list is even, there is no middle value. So the median is the mean of the two middle value.

For example,
[2,3,4], the median is 3

[2,3], the median is (2 + 3) / 2 = 2.5

Design a data structure that supports the following two operations:

void addNum(int num) - Add a integer number from the data stream to the data structure.
double findMedian() - Return the median of all elements so far.
 Example:

addNum(1)
addNum(2)
findMedian() -> 1.5
addNum(3) 
findMedian() -> 2
leetcode:295
 */
package ds.Math;

import java.util.Collections;
import java.util.PriorityQueue;

public class MedianDataStream {

	PriorityQueue<Integer> min=new PriorityQueue<>();
	PriorityQueue<Integer> max=new PriorityQueue<>(1000, Collections.reverseOrder());
	public static void main(String[] args) {
		
	}
	public void addNum(int num) {
        max.offer(num);
        min.offer(max.poll());
        if(max.size()<min.size()) {
        	max.offer(min.poll());
        }
    }
    
    public double findMedian() {
    	if(max.size()==min.size()) return (max.peek() + min.peek()) /  2.0;
    	else return max.peek();
    }

}

//PriorityQueue<Integer> minHeap;
//PriorityQueue<Integer> maxHeap;
///** initialize your data structure here. */
//public MedianFinder() {
//    minHeap=new PriorityQueue<Integer>();
//    maxHeap=new PriorityQueue<Integer>((a,b)->b-a);
//}
//
//public void addNum(int num) {
//    if(minHeap.size()==0){
//        minHeap.add(num);
//        return;
//    }
//    if(num<minHeap.peek()){
//        maxHeap.offer(num);
//    } else{
//        minHeap.offer(num);
//    }
//    if(minHeap.size()-maxHeap.size()==2){
//        maxHeap.offer(minHeap.poll());
//    }
//    if(maxHeap.size()-minHeap.size()==2){
//        minHeap.offer(maxHeap.poll());
//    }
//    return;
//}
//
//public double findMedian() {
//    if(minHeap.size()>maxHeap.size()){
//        return minHeap.peek();
//    } else if(maxHeap.size()>minHeap.size()){
//        return maxHeap.peek();
//    } else{
//        return (double)(minHeap.peek()+maxHeap.peek())/2.0;
//    }
//}