package ds.Math;

public class LookAndSayProblem {
	public static void main(String[] args) {
		int n=8;
		if(n==1)
			System.out.println("1");
		if(n==2)
			System.out.println("11");
		String str="11";
		for(int i=3;i<=n;i++){
			str+="$";
			int len=str.length();
			int count=1;
			String temp="";
			// Process previous term to find the next term
	        for (int j = 1; j < len; j++)
	        {
	            // If current character does't match
	            if (str.charAt(j) != str.charAt(j-1))
	            {
	                // Append count of str[j-1] to temp
	                temp += count;
	 
	                // Append str[j-1]
	                temp += str.charAt(j-1);
	 
	                // Reset count
	                count = 1;
	            }
	 
	            //  If matches, then increment count of matching
	            // characters
	            else count++;
	        }
	 
	        // Update str
	        str = temp;
		}
		System.out.println(str);
	}
}
