/*
 * This program converts fraction to a recurring decimal value. 
 * 0.333333...., 0.66666... are called recurring decimal values
 * 0.235262346723678... are called non recurring decimals..there is no specific pattern
 */
package ds.Math;

import java.util.HashMap;

public class FractionToRecurringDecimal {
	public static void main(String[] args) {
		FractionToRecurringDecimal f=new FractionToRecurringDecimal();
		System.out.println("10/3 is "+f.fractionToDecimal(7,12));
	}
	public String fractionToDecimal(int num, int den) {
		if (num == 0) {
            return "0";
        }
		StringBuilder res=new StringBuilder();
		res.append((num<0 ^ den<0)?"-":"");
		HashMap<Long, Integer> hm=new HashMap<Long,Integer>();
		long numerator = Math.abs((long)num);
        long denominator = Math.abs((long)den);	//we have converted to long because of overflow problems.
		res.append(numerator/denominator);
		if(numerator%denominator==0) return res.toString();
		res.append(".");
		numerator=numerator%denominator;
		hm.put(numerator, res.length());
		while(numerator!=0){
			numerator=numerator*10;
			res.append(numerator/denominator);
			numerator=numerator%denominator;
			if(hm.containsKey(numerator)){
				int index=hm.get(numerator);
				res.insert(index, "(");
				res.append(")");
				break;
			}
			else{
				hm.put(numerator, res.length());
			}
		}
		return res.toString();
    }
}
