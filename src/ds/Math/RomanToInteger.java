package ds.Math;

import java.util.HashMap;

public class RomanToInteger {

	public static void main(String[] args) {
		HashMap<Character, Integer> hm=new HashMap<Character, Integer>();
		hm.put('I', 1);
		hm.put('V', 5);
		hm.put('X', 10);
		hm.put('L', 50);
		hm.put('C', 100);
		hm.put('D', 500);
		hm.put('M', 1000);
		String s="MMMCCIX";
		int sum=0;
		sum+=hm.get(s.charAt(s.length()-1));
		for(int i=s.length()-2;i>=0;i--){
			if(hm.get(s.charAt(i))<hm.get(s.charAt(i+1)))
				sum-=hm.get(s.charAt(i));
			else
				sum+=hm.get(s.charAt(i));
		}
		System.out.println(sum);
	}

}
