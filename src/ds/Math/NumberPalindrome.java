/*
 * Determine whether an integer is a palindrome. Do this without extra space.
 * Time complexity-O(logn)
 * Space complexity-O(1)
 * https://leetcode.com/problems/palindrome-number/solution/
 */
package ds.Math;

public class NumberPalindrome {

	public static void main(String[] args) {
		//int number=2147483648;
		int number=12321;
		if(number<0 ||(number!=0 && number/10==0))
			System.out.println(false);
		
		int rev=0;
		//we check only for half the number
		while(number>rev)
		{
			rev=(rev*10)+number%10;
			number=number/10;
		}
		//the below condition number==rev/10 is used if the number has odd number of digits
		System.out.println(number==rev || number==rev/10);
	}

}
