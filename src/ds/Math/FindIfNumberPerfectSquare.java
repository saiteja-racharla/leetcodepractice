/*
 * THis program uses newton method. 
 */
package ds.Math;

public class FindIfNumberPerfectSquare {

	public static void main(String[] args) {
		FindIfNumberPerfectSquare f=new FindIfNumberPerfectSquare();
		int num=808201;
		System.out.println(num+" is a perfect square-"+f.isPerfectSquare(num));
		System.out.println(num+" is a perfect square-"+f.isPerfectSquareBinary(num));
		num=20;
		System.out.println(num+" is a perfect square-"+f.isPerfectSquare(num));
		System.out.println(num+" is a perfect square-"+f.isPerfectSquareBinary(num));
	}
	public boolean isPerfectSquare(int num){
		long x=num;		//we are using long here because if it is int, it fails for the number 808201
		while(x*x>num){
			x=(x+num/x)/2;
		}
		return x*x==num;
	}
	public boolean isPerfectSquareBinary(int num) {
		long left=0,right=num/2;
		while(left<=right) {
			long sq=left+(right-left)/2;
			if(sq*sq==num) {
				return true;
			} else if(sq*sq>num) {
				right=sq-1;
			} else {
				left=sq+1;
			}
		}
		return false;
	}
}
