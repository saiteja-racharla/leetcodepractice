/*
 * Swap 2 digits in a number to make it maximum number
 * Time complexity-O(n)
 * Space complexity-O(10) i.e, is O(1)
 */

package ds.Math;

public class MaximumSwap {
	public static void main(String[] args) {
		int num=54213;
		System.out.println(maximumSwap(num));
	}
	public static int maximumSwap(int num) {
        char[] digits = Integer.toString(num).toCharArray();
        
        int[] buckets = new int[10];
        for (int i = 0; i < digits.length; i++) {
            buckets[digits[i] - '0'] = i;
        }
        
        for (int i = 0; i < digits.length; i++) {
            for (int k = 9; k > digits[i] - '0'; k--) {
                if (buckets[k] > i) {
                    char tmp = digits[i];
                    digits[i] = digits[buckets[k]];
                    digits[buckets[k]] = tmp;
                    return Integer.valueOf(new String(digits));
                }
            }
        }
        
        return num;
    }
}
