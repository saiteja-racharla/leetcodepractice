package ds.Math;

public class Fibonacci {
	public static void main(String[] args) {
		System.out.println(fib(5));
		System.out.println(fibNonRec(5));
	}
	public static int fib(int N) {
		if(N<2) return N;    
        return fib(N-1)+fib(N-2);
    }
	public static int fibNonRec(int N) {
		if(N<2) return N;
		int sum=0;
		int prevToPrev=0;
		int prev=1;
		for(int i=2;i<=N;i++) {
			sum=prev+prevToPrev;
			prevToPrev=prev;
			prev=sum;
		}
		return sum;
    }
}
