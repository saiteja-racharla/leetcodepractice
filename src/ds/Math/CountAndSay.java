package ds.Math;

public class CountAndSay {
	public static void main(String[] args) {
		System.out.println(countAndSay(5));
	}
	public static String countAndSay(int n) {
		StringBuffer sb=new StringBuffer("1");
		if(n==1)
			return sb.toString();
		int count=1;
		for(int i=2;i<=n;i++) {
			String s=sb.toString();
			sb=new StringBuffer();
			count=1;
			char c=s.charAt(0);
			int j=1;
			for(j=1;j<s.length();j++) {
				if(s.charAt(j)==c) {
					count++;
				} else {
					sb.append(count).append(s.charAt(j-1));
					c=s.charAt(j);
					count=1;
				}
			}
			sb.append(count).append(c);
		}
		return sb.toString();
	}
}
