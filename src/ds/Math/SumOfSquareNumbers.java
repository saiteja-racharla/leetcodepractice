/*
 * Given a non-negative integer c, your task is to decide whether there're two integers 
 * a and b such that a2 + b2 = c.
 */
package ds.Math;

public class SumOfSquareNumbers {
	public static void main(String[] args) {
		SumOfSquareNumbers s=new SumOfSquareNumbers();
		int num=1000;
		System.out.println(s.checkIfSumOfSquareNumbersExists(num));
	}
	public boolean checkIfSumOfSquareNumbersExists(int num){
		if(num<0)
            return false;
        int left=0,right=(int)Math.sqrt(num);
        int cur=0;
        while(left<=right){
            cur=(left*left)+(right*right);
            if(cur<num)
                left++;
            else if(cur>num)
                right--;
            else
                return true;
        }
        return false;
	}
}
