package ds.Math;

public class FactorialTrailingZeroes {

	public static void main(String[] args) {
		int n=1808548329;
		int result=0;
		int i=1;
		while(i<=n){
			i=5*i;
			result=result+(n/i);
		}
		System.out.println("Number of trailing zeroes "+result);
		n=1808548329;
		result=0;
		//can also be done in this way
		while(n>0){
			n=n/5;
			result=result+n;
		}
		System.out.println(result);
	}

}
