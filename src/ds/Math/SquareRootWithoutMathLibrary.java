//Newton-Rapson Method
//Binary search related method
package ds.Math;

public class SquareRootWithoutMathLibrary {
	public static float floorSqrt(float n){
	   float x = n;
	   float y = 1;
	   float e = 0.000001f; /* e decides the accuracy level*/
	   while(x - y > e)
	   {
		   x = (x + y)/2;
		   y = n/x;
	   }
	   return x;
   }
	public static int floorSqrt1(int x){
		if (x == 0)
	        return 0;
	    int left = 1, right = Integer.MAX_VALUE;
	    while (true) {
	        int mid = left + (right - left)/2;
	        if (mid > x/mid) {
	            right = mid - 1;
	        } else {
	            if (mid + 1 > x/(mid + 1))
	                return mid;
	            left = mid + 1;
	        }
	    }
	   }
	public static void main(String[] args) {
		float n = 3.2f;
		System.out.println(floorSqrt(n));
		System.out.println(floorSqrt1(175));
	}
}
