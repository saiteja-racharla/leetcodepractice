package ds.Math;

public class NumberReverse {
	public static void main(String[] args) {
		int x=1534236469;	//the reverse of this number is exceeding the integer limit. Thats why we are 
		int rev=0;			//getting wrong answer
		while(x>0){
			rev=(rev*10)+x%10;
			if(rev>Integer.MAX_VALUE || rev<Integer.MIN_VALUE)
				System.out.println("overflow occured");
			x=x/10;
		}
		System.out.println(rev);
	}
}
