/*
 * Given a positive integer n, find the least number of perfect square numbers 
 * (for example, 1, 4, 9, 16, ...) which sum to n.
 * For example, given n = 12, return 3 because 12 = 4 + 4 + 4; given n = 13, return 2 because 13 = 4 + 9.
 */
package ds.Math;

public class NumberFormedByPerfectSquaresAddition {

	public static void main(String[] args) {
		NumberFormedByPerfectSquaresAddition n=new NumberFormedByPerfectSquaresAddition();
		System.out.println(n.numSquares(12));
	}

	public int numSquares(int n) {
		if(n<=3)
            return n;
		int dp[]=new int[n+1];
		dp[0]=0;
		dp[1]=1;
		dp[2]=2;
		dp[3]=3;
		for(int i=4;i<=n;i++){
			dp[i]=i;
			if(i==9) {
				System.out.println(i);
			}
			for(int j=1;j<=i;j++){
				int temp=j*j;
				if(temp>i){
					break;
				}
				else{
					dp[i]=Math.min(dp[i], 1+dp[i-temp]);
				}
			}
		}
		for(int i=0;i<dp.length;i++){
			System.out.print(dp[i]+"\t");
		}
		return dp[n];
    }
}
