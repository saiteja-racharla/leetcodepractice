package ds.Math;

public class GCD {

	public static void main(String[] args) {
		GCD gcd=new GCD();
		System.out.println(gcd.gcdIterative(36,60));
		System.out.println(gcd.gcdRecursive(36,60));
	}
	public int gcdIterative(int a,int b) {
		while(a!=b) {
			if(a>b)
				a=a-b;
			else
				b=b-a;
		}
		return a;
	}
	public int gcdRecursive(int a,int b) {
		if(b==0) return a;
		else return gcdRecursive(b, a%b);
	}
}
