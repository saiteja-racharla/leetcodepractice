/**
 * Given a list of unique words, find all pairs of distinct indices (i, j) in the given list, so that the concatenation of 
 * the two words, i.e. words[i] + words[j] is a palindrome.
 * 	Input: ["abcd","dcba","lls","s","sssll"]
	Output: [[0,1],[1,0],[3,2],[2,4]] 
	Explanation: The palindromes are ["dcbaabcd","abcddcba","slls","llssssll"]
	
	Input: ["bat","tab","cat"]
	Output: [[0,1],[1,0]] 
	Explanation: The palindromes are ["battab","tabbat"]
 * 
 * Time Complexity-O(k2.n)
 * Space Complexity-O((k+n)2)
 */
package ds.strings1d;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class PalindromePairs {

	public static void main(String[] args) {
		PalindromePairs pp=new PalindromePairs();
		String[] words= {"abcd","dcba","lls","s","sssll"};
		List<List<Integer>> result=pp.palindromePairs(words);
		System.out.println(result);
	}
	public List<String> allValidSuffixes(String word){
        List<String> validSuffixes=new ArrayList<>();
        for(int i=0;i<word.length();i++){
            if(isPalindrome(word,0,i)){
                validSuffixes.add(word.substring(i+1,word.length()));
            }
        }
        return validSuffixes;
    }
    public List<String> allValidPrefixes(String word){
        List<String> validPrefixes=new ArrayList<>();
        for(int i=0;i<word.length();i++){
            if(isPalindrome(word,i,word.length()-1)){
                validPrefixes.add(word.substring(0,i));
            }
        }
        return validPrefixes;
    }
    public boolean isPalindrome(String word,int front,int end){
        while(front<=end){
            if(word.charAt(front)!=word.charAt(end))
                return false;
            front++;
            end--;
        }
        return true;
    }
    public List<List<Integer>> palindromePairs(String[] words) {
        HashMap<String,Integer> wordset=new HashMap<>();
        for(int i=0;i<words.length;i++){
            wordset.put(words[i],i);
        }
        List<List<Integer>> result=new ArrayList<>();
        for(int i=0;i<words.length;i++){
            //case 1: words with same length
            String word=words[i];
            String reverse=new StringBuilder(word).reverse().toString();
            if(wordset.containsKey(reverse) && wordset.get(reverse)!=i){
                result.add(new ArrayList<>(Arrays.asList(i,wordset.get(reverse))));
            }
            //case 2: Suffix matching
            for(String suffix:allValidSuffixes(words[i])){
                String reverseSuffix=new StringBuilder(suffix).reverse().toString();
                if(wordset.containsKey(reverseSuffix) && wordset.get(reverseSuffix)!=i){
                    result.add(new ArrayList<>(Arrays.asList(wordset.get(reverseSuffix),i)));
                }
            }
            //case 3: Prefix matching
            for(String prefix:allValidPrefixes(words[i])){
                String reversePrefix=new StringBuilder(prefix).reverse().toString();
                if(wordset.containsKey(reversePrefix) && wordset.get(reversePrefix)!=i){
                    result.add(new ArrayList<>(Arrays.asList(i,wordset.get(reversePrefix))));
                }
            }
        }
        return result;
    }
}
