/**
 * https://leetcode.com/problems/course-schedule-iii/
 * Leetcode-630
 */
package ds.graphs;

import java.util.Arrays;
import java.util.PriorityQueue;

public class CourseScheduleIII {
	public static void main(String[] args) {
		CourseScheduleIII c=new CourseScheduleIII();
		int[][] courses= {
				{5,7}, 
				{3,5}, 
				{10,18},
				{4,16},
				{10,14}
		};
		System.out.println("Number of courses can complete---"+c.scheduleCourse(courses));
	}
	public int scheduleCourse(int[][] courses) {
		Arrays.sort(courses, (a, b) -> a[1] - b[1]);
        PriorityQueue<Integer> queue = new PriorityQueue<>((a, b) -> b - a);
        int time = 0;
        for (int[] c: courses) {
        	time+=c[0];
            queue.offer(c[0]);
            if(time>c[1])
                time-=queue.poll();
        }
        return queue.size();
	}
}
