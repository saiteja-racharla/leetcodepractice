/* 
 * Breadth first search, BFS
 */
package ds.graphs;

class Queue{
	private final int SIZE = 20;
	private int[] queArray;
	private int front;
	private int rear;
	public Queue() // constructor
	{
		queArray = new int[SIZE];
		front = 0;
		rear = -1;
	}
	public void insert(int j) // put item at rear of queue
	{
		if(rear == SIZE-1)
			rear = -1;
		queArray[++rear] = j;
	}
	public int remove() // take item from front of queue
	{
		int temp = queArray[front++];
		if(front == SIZE)
		front = 0;
		return temp;
	}
	public boolean isEmpty() // true if queue is empty
	{
		return ( rear+1==front || (front+SIZE-1==rear) );
	}
}

class GraphForBFS{
	private Vertex vertexList[]; 
	private int adjacencyMatrix[][]; 
	private int numberOfVertices;
	private Queue theQueue;
	public GraphForBFS(int MAX_VERTS){
		vertexList = new Vertex[MAX_VERTS];
		adjacencyMatrix = new int[MAX_VERTS][MAX_VERTS];
		numberOfVertices = 0;
		for(int j=0; j<MAX_VERTS; j++) // set adjacency
			for(int k=0; k<MAX_VERTS; k++) // matrix to 0
				adjacencyMatrix[j][k] = 0;
		theQueue = new Queue();
	}
	public void addVertex(char lab)
	{
		vertexList[numberOfVertices++] = new Vertex(lab);
	}
	public void addEdge(int start, int end)
	{
		adjacencyMatrix[start][end] = 1;
		adjacencyMatrix[end][start] = 1;
	}
	public void displayVertex(int v)
	{
		System.out.print(vertexList[v].label);
	}
	public void bfs() // breadth-first search
	{ // begin at vertex 0
		vertexList[0].wasVisited = true; // mark it
		displayVertex(0); // display it
		theQueue.insert(0); // insert at tail
		int v2;
		while( !theQueue.isEmpty() ) // until queue empty,
		{
			int v1 = theQueue.remove(); // remove vertex at head
			// until it has no unvisited neighbors
			while( (v2=getAdjUnvisitedVertex(v1)) != -1 )
			{ // get one,
				vertexList[v2].wasVisited = true; // mark it
				displayVertex(v2); // display it
				theQueue.insert(v2); // insert it
			} // end while
		} // end while(queue not empty)
		// queue is empty, so we�re done
		for(int j=0; j<numberOfVertices; j++) // reset flags
			vertexList[j].wasVisited = false;
	} // end bfs()
	public int getAdjUnvisitedVertex(int v)
	{
		for(int j=0; j<numberOfVertices; j++)
			if(adjacencyMatrix[v][j]==1 && vertexList[j].wasVisited==false)
					return j;
		return -1;
	} // end getAdjUnvisitedVertex()
}

public class BreadthFirstSearchAdjacencyMatrix {
	public static void main(String[] args) {
		GraphForBFS theGraph = new GraphForBFS(5);
		theGraph.addVertex('A'); // 0 (start for dfs)
		theGraph.addVertex('B'); // 1
		theGraph.addVertex('C'); // 2
		theGraph.addVertex('D'); // 3
		theGraph.addVertex('E'); // 4
		theGraph.addEdge(0, 1); // AB
		theGraph.addEdge(1, 2); // BC
		theGraph.addEdge(0, 3); // AD
		theGraph.addEdge(3, 4); // DE
		System.out.print("Visits: ");
		theGraph.bfs(); // breadth-first search
		System.out.println();
	}
}
