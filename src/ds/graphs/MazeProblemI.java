/*
 * There is a ball in a maze with empty spaces and walls. The ball can go through empty spaces by rolling 
 * up, down, left or right, but it won't stop rolling until hitting a wall. When the ball stops, it 
 * could choose the next direction.
Given the ball's start position, the destination and the maze, determine whether the ball could stop 
at the destination.
The maze is represented by a binary 2D array. 1 means the wall and 0 means the empty space. You may 
assume that the borders of the maze are all walls. The start and destination coordinates are 
represented by row and column indexes.
https://leetcode.com/problems/the-maze/description/
 */
package ds.graphs;

public class MazeProblemI {

	public static void main(String[] args) {
		int[][] maze={
				{0,0,1,0,0},
				{0,0,0,0,0},
				{0,0,0,1,0},
				{1,1,0,1,1},
				{0,0,0,0,0}
		};
		int[] start={0,4};
		int[] destination={4,4};
		System.out.println(hasPath(maze,start,destination)?"Path exists":"No path exists");
	}
	public static boolean hasPath(int[][] maze, int[] start, int[] destination) {
		boolean[][] visited=new boolean[maze.length][maze[0].length];
		return hasPathUtil(maze,start,destination,visited);
    }
	public static boolean hasPathUtil(int[][] maze, int[] start, int[] destination,boolean[][] visited) {
		if(visited[start[0]][start[1]])
			return false;
		if(start[0]==destination[0] && start[1]==destination[1])
			return true;
		visited[start[0]][start[1]]=true;
		int left=start[1]-1,right=start[1]+1,up=start[0]-1,down=start[0]+1;
		while(right<maze[0].length && maze[start[0]][right]==0)
			right++;
		if(hasPathUtil(maze, new int[]{start[0],right-1}, destination, visited))
			return true;
		while(left>=0 && maze[start[0]][left]==0)
			left--;
		if(hasPathUtil(maze, new int[]{start[0],left+1}, destination, visited))
			return true;
		while (up >= 0 && maze[up][start[1]] == 0) //up
            up--;
        if (hasPathUtil(maze, new int[] {up + 1, start[1]}, destination, visited))
            return true;
        while (down < maze.length && maze[down][start[1]] == 0) //down
        	down++;
        if (hasPathUtil(maze, new int[] {down - 1, start[1]}, destination, visited))
            return true;
		return false;
	}
}
