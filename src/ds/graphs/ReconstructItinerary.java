package ds.graphs;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Stack;

public class ReconstructItinerary {
	static LinkedList<String> route;
	static HashMap<String, PriorityQueue<String>> targets;
	public static void main(String[] args) {
		List<List<String>> tickets=new LinkedList<List<String>>();
		tickets.add(Arrays.asList(new String[]{"MUC","LHR"}));
		tickets.add(Arrays.asList(new String[]{"JFK","MUC"}));
		tickets.add(Arrays.asList(new String[]{"SFO","SJC"}));
		tickets.add(Arrays.asList(new String[]{"LHR","SFO"}));
		findItinerary(tickets);
		for(String r:route) {
			System.out.print(r+"\t");
		}
		tickets.clear();
		System.out.println();
		tickets.add(Arrays.asList(new String[]{"JFK","SFO"}));
		tickets.add(Arrays.asList(new String[]{"JFK","ATL"}));
		tickets.add(Arrays.asList(new String[]{"SFO","ATL"}));
		tickets.add(Arrays.asList(new String[]{"ATL","JFK"}));
		tickets.add(Arrays.asList(new String[]{"ATL","SFO"}));
		findItinerary(tickets);
		for(String r:route) {
			System.out.print(r+"\t");
		}
	}

	public static List<String> findItinerary(List<List<String>> tickets) {
		route=new LinkedList<>();
		targets=new HashMap<String, PriorityQueue<String>>();
		for(List<String> ticket:tickets) {
			targets.computeIfAbsent(ticket.get(0), k->new PriorityQueue<>()).add(ticket.get(1));
		}
		//visit("JFK");
		visitIterative("JFK");
		return route;
    }
	public static void visit(String airport) {
		while(targets.containsKey(airport) && !targets.get(airport).isEmpty()) {
			visit(targets.get(airport).poll());
		}
		route.add(0, airport);
	}
	public static void visitIterative(String airport) {
		Stack<String> stack=new Stack<>();
		stack.push(airport);
		while(!stack.isEmpty()) {
			while(targets.containsKey(stack.peek()) && !targets.get(stack.peek()).isEmpty()) {
				stack.push(targets.get(stack.peek()).poll());
			}
			route.add(0,stack.pop());
		}
	}
}
