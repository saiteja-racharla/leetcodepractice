package ds.graphs;

import java.util.LinkedList;
import java.util.Queue;

public class FriendCircles {

	public static void main(String[] args) {
		int[][] M={{1,1,0},
				{1,1,0},
				{0,0,1}};
		System.out.println(findCircleNumBFS(M));
	}
	public static int findCircleNumBFS(int[][] M) {
        Queue<Integer> q=new LinkedList<Integer>();
        boolean[] visited=new boolean[M.length];
        int count=0;
        for(int i=0;i<M.length;i++){
    		if(!visited[i]){
    			q.add(i);
    	        while(!q.isEmpty()){
    	        	int poll=q.poll();
    	        	visited[poll]=true;
    	        	for(int j=0;j<M.length;j++){
    	        		 if (M[poll][j] == 1 && !visited[j])
                             q.add(j);
    	        	}
    	        }
        		count++;
    		}
        }
        return count;
    }
}
