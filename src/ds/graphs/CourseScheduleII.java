/*
 * Leetcode-210
 * There are a total of n courses you have to take, labeled from 0 to n - 1.

Some courses may have prerequisites, 
for example to take course 0 you have to first take course 1, which is expressed as a pair: [0,1]
Given the total number of courses and a list of prerequisite pairs, return the ordering of courses 
you should take to finish all courses.

There may be multiple correct orders, you just need to return one of them. 
If it is impossible to finish all courses, return an empty array.

For example:
2, [[1,0]]
There are a total of 2 courses to take. To take course 1 you should have finished course 0. 
So the correct course order is [0,1]

4, [[1,0],[2,0],[3,1],[3,2]]
There are a total of 4 courses to take. To take course 3 you should have finished both courses 1 and 2. 
Both courses 1 and 2 should be taken after you finished course 0. 
So one correct course order is [0,1,2,3]. Another correct ordering is[0,2,1,3].
 */
package ds.graphs;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class CourseScheduleII {

	public static void main(String[] args) {
		CourseScheduleII ts=new CourseScheduleII();
		int[][] prerequisites={{5,1},{5,6},{5,4},{3,2},{2,1},{5,3},{7,5}};
		int numCourses=8;	//including zero
		int [] order = ts.findOrder(numCourses, prerequisites);
		if(order.length==0)
			System.out.println("No element to print. Cycle exists");
		for(int i:order) System.out.print(i+"\t");
	}
	
	public int[] findOrder(int numCourses, int[][] prerequisites) {
		ArrayList<ArrayList<Integer>> graph=new ArrayList<>();
		int[] indegree=new int[numCourses];
		int[] output=new int[numCourses];
		int index=0;
		for(int i=0;i<numCourses;i++) {
			graph.add(new ArrayList<>());
		}
		for(int i=0;i<prerequisites.length;i++) {
			graph.get(prerequisites[i][1]).add(prerequisites[i][0]);
			indegree[prerequisites[i][0]]++;
		}
		Queue<Integer> queue=new LinkedList<Integer>();
		boolean[] visited=new boolean[numCourses];
		for(int i=0;i<numCourses;i++) {
			if(indegree[i]==0) {
				queue.add(i);
				visited[i]=true;
			}
		}
		while(!queue.isEmpty()) {
			int course=queue.poll();
			output[index]=course;
			index++;
			for(int i=0;i<graph.get(course).size();i++) {
				int b=graph.get(course).get(i);
				indegree[b]--;
				if(indegree[b]==0) {
					queue.add(b);
					visited[b]=true;
				}
			}
		}
		for(boolean i:visited) {
			if(i==false)
				return new int[0];
		}
		return output;
    }
	
}
