package ds.graphs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CloneUndirectedGraph {

	public static void main(String[] args) {
		CloneUndirectedGraph c=new CloneUndirectedGraph();
		UndirectedGraphNode u=new UndirectedGraphNode(0);
		c.cloneUtil(u,new HashMap<>());
	}
	public UndirectedGraphNode cloneUtil(UndirectedGraphNode node, HashMap<Integer,UndirectedGraphNode> map){
		if(map.containsKey(node.label)){
			return map.get(node.label);
		}
		else{
			UndirectedGraphNode clone = new UndirectedGraphNode(node.label);
			map.put(node.label, clone);
			for(int i=0;i<clone.neighbors.size();i++){
				clone.neighbors.add(cloneUtil(node.neighbors.get(i), map));
			}
			return clone;
		}
	}
}

class UndirectedGraphNode{
	int label;
	List<UndirectedGraphNode> neighbors;
	UndirectedGraphNode(int x) { 
		label = x; 
		neighbors = new ArrayList<UndirectedGraphNode>(); 
	}
}
