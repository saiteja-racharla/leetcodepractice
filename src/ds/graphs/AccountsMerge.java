package ds.graphs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class AccountsMerge {
	public static void main(String[] args) {
		List<List<String>> accounts=Arrays.asList(
				Arrays.asList("John", "johnsmith@mail.com", "john00@mail.com"),
				Arrays.asList("John", "johnnybravo@mail.com"),
				Arrays.asList("John", "johnsmith@mail.com", "john_newyork@mail.com"),
				Arrays.asList("Mary", "mary@mail.com"));
		AccountsMerge ac=new AccountsMerge();
		List<List<String>> result=ac.accountsMerge(accounts);
		System.out.println(Arrays.toString(result.toArray()));
	}
	public List<List<String>> accountsMerge(List<List<String>> accounts) {
		HashMap<String,String> emailToNameLink=new HashMap<String,String>();
		HashMap<String,HashSet<String>> graph=new HashMap<String,HashSet<String>>();
		//Build the graph and the hashset
		for(List<String> account:accounts) {
			String name=account.get(0);
			for(int i=1;i<account.size();i++) {
				graph.putIfAbsent(account.get(i), new HashSet<>());
				emailToNameLink.put(account.get(i), name);
				if(i==1)
					continue;
				graph.get(account.get(i)).add(account.get(i-1));
				graph.get(account.get(i-1)).add(account.get(i));
			}
		}
		
		//BFS traversal to traverse all nodes
		List<List<String>> result=new ArrayList<List<String>>();
		Set<String> visited=new HashSet<String>();
		for(String email:graph.keySet()) {
			if(!visited.contains(email)) {
				visited.add(email);
				List<String> newList=bfs(graph,visited,email);
				Collections.sort(newList);
				newList.add(0, emailToNameLink.get(newList.get(0)));
				result.add(newList);
			}
		}
		
		return result;
    }
	public List<String> bfs(HashMap<String,HashSet<String>> graph, Set<String> visited, String email) {
		List<String> newList = new ArrayList<>();
        java.util.Queue<String> queue = new LinkedList<String>();
        queue.add(email);
        while(!queue.isEmpty()) {
        	int size=queue.size();
        	for(int i=0;i<size;i++) {
        		String curEmail=queue.poll();
        		newList.add(curEmail);
        		Set<String> neighbours=graph.get(curEmail);
        		for(String neighbour:neighbours) {
        			if(!visited.contains(neighbour)) {
        				queue.add(neighbour);
        				visited.add(neighbour);
        			}
        		}
        	}
        }
        return newList;
		
	}
}
