/*
 * 
 * Leetcode-207
 * There are a total of n courses you have to take, labeled from 0 to n - 1.

Some courses may have prerequisites, for example to take course 0 you have to first take course 1, 
which is expressed as a pair: [0,1]

Given the total number of courses and a list of prerequisite pairs, is it possible for you to 
finish all courses?

For example:

2, [[1,0]]
There are a total of 2 courses to take. To take course 1 you should have finished course 0.
So it is possible.

2, [[1,0],[0,1]]
There are a total of 2 courses to take. To take course 1 you should have finished course 0, and 
to take course 0 you should also have finished course 1. So it is impossible.
 */

package ds.graphs;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class CourseSchedule {

	public static void main(String[] args) {
		int[][] prerequisites={{5,1},{5,6},{5,4},{3,2},{2,1},{5,3},{7,5},{3,7}};
		System.out.println(canFinish(8, prerequisites));
		int arr[][]={{1,2},{2,3},{3,4},{3,1}};
		System.out.println(canFinish(5, arr));
		int arr2[][]={{1,2},{2,3},{3,4}};
		System.out.println(canFinish(5, arr2));
	}
	public static boolean canFinish(int numCourses, int[][] prerequisites) {
		ArrayList<ArrayList<Integer>> graph=new ArrayList<ArrayList<Integer>>();
		int[] indegree=new int[numCourses];
		
		  for(int i=0;i<numCourses;i++) { 
			  graph.add(new ArrayList<Integer>()); 
		  }
		 
		for(int i=0;i<prerequisites.length;i++) {
			graph.get(prerequisites[i][1]).add(prerequisites[i][0]);
			indegree[prerequisites[i][0]]++;
		}
		Queue<Integer> queue=new LinkedList<Integer>();
		boolean[] visited=new boolean[numCourses];
		for(int i=0;i<numCourses;i++) {
			if(indegree[i]==0) {
				queue.add(i);
				visited[i]=true;
			}
		}
		while(!queue.isEmpty()) {
			int course=queue.poll();
			for(int i=0;i<graph.get(course).size();i++) {
				int b=graph.get(course).get(i);
				indegree[b]--;
				if(indegree[b]==0) {
					queue.add(b);
					visited[b]=true;
				}
			}
		}
		for(boolean b:visited) {
			if(b==false)
				return false;
		}
		return true;
    }
}
