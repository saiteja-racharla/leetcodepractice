/*
 * walls and gates
 * You are given a m x n 2D grid initialized with these three possible values.
-1 - A wall or an obstacle.
0 - A gate.
INF - Infinity means an empty room. We use the value 231 - 1 = 2147483647 to represent INF as you 
may assume that the distance to a gate is less than 2147483647.
Fill each empty room with the distance to its nearest gate. If it is impossible to reach a gate, 
it should be filled with INF.
 */
package ds.graphs;

import java.util.LinkedList;
import java.util.Queue;

public class WallsAndGates {
	private static final int EMPTY = Integer.MAX_VALUE;
	private static final int GATE = 0;
	public static void main(String[] args) {
		int INF=Integer.MAX_VALUE;
		int[][] rooms={
				{INF,-1,0,INF},
				{INF,INF,INF,-1},
				{INF,-1,INF,-1},
				{0,-1,INF,INF}
		};
		wallsAndGates(rooms);
	}
	public static void wallsAndGates(int[][] rooms) {
		Queue<int[]> q=new LinkedList<int[]>();
        for(int i=0;i<rooms.length;i++){
        	for(int j=0;j<rooms[0].length;j++){
        		if(rooms[i][j]==GATE){
        			q.add(new int[]{i,j});
        		}
        	}
        }
        bfs(rooms,q);
        for(int i=0;i<rooms.length;i++){
	        for(int j=0;j<rooms[0].length;j++){
	    			System.out.print(rooms[i][j]+"\t");
	    	}
	        System.out.println();
        }
    }
	public static void bfs(int[][] rooms,Queue<int[]> q){
		int[][] dirs={{0,1},{0,-1},{1,0},{-1,0}};
		while(!q.isEmpty()){
			int[] polledEle=q.poll();
			for(int[] dir:dirs){
				int x=polledEle[0]+dir[0];
				int y=polledEle[1]+dir[1];
				if(x>=0 && y>=0 && x<rooms.length && y<rooms[0].length && rooms[x][y]==EMPTY){
					rooms[x][y]=rooms[polledEle[0]][polledEle[1]]+1;
					q.add(new int[]{x,y});
				}
			}
		}
	}

}
