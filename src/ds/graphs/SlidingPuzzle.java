/**
 * https://leetcode.com/problems/sliding-puzzle/
 * Leetcode-373
 * On a 2x3 board, there are 5 tiles represented by the integers 1 through 5, and an empty square represented by 0.
A move consists of choosing 0 and a 4-directionally adjacent number and swapping it.
The state of the board is solved if and only if the board is [[1,2,3],[4,5,0]].
Given a puzzle board, return the least number of moves required so that the state of the board is solved. If it is impossible for the state of the board to be solved, return -1.

Examples:
Input: board = [[1,2,3],[4,0,5]]
Output: 1
Explanation: Swap the 0 and the 5 in one move.

Input: board = [[1,2,3],[5,4,0]]
Output: -1
Explanation: No number of moves will make the board solved.
 */
package ds.graphs;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Queue;
import java.util.Set;

public class SlidingPuzzle {
	public static void main(String[] args) {
		SlidingPuzzle s=new SlidingPuzzle();
		int[][] board1=new int[][] {{1,2,3},{4,0,5}};
		System.out.println("Number of moves for board1 "+Arrays.deepToString(board1)+" is="+s.slidingPuzzle(board1));
		int[][] board2=new int[][] {{1,2,3},{5,4,0}};
		System.out.println("Number of moves for board2 "+Arrays.deepToString(board2)+" is="+s.slidingPuzzle(board2));
		int[][] board3=new int[][] {{4,1,2},{5,0,3}};
		System.out.println("Number of moves for board3 "+Arrays.deepToString(board3)+" is="+s.slidingPuzzle(board3));
	}
	public int slidingPuzzle(int[][] board) {
		int R=board.length,C=board[0].length;
		int sr=0,sc=0;
		source:
			for(sr=0;sr<R;sr++) {
				for(sc=0;sc<C;sc++) {
					if(board[sr][sc]==0)
						break source;
				}
			}
		int[][] dirs= {{1,0},{-1,0},{0,1},{0,-1}};
		Queue<SlidingPuzzleNode> queue=new ArrayDeque<>();
		Set<String> seen=new HashSet<>();
		SlidingPuzzleNode originalNode=new SlidingPuzzleNode(board, sr, sc, 0);
		queue.add(originalNode);
		seen.add(originalNode.boardstring);
		String target=Arrays.deepToString(new int[][] {{1,2,3},{4,5,0}});
		while(!queue.isEmpty()) {
			SlidingPuzzleNode node=queue.remove();
			if(node.boardstring.equals(target))
				return node.depth;
			for(int[] dir:dirs) {
				int new_r=node.row+dir[0];
				int new_c=node.col+dir[1];
				if((Math.abs(new_r-node.row)+Math.abs(new_c-node.col)!=1) ||
						new_r<0 || new_r>=R || new_c<0 || new_c>=C)
					continue;
				int[][] newboard=new int[R][C];
				int t=0;
				for(int[] row:node.board) {
					newboard[t++]=row.clone();
				}
				newboard[node.row][node.col]=newboard[new_r][new_c];
				newboard[new_r][new_c]=0;
				SlidingPuzzleNode newPuzzleBoard=new SlidingPuzzleNode(newboard, new_r, new_c, node.depth+1);
				if(seen.contains(newPuzzleBoard.boardstring))
					continue;
				queue.offer(newPuzzleBoard);
				seen.add(newPuzzleBoard.boardstring);
			}
		}
		return -1;
	}
}
class SlidingPuzzleNode{
	int[][] board;
	String boardstring;
	int row;
	int col;
	int depth;
	public SlidingPuzzleNode(int[][] board,int row,int col,int depth) {
		this.board=board;
		this.boardstring=Arrays.deepToString(board);
		this.row=row;
		this.col=col;
		this.depth=depth;
	}
}


