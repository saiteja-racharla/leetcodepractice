package ds.graphs;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class GraphForPrimsAlgorithm<T>{
	private List<EdgeForPrimsAlgorithm<T>> allEdges;
    private Map<Long,VertexForPrimsAlgorithm<T>> allVertex;
    boolean isDirected = false;
    
    public GraphForPrimsAlgorithm(boolean isDirected){
        allEdges = new ArrayList<EdgeForPrimsAlgorithm<T>>();
        allVertex = new HashMap<Long,VertexForPrimsAlgorithm<T>>();
        this.isDirected = isDirected;
    }
    
    public void addEdge(long id1, long id2){
        addEdge(id1,id2,0);
    }
    public void addEdge(long id1,long id2, int weight){
        VertexForPrimsAlgorithm<T> vertex1 = null;
        if(allVertex.containsKey(id1)){
            vertex1 = allVertex.get(id1);
        }else{
            vertex1 = new VertexForPrimsAlgorithm<T>(id1);
            allVertex.put(id1, vertex1);
        }
        VertexForPrimsAlgorithm<T> vertex2 = null;
        if(allVertex.containsKey(id2)){
            vertex2 = allVertex.get(id2);
        }else{
            vertex2 = new VertexForPrimsAlgorithm<T>(id2);
            allVertex.put(id2, vertex2);
        }

        EdgeForPrimsAlgorithm<T> edge = new EdgeForPrimsAlgorithm<T>(vertex1,vertex2,isDirected,weight);
        allEdges.add(edge);
        vertex1.addAdjacentVertex(edge, vertex2);
        if(!isDirected){
            vertex2.addAdjacentVertex(edge, vertex1);
        }

    }
    public List<EdgeForPrimsAlgorithm<T>> getAllEdges(){
        return allEdges;
    }
    
    public Collection<VertexForPrimsAlgorithm<T>> getAllVertex(){
        return allVertex.values();
    }
}
class EdgeForPrimsAlgorithm<T>{
	private boolean isDirected = false;
    private VertexForPrimsAlgorithm<T> vertex1;
    private VertexForPrimsAlgorithm<T> vertex2;
    private int weight;
    
    EdgeForPrimsAlgorithm(VertexForPrimsAlgorithm<T> vertex1, VertexForPrimsAlgorithm<T> vertex2){
        this.vertex1 = vertex1;
        this.vertex2 = vertex2;
    }
    EdgeForPrimsAlgorithm(VertexForPrimsAlgorithm<T> vertex1, VertexForPrimsAlgorithm<T> vertex2,boolean isDirected,int weight){
        this.vertex1 = vertex1;
        this.vertex2 = vertex2;
        this.weight = weight;
        this.isDirected = isDirected;
    }
    int getWeight(){
        return weight;
    }
    VertexForPrimsAlgorithm<T> getVertex1(){
        return vertex1;
    }
    
    VertexForPrimsAlgorithm<T> getVertex2(){
        return vertex2;
    }
}

class VertexForPrimsAlgorithm<T> {
	long id;
    private T data;
    private List<EdgeForPrimsAlgorithm<T>> edges = new ArrayList<>();
    private List<VertexForPrimsAlgorithm<T>> adjacentVertex = new ArrayList<>();
    
    VertexForPrimsAlgorithm(long id){
        this.id = id;
    }
    
    public long getId(){
        return id;
    }
    
    public void setData(T data){
        this.data = data;
    }
    
    public T getData(){
        return data;
    }
    
    public void addAdjacentVertex(EdgeForPrimsAlgorithm<T> e, VertexForPrimsAlgorithm<T> v){
        edges.add(e);
        adjacentVertex.add(v);
    }
    
    public String toString(){
        return String.valueOf(id);
    }
    
    public List<VertexForPrimsAlgorithm<T>> getAdjacentVertexes(){
        return adjacentVertex;
    }
    
    public List<EdgeForPrimsAlgorithm<T>> getEdges(){
        return edges;
    }
    
    public int getDegree(){
        return edges.size();
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        VertexForPrimsAlgorithm other = (VertexForPrimsAlgorithm) obj;
        if (id != other.id)
            return false;
        return true;
    }
}

class BinaryMinHeapForPrimsAlgorithm<T> {
	private List<Node> allNodes = new ArrayList<>();
    private Map<T,Integer> nodePosition = new HashMap<>();
	class Node {
        int weight;
        T key;
    }
	public boolean containsData(T key){
        return nodePosition.containsKey(key);
    }
	public Integer getWeight(T key) {
        Integer position = nodePosition.get(key);
        if( position == null ) {
            return null;
        } else {
            return allNodes.get(position).weight;
        }
    }
	private void swap(Node node1,Node node2){
        int weight = node1.weight;
        T data = node1.key;
        
        node1.key = node2.key;
        node1.weight = node2.weight;
        
        node2.key = data;
        node2.weight = weight;
    }
	private void updatePositionMap(T data1, T data2, int pos1, int pos2){
        nodePosition.remove(data1);
        nodePosition.remove(data2);
        nodePosition.put(data1, pos1);
        nodePosition.put(data2, pos2);
    }
	public void add(int weight,T key) {
        Node node = new Node();
        node.weight = weight;
        node.key = key;
        allNodes.add(node);
        int size = allNodes.size();
        int current = size - 1;
        int parentIndex = (current - 1) / 2;
        nodePosition.put(node.key, current);

        while (parentIndex >= 0) {
            Node parentNode = allNodes.get(parentIndex);
            Node currentNode = allNodes.get(current);
            if (parentNode.weight > currentNode.weight) {
                swap(parentNode,currentNode);
                updatePositionMap(parentNode.key,currentNode.key,parentIndex,current);
                current = parentIndex;
                parentIndex = (parentIndex - 1) / 2;
            } else {
                break;
            }
        }
    }
	public void decrease(T data, int newWeight){
        Integer position = nodePosition.get(data);
        allNodes.get(position).weight = newWeight;
        int parent = (position -1 )/2;
        while(parent >= 0){
            if(allNodes.get(parent).weight > allNodes.get(position).weight){
                swap(allNodes.get(parent), allNodes.get(position));
                updatePositionMap(allNodes.get(parent).key,allNodes.get(position).key,parent,position);
                position = parent;
                parent = (parent-1)/2;
            }else{
                break;
            }
        }
    }
	public boolean empty(){
        return allNodes.size() == 0;
    }
	public T extractMin(){
        Node node = extractMinNode();
        return node.key;
    }
	public Node extractMinNode() {
        int size = allNodes.size() -1;
        Node minNode = new Node();
        minNode.key = allNodes.get(0).key;
        minNode.weight = allNodes.get(0).weight;

        int lastNodeWeight = allNodes.get(size).weight;
        allNodes.get(0).weight = lastNodeWeight;
        allNodes.get(0).key = allNodes.get(size).key;
        nodePosition.remove(minNode.key);
        nodePosition.remove(allNodes.get(0));
        nodePosition.put(allNodes.get(0).key, 0);
        allNodes.remove(size);

        int currentIndex = 0;
        size--;
        while(true){
            int left = 2*currentIndex + 1;
            int right = 2*currentIndex + 2;
            if(left > size){
                break;
            }
            if(right > size){
                right = left;
            }
            int smallerIndex = allNodes.get(left).weight <= allNodes.get(right).weight ? left : right;
            if(allNodes.get(currentIndex).weight > allNodes.get(smallerIndex).weight){
                swap(allNodes.get(currentIndex), allNodes.get(smallerIndex));
                updatePositionMap(allNodes.get(currentIndex).key,allNodes.get(smallerIndex).key,currentIndex,smallerIndex);
                currentIndex = smallerIndex;
            }else{
                break;
            }
        }
        return minNode;
    }
}

public class PrimsAlgorithm {
	public List<EdgeForPrimsAlgorithm<Integer>> primMST(GraphForPrimsAlgorithm<Integer> graph){

        //binary heap + map data structure
		BinaryMinHeapForPrimsAlgorithm<VertexForPrimsAlgorithm<Integer>> minHeap = new BinaryMinHeapForPrimsAlgorithm<>();

        //map of vertex to edge which gave minimum weight to this vertex.
        Map<VertexForPrimsAlgorithm<Integer>,EdgeForPrimsAlgorithm<Integer>> vertexToEdge = new HashMap<>();

        //stores final result
        List<EdgeForPrimsAlgorithm<Integer>> result = new ArrayList<>();

        //insert all vertices with infinite value initially.
        for(VertexForPrimsAlgorithm<Integer> v : graph.getAllVertex()){
            minHeap.add(Integer.MAX_VALUE, v);
        }

        //start from any random vertex
        VertexForPrimsAlgorithm<Integer> startVertex = graph.getAllVertex().iterator().next();

        //for the start vertex decrease the value in heap + map to 0
        minHeap.decrease(startVertex, 0);

        //iterate till heap + map has elements in it
        while(!minHeap.empty()){
            //extract min value vertex from heap + map
            VertexForPrimsAlgorithm<Integer> current = minHeap.extractMin();

            //get the corresponding edge for this vertex if present and add it to final result.
            //This edge wont be present for first vertex.
            EdgeForPrimsAlgorithm<Integer> spanningTreeEdge = vertexToEdge.get(current);
            if(spanningTreeEdge != null) {
                result.add(spanningTreeEdge);
            }

            //iterate through all the adjacent vertices
            for(EdgeForPrimsAlgorithm<Integer> edge : current.getEdges()){
                VertexForPrimsAlgorithm<Integer> adjacent = getVertexForEdge(current, edge);
                //check if adjacent vertex exist in heap + map and weight attached with this vertex is greater than this edge weight
                if(minHeap.containsData(adjacent) && minHeap.getWeight(adjacent) > edge.getWeight()){
                    //decrease the value of adjacent vertex to this edge weight.
                    minHeap.decrease(adjacent, edge.getWeight());
                    //add vertex->edge mapping in the graph.
                    vertexToEdge.put(adjacent, edge);
                }
            }
        }
        return result;
    }

    private VertexForPrimsAlgorithm<Integer> getVertexForEdge(VertexForPrimsAlgorithm<Integer> v, EdgeForPrimsAlgorithm<Integer> e){
        return e.getVertex1().equals(v) ? e.getVertex2() : e.getVertex1();
    }
	public static void main(String[] args) {
		GraphForPrimsAlgorithm<Integer> graph = new GraphForPrimsAlgorithm<>(false);
	     /* graph.addEdge(0, 1, 4);
	        graph.addEdge(1, 2, 8);
	        graph.addEdge(2, 3, 7);
	        graph.addEdge(3, 4, 9);
	        graph.addEdge(4, 5, 10);
	        graph.addEdge(2, 5, 4);
	        graph.addEdge(1, 7, 11);
	        graph.addEdge(0, 7, 8);
	        graph.addEdge(2, 8, 2);
	        graph.addEdge(3, 5, 14);
	        graph.addEdge(5, 6, 2);
	        graph.addEdge(6, 8, 6);
	        graph.addEdge(6, 7, 1);
	        graph.addEdge(7, 8, 7);*/

	        graph.addEdge(1, 2, 3);
	        graph.addEdge(2, 3, 1);
	        graph.addEdge(3, 1, 1);
	        graph.addEdge(1, 4, 1);
	        graph.addEdge(2, 4, 3);
	        graph.addEdge(4, 5, 6);
	        graph.addEdge(5, 6, 2);
	        graph.addEdge(3, 5, 5);
	        graph.addEdge(3, 6, 4);

	        PrimsAlgorithm prims = new PrimsAlgorithm();
	        Collection<EdgeForPrimsAlgorithm<Integer>> edges = prims.primMST(graph);
	        for(EdgeForPrimsAlgorithm<Integer> edge : edges){
	            System.out.println(edge.getVertex1()+" "+edge.getVertex2());
        }
	}
}
