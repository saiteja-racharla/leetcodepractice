
/*
 * Depth first search
 */
package ds.graphs;

import java.util.Scanner;
import java.util.Stack;

class Vertex{
	public char label; 
	public boolean wasVisited;
	public Vertex(char lab)
	{
		label = lab;
		wasVisited = false;
	}
}

class Graph{
	private Vertex vertexList[]; 
	private int adjacencyMatrix[][]; 
	private int numberOfVertices;
	private Stack theStack;
	public Graph(int MAX_VERTS){
		vertexList = new Vertex[MAX_VERTS];
		adjacencyMatrix = new int[MAX_VERTS][MAX_VERTS];
		numberOfVertices = 0;
		for(int j=0; j<MAX_VERTS; j++)
			for(int k=0; k<MAX_VERTS; k++) 
				adjacencyMatrix[j][k] = 0;
		theStack = new Stack();
	}
	public void addVertex(char lab)
	{
		vertexList[numberOfVertices++] = new Vertex(lab);
	}
	
	public void addEdge(int start, int end)
	{
		adjacencyMatrix[start][end] = 1;
		adjacencyMatrix[end][start] = 1;
	}
	
	public void displayVertex(int v)
	{
		System.out.print(vertexList[v].label);
	}
	public int getAdjUnvisitedVertex(int v)
	{
		for(int j=0; j<numberOfVertices; j++){
			if(adjacencyMatrix[v][j]==1 && vertexList[j].wasVisited==false)
				return j; // return first such vertex
		}
		return -1; // no such vertices
	}
	public void dfs() // depth-first search
	{ // begin at vertex 0
		vertexList[0].wasVisited = true; // mark it
		displayVertex(0); // display it
		theStack.push(0); // push it
		while( !theStack.isEmpty() ) // until stack empty,
		{
			// get an unvisited vertex adjacent to stack top
			int v = getAdjUnvisitedVertex( (int)theStack.peek() );
			if(v == -1) // if no such vertex,
				theStack.pop(); // pop a new one
			else // if it exists,
			{
				vertexList[v].wasVisited = true; // mark it
				displayVertex(v); // display it
				theStack.push(v); // push it
			}
		} // end while
		// stack is empty, so we�re done
		for(int j=0; j<numberOfVertices; j++) // reset flags
			vertexList[j].wasVisited = false;
	} // end dfs
	public void mst() // minimum spanning tree (depth first)
	{ // start at 0
		vertexList[0].wasVisited = true; // mark it
		theStack.push(0); // push it
		while( !theStack.isEmpty() ) // until stack empty
		{ // get stack top
			int currentVertex = (int)theStack.peek();
			// get next unvisited neighbor
			int v = getAdjUnvisitedVertex(currentVertex);
			if(v == -1) // if no more neighbors
				theStack.pop(); // pop it away
			else // got a neighbor
			{
				vertexList[v].wasVisited = true; // mark it
				theStack.push(v); // push it
				// display edge
				displayVertex(currentVertex); // from currentV
				displayVertex(v); // to v
				System.out.print(" ");
			}
		} // end while(stack not empty)
		// stack is empty, so we�re done
		for(int j=0; j<numberOfVertices; j++) // reset flags
			vertexList[j].wasVisited = false;
	} // end tree
}

public class DepthFirstSearchAdjacencyMatrix {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		Graph theGraph = new Graph(5);
		theGraph.addVertex('A'); // 0 (start for dfs)
		theGraph.addVertex('B'); // 1
		theGraph.addVertex('C'); // 2
		theGraph.addVertex('D'); // 3
		theGraph.addVertex('E'); // 4
		theGraph.addEdge(0, 1); // AB
		theGraph.addEdge(1, 2); // BC
		theGraph.addEdge(0, 3); // AD
		theGraph.addEdge(3, 4); // DE
		System.out.print("Visits: ");
		theGraph.dfs(); // depth-first search
		System.out.println();
	}

}
