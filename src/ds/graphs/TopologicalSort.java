/*
 * topological sort.
 * Space and time complexity is O(V+E)
 */
package ds.graphs;

class VertexForTopologicalSort{
	public char label; // label (e.g. �A�)
	public VertexForTopologicalSort(char lab) // constructor
	{ 
		label = lab; 
	}
}

class GraphForTopologicalSort{
	private Vertex vertexList[]; // list of vertices
	private int adjacencyMatrix[][]; // adjacency matrix
	private int numberOfVertices; // current number of vertices
	private char sortedArray[];
	public GraphForTopologicalSort(int MAX_VERTS) // constructor
	{
		vertexList = new Vertex[MAX_VERTS];
		// adjacency matrix
		adjacencyMatrix = new int[MAX_VERTS][MAX_VERTS];
		numberOfVertices = 0;
		for(int j=0; j<MAX_VERTS; j++) // set adjacency
			for(int k=0; k<MAX_VERTS; k++) // matrix to 0
				adjacencyMatrix[j][k] = 0;
		sortedArray = new char[MAX_VERTS]; // sorted vert labels
	} // end constructor
	public void addVertex(char lab)
	{
		vertexList[numberOfVertices++] = new Vertex(lab);
	}
	public void addEdge(int start, int end)
	{
		adjacencyMatrix[start][end] = 1;
	}
	public void displayVertex(int v)
	{
		System.out.print(vertexList[v].label);
	}
	public void topo() // topological sort
	{
		int orig_nVerts = numberOfVertices; // remember how many verts
		while(numberOfVertices > 0) // while vertices remain,
		{
			// get a vertex with no successors, or -1
			int currentVertex = noSuccessors();
			if(currentVertex == -1) // must be a cycle
			{
				System.out.println("ERROR: Graph has cycles");
				return;
			}
			// insert vertex label in sorted array (start at end)
			sortedArray[numberOfVertices-1] = vertexList[currentVertex].label;
			deleteVertex(currentVertex); // delete vertex
		} // end while
		// vertices all gone; display sortedArray
		System.out.print("Topologically sorted order: ");
		for(int j=0; j<orig_nVerts; j++)
			System.out.print( sortedArray[j] );
		System.out.println("");
	} // end topo
	/****************************************************************/
	public int noSuccessors() // returns vert with no successors
	{ // (or -1 if no such verts)
		boolean isEdge; // edge from row to column in adjMat
		for(int row=0; row<numberOfVertices; row++) // for each vertex,
		{
			isEdge = false; // check edges
			for(int col=0; col<numberOfVertices; col++)
			{
				if( adjacencyMatrix[row][col] > 0 )
				{ 
					isEdge = true;
					break; // this vertex has a successor
				}
			} // try another
			if( !isEdge ) // if no edges,
				return row; // has no successors
		}
		return -1; // no such vertex
	} // end noSuccessors()
	
	public void deleteVertex(int delVert)
	{
		if(delVert != numberOfVertices-1) // if not last vertex,
		{ // delete from vertexList
			for(int j=delVert; j<numberOfVertices-1; j++)
				vertexList[j] = vertexList[j+1];
			// delete row from adjMat
			for(int row=delVert; row<numberOfVertices-1; row++)
				moveRowUp(row, numberOfVertices);
			// delete col from adjMat
			for(int col=delVert; col<numberOfVertices-1; col++)
				moveColLeft(col, numberOfVertices-1);
		}
		numberOfVertices--; // one less vertex
	} // end deleteVertex
	private void moveRowUp(int row, int length)
	{
		for(int col=0; col<length; col++)
			adjacencyMatrix[row][col] = adjacencyMatrix[row+1][col];
	}
	// -------------------------------------------------------------
	private void moveColLeft(int col, int length)
	{
		for(int row=0; row<length; row++)
			adjacencyMatrix[row][col] = adjacencyMatrix[row][col+1];
	}
	
}

public class TopologicalSort {
	public static void main(String[] args)
	{
		GraphForTopologicalSort theGraph = new GraphForTopologicalSort(8);
		theGraph.addVertex('A'); // 0
		theGraph.addVertex('B'); // 1
		theGraph.addVertex('C'); // 2
		theGraph.addVertex('D'); // 3
		theGraph.addVertex('E'); // 4
		theGraph.addVertex('F'); // 5
		theGraph.addVertex('G'); // 6
		theGraph.addVertex('H'); // 7
		theGraph.addEdge(0, 3); // AD
		theGraph.addEdge(0, 4); // AE
		theGraph.addEdge(1, 4); // BE
		theGraph.addEdge(2, 5); // CF
		theGraph.addEdge(3, 6); // DG
		theGraph.addEdge(4, 6); // EG
		theGraph.addEdge(5, 7); // FH
		theGraph.addEdge(6, 7); // GH
		theGraph.topo(); // do the sort
	} // end class TopoApp
}
