/*
 * Design a data structure that supports all following operations in average O(1) time.

	insert(val): Inserts an item val to the set if not already present.
	remove(val): Removes an item val from the set if present.
	getRandom: Returns a random element from current set of elements. Each element must have 
	the same probability of being returned.
	
	insertion
	hashmap				list		(hashmap value means position in the list)
	1,0					1
	4,1					1,4
	2,2					1,4,2
	3,3					1,4,2,3
	
	Deletion(removing element 4)
	hashmap				list
	1,0					1
	4,1					4
	2,2					2
	3,1					3			
	now we remove 3 from both hashmap and list
	
 */
package ds.advanced.datastructures;
import java.util.*;
public class InsertDeleteGetRandom {
	public static void main(String[] args) {
		RandomizedSet res=new RandomizedSet();
		res.insert(1);
		res.insert(4);
		res.insert(2);
		res.insert(3);
		System.out.println(res.remove(4));
	}
}

class RandomizedSet {
    ArrayList<Integer> nums;
    HashMap<Integer, Integer> locs;
    java.util.Random rand = new java.util.Random();
    /** Initialize your data structure here. */
    public RandomizedSet() {
        nums = new ArrayList<Integer>();
        locs = new HashMap<Integer, Integer>();
    }
    
    /** Inserts a value to the set. Returns true if the set did not already contain the specified element. */
    public boolean insert(int val) {
        boolean contain = locs.containsKey(val);
        if ( contain ) return false;
        locs.put( val, nums.size());
        nums.add(val);
        return true;
    }
    
    /** Removes a value from the set. Returns true if the set contained the specified element. */
    public boolean remove(int val) {
        boolean contain = locs.containsKey(val);
        if ( ! contain ) return false;
        int loc = locs.get(val);
        if (loc < nums.size() - 1 ) { // not the last one than swap the last one with this val
            int lastone = nums.get(nums.size() - 1 );
            nums.set( loc , lastone );
            locs.put(lastone, loc);
        }
        locs.remove(val);
        nums.remove(nums.size() - 1);
        return true;
    }
    
    /** Get a random element from the set. */
    public int getRandom() {
        return nums.get( rand.nextInt(nums.size()) );
    }
}