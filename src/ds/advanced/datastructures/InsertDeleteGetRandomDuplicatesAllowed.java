package ds.advanced.datastructures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class InsertDeleteGetRandomDuplicatesAllowed {

	public static void main(String[] args) {
		InsertDeleteGetRandomOperations operations=new InsertDeleteGetRandomOperations();
		operations.insert(1);
		operations.insert(1);
		operations.insert(2);
		System.out.println(operations.getRandom());
		operations.remove(1);
		System.out.println(operations.getRandom());
		operations.remove(1);
		System.out.println(operations.getRandom());
	}

}
class InsertDeleteGetRandomOperations{
	List<Integer> list;
	HashMap<Integer, Set<Integer>> hm;
	Random random;
	 /** Initialize your data structure here. */
    public InsertDeleteGetRandomOperations() {
    	list=new ArrayList<>();
    	hm=new HashMap<>();
    	random=new Random();
    }
    
    /** Inserts a value to the collection. Returns true if the collection did not already contain the specified element. */
    public boolean insert(int val) {
    	boolean contains=false;
        if(!hm.containsKey(val)) {
        	contains= true;
        	hm.put(val, new HashSet<>());
        }
    	list.add(val);
    	hm.get(val).add(list.size()-1);
    	return contains;
    }
    
    /** Removes a value from the collection. Returns true if the collection contained the specified element. */
    public boolean remove(int val) {
        if(!hm.containsKey(val) || list.size()==0) {
        	return false;
        }
        int remove_index=hm.get(val).iterator().next();
        hm.get(val).remove(remove_index);
        int lastEle=list.get(list.size()-1);
        int lastIndex=list.size()-1;
        list.set(remove_index, lastEle);
        hm.get(lastEle).add(remove_index);
        hm.get(lastEle).remove(lastIndex);
        list.remove(list.size()-1);
        return true;
    }
    
    /** Get a random element from the collection. */
    public int getRandom() {
        return list.get(random.nextInt(list.size()));
    }
}
