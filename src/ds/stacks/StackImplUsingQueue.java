/*
 * Implement Stack using one queue
 * Time complexity-push-O(n)
 * 					pop-O(1)
 */

package ds.stacks;

import java.util.LinkedList;
import java.util.Queue;

public class StackImplUsingQueue {
	static Queue<Integer> queue=new LinkedList<Integer>();
	public static void main(String[] args){
		push(1);
		push(2);
		push(3);
	}
	public static void push(int element){
		if(queue.isEmpty()){
			queue.add(element);
		}
		else{
			queue.add(element);
			for(int i=1;i<queue.size();i++){
				queue.add(queue.poll());
			}
		}
		
	}
	public static int pop(){
		return queue.poll();
	}
	public static int top(){
		return queue.peek();
		
	}
	public static boolean isEmpty(){
		return queue.isEmpty();
		
	}
}
