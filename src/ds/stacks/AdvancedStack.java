/*
 * This program performs normal performs of stack like push,pop etc., with one additional
 * capability getMinimum().
 * getMinimum() returns minimum element from the stack at any point of time.
 * Time complexity for getMinimum() is O(1) and space complexity is O(1)
 * 
 * while insertion,
 * if number is less than minElement, minElement becomes 2*element-minElement
 * 
 * while popping, Get the top element of stack
 * If top element from stack is less than minElement, minElement becomes 2*minElement-topElement
 * 
 */

package ds.stacks;

public class AdvancedStack {
	static int minElement=0;
	static int[] stack=new int[10];
	static int top=0;
	public static void main(String[] args) {
		push(5);
        push(3);
        getMinimum();
        push(1);
        push(5);
        getMinimum();
        pop();
        getMinimum();
        pop();
        peek();
	}
	public static void push(int element){
		if (top==0){
			minElement=element;
			stack[top++]=element;
		}
		else if(element<minElement){
			stack[top++]=2*element-minElement;
			minElement=element;
		}
		else
			stack[top++]=element;
		System.out.println("Pushed element is "+element);
	}
	public static int pop(){
		if (top==0)
        {
            System.out.println("Stack is empty");
        }
		System.out.print("Top Most Element Removed: ");
        Integer t = stack[top--];
 
        // Minimum will change as the minimum element
        // of the stack is being removed.
        if (t < minElement)
        {
        	System.out.println(minElement);
            minElement = 2*minElement - t;
            return minElement;
        }
        else{
        	System.out.println(t);
            return t;
        }
	}
	public static void peek(){
		System.out.println("top most element is "+stack[top-1]);
	}
	public static void getMinimum(){
		System.out.println("Min Element is "+minElement);
	}
}
