/*
 * This program calculates the maximum rectangular area in histogram.
 * Time complexity-O(n)
 * Space complexity-Size of stack
 * 
 * Algorithm
 * 1. If stack is empty, push the element on to stack
 * 2. If stack is not empty and element on stack is less than current element, push on to stack.
 * 3. If stack is not empty and element on stack is greater than current element, pop from stack until
 * we encounter an element that is less than or equal to current element.
 * Then we calculate area. If maximum area is less than current area, maxarea becomes current area
 */

package ds.stacks;

import java.util.Stack;

public class MaxRectangularAreaInHistogram {
	public static int findMaxAreaInHistogram(int[] arr){
		Stack<Integer> stack=new Stack<Integer>();
		int maxArea=-1;
		int area=0;
		int i=0;
		while(i<arr.length){
			if(stack.isEmpty() || arr[i]>=arr[stack.peek()]){
				stack.push(i++);
			}
			else{
				int top=stack.pop();
				if(stack.isEmpty()){
					area=arr[top]*i;
				}
				else{
					area=arr[top]*(i-stack.peek()-1);
				}
				if(maxArea<area){
					maxArea=area;
				}
			}
		}
		while(!stack.isEmpty()){
			int top=stack.pop();
			if(stack.isEmpty()){
				area=arr[top]*i;
			}
			else{
				area=arr[top]*(i-stack.peek()-1);
			}
			if(maxArea<area){
				maxArea=area;
			}
		}
		return maxArea;
	}
	public static void main(String[] args) {
		int arr[]={1,2,1,5,6,2,3};
//		int arr[]={3,4,1,4,5};
		System.out.println("Maximum rectangle area is "+findMaxAreaInHistogram(arr));
	}
}
