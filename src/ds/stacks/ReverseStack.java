package ds.stacks;

import java.util.Stack;

public class ReverseStack {

	public static void main(String[] args) {
		Stack<Character> st = new Stack<>();
		st.push('s');
		st.push('a');
		st.push('i');
		st.push('t');
		st.push('e');
		st.push('j');
		st.push('a');
		System.out.println("Before reverse");
		System.out.println(st);
		reverse(st);
		System.out.println("After reverse");
		System.out.println(st);
	}
	public static void reverse(Stack st){
		if(st.isEmpty()){
			return;
		}
		char c=(char) st.pop();
		reverse(st);
		insertAtBottom(st,c);
	}
	public static void insertAtBottom(Stack st, char c){
		if(st.isEmpty()){
			st.push(c);
			return;
		}
		char str=(char) st.pop();
		insertAtBottom(st, c);
		st.push(str);
	}
}
