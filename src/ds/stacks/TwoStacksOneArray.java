package ds.stacks;

import java.util.EmptyStackException;

public class TwoStacksOneArray {
	int a[];
	int topOne=0;
	int topTwo=0;
	int size=0;
	public static void main(String[] args) {
		TwoStacksOneArray obj=new TwoStacksOneArray();
		obj.initialize(30);
		obj.push(1, 20);
		obj.push(1, 21);
		obj.push(2, 22);
		obj.push(2, 23);
		System.out.println("popped element from 1st stack "+obj.pop(1));
		System.out.println("popped element from 1st stack "+obj.pop(1));
		System.out.println("popped element from 1st stack "+obj.pop(1));
		System.out.println("popped element from 2nd stack "+obj.pop(2));
		System.out.println("popped element from 2nd stack "+obj.pop(2));
		System.out.println("popped element from 2nd stack "+obj.pop(2));
	}
	public void initialize(int size){
		this.size=size;
		a=new int[size];
		topOne=-1;
		topTwo=size;
	}
	public void push(int stackID,int data){
		if(topTwo==topOne+1){
			throw new StackOverflowError("Array is full");
		}
		if(stackID==1){
			a[++topOne]=data;
		}
		else if(stackID==2){
			a[--topTwo]=data;
		}
		else
			return;
	}
	public int pop(int stackID){
		int data = 0;
		if(stackID==1){
			if(topOne==-1) {
				System.out.println("First stack is empty");
				throw new EmptyStackException();
			}
			data=a[topOne];
			a[topOne]=0;
			topOne=topOne-1;
		}
		else if(stackID==2){
			if(topTwo==size){
				System.out.println("Second stack is empty");
				throw new EmptyStackException();
			}
			data=a[topTwo];
			a[topTwo]=0;
			topTwo+=1;
		}
		return data;
	}
	public boolean isEmpty(int stackID){
		if(stackID==1){
			return topOne==-1;
		}
		else if(stackID==2){
			return topTwo==this.size;
		}
		else
			return true;
	}
}
