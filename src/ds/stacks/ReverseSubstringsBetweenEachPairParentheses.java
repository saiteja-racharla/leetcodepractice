/**
 * Reverse Substrings Between Each Pair of Parentheses
 * You are given a string s that consists of lower case English letters and brackets. 

Reverse the strings in each pair of matching parentheses, starting from the innermost one.

Your result should not contain any brackets.
Example 1:
Input: s = "(abcd)"
Output: "dcba"

Example 2:
Input: s = "(u(love)i)"
Output: "iloveu"
Explanation: The substring "love" is reversed first, then the whole string is reversed.

Example 3:
Input: s = "(ed(et(oc))el)"
Output: "leetcode"
Explanation: First, we reverse the substring "oc", then "etco", and finally, the whole string.

Example 4:
Input: s = "a(bcdefghijkl(mno)p)q"
Output: "apmnolkjihgfedcbq"
https://leetcode.com/problems/reverse-substrings-between-each-pair-of-parentheses/description/
 */
package ds.stacks;

import java.util.Stack;

public class ReverseSubstringsBetweenEachPairParentheses {

	public static void main(String[] args) {

		System.out.println("(abcd)----"+reverseParentheses("(abcd)"));
		System.out.println("(u(love)i)----"+reverseParentheses("(u(love)i)"));
		System.out.println("(ed(et(oc))el)----"+reverseParentheses("(ed(et(oc))el)"));

		System.out.println("a(bcdefghijkl(mno)p)q----"+reverseParentheses("a(bcdefghijkl(mno)p)q"));
		System.out.println("(abcd((x(a)yz)qr))----"+reverseParentheses("(abcd((x(a)yz)qr))"));
		System.out.println("yfgnxf----"+reverseParentheses("yfgnxf"));
		System.out.println("ta()usw((((a))))----"+reverseParentheses("ta()usw((((a))))"));

	}
	public static String reverseParentheses(String s) {
		Stack<String> st=new Stack<>();
		String temp="";
		for(int i=0;i<s.length();i++) {
			if(s.charAt(i)=='(') {
				if(temp.length()>0) {
					st.push(temp);
					temp="";
				}
				st.push("(");
			}
			else if(s.charAt(i)==')') {
				if(temp.length()>0) {
					st.push(temp);
					temp="";
				}
				String currTemp="";
				while(!st.peek().equals("(")) {
					currTemp+=reverseString(st.pop());
				}
				st.pop();
				if(currTemp.length()>0)
					st.push(currTemp);
			} else {
				temp=temp+s.charAt(i);
			}
		}
		String result="";
		while(!st.isEmpty()) {
			 result+=reverseString(st.pop());
		}
		st.push(reverseString(result));
		return (st.isEmpty()?"":st.pop())+temp;
	}
	public static String reverseString(String s) {
		StringBuffer temp=new StringBuffer();
		for(int i=s.length()-1;i>=0;i--) {
			temp.append(s.charAt(i));
		}
		return temp.toString();
	}

	/*
	 * public static String reverseParentheses(String s) { Stack<String> st=new
	 * Stack<>(); int i=0; String temp=""; String pre=""; while(i<s.length() &&
	 * s.charAt(i)!='(') { pre=pre+s.charAt(i); i++; } while(i<s.length()) {
	 * if(s.charAt(i)=='(') { if(temp.length()>0) { st.push(temp); temp=""; } } else
	 * if(s.charAt(i)==')') { if(temp.length()>0) { if(i+1>s.length()-1) {
	 * st.push(temp);temp=""; break; } st.push(reverseString(temp)); temp=""; } else
	 * { String firstPoppedEleRev=reverseString(st.pop()); String
	 * secondPoppedEleRev=reverseString(st.pop());
	 * st.push(firstPoppedEleRev+secondPoppedEleRev); } } else {
	 * temp=temp+s.charAt(i); } i++; } String result=""; while(!st.isEmpty()) {
	 * result+=reverseString(st.pop()); } return pre+result+temp; }
	 */
}
