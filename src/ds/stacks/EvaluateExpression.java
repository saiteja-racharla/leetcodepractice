/*output
((([]){}))
Correct expression
[][
wrong expression
*/
package ds.stacks;

import java.util.Scanner;
import java.util.Stack;

public class EvaluateExpression {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter expressions");
		String st=sc.nextLine();
		boolean result=checkExpressions(st);
		if(result==true)
			System.out.println("Correct expression");
		else
			System.out.println("Wrong expression");
		if(sc!=null)
			sc.close();
	}
	public static boolean checkExpressions(String st){
		Stack<Character> input = new Stack<Character>();
		boolean result=false;
		for(int i=0;i<st.length();i++){
			if(st.charAt(i)=='{' || st.charAt(i)=='(' || st.charAt(i)=='[')
				input.push(st.charAt(i));
			else if(st.charAt(i)=='}' || st.charAt(i)==')' || st.charAt(i)==']'){
				if(input.isEmpty()){
					result=false;
					break;
				}
				else{
					result=checkPriority(input.pop(),st.charAt(i));
					if(result==false)
						break;
				}
			}
		}
		if(input.size()>0)
			return false;
		return result;
	}
	public static boolean checkPriority(char ch1,char ch2){
		if(ch1=='(' && ch2==')')
			return true;
		else if(ch1=='{' && ch2=='}')
			return true;
		else if(ch1=='[' && ch2==']')
			return true;
		else 
			return false;
	}

}
