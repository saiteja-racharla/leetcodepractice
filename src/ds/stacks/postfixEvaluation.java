/*input  123+*
	output	5
 * This program evaluates the postfix expression
 */
package ds.stacks;

import java.util.Scanner;
import java.util.Stack;

public class postfixEvaluation {

	public static void main(String[] args) {
		Stack stack=new Stack();
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter expressions");
		String st=sc.nextLine();
		for(int i=0;i<st.length();i++){
			if(Character.isDigit(st.charAt(i))){
				stack.push(Integer.parseInt(String.valueOf(st.charAt(i))));
			}
			else{
				int operand1=(int)stack.pop();
				int operand2=(int)stack.pop();
				int result=calculateOperation(operand1,operand2,st.charAt(i));
				stack.push(result);
			}
		}
		System.out.println(stack.pop());
		if(sc!=null)
			sc.close();

	}
	private static int calculateOperation(int operand2, int operand1, char charAt) {
		if(charAt=='+')
			return operand1+operand2;
		else if(charAt=='-')
			return operand1-operand2;
		else if(charAt=='*')
			return operand1*operand2;
		else if(charAt=='/')
			return operand1/operand2;
		return 0;
	}

}
