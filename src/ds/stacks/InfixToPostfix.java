/**
 * This program converts infix expression to postfix expression
 */

package ds.stacks;

import java.util.Scanner;
import java.util.Stack;

public class InfixToPostfix {
	static Stack<Character> output=new Stack<Character>();
	static Stack<Character> operator=new Stack<Character>();
	public static void main(String[] args) {
		operator.push('#');
		char ch,temp;
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter expressions");
		String st=sc.nextLine();
		for(int i=0;i<st.length();i++){
			if(Character.isLetter(st.charAt(i)))
					output.push(st.charAt(i));
			else{
				ch=st.charAt(i);
				switch(ch){
				case '+':
				case '-':check(ch);
					operator.push(ch);
					break;
				case '*':
				case '/':check(ch);
					operator.push(ch);
					break;
				case '(':operator.push(ch);break;
				case ')':
					temp=operator.pop();
					while(temp!='('){
						output.push(temp);
						temp=operator.pop();
					}break;
				}
			}
		}
		while(operator.peek()!='#'){
			output.push(operator.pop());
		}
		while(!output.isEmpty()){
			System.out.print(output.pop()+" ");
		}
		if(sc!=null)
			sc.close();
	}
	public static void check(char ch){
		/*If operator on stack has equal/higher priority than input character, then pop the  
		character from stack and push it to output*/
		if(priority((char) operator.peek())>=priority(ch)){
			output.push(operator.pop());
		}
	}
	public static void checkPriority(char ch){
		
	}
	public static int priority(char ch){
		if(ch=='+' || ch=='-')
			return 1;
		else if(ch=='*' || ch=='/')
			return 2;
		else if(ch=='#')
			return 0;
		else 
			return 0;
	}
}
