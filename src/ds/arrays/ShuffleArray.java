package ds.arrays;

import java.util.Random;

public class ShuffleArray {
	static int[] arr={4,6,8,9,1,4,10,11,45,10};
	public static void main(String[] args) {
		int[] pos = shuffle();
		for(int i=0;i<arr.length;i++)
			System.out.print(pos[i]+"\t");
	}
	public static int[] shuffle(){
		int rand[]=arr.clone();
		Random random=new Random();
		for(int i=1;i<rand.length;i++){
			int j=random.nextInt(i+1);	//nextInt returns random value from 0 to i(i is included). 
			//thats the reason we use i+1 as end
			swap(rand,i,j);
		}
		return rand;
	}
	public static int[] reset(){
		return arr;
	}
	
	public static void swap(int[] rand,int i,int j){
		int temp=rand[i];
		rand[i]=rand[j];
		rand[j]=temp;
	}
}
