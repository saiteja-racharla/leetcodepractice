/*
 * Given two integers n and k, return all possible combinations of k numbers out of 1 ... n.
   For example,
   If n = 4 and k = 2, a solution is:
   	[
	  [2,4],
	  [3,4],
	  [2,3],
	  [1,2],
	  [1,3],
	  [1,4],
	]
 */
package ds.arrays;

import java.util.ArrayList;
import java.util.List;

public class Combinations {

	public static void main(String[] args) {
		Combinations c=new Combinations();
		List<List<Integer>> ls = c.combine(4, 2);
		for(List<Integer> temp:ls){
			System.out.println(temp);
		}
	}
	public List<List<Integer>> combine(int n, int k) {
		int[] arr=new int[4];
		for(int i=0;i<arr.length;i++){
			arr[i]=i+1;
		}
		List<Integer> temp=new ArrayList<Integer>();
		List<List<Integer>> result=new ArrayList<List<Integer>>();
		combineUtil(arr, k, 0, 0,temp,result);
		return result;
    }
	public void combineUtil(int[] arr, int k, int arrayLength, int startIndex,List<Integer> temp,List<List<Integer>> result){
		if(arrayLength==k){
			result.add(new ArrayList<>(temp));
			return;
		}
		for(int i=startIndex;i<arr.length;i++){
			temp.add(arr[i]);
			combineUtil(arr, k, arrayLength+1, i+1, temp, result);
			temp.remove(temp.size()-1);
		}
	}
}
