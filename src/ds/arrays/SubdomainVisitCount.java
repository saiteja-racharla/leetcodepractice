/**
 * A website domain like "discuss.leetcode.com" consists of various subdomains. At the top level, we have "com", at the next level, we have "leetcode.com", and at the lowest level, "discuss.leetcode.com". When we visit a domain like "discuss.leetcode.com", we will also visit the parent domains "leetcode.com" and "com" implicitly.
Now, call a "count-paired domain" to be a count (representing the number of visits this domain received), followed by a space, followed by the address. An example of a count-paired domain might be "9001 discuss.leetcode.com".
We are given a list cpdomains of count-paired domains. We would like a list of count-paired domains, (in the same format as the input, and in any order), that explicitly counts the number of visits to each subdomain.
Example 1:
Input: 
["9001 discuss.leetcode.com"]
Output: 
["9001 discuss.leetcode.com", "9001 leetcode.com", "9001 com"]
Explanation: 
We only have one website domain: "discuss.leetcode.com". As discussed above, the subdomain "leetcode.com" and "com" will also be visited. So they will all be visited 9001 times.

Example 2:
Input: 
["900 google.mail.com", "50 yahoo.com", "1 intel.mail.com", "5 wiki.org"]
Output: 
["901 mail.com","50 yahoo.com","900 google.mail.com","5 wiki.org","5 org","1 intel.mail.com","951 com"]
Explanation: 
We will visit "google.mail.com" 900 times, "yahoo.com" 50 times, "intel.mail.com" once and "wiki.org" 5 times. For the subdomains, we will visit "mail.com" 900 + 1 = 901 times, "com" 900 + 50 + 1 = 951 times, and "org" 5 times.
 */
package ds.arrays;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubdomainVisitCount {
	public static void main(String[] args) {
		SubdomainVisitCount sb=new SubdomainVisitCount();
		String[] cpdomains= {"9001 discuss.leetcode.com"};
		String[] cpdomains1= {"900 google.mail.com", "50 yahoo.com", "1 intel.mail.com", "5 wiki.org"};
		List<String> cpdomainsCount1=sb.subdomainVisits(cpdomains);
		List<String> cpdomainsCount2=sb.subdomainVisits(cpdomains1);
		cpdomainsCount1.forEach(System.out::println);
		cpdomainsCount2.forEach(System.out::println);
		System.out.println("*********************************************");
		List<String> cpdomainsCount3=sb.subdomainVisits1(cpdomains);
		List<String> cpdomainsCount4=sb.subdomainVisits1(cpdomains1);
		cpdomainsCount3.forEach(System.out::println);
		cpdomainsCount4.forEach(System.out::println);
	}
	//THis method is consuming more time when executing in leetcode. 
	public List<String> subdomainVisits(String[] cpdomains) {
		HashMap<String, Integer> countDomainVisits=new HashMap<String, Integer>();
		for(String s:cpdomains){
			String subArr[]=s.split("\\s");
			int count=Integer.parseInt(subArr[0]);
			String[] domains=subArr[1].split("[.]");
			String temp="";
			for(int i=domains.length-1;i>=0;i--) {
				temp = temp.length()==0?domains[i]+temp:domains[i]+"."+temp;
				countDomainVisits.put(temp, countDomainVisits.getOrDefault(temp, 0)+count);
			}
		}
		List<String> output=new ArrayList<String>();
		countDomainVisits.forEach((k, v) -> output.add(v+" "+k));
		return output; 
    }
	//Instead of doing split, we traverse and find where '.' is present.
	public List<String> subdomainVisits1(String[] cpdomains) {
		Map<String, Integer> countDomainVisits=new HashMap<String, Integer>();
		for(String s:cpdomains){
			int index=s.indexOf(" ");
			int count=Integer.parseInt(s.substring(0, index));
			countDomainVisits.put(s.substring(index+1), countDomainVisits.getOrDefault(s.substring(index+1), 0)+count);
			for(int i=index+1;i<s.length();i++) {
				if(s.charAt(i)=='.') {
					countDomainVisits.put(s.substring(i+1), countDomainVisits.getOrDefault(s.substring(i+1), 0)+count);
				}
			}
		}
		List<String> output=new ArrayList<String>();
		for(Map.Entry<String, Integer> entry : countDomainVisits.entrySet()){
			output.add(entry.getValue() + " " + entry.getKey()); 
		}
		return output; 
    }
}
