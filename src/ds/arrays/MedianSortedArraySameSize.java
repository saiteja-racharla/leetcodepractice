/*
 * ******************************This program is not working***************************
 * Time Complexity: O(logn)
 */

package ds.arrays;

public class MedianSortedArraySameSize {

	public static void main(String[] args) {
		int ar1[] = {1, 12, 15, 26, 38};
	    int ar2[] = {2, 13, 17, 30, 45};
	    int firstArrayLength=ar1.length;
	    int secondArrayLength=ar2.length;
	    if (firstArrayLength == secondArrayLength)
	        System.out.println("Median is " +getMedian(ar1, ar2, 0,ar1.length-1,0,ar2.length-1));
	    else
	    	System.out.println("Doesn't work for arrays of unequal size");
	}
	
	public static int getMedian(int[] ar1, int[] ar2,int firstStartIndex,int firstEndIndex,int secondStartIndex,int secondEndIndex){
		int length=firstEndIndex-firstStartIndex+1;
		if (length <= 0)
	        return -1;
	    if (length == 1)
	        return (ar1[0] + ar2[0])/2;
	    if (length == 2)
	        return (Math.max(ar1[firstStartIndex], ar2[secondStartIndex]) + Math.min(ar1[firstEndIndex], ar2[secondEndIndex])) / 2;
	    int m1 = median(ar1, firstStartIndex,firstEndIndex); /* get the median of the first array */
	    int m2 = median(ar2, secondStartIndex,secondEndIndex); /* get the median of the second array */
	    if (m1 == m2)
	        return m1;
	    if (m1 < m2)
	    {
	        if ((length) % 2 == 0)
	        	return getMedian(ar1,ar2,((length)/2)-1,firstEndIndex,secondStartIndex,length-length/2);
	        return getMedian(ar1, ar2,length/2+1,firstEndIndex,secondStartIndex,length-length/2-1);
	    }
	    
	    if (length % 2 == 0)
	        return getMedian(ar1,ar2,firstStartIndex,length - length/2 + 1,length/2 - 1, secondEndIndex);
	    return getMedian(ar1, ar2,firstStartIndex,length - length/2+1,length/2, secondEndIndex);
	}
	public static int median(int arr[],int startIndex,int endIndex)
	{
		int n=endIndex-startIndex+1;
	    if (n%2 == 0)
	        return (arr[(startIndex+endIndex)/2] + arr[(startIndex+endIndex)/2+1])/2;
	    else
	        return arr[(startIndex+endIndex)/2];
	}

}
