package ds.arrays;

public class KthSmallestElementSortedMatrix {
	public static void main(String[] args) {
		int[][] matrix={
				{1,5,9},
				{10,11,13},
				{12,13,15}
		};
		int k=8;
		System.out.println(kthSmallest(matrix,k));
	}
	public static int kthSmallest(int[][] matrix, int k) {
      int row = matrix.length, col = matrix.length;
      int left = matrix[0][0], right = matrix[row - 1][col - 1];
      while (left < right) {
        int mid = left + (right - left) / 2;
        int[] smallLargePair = { matrix[0][0], matrix[row - 1][col - 1] };
        int countLessEqual=count(matrix,mid,smallLargePair);
        if(countLessEqual==k)
        	return smallLargePair[0];
        if (countLessEqual <k) {
          left = smallLargePair[1];
        } else {
          right = smallLargePair[0];
        }
      }
      return left;
    }
	public static int count(int[][] matrix, int mid,int[] smallLargePair) {
		int n=matrix.length;
	    int row=n-1;
	    int col=0;
	    int count = 0;
	 
	    while(row>=0&&col<n){
	        if(matrix[row][col]<=mid){
	        	// as matrix[row][col] is less than or equal to the mid, let's keep track of the
	            // biggest number less than or equal to the mid
	        	smallLargePair[0] = Math.max(smallLargePair[0], matrix[row][col]);
	            count += row+1;
	            col++;
	        }else{
	        	// as matrix[row][col] is bigger than the mid, let's keep track of the
	            // smallest number greater than the mid
	        	smallLargePair[1]=Math.min(smallLargePair[1], matrix[row][col]);
	            row--;
	        }
	    }
	    return count;
	}
}
