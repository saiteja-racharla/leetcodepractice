/*
 * Given an integer array, find three numbers whose product is maximum and output the maximum product.
 * 
 * Input: [1,2,3,4]
	Output: 24
 * Time complexity-O(n)
 */

package ds.arrays;

public class MaximumProductThreeNumbers {
	public static void main(String[] args) {
		int nums[]={-10,-11,-12,3,7,2,5};
		int min1=Integer.MAX_VALUE, min2=Integer.MAX_VALUE;
		int max1=Integer.MIN_VALUE, max2=Integer.MIN_VALUE, max3=Integer.MIN_VALUE;
		for(int i:nums){
			if(i>=max1){
				max3=max2;
				max2=max1;
				max1=i;
			}
			else if(i>=max2){
				max3=max2;
				max2=i;
			}
			else if(i>=max3){
				max3=i;
			}
			if(i<min1){
				min2=min1;
				min1=i;
			}
			else if(i<min2){
				min2=i;
			}
		}
		System.out.println(Math.max(min1*min2*max1, max1*max2*max3));
	}
}
