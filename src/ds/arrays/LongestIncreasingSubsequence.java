/*
 * Given an unsorted array of integers, find the length of longest increasing subsequence.

	Example:
	Input: [10,9,2,5,3,7,101,18]
	Output: 4 
	Explanation: The longest increasing subsequence is [2,3,7,101], therefore the length is 4. 
	
	https://leetcode.com/problems/longest-increasing-subsequence/discuss/74897/Fast-Java-Binary-Search-Solution-with-detailed-explanation
 */
package ds.arrays;

import java.util.Arrays;

public class LongestIncreasingSubsequence {
	public static void main(String[] args) {
		int[] nums={10,9,2,5,3,7,101,18};
		lengthOfLIS(nums);
	}
	 public static int lengthOfLIS(int[] nums) {            
	        int[] dp = new int[nums.length];
	        int len = 0;

	        for(int x : nums) {
	            int i = Arrays.binarySearch(dp, 0, len, x);
	            if(i < 0) {
	            	i = -(i + 1);
	            }
	            dp[i] = x;
	            if(i == len){
	            	len++;
	            }
	        }

	        return len;
	    }
}
