/*
 * Given an unsorted array return whether an increasing subsequence of length 3 exists or not in the array.

Formally the function should:

Return true if there exists i, j, k 
such that arr[i] < arr[j] < arr[k] given 0 <= i < j < k <= n-1 else return false.
Note: Your algorithm should run in O(n) time complexity and O(1) space complexity.

Example 1:
Input: [1,2,3,4,5]
Output: true
 */
package ds.arrays;

public class IncreasingTripletSubsequence {

	public static void main(String[] args) {
		int[] nums={1,0,6,2,10};	//returns true
		System.out.println("Is increasing subsequence-"+increasingTriplet(nums));
	}
	public static boolean increasingTriplet(int[] nums) {
		int a=Integer.MAX_VALUE, b=Integer.MAX_VALUE;
		for(int x:nums){
			if(x<=a)
				a=x;
			else if(x<=b)
				b=x;
			else
				return true;
		}
		return false;
	}

}
