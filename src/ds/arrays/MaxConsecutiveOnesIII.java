/**
 * Given an array A of 0s and 1s, we may change up to K values from 0 to 1.
Return the length of the longest (contiguous) subarray that contains only 1s. 
 * https://leetcode.com/problems/max-consecutive-ones-iii/
 */
package ds.arrays;

import java.util.LinkedList;
import java.util.Queue;

public class MaxConsecutiveOnesIII {

	public static void main(String[] args) {
		MaxConsecutiveOnesIII m=new MaxConsecutiveOnesIII();
		int A[]= {0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1};
		int K=3;
		System.out.println(m.longestOnes(A, K));
	}
	/*
	 * This solution uses queue
	 */
	public int longestOnes(int[] A, int K) {
        int left=0,right=0,max=0;
        Queue<Integer> queue=new LinkedList<>();
        while(right<=A.length-1){
            if(A[right]==0){
                queue.offer(right);
            }
            while(queue.size()>K){
                left=queue.poll()+1;
            }
            max=Math.max(max,right-left+1);
            right++;
        }
        return max;
    }
	/*
	 * This one without using queue
	 */
	public int longestOnes1(int[] A, int K) {
		int left=0,right=0,max=0;
        while(right<=A.length-1){
            if(A[right]==0){
                K--;
            }
            while(K<0){
                if(A[left]==0) K++;
                left++;
            }
            max=Math.max(max,right-left+1);
            right++;
        }
        return max;
    }
}
