package ds.arrays;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Combinations3 {

	public static void main(String[] args) {
		List<List<Integer>> result = combinationSum3(3,9);
		for(List<Integer> temp:result){
			for(Integer temps:temp){
				System.out.print(temps+"\t");
			}
			System.out.println();
		}
	}
	public static List<List<Integer>> combinationSum3(int k, int n) {
        List<Integer> temp=new ArrayList<Integer>();
        List<List<Integer>> result=new ArrayList<List<Integer>>();
        combination3Util(k, n, result, temp, 1);
        return result;
    }
	public static void combination3Util(int k, int n, List<List<Integer>> result, List<Integer> temp,int start){
		if(n<0)
			return;
		if(n==0 && temp.size()==k)
			result.add(new ArrayList<Integer>(temp));
		for(int i=start;i<=9;i++){
			if(temp.size()>=k)
				break;
			temp.add(i);
			combination3Util(k, n-i, result, temp,i+1);
			temp.remove(temp.size()-1);
		}
	}
}
