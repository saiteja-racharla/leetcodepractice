/**  Unique Number of Occurrences
 * Given an array of integers arr, write a function that returns true if and only if the number of occurrences of each value in the array is unique.

Example 1:
Input: arr = [1,2,2,1,1,3]
Output: true
Explanation: The value 1 has 3 occurrences, 2 has 2 and 3 has 1. No two values have the same number of occurrences.

Example 2:
Input: arr = [1,2]
Output: false

Example 3:
Input: arr = [-3,0,1,-3,1,1,1,-3,10,0]
Output: true
https://leetcode.com/problems/unique-number-of-occurrences/description/
 */

package ds.arrays;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class UniqueNumberOfOccurences {

	public static void main(String[] args) {
		int[] arr1= {1,2,2,1,1,3};
		int[] arr2= {1,2};
		int[] arr3= {-3,0,1,-3,1,1,1,-3,10,0};
		UniqueNumberOfOccurences u=new UniqueNumberOfOccurences();
		System.out.println(u.uniqueOccurrences(arr1));
		System.out.println(u.uniqueOccurrences(arr2));
		System.out.println(u.uniqueOccurrences(arr3));

	}
	public boolean uniqueOccurrences(int[] arr) {
        HashMap<Integer, Integer> count=new HashMap<Integer, Integer>();
		for(int i:arr) {
			count.put(i, count.getOrDefault(i, 0)+1);
		}
		Set<Integer> values=new HashSet<Integer>(count.values());
        return values.size()==count.size();
    }
}
