package ds.arrays;

import java.util.Arrays;

public class SortColors {

	public static void main(String[] args) {
		int[] nums= {2,0,2,1,1,0};
		sortColors(nums);
		System.out.println(Arrays.toString(nums));
	}
	public static void sortColors(int[] nums) {
		int j=0,k=nums.length-1;
		for(int i=0;i<=k;i++) {
			if(nums[i]==0) {
				swap(nums,i,j++);
			} 
			if(nums[i]==2) {
				swap(nums,i--,k--);
			}
		}
	}
	public static int[] swap(int nums[],int i, int j){
        int temp=nums[i];
        nums[i]=nums[j];
        nums[j]=temp;
        return nums;
    }
}
