/*
 * Given an array of citations (each citation is a non-negative integer) of a researcher, 
 * write a function to compute the researcher's h-index. A scientist has index h if h 
 * of his/her N papers have at least h citations each, and the other N - h papers have no 
 * more than h citations each.

For example, given citations = [3, 0, 6, 1, 5], which means the researcher has 
5 papers in total and each of them had received 3, 0, 6, 1, 5 citations respectively. 
Since the researcher has 3 papers with at least 3 citations each and the remaining two 
with no more than 3 citations each, his h-index is 3.
input {4,1,5,6,2,7}  - output -4
 */
package ds.arrays;

import java.util.Arrays;

public class ResearcherHIndex {
	public static void main(String[] args) {
		/* first approach */
		int[] citations={4,1,5,6,2,7,10};
		 Arrays.sort(citations);
		 int result = 0;    
		 for(int i=0; i<citations.length; i++){
			 int smaller = Math.min(citations[i], citations.length-i);
			 result = Math.max(result, smaller);
		 }
		 System.out.println(result);
		 
		 /* second approach */
		 result=0;
		 int length=citations.length;
	        if (length == 0)
	        	System.out.println(0);
	        int[] arr2=new int[length+1];
	        for(int i=0;i<citations.length;i++){
	            if(citations[i]>=length)
	                arr2[length]+=1;
	            else
	                arr2[citations[i]]+=1;
	        }
	        for(int i=length;i>=0;i--){
	            result+=arr2[i];
	            if(i<=result)
	                System.out.println(i);
	        }
	        System.out.println(0);
	}
}
