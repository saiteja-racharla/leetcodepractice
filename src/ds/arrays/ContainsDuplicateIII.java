/*
 * Leetcode-220
 * Given an array of integers, find out whether there are two distinct indices i and j in the array 
 * such that the absolute difference between nums[i] and nums[j] is at most t and the 
 * absolute difference between i and j is at most k.
 */
package ds.arrays;

import java.util.HashMap;
import java.util.Map;

public class ContainsDuplicateIII {

	public static void main(String[] args) {
		ContainsDuplicateIII c=new ContainsDuplicateIII();
		int nums[]={5,2,6,7,8,1,3,5};
		int k=4;
		int t=2;
		c.containsNearbyAlmostDuplicate(nums, k, t);
	}
	public boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
        if (k < 1 || t < 0) return false;
        Map<Long, Long> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            long remappedNum = (long) nums[i] - Integer.MIN_VALUE;
            long bucket = remappedNum / ((long) t + 1);
            if (map.containsKey(bucket)
                    || (map.containsKey(bucket - 1) && remappedNum - map.get(bucket - 1) <= t)
                        || (map.containsKey(bucket + 1) && map.get(bucket + 1) - remappedNum <= t))
                            return true;
            if (map.entrySet().size() >= k) {
                long lastBucket = ((long) nums[i - k] - Integer.MIN_VALUE) / ((long) t + 1);
                map.remove(lastBucket);
            }
            map.put(bucket, remappedNum);
        }
        return false;
    }
}
