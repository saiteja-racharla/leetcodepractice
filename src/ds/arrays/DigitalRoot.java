/*
 * Given a non-negative integer num, repeatedly add all its digits until the result has only one digit.
For example:
Given num = 38, the process is like: 3 + 8 = 11, 1 + 1 = 2. Since 2 has only one digit, return it.
Time Complexity-O(1)

https://en.wikipedia.org/wiki/Digital_root#Congruence_formula
3 cases
				=0(if n==0)
digital root(n)	=9(if n!=0 && n==9)
				=n mod 9(if n!=0 && n!=9)
				
				OR
				
				=1+((n-1)%9)--------For all positive numbers
 */
package ds.arrays;

public class DigitalRoot {
	public static void main(String[] args) {
		//One way of writing code
		int number=12345;
		System.out.println(1+((number-1)%9));
		//another way of writing code
		int res=number%9;
		System.out.println((res != 0 || number == 0) ? res : 9);
	}
}
