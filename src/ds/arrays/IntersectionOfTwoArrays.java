/*
 * Given two arrays, write a function to compute their intersection.

Example:
Given nums1 = [1, 2, 2, 1], nums2 = [2, 2], return [2, 2].

Note:
Each element in the result should appear as many times as it shows in both arrays.
The result can be in any order.
Follow up:
What if the given array is already sorted? How would you optimize your algorithm?
What if nums1's size is small compared to nums2's size? Which algorithm is better?
What if elements of nums2 are stored on disk, and the memory is limited such that you cannot load all elements into the memory at once?
If only nums2 cannot fit in memory, put all elements of nums1 into a HashMap, read chunks of array that fit into the memory, and record the intersections.

If both nums1 and nums2 are so huge that neither fit into the memory, sort them individually (external sort), then read 2 elements from each array at a time in memory, record intersections.

 */
package ds.arrays;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class IntersectionOfTwoArrays {
	public static void main(String[] args) {
		int[] one={1};
		int[] two={1,1};
		int[] result=intersection(one,two);
		for(int i:result){
			System.out.print(i+"\t");
		}
	}
	
	public static int[] intersection(int[] one,int[] two){
		int[] result;
		HashMap<Integer,Integer> hm=new HashMap<Integer,Integer>();
		List<Integer> res=new ArrayList<Integer>();
		for(int i=0;i<one.length;i++){
			hm.put(one[i], hm.getOrDefault(one[i], 0)+1);
		}
		for(int i=0;i<two.length;i++){
			if(hm.containsKey(two[i]) && hm.get(two[i])>0){
				res.add(two[i]);
				hm.put(two[i], hm.getOrDefault(two[i], 0)-1);
			}
		}
		result=new int[res.size()];
		for(int i=0;i<res.size();i++){
			result[i]=res.get(i);
		}
		return result;
	}
}
