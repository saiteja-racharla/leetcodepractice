package ds.arrays;

import java.util.stream.Stream;

public class SortArrayByParity {
	public static void main(String[] args) {
		int[] A= {3,1,2,4,5};
		for (int i = 0; i < A.length; i++) {
			System.out.print(A[i]+" ");
		}
		System.out.println("\nAfter modifying array");
		int[] result=sortArrayByParity(A);
		for (int i = 0; i < result.length; i++) {
			System.out.print(result[i]+" ");
		}
	}
	public static int[] sortArrayByParity(int[] A) {
		int i=0,j=A.length-1;
		while(i<j) {
			if(A[i]%2==0) {
				i++;
			}
			else {
				if(A[j]%2==0) {
					swap(A,i,j);
					i++;
				}
				else {
					j--;
				}
			}
		}
		return A;
    }
	public static void swap(int[] A,int i,int j) {
		int temp=A[i];
		A[i]=A[j];
		A[j]=temp;
	}
}
