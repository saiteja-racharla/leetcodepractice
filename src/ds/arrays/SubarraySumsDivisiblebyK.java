/**
 * Given an array A of integers, return the number of (contiguous, non-empty) subarrays that have a sum divisible by K.
Example 1:
Input: A = [4,5,0,-2,-3,1], K = 5
Output: 7
Explanation: There are 7 subarrays with a sum divisible by K = 5:
[4, 5, 0, -2, -3, 1], [5], [5, 0], [5, 0, -2, -3], [0], [0, -2, -3], [-2, -3]
 
Note:
1 <= A.length <= 30000
-10000 <= A[i] <= 10000
2 <= K <= 10000

Time complexity-O(N)
Space Complexity-O(K)
 */
package ds.arrays;

import java.util.HashMap;
import java.util.Map;

public class SubarraySumsDivisiblebyK {

	public static void main(String[] args) {
		SubarraySumsDivisiblebyK s=new SubarraySumsDivisiblebyK();
		int arr[]= {4,5,0,-2,-3,1};
		int k=5;
		System.out.println(s.subarraysDivByK(arr,k));
		System.out.println(s.subarraysDivByKSol2(arr,k));
	}
	//Using hashmap. Instead of hashmap, we can go with array also
	public int subarraysDivByK(int[] A, int K) {
		Map<Integer, Integer> hm=new HashMap<Integer, Integer>();
		hm.put(0, 1);
		int sum=0,count=0;
		for(int a:A) {
			sum=(sum+a)%K;
			if(sum<0)
				sum=sum+K;
			if(hm.containsKey(sum))
				count=count+hm.get(sum);
			hm.put(sum, hm.getOrDefault(sum, 0)+1);
		}
		return count;
    }
	
	public int subarraysDivByKSol2(int[] A, int K) {
		int[] hm=new int[K];
		hm[0]= 1;
		int sum=0,count=0;
		for(int a:A) {
			sum=(sum+a)%K;
			if(sum<0)
			sum=sum+K;
			count=count+hm[sum];
			hm[sum]= hm[sum]+1;
		}
		return count;
    }
}
