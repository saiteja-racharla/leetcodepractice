/*
 * This program finds the duplicate elements in the array
 * Time complexity-O(n)
 * Space complexity-O(n)
 */

package ds.arrays;

import java.util.HashSet;
import java.util.Hashtable;

public class FindDuplicates1 {

	public static void main(String[] args) {
		int arr[]={1,2,3,4,5,6,6,3,6};
		Hashtable ht=new Hashtable();
		for(int i=0;i<arr.length;i++){
			if(ht.containsKey(arr[i]))
				System.out.println(arr[i]);
			else
				ht.put(arr[i],i);	
		}

	}

}
