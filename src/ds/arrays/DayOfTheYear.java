/**Given a string date representing a Gregorian calendar date formatted as YYYY-MM-DD, return the day 
 * number of the year.
 * Example 1:
 * Input: date = "2019-01-09"
 * Output: 9
 * Explanation: Given date is the 9th day of the year in 2019.
 * 
 * Example 2:
 * Input: date = "2019-02-10"
 * Output: 41
 * 
 * Leap year should be divisible by 4. 
 * If the leap year is divisible by 100, then it should also be divisible by 400
 * For example, 1700,1900 is not leap year since it is divisible by 100, But not by 400.
 */
package ds.arrays;

import java.util.Arrays;

public class DayOfTheYear {

	public static void main(String[] args) {
		System.out.println(dayOfYear("2004-03-01"));
	}
	public static int dayOfYear(String date) {
		int[] arr=Arrays.stream(date.split("-")).mapToInt(i->Integer.parseInt(i)).toArray();
		int[] monthArr= {31,28,31,30,31,30,31,31,30,31,30,31};
		int totalDays=0;
		for(int i=0;i<arr[1]-1;i++) {
			totalDays+=monthArr[i];
		}
		totalDays+=arr[2];
		if((arr[0]%4==0 && arr[0] % 100 != 0 || arr[0] % 400 == 0) && arr[1]>2) {
			totalDays+=1;
		}
        return totalDays;
    }
}
