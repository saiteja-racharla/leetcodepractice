package ds.arrays;

public class IsHappyNumber {
	public static void main(String[] args) {
		int n=7;
		int slow=n,fast=n;
        do{
            slow=squareOfNumber(slow);
            fast=squareOfNumber(fast);
            fast=squareOfNumber(fast);
        }while(slow!=fast);
        if(slow==1)
            System.out.println("Happy number");
        else
        	System.out.println("Not a happy number");
	}
	public static int squareOfNumber(int number){
        int sum=0,temp=0;
        while(number!=0){
            temp=number%10;
            sum=sum+(temp*temp);
            number=number/10;
        }
        return sum;
    }
}
