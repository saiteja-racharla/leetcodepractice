package ds.arrays;

public class MaximumProductSubArray {

	public static void main(String[] args) {
		//int[] A={12, 2, -3, -5, -6, 2};
		int[] A= {2,3,-2,4};
		if (A == null || A.length == 0) {
            System.out.println(0);
            System.exit(0);
        }
        int max = A[0], min = A[0], result = A[0];
        for (int i = 1; i < A.length; i++) {
            int temp = max;
            max = Math.max(Math.max(max * A[i], min * A[i]), A[i]);
            min = Math.min(Math.min(temp * A[i], min * A[i]), A[i]);
            if (max > result) {
                result = max;
            }
        }
        System.out.println(result);
	}

}
