/*
 * Given a collection of integers that might contain duplicates, nums, return all possible subsets.
	Note: The solution set must not contain duplicate subsets.
	For example,
	If nums = [1,2,2], a solution is:
	[
	  [2],
	  [1],
	  [1,2,2],
	  [2,2],
	  [1,2],
	  []
	]
 */
package ds.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PrintSubsets1 {

	public static void main(String[] args) {
		PrintSubsets1 ps=new PrintSubsets1();
		int[] nums={1,2,2};
		List<List<Integer>> result = ps.subsets(nums);
		for(List<Integer> ls:result){
			System.out.println(ls);
		}
	}
	public List<List<Integer>> subsets(int[] nums) {
	    List<List<Integer>> list = new ArrayList<>();
	    Arrays.sort(nums);
	    backtrack(list, new ArrayList<>(), nums, 0);
	    return list;
	}
	private void backtrack(List<List<Integer>> list , List<Integer> tempList, int [] nums, int start){
	    list.add(new ArrayList<>(tempList));
	    for(int i = start; i < nums.length; i++){
	    	if(i>start && nums[i]==nums[i-1]) continue;
	        tempList.add(nums[i]);
	        backtrack(list, tempList, nums, i + 1);
	        tempList.remove(tempList.size() - 1);
	    }
	}

}
