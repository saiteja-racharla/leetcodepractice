/**
 * Given an integer array arr and an integer difference, return the length of the longest subsequence in arr which is an arithmetic sequence such that the difference between adjacent 
 * elements in the subsequence equals difference.
Example 1:
Input: arr = [1,2,3,4], difference = 1
Output: 4
Explanation: The longest arithmetic subsequence is [1,2,3,4].

Example 2:
Input: arr = [1,3,5,7], difference = 1
Output: 1
Explanation: The longest arithmetic subsequence is any single element.

Example 3:
Input: arr = [1,5,7,8,5,3,4,2,1], difference = -2
Output: 4
Explanation: The longest arithmetic subsequence is [7,5,3,1].
 */
package ds.arrays;

import java.util.HashMap;

public class LongestArithmeticSubsequenceGivenDifference {

	public static void main(String[] args) {
		LongestArithmeticSubsequenceGivenDifference l=new LongestArithmeticSubsequenceGivenDifference();
		System.out.println(l.longestSubsequence(new int[] {1,2,3,4}, 1));
		System.out.println(l.longestSubsequence(new int[] {1,3,5,7}, 1));
		System.out.println(l.longestSubsequence(new int[] {1,5,7,8,5,3,4,2,1}, -2));
	}
	public int longestSubsequence(int[] arr, int difference) {
		HashMap<Integer, Integer> dp=new HashMap<Integer, Integer>();
		int longest=0;
		for(int a:arr) {
			dp.put(a, dp.getOrDefault(a-difference, 0)+1);
			longest=Math.max(dp.get(a),longest);
		}
		return longest;
    }

}
