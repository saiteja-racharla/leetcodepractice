/*
 * Given an array and target sum. Prints triplets whose sum equals to target.
 * Dont print duplicate triplets.(line 21 serves this purpose)
 * 3 sum problem
 * Time Complexity:O(n2)
 */
package ds.arrays;

import java.util.Arrays;

public class ThreeSumProblem {
	public static void main(String[] args) {
		int a[]={0,-4,-1,-4,-2,-3,2};
		int target=0;
		printTriplets(a, target);
	}
	public static void printTriplets(int a[],int target){
		Arrays.sort(a);
		for(int i=0;i<a.length-2;i++){
			if (i == 0 || (i > 0 && a[i] != a[i-1])) {
				int tempTarget=target-a[i];
				int low=i+1;
				int high=a.length-1;
				while(low<high){
					if(a[low]+a[high]==tempTarget){
    					System.out.println(a[i]+","+a[low]+","+a[high]);
    					while(low<high && a[low]==a[low+1])	low++;
    					while(low<high && a[high]==a[high-1]) high--;
    					low++;high--;
    				}
    				else if(a[low]+a[high]>tempTarget)  high--;
    				else    low++;
				}
			}
		}
	}
}
