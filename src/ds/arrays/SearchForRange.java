/*
 * Can be done in another way also.
 * 2 binary searches. Once you get lower value(result[0]), start from low to end of array.
 */
package ds.arrays;

public class SearchForRange {
	public static void main(String[] args) {
		SearchForRange s=new SearchForRange();
		int nums[]={5, 7, 7, 8, 8, 10};
		int target=-4;
		int[] range=s.searchRange(nums, target);
		System.out.println(range[0]+"\t"+range[1]);
	}
	public int[] searchRange(int[] a, int target){
		int[] result=new int[2];
		result[0]=-1;result[1]=-1;
		if(a.length==0)
			return result;
		int low=0,high=a.length-1;
		while(low<=high){
			int mid=(low+high)/2;
			if(a[mid]==target){
				low=mid;high=mid;
				while(low>=0 && a[low]==target){
					low--;
				}
				result[0]=low+1;
				while(high<a.length && a[high]==target){
					high++;
				}
				result[1]=high-1;
				break;
			}
			else if(a[mid]>target){
				high=mid-1;
			}
			else{
				low=mid+1;
			}
		}
		return result;
	}
}
