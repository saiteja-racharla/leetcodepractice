package ds.arrays;

public class MinimumElementRotatedSortedArrayII {

	public static void main(String[] args) {
		int a[]={10,1,10,10,10};
		System.out.println(findMin(a));

	}
	public static int findMin(int[] nums) {
        int low=0,high=nums.length-1;
        while(low<high){
            if(nums[low]<nums[high]){
                return nums[low];
            }
            int mid=(low+high)/2;
            if(nums[mid]>=nums[low])
                low=mid+1;
            else
                high=mid;
        }
        return nums[low];
    }
}
