/*
 * Rotate the array by k places(doing both rotate left and rotate right)
 * Rotate an array of n elements to the right by k steps.
   For example, with n = 7 and k = 3, the array [1,2,3,4,5,6,7] is rotated to [5,6,7,1,2,3,4].
 */

package ds.arrays;

public class RotateByKPlaces {
	public static void main(String[] args){
		RotateByKPlaces rp=new RotateByKPlaces();
		int nums1[]={1,2,3,4,5,6,7};
		int nums2[]={1,2,3,4,5,6,7};
		int k=3;
		System.out.println("Original array");
		for(int i:nums1){
			System.out.print(i+"\t");
		}
		int left[]=rp.rotateArrayLeft(nums1, k,nums1.length);
		int right[]=rp.rotateArrayLeft(nums2, nums2.length-k,nums2.length);
		System.out.println("\nRotate left");
		for(int i:left){
			System.out.print(i+"\t");
		}
		System.out.println("\nRotate right");
		for(int i:right){
			System.out.print(i+"\t");
		}
	}
	public int[] rotateArrayLeft(int[] arr,int d,int n){
		int i, j, k, temp;
		  for (i = 0; i < gcd(d, n); i++)
		  {
		    /* move i-th values of blocks */
		    temp = arr[i];
		    j = i;
		    while(true)
		    {
		      k = j + d;
		      if (k >= n)
		        k = k - n;
		      if (k == i)
		        break;
		      arr[j] = arr[k];
		      j = k;
		    }
		    arr[j] = temp;
		  }
		return arr;
	}
	public int gcd(int n, int k){
		int gcd=0;
		for(int i=1;i<=n && i<=k;i++){
			if(n%i==0 && k%i==0)
				gcd=i;
		}
		return gcd;
	}
}
