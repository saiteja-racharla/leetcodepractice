/*
 * Given two arrays, write a function to compute their intersection.

Example:
Given nums1 = [1, 2, 2, 1], nums2 = [2, 2], return [2].

Note:
Each element in the result must be unique.
The result can be in any order.
 */
package ds.arrays;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class IntersectionOfTwoArrays2 {

	public static void main(String[] args) {
		int[] one={1,2,2,1};
		int[] two={2,2};
		int[] result=intersection(one,two);
		for(int i:result){
			System.out.print(i+"\t");
		}
	}
	public static int[] intersection(int[] one,int[] two){
		int[] result;
		HashSet<Integer> hs=new HashSet<Integer>();
		List<Integer> res=new ArrayList<Integer>();
		for(int i=0;i<one.length;i++){
			hs.add(one[i]);
		}
		for(int i=0;i<two.length;i++){
			if(hs.contains(two[i])){
				res.add(two[i]);
				hs.remove(two[i]);
			}
		}
		result=new int[res.size()];
		for(int i=0;i<res.size();i++){
			result[i]=res.get(i);
		}
		return result;
	}
}
