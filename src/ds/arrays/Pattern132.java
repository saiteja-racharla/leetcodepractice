/**
 * 132 Pattern
 * Given a sequence of n integers a1, a2, ..., an, a 132 pattern is a subsequence ai, aj, ak such that i < j < k and ai < ak < aj. Design an algorithm that takes a list of n numbers as input and checks whether there is a 132 pattern in the list.

Note: n will be less than 15,000.

Example 1:
Input: [1, 2, 3, 4]
Output: False
Explanation: There is no 132 pattern in the sequence.

Example 2:
Input: [3, 1, 4, 2]
Output: True
Explanation: There is a 132 pattern in the sequence: [1, 4, 2].

Example 3:
Input: [-1, 3, 2, 0]
Output: True
Explanation: There are three 132 patterns in the sequence: [-1, 3, 2], [-1, 3, 0] and [-1, 2, 0].

https://leetcode.com/problems/132-pattern/discuss/94071/Single-pass-C%2B%2B-O(n)-space-and-time-solution-(8-lines)-with-detailed-explanation.
In this solution, we try to find largest s3 which is less than s2.
 */
package ds.arrays;

import java.util.Stack;

public class Pattern132 {

	public static void main(String[] args) {
		int[] nums1= {1,2,3,4};
		System.out.println(find132pattern(nums1));
		int[] nums2= {3,1,4,2};
		System.out.println(find132pattern(nums2));
		int[] nums3= {-1,3,2,0};
		System.out.println(find132pattern(nums3));
		int[] nums4= {2,3,5,6,4,1,2,2,4};
		System.out.println(find132pattern(nums4));
		int[] nums5= {1,0,1,-4,-3};
		System.out.println(find132pattern(nums5));
		int[] nums6= {9,11,8,9,10,7,9};
		System.out.println(find132pattern(nums6));
		int[] nums7= {-2,1,2,-2,1,2};
		System.out.println(find132pattern(nums7));
	}
	public static boolean find132pattern(int[] nums) {
		int s3=Integer.MIN_VALUE;
		Stack<Integer> stack=new Stack<Integer>();
		for(int i=nums.length-1;i>=0;i--) {
			if(nums[i]<s3)
				return true;
			else {
				if(!stack.isEmpty()) {
					if(nums[i]<=stack.peek()) {
						stack.push(nums[i]);
						continue;
					} else {
						while(!stack.isEmpty()) {
							if(nums[i]>stack.peek()) {
								s3=stack.pop();
							} else {
								break;
							}
						}
						stack.push(nums[i]);
					}
				} else {
					stack.push(nums[i]);
				}
			}
		}
		return false;
    }
	//removed unnecessary code from above solution
	public static boolean find132patternOptmized(int[] nums) {
		int s3=Integer.MIN_VALUE;
		Stack<Integer> stack=new Stack<Integer>();
		for(int i=nums.length-1;i>=0;i--) {
			if(nums[i]<s3)
				return true;
			else {
				while(!stack.isEmpty() && nums[i]>stack.peek()) {
					s3=stack.pop();
				}
			}
			stack.push(nums[i]);
		}
		return false;
    }
}
