/**
 * Given an array of non negative numbers and a total, is there subset of numbers in this array which adds up
 * to given total.
 * Time complexity - O(input.size * total_sum)
 * Space complexity - O(input.size*total_sum)
 */
package ds.arrays;

public class SubsetSum {

	public static void main(String[] args) {
		
		int[] nums= {1,5,11,5};
		System.out.println(canPartition(nums,13));
	}
	public static boolean canPartition(int[] nums, int target) {
		boolean dp[][]=new boolean[nums.length+1][target+1];
		dp[0][0]=true;
		for(int i=1;i<=nums.length;i++) {
			for(int j=1;j<=target;j++) {
				if(j-nums[i-1]>=0)
					dp[i][j]=dp[i-1][j]||dp[i-1][j-nums[i-1]];
				else
					dp[i][j]=dp[i-1][j];
					
				
			}
		}
		return dp[nums.length][target];
    }


}
