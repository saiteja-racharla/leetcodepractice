package ds.arrays;

import java.util.TreeSet;

public class FindDuplicates3 {
	public static void main(String[] args) {
//		int[] nums={4,3,2,1,1,3,4};
//		int[] nums={22,16,10,4,3,1,0};
		int[] nums={22,16,30,20,10,3};
		int k=2,t=2;
		System.out.println(containsNearbyAlmostDuplicate(nums,k,t));
	}
	public static boolean containsNearbyAlmostDuplicate(int[] nums, int k, int t) {
		TreeSet<Long> set = new TreeSet<>();
        for(int i = 0;i < nums.length;i++){
            Long ceiling = set.ceiling((long)nums[i]);
            Long  floor = set.floor((long)nums[i]);
            if(ceiling != null&& ceiling- nums[i] <= t || floor != null &&nums[i] - floor <= t) 
            	return true;
            set.add((long)nums[i]);
            if(i >= k) {
                set.remove((long)nums[i - k]);
            }
        } 
		return false;
	}
}
