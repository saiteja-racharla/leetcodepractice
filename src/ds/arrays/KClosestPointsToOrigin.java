package ds.arrays;

public class KClosestPointsToOrigin {
	public static void main(String[] args) {
		int[][] points= {{3,3},{5,-1},{-2,4}};
		int k=3;
		int[][] result=kClosest(points,k);
		for(int[] res:result) {
			System.out.println(res[0]+","+res[1]);
		}
	}
	public static int[][] kClosest(int[][] points, int K) {
		int start = 0, end = points.length - 1;
        int index = 0;
        while (start <= end) {
            index = partition(points, start, end);
            if (index == K) {
                break;
            }
            if (index > K) {
                end = index - 1;
            } else {
                start = index + 1;
            }
        }
        int[][] result = new int[index][2];
        for (int i = 0; i < index; i++) {
            result[i] = points[i];
        }
        return result;
    }
	public static int partition(int[][] points,int left, int right) {
		int partitionIndex=0;
		int[] pivot=points[right];
		int pivotIndex=right;
		partitionIndex=left;
		for(int i=left;i<=right-1;i++) {
			int[] p1=points[i];
			int[] p2=points[pivotIndex];
			if(!isLarger(p1,p2)){
				swap(points,i,partitionIndex);
				partitionIndex++;
			}
		}
		swap(points,partitionIndex,pivotIndex);
		return partitionIndex;
	}
	private static void swap(int[][] points, int i1, int i2) {
        int[] temp = points[i1];
        points[i1] = points[i2];
        points[i2] = temp;
    }
	private static boolean isLarger(int[] p1,int[] p2) {
        return p1[0] * p1[0] + p1[1] * p1[1] > p2[0] * p2[0] + p2[1] * p2[1];
    }
}
