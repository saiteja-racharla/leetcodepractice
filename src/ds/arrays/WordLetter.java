package ds.arrays;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class WordLetter {

	public static void main(String[] args) {
		String beginWord="hit", endWord = "cog"; 
		List<String> wordList = Arrays.asList("hot","dot","dog","lot","log","cog");
		/*Set<String> wordSet=new HashSet<>(wordList);
	    Set<String> reached=new HashSet<>();
	    reached.add(beginWord);
	    wordSet.remove(beginWord);
	    int level=1;
	    while(!reached.isEmpty()) {
	    	Set<String> reachedNext=new HashSet<>();
	    	for(String s:reached) {
	    		for(int i=0;i<s.length();i++) {
	    			char[] c=s.toCharArray();
	    			for(char j='a';j<='z';j++) {
	    				c[i]=j;
	    				String newS=new String(c);
	    				if(wordSet.remove(newS)) {
	    					reachedNext.add(newS);
	    					if(endWord.equals(newS)) System.out.println(level+1);
	    				}
	    			}
	    		}
	    	}
	    	reached=reachedNext;
	    	level++;
	    }
	    System.out.println(0);*/
		HashSet<String> wordset=new HashSet<String>(wordList);
		HashSet<String> reached=new HashSet<String>();
		reached.add(beginWord);
		wordset.remove(beginWord);
		int level=1;
		while(!reached.isEmpty()){
			HashSet<String> reachedNext=new HashSet<String>();
			for(String s:reached){
				for(int i=0;i<s.length();i++){
					char[] c=s.toCharArray();
					for(char j='a';j<='z';j++){
						c[i]=j;
						String temp=new String(c);
						if(wordset.contains(temp)){
							wordset.remove(temp);
							reachedNext.add(temp);
							if(endWord.equals(temp)) System.out.println(level+1);
						}
					}
				}
			}
			reached=reachedNext;
			level++;
		}
		System.out.println(0);
	}

}
