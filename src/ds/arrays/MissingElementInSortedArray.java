/**
 * Given a sorted array A of unique numbers, find the K-th missing number starting from the leftmost number of the array.
Example 1:
Input: A = [4,7,9,10], K = 1
Output: 5
Explanation: 
The first missing number is 5.

Example 2:
Input: A = [4,7,9,10], K = 3
Output: 8
Explanation: 
The missing numbers are [5,6,8,...], hence the third missing number is 8.

Example 3:
Input: A = [1,2,4], K = 3
Output: 6
Explanation: The missing numbers are [3,5,6,7,...], hence the third missing number is 6.
 */
package ds.arrays;

public class MissingElementInSortedArray {

	public static void main(String[] args) {
		MissingElementInSortedArray ms=new MissingElementInSortedArray();
		int arr1[]= {4,7,9,10,11,14,15};
		System.out.println(ms.missingElement(arr1, 8));
		System.out.println(ms.missingElement(arr1, 3));
	}
	//Followed solution 2 of leetcode
    int missingNumber(int index,int[] nums){
        return nums[index]-nums[0]-index;
    }
    public int missingElement(int[] nums, int k) {
        int n=nums.length;
        int left=0,right=n-1;
        //if k is greater than last element of array
        if (k > missingNumber(n - 1, nums))
            return nums[n - 1] + k - missingNumber(n - 1, nums);
        while(left!=right){
            int mid=left+(right-left)/2;
            if(missingNumber(mid,nums)<k){
                left=mid+1;
            } else{
                right=mid;
            }
        }
        return nums[left-1]+k-missingNumber(left-1,nums);
    }
}
