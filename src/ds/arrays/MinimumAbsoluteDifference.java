/**  Minimum absolute difference
 * Given an array of distinct integers arr, find all pairs of elements with the minimum absolute difference of any two elements. 
Return a list of pairs in ascending order(with respect to pairs), each pair [a, b] follows
a, b are from arr
a < b
b - a equals to the minimum absolute difference of any two elements in arr
Example 1:
Input: arr = [4,2,1,3]
Output: [[1,2],[2,3],[3,4]]
Explanation: The minimum absolute difference is 1. List all pairs with difference equal to 1 in ascending order.

Example 2:
Input: arr = [1,3,6,10,15]
Output: [[1,3]]

Example 3:
Input: arr = [3,8,-10,23,19,-4,-14,27]
Output: [[-14,-10],[19,23],[23,27]]

https://leetcode.com/problems/minimum-absolute-difference/description/
 */
package ds.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MinimumAbsoluteDifference {

	public static void main(String[] args) {
		MinimumAbsoluteDifference m=new MinimumAbsoluteDifference();
		int[] arr1= {4,2,1,3};
		int[] arr2= {1,3,6,10,15};
		int[] arr3= {3,8,-10,23,19,-4,-14,27};
		System.out.println(m.minimumAbsDifference(arr1));
		System.out.println(m.minimumAbsDifference(arr2));
		System.out.println(m.minimumAbsDifference(arr3));
	}
	public List<List<Integer>> minimumAbsDifference(int[] arr) {
        List<List<Integer>> minimumAbosoluteDifference = new ArrayList<List<Integer>>();
		Arrays.parallelSort(arr);
		int min=Integer.MAX_VALUE;
		for(int i=1;i<arr.length;i++) {
			int currMin=arr[i]-arr[i-1];
			if(currMin<min) {
				minimumAbosoluteDifference.clear();
				min=currMin;
				minimumAbosoluteDifference.add(Arrays.asList(arr[i-1],arr[i]));
			} else if(currMin==min) {
				minimumAbosoluteDifference.add(Arrays.asList(arr[i-1],arr[i]));
			} else {
				//do not do anything. continue
			}
		}
		return minimumAbosoluteDifference;
    }
}
