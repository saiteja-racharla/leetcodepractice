/*
 * Given an array and target sum. Prints quadruplets whose sum equals to target.
 * Dont print duplicate quadruplets.
 * 4 sum problem
 * Time Complexity:O(n2)
 */
package ds.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FourSumProblem {
	public static void main(String[] args) {
		ArrayList<List<Integer>> res=new ArrayList<List<Integer>>();
		int nums[]={0,0,0,0,0};
		int target=0;
		int len = nums.length;
		if (nums == null || len < 4)
			System.out.println("Null result");
		Arrays.sort(nums);
		int max = nums[len - 1];
		if (4 * nums[0] > target || 4 * max < target)
			System.out.println("Empty Result");
		for(int i=0;i<nums.length-3;i++){
			if(nums[i]+nums[i+1]+nums[i+2]+nums[i+3]>target){
				break;
			}
			if(nums[i]+nums[nums.length-1]+nums[nums.length-2]+nums[nums.length-3]<target)
				continue;
			if(i>0&&nums[i]==nums[i-1])
				continue;
			int remainingSum=target-nums[i];
			res=(ArrayList<List<Integer>>) ThreeSum(nums, remainingSum, nums[i],res,i+1,nums.length-2);
		}
		System.out.println("results size is "+res.size());
	}
	public static List<List<Integer>> ThreeSum(int[] a, int target, int number, ArrayList<List<Integer>> fourSumList,int low1, int high1){
		for(int i=low1;i<high1;i++){
			if(a[i]+a[i+1]+a[i+2]>target)
				break;
			if(a[i]+a[a.length-1]+a[a.length-2]<target)
				continue;
			if(i>low1-1&&a[i]==a[i-1])
				continue;
			int low=i+1;
			int high=a.length-1;
			while(low<high){
				int sum=a[i]+a[low]+a[high];
				if(sum==target){
					fourSumList.add(Arrays.asList(number,a[i],a[low],a[high]));
					System.out.println(number+","+a[i]+","+a[low]+","+a[high]);
					while(low<high && a[low]==a[low+1])	low++;
					while(low<high && a[high]==a[high-1]) high--;
					low++;high--;
				}
				else if(a[low]+a[high]>target)  high--;
				else    low++;
			}
		}
		return fourSumList;
	}

}
