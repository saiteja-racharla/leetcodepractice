/**
 * Given an unsorted integer array, find the smallest missing positive integer.
 * first missing positive integer
 * first missing positive number
 */
package ds.arrays;

public class FirstMissingPositiveInteger {
	public static void main(String[] args) {
		int[] nums={3,4,-1,1};
		System.out.println("First missing positive number is "+firstMissingPositive(nums));
	}
	public static int firstMissingPositive(int[] nums) {
        int i=0;
        while(i<nums.length){
            if(nums[i]<=0 || nums[i]==i+1 || nums[i]>nums.length) 
            	i++;
            else if(nums[nums[i]-1]!=nums[i]) 
            	swap(nums,i,nums[i]-1);
            else i++;
        }
        i=0;
        while(i<nums.length){
            if(nums[i]==i+1) 
            	i++;
            else 
            	break;
        }
        return i+1;
    }
	public static void swap(int[] nums,int i,int j){
        int temp=nums[i];
        nums[i]=nums[j];
        nums[j]=temp;
    }
}
