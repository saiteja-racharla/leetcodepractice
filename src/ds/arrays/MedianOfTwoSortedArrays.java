/**
 * There are two sorted arrays nums1 and nums2 of size m and n respectively.
Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).
You may assume nums1 and nums2 cannot be both empty.
Example 1:
nums1 = [1, 3]
nums2 = [2]
The median is 2.0

Example 2:
nums1 = [1, 2]
nums2 = [3, 4]

The median is (2 + 3)/2 = 2.5
 */
package ds.arrays;

public class MedianOfTwoSortedArrays {

	public static void main(String[] args) {
		int[] nums1= {1,2,3};
		int[] nums2= {4,5,6};
		MedianOfTwoSortedArrays s=new MedianOfTwoSortedArrays();
		System.out.println(s.findMedianSortedArrays(nums1, nums2));
	}
	public double findMedianSortedArrays(int[] nums1, int[] nums2) {
		/* Solution found from tushar roy video.
        https://www.youtube.com/watch?v=LPFhl65R7ww   */
        // Length of nums1 should always be less than length of nums2
        if(nums1.length>nums2.length)
            return findMedianSortedArrays(nums2,nums1);
        int x=nums1.length;
        int y=nums2.length;
        
        int low=0;
        int high=x;
        while(low<=high){
            int partitionX=(low+high)/2;
            int partitionY=(x+y+1)/2-partitionX;
            
            // if partitionX is zero, it means no elements on left side of nums1 array. So its value is assumed as -INF
            // if partitionX is length of nums1, it means no elements on right side of nums1 array. So its value is assumed as +INF
            int maxLeftX=(partitionX==0)?Integer.MIN_VALUE:nums1[partitionX-1];
            int minRightX=(partitionX==x)?Integer.MAX_VALUE:nums1[partitionX];
            
            // if partitionY is zero, it means no elements on left side of nums2 array. So its value is assumed as -INF
            // if partitionY is length of nums2, it means no elements on right side of nums2 array. So its value is assumed as +INF
            int maxLeftY=(partitionY==0)?Integer.MIN_VALUE:nums2[partitionY-1];
            int minRightY=(partitionY==y)?Integer.MAX_VALUE:nums2[partitionY];
            
            if(maxLeftX<=minRightY && maxLeftY<=minRightX){
                //even number of elements
                if ((x + y) % 2 == 0) {
                    return ((double)Math.max(maxLeftX, maxLeftY) + Math.min(minRightX, minRightY))/2;
                }
                //odd number of elements
                else{
                    return (double)Math.max(maxLeftX, maxLeftY);
                }
            }
            //we are too far on right side for partitionX. Go on left side.
            else if(maxLeftX>minRightY){
                high=partitionX-1;
            }
            //we are too far on left side for partitionX. Go on right side.
            else{
                low = partitionX + 1;
            }
        }
        
        //Only we we can come here is if input arrays were not sorted. Throw in that scenario.
        throw new IllegalArgumentException();
    }
}
