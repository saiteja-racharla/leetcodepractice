/**
 * Given a binary array, find the maximum number of consecutive 1s in this array if you can flip at most one 0.

Example 1:
Input: [1,0,1,1,0]
Output: 4
Explanation: Flip the first zero will get the the maximum number of consecutive 1s.
    After flipping, the maximum number of consecutive 1s is 4.
Note:
The input array will only contain 0 and 1.
The length of input array is a positive integer and will not exceed 10,000
Follow up:
What if the input numbers come in one by one as an infinite stream? In other words, you can't store all numbers coming from the stream as it's too large to hold in memory. Could you solve it efficiently?
Leetcode:487
https://leetcode.com/problems/max-consecutive-ones-ii/
https://leetcode.com/problems/max-consecutive-ones-ii/discuss/96920/Java-clean-solution-easily-extensible-to-flipping-k-zero-and-follow-up-handled
 */
package ds.arrays;

public class MaxConsecutiveOnesII {

	public static void main(String[] args) {
		
	}
	 public int findMaxConsecutiveOnes(int[] nums) {
	        int left=0,right=0,firstZero=-1;
	        int max=0;
	        while(right<=nums.length-1){
	            if(firstZero==-1 && nums[right]==0){
	                firstZero=right;
	            } else if(firstZero!=-1 && nums[right]==0){
	                left=firstZero+1;
	                firstZero=right;
	            }
	            max=Math.max(max,right-left+1);
	            right++;
	        }
	        return max;
	    }
}
