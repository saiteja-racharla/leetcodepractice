/**
 * Initially the array is
 * 0	1	2	3	4	5	6	7	8	9	10	11	12
 * 13	13	13	13	13	13	13	13	13	13	13	13	13		initial values
 * 0	13	1	13	2	13	3	13	4	13	5	13	6		first iteration of coin(we use coin[0])
 * 0	13	1	13	2	1	3	2	4	3	2	4	3		second iteration of coin(we use coin[1])
 * 0	13	1	13	2	1	3	2	4	3	1	4	2		third iteration of coin(we use coin[2])
 * 
 */
package ds.arrays;

import java.util.Arrays;

public class CoinChange {
	public static void main(String[] args) {
		int[] coins={2,5,10};
		int amount=12;
		System.out.println(coinChange(coins, amount));
		System.out.println(coinChange1(coins, amount));
	}
	public static int coinChange(int[] coins, int amount) {
		int[] T = new int[amount+1];
        Arrays.fill(T,amount+1);
        T[0]=0;
        for(int j=0;j<coins.length;j++){
            for(int i=1;i<=amount;i++){
                if(i>=coins[j]){
                    T[i]=Math.min(T[i],1+T[i-coins[j]]);
                }
            }
        }
        return T[amount]>amount?-1:T[amount];
    }
	public static int coinChange1(int[] coins, int amount) {
		int[] T = new int[amount+1];
        Arrays.fill(T,amount+1);
        T[0]=0;
        for(int i=1;i<=amount;i++){
        	for(int j=0;j<coins.length;j++){
                if(i>=coins[j]){
                    T[i]=Math.min(T[i],1+T[i-coins[j]]);
                }
            }
        }
        return T[amount]>amount?-1:T[amount];
    }
}
