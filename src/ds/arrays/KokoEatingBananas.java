/**Leetcode-875
 * https://leetcode.com/problems/koko-eating-bananas/description/
 * Koko loves to eat bananas.  There are N piles of bananas, the i-th pile has piles[i] bananas.  The guards have gone and will come back in H hours.

Koko can decide her bananas-per-hour eating speed of K.  Each hour, she chooses some pile of bananas, and eats K bananas from that pile.  If the pile has less than K bananas, she eats all of them instead, and won't eat any more bananas during this hour.

Koko likes to eat slowly, but still wants to finish eating all the bananas before the guards come back.

Return the minimum integer K such that she can eat all the bananas within H hours.

 Input: piles = [3,6,7,11], H = 8
Output: 4

Input: piles = [30,11,23,4,20], H = 5
Output: 30

Input: piles = [30,11,23,4,20], H = 6
Output: 23
 */
package ds.arrays;

public class KokoEatingBananas {

	public static void main(String[] args) {
		int[] piles= {3,6,7,11};
		int H=8;
		System.out.println(minEatingSpeed(piles, H));
	}
	public static int minEatingSpeed(int[] piles, int H) {
        int left=1,right=0;
        for(int i:piles){
        	right=Math.max(i,right);
        }
        while(left<right){
        	int K=(left+right)/2;
        	int countHour = 0; // Hours take to eat all bananas at speed K.
            
            for (int pile : piles) {
                countHour = countHour + pile / K;
                if (pile % K != 0)
                    countHour++;
            }
            if(countHour > H) {
            	left=K+1;
            } else {
            	right=K-1;
            }
        }
		return left;
    }
}
