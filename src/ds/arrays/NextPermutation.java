/**
 * Implement next permutation, which rearranges numbers into the lexicographically next greater permutation of numbers.

If such arrangement is not possible, it must rearrange it as the lowest possible order (ie, sorted in ascending order).

The replacement must be in-place and use only constant extra memory.

Here are some examples. Inputs are in the left-hand column and its corresponding outputs are in the right-hand column.

1,2,3 -> 1,3,2
3,2,1 -> 1,2,3
1,1,5 -> 1,5,1
 */
package ds.arrays;

import java.util.Arrays;

public class NextPermutation {
	public static void main(String[] args) {
		//Same code as NextGreaterElementIII
		NextPermutation next=new NextPermutation();
		int[] nums={9,3,7,6,4};
		next.nextPermutation(nums);
		System.out.println(Arrays.toString(nums));
	}
	public void nextPermutation(int[] a) {
        int i=a.length-2;
        while(i>=0 && a[i]>=a[i+1]){
            i--;
        }
        if(i<0) {
            reverse(a,0);  
            return;
        }
        int j=a.length-1;
        while(j>=0 && a[j]<=a[i]){
            j--;
        }
        swap(a,i,j);
        reverse(a,i+1);
    }
    public void reverse(int[] a,int start){
        int i=start,j=a.length-1;
        while(i<=j){
            swap(a,i,j);
            i++;
            j--;
        }
    }
    public void swap(int[] a,int i,int j){
        int temp=a[i];
        a[i]=a[j];
        a[j]=temp;
    }
}
