/**
 * In a row of dominoes, A[i] and B[i] represent the top and bottom halves of the i-th domino.  (A domino is a tile with two numbers from 1 to 6 - one on each half of the tile.)
We may rotate the i-th domino, so that A[i] and B[i] swap values.
Return the minimum number of rotations so that all the values in A are the same, or all the values in B are the same.
If it cannot be done, return -1.

 * https://leetcode.com/problems/minimum-domino-rotations-for-equal-row/description/
 * 
 * https://leetcode.com/problems/minimum-domino-rotations-for-equal-row/discuss/252633/JavaPython-3-one-pass-counting-O(A-+-B)
 */
package ds.arrays;

public class MinimumDominoRotationsForEqualRow {

	public static void main(String[] args) {
		int[] a= {2,1,2,4,2,2};
		int[] b= {5,2,6,2,3,2};
		MinimumDominoRotationsForEqualRow mr=new MinimumDominoRotationsForEqualRow();
		System.out.println("Minimum number of rotations "+mr.minDominoRotations(a, b));
	}
	public int minDominoRotations(int[] A, int[] B) {
		int[] countA=new int[7];
		int[] countB=new int[7];
		int[] same=new int[7];
		for(int i=0;i<A.length;i++) {
			countA[A[i]]++;
			countB[B[i]]++;
			if(A[i]==B[i]) same[A[i]]++;
		}
		for(int i=1;i<countA.length;i++) {
			if(countA[i]+countB[i]-same[i]>=A.length)
				return Math.min(countA[i], countB[i])-same[i];
		}
		return -1;
    }
}
