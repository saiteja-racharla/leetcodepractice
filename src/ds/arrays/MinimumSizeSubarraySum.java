package ds.arrays;

public class MinimumSizeSubarraySum {

	public static void main(String[] args) {
		int nums[]={2,3,1,2,4,3};
		int s=7;
		int startIndex=0;
		int finalIndex=0;
		int minLength=0;
		int length=0;
		int sum=0;
		for(int i=0;i<nums.length;i++){
			sum=sum+nums[i];
			if(sum<s)
				continue;
			else{
				length=i-startIndex+1;
				if(minLength==0)
					minLength=length;
				else if(length<minLength){
					minLength=length;
					finalIndex=startIndex;
				}
				sum=sum-nums[i]-nums[startIndex];
				startIndex++;
				i--;
			}
		}
		System.out.println(minLength);
		/*if (a == null || a.length == 0){
		    System.out.println(a);
		    System.exit(0);
		}
		  
		  int i = 0, j = 0, sum = 0, min = Integer.MAX_VALUE;
		  
		  while (j < a.length) {
		    sum += a[j++];
		    
		    while (sum >= s) {
		      min = Math.min(min, j - i);
		      sum -= a[i++];
		    }
		  }
		  
		  System.out.println(min == Integer.MAX_VALUE ? 0 : min);*/
	}

}
