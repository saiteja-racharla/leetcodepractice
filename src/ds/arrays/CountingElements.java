package ds.arrays;

import java.util.HashMap;

public class CountingElements {

	public static void main(String[] args) {
		int arr[]= {1,3,2,3,2,1};
		HashMap<Integer,Integer> hm=new HashMap<>();
        int count=0;
        for(int ele:arr){
            if(!hm.containsKey(ele) && hm.containsKey(ele-1)){
                count+=hm.get(ele-1);
            }
            if(hm.containsKey(ele+1)){
                count++;
            }
            hm.put(ele,hm.getOrDefault(ele,0)+1);
        }
        System.out.println(count);
	}

}
