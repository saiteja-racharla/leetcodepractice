/**
 * Design a class to find the kth largest element in a stream. Note that it is the kth largest element in the sorted order, not the kth distinct element.

Your KthLargest class will have a constructor which accepts an integer k and an integer array nums, which contains initial elements from the stream. For each call to the method KthLargest.add, return the element representing the kth largest element in the stream.
int k = 3;
int[] arr = [4,5,8,2];
KthLargest kthLargest = new KthLargest(3, arr);
kthLargest.add(3);   // returns 4
kthLargest.add(5);   // returns 5
kthLargest.add(10);  // returns 5
kthLargest.add(9);   // returns 8
kthLargest.add(4);   // returns 8
 */
package ds.arrays;

import java.util.PriorityQueue;

public class KthLargestElementStream {
	PriorityQueue<Integer> p;
	int k=0;
	public static void main(String[] args) {
		int[] arr= {4,5,8,2};
		int k=3;
		KthLargestElementStream kthLargest = new KthLargestElementStream(3, arr);
		kthLargest.add(3);   // returns 4
		kthLargest.add(5);   // returns 5
		kthLargest.add(10);  // returns 5
		kthLargest.add(9);   // returns 8
		kthLargest.add(4);   // returns 8
	}
	public KthLargestElementStream(int k, int[] nums) {
		this.k=k;
        p=new PriorityQueue<Integer>(k);
        for(int i:nums) {
        	add(i);
        }
    }
    
    public int add(int val) {
    	if(p.size()<val)
    		p.offer(val);
    	else if(val>p.peek()) {
    		p.poll();
        	p.offer(val);
    	}
        return p.peek();
    }
}
