/** Leetcode- 1010.
 * https://leetcode.com/problems/pairs-of-songs-with-total-durations-divisible-by-60/description/
 * Pairs of Songs With Total Durations Divisible by 60
 * Input: [30,20,150,100,40]
Output: 3
Explanation: Three pairs have a total duration divisible by 60:
(time[0] = 30, time[2] = 150): total duration 180
(time[1] = 20, time[3] = 100): total duration 120
(time[1] = 20, time[4] = 40): total duration 60

Input: [60,60,60]
Output: 3
Explanation: All three pairs have a total duration of 120, which is divisible by 60.
 */

package ds.arrays;

import java.util.HashMap;

public class PairOfSongsDivisibleBy60 {
	public static void main(String[] args) {
		int[] time= {30,20,150,80,40};
		int result=numPairsDivisibleBy60Solution2(time);
		System.out.println(result);
	}
    public static int numPairsDivisibleBy60(int[] time) {
    	HashMap<Integer, Integer> hm=new HashMap<Integer, Integer>();
    	int count=0;
    	for(int t:time) {
    		if(hm.containsKey((60-t%60)%60))
    			count=count+hm.get((60-t%60)%60);
    		hm.put(t%60,hm.getOrDefault(t%60, 0)+1);
    	}
    	return count;
    }
    //The above solution can be improved by using an array instead of hashmap.
    //https://leetcode.com/problems/pairs-of-songs-with-total-durations-divisible-by-60/discuss/256738/JavaC++Python-Two-Sum-with-K-60
    public static int numPairsDivisibleBy60Solution2(int[] time) {
    	int c[]=new int[60];
    	int count=0;
    	for(int t:time) {
    		count=count+c[(60-t%60)%60];
    		c[t%60]+=1;
    	}
    	return count;
    }
}
