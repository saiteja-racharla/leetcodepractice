/**
 * In an alien language, surprisingly they also use english lowercase letters, but possibly in a different order. The order of the alphabet is some permutation of lowercase letters.

Given a sequence of words written in the alien language, and the order of the alphabet, return true if and only if the given words are sorted lexicographicaly in this alien language.
Example 1:

Input: words = ["hello","leetcode"], order = "hlabcdefgijkmnopqrstuvwxyz"
Output: true
Explanation: As 'h' comes before 'l' in this language, then the sequence is sorted.
Example 2:

Input: words = ["word","world","row"], order = "worldabcefghijkmnpqstuvxyz"
Output: false
Explanation: As 'd' comes after 'l' in this language, then words[0] > words[1], hence the sequence is unsorted.
Example 3:

Input: words = ["apple","app"], order = "abcdefghijklmnopqrstuvwxyz"
Output: false
Explanation: The first three characters "app" match, and the second string is shorter (in size.) According to lexicographical rules "apple" > "app", because 'l' > '', where '' is defined as the blank character which is less than any other character (More info).
 */
package ds.arrays;

public class VerifyingAlienDictionary {

	public static void main(String[] args) {
		String[] words={"hello","leetcode"};
		String order="hlabcdefgijkmnopqrstuvwxyz";
//		String[] words={"word","world","row"};
//		String order="worldabcefghijkmnpqstuvxyz";
//		String[] words={"apple","app"};
//		String order="abcdefghijklmnopqrstuvwxyz";
//		String[] words={"kuvp","q"};
//		String order="ngxlkthsjuoqcpavbfdermiywz";
		System.out.println(isAlienSorted(words,order));
	}
	public static boolean isAlienSorted(String[] words, String order) {
		int orderArr[]=new int[order.length()];
        for(int i=0;i<order.length();i++){
            orderArr[order.charAt(i)-'a']=i;
        }
        outer:
        for(int k=1;k<words.length;k++){
            String firstWord=words[k-1];
            String secondWord=words[k];
            int i=0,j=0;
            while(i<firstWord.length() && j<secondWord.length()){
            	//only when two characters are not equal, then only compare
                if(orderArr[firstWord.charAt(i)-'a']!=orderArr[secondWord.charAt(j)-'a']){
                	//if the two characters are not equal,then if one is greater than two, return false,
                	//else return true-no need to compare remaining string
                	if(orderArr[firstWord.charAt(i)-'a']>orderArr[secondWord.charAt(j)-'a'])
                		return false;
                	else
                		continue outer;
                }
                i++;
                j++;
            }
            if(firstWord.length()>secondWord.length())
            	return false;
        }
        return true;
    }
}
