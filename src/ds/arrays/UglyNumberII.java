/*
 * Write a program to find the n-th ugly number.

	Ugly numbers are positive numbers whose prime factors only include 2, 3, 5. 
	For example, 1, 2, 3, 4, 5, 6, 8, 9, 10, 12 is the sequence of the first 10 ugly numbers.

	Note that 1 is typically treated as an ugly number, and n does not exceed 1690.
 */
package ds.arrays;

public class UglyNumberII {

	public static void main(String[] args) {
		UglyNumberII u=new UglyNumberII();
		System.out.println(u.nthUglyNumber(12));
		System.out.println(u.nthUglyNumber1(12));
	}
	public int nthUglyNumber(int n) {
		if(n <= 0) return 0; // get rid of corner cases 
        if(n == 1) return 1; // base case
        int[] nums=new int[n];
		int t2=0,t3=0,t5=0;
		nums[0]=1;
        for(int i=1;i<n;i++){
        	nums[i]=Math.min(nums[t2]*2, Math.min(nums[t3]*3, nums[t5]*5));
        	if(nums[i]==nums[t2]*2) t2++;
        	if(nums[i]==nums[t3]*3) t3++;
        	if(nums[i]==nums[t5]*5) t5++;
        }
        return nums[n-1];
    }

	public int nthUglyNumber1(int n) {
		if(n <= 0) return 0; // get rid of corner cases 
        if(n == 1) return 1; // base case
        int[] nums=new int[n];
        int[] primes={2,3,5};
        int[] idx = new int[primes.length];
		nums[0]=1;
        for(int i=1;i<n;i++){
        	nums[i]=Integer.MAX_VALUE;
        	for(int j=0;j<primes.length;j++)
        		nums[i]=Math.min(nums[i],nums[idx[j]]*primes[j]);
        	for(int j=0;j<primes.length;j++){
        		if(nums[idx[j]]*primes[j]==nums[i])
        			idx[j]++;
        	}
        }
        return nums[n-1];
    }
}
