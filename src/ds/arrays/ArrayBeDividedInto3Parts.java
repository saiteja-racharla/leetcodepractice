/**https://leetcode.com/problems/partition-array-into-three-parts-with-equal-sum/description/
 * Leetcode: 1013
 * Given an array A of integers, return true if and only if we can partition the array into three non-empty parts with equal sums.
Formally, we can partition the array if we can find indexes i+1 < j with (A[0] + A[1] + ... + A[i] == A[i+1] + A[i+2] + ... + A[j-1] == A[j] + A[j-1] + ... + A[A.length - 1])
Input: [0,2,1,-6,6,-7,9,1,2,0,1]
Output: true
Explanation: 0 + 2 + 1 = -6 + 6 - 7 + 9 + 1 = 2 + 0 + 1

Input: [0,2,1,-6,6,7,9,-1,2,0,1]
Output: false

Input: [3,3,6,5,-2,2,5,1,-9,4]
Output: true
Explanation: 3 + 3 = 6 = 5 - 2 + 2 + 5 + 1 - 9 + 4
 */
package ds.arrays;

import java.util.Arrays;

public class ArrayBeDividedInto3Parts {

	public static void main(String[] args) {
		int[] A= {0,2,1,-6,6,-7,9,1,2,0,1};
		System.out.println(canThreePartsEqualSum(A));

	}
	public static boolean canThreePartsEqualSum(int[] A) {
        if(A.length==0 || A.length<3) return false;
        int count=Arrays.stream(A).sum();
        if(count%3==0) {
            int equalSum=count/3;
            boolean firstSum=false,secondSum=false,thirdSum=true;
            int sum=0,index=0;
            while(!(sum==equalSum) && index<A.length) {
            	sum=sum+A[index++];
            }
            if(sum==equalSum)
            	firstSum=true;
            sum=0;
            while(!(sum==equalSum) && index<A.length) {
            	sum=sum+A[index++];
            }
            if(sum==equalSum)
            	secondSum=true;
            return firstSum && secondSum && thirdSum;
        } else {
        	return false;
        }
    }
}
