/*
 * For example, Given sorted array nums = [1,1,1,2,2,3],
   Your function should return length = 5, with the first five elements of nums being 1, 1, 2, 2 and 3. 
   It doesn't matter what you leave beyond the new length.
 */
package ds.arrays;

public class RemoveDuplicates2 {
	public static void main(String[] args) {
		RemoveDuplicates2 rs=new RemoveDuplicates2();
		int[] nums={1,1,1,2,2,3};
		System.out.println("After allowing maximum of only 2 duplicates for each element. Size of array becomes "+rs.RemoveDuplicatesFromArray(nums));
	}
	public int RemoveDuplicatesFromArray(int[] nums){
		int i=0;
		for(int n:nums){
			if(i<2 || n>nums[i-2])
				nums[i++]=n;
		}
		return i;
	}
}
