/**
 * https://leetcode.com/problems/top-k-frequent-elements/
 */
package ds.arrays;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TopKFrequentElements {
	public static void main(String[] args) {
		int k=0;
		List<Integer> result;
		/*int[] nums={1,1,1,2,2,3};
		k=2;
		result=topKFrequent(nums,k);
		for(int i:result) System.out.print(i+"\t");
		System.out.println();*/
		
		int[] nums1={1,1,2,2,2,3,3,3,3,4};
		k=2;
		result=topKFrequent(nums1,k);
		for(int i:result) System.out.print(i+"\t");
		System.out.println();
		
		/*
		 * int[] nums2={1,2,3}; k=2; result=topKFrequent(nums2,k); for(int i:result)
		 * System.out.print(i+"\t");
		 */
		
	}
	
	public static List<Integer> topKFrequent(int[] nums, int k) {

		@SuppressWarnings("unchecked")
		List<Integer>[] bucket = new List[nums.length + 1];
		Map<Integer, Integer> frequencyMap = new HashMap<Integer, Integer>();

		for (int n : nums) {
			frequencyMap.put(n, frequencyMap.getOrDefault(n, 0) + 1);
		}

		for (int key : frequencyMap.keySet()) {
			int frequency = frequencyMap.get(key);
			if (bucket[frequency] == null) {
				bucket[frequency] = new ArrayList<>();
			}
			bucket[frequency].add(key);
		}

		List<Integer> res = new ArrayList<>();

		for (int pos = bucket.length - 1; pos >= 0 && res.size() < k; pos--) {
			if (bucket[pos] != null) {
				res.addAll(bucket[pos]);
			}
		}
		return res;
	}
	
	public static List<Integer> topKFrequentSol(int[] nums, int k) {
		Map<Integer, Integer> frequencyMap = new HashMap<Integer, Integer>();
		List<Integer> bucket = new ArrayList<Integer>();
		for (int n : nums) {
			frequencyMap.put(n, frequencyMap.getOrDefault(n, 0) + 1);
		}
		for (int key : frequencyMap.keySet()) {
			int frequency = frequencyMap.get(key);
			if (frequency >= k) {
				bucket.add(key);
			}
			
		}
		return bucket;
	}
}
