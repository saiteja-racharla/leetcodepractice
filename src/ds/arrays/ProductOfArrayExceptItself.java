/*
 * Given an array of n integers where n > 1, nums, return an array output such that output[i] 
 * is equal to the product of all the elements of nums except nums[i].
For example, given [1,2,3,4], return [24,12,8,6].
Time complexity: O(n)
 */

package ds.arrays;

public class ProductOfArrayExceptItself {
	public static void main(String[] args){
		int[] arr={1,2,3,4,5};
		int[] output=new int[arr.length];
		output[0]=1;
		for(int i=1;i<arr.length;i++){
			output[i]=output[i-1]*arr[i-1];
		}
		int temp=1;
		for(int i=arr.length-1;i>=0;i--){
			output[i]=temp*output[i];
			temp=temp*arr[i];
		}
		for(int i=0;i<output.length;i++){
			System.out.print(output[i]+"\t");
		}
	}
}
