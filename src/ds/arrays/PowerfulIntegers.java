package ds.arrays;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PowerfulIntegers {

	public static void main(String[] args) {
		PowerfulIntegers ps=new PowerfulIntegers();
		System.out.println(ps.powerfulIntegers(2, 3, 10));
		System.out.println(ps.powerfulIntegers(2, 1, 10));
		System.out.println(ps.powerfulIntegers(1, 2, 1000000));
	}
	//This one took 2ms to execute.
	public List<Integer> powerfulIntegers(int x, int y, int bound) {
		List<Integer> output=new ArrayList<Integer>();
        Set<Integer> temp=new HashSet<Integer>();
        for(int i=0;Math.pow(x, i)<bound;i++) {
        	for(int j=0,k=(int) (Math.pow(x, i)+Math.pow(y, j));k<=bound;j++) {
        		int sum=(int) (Math.pow(x, i)+Math.pow(y, j));
        		temp.add(sum);
        		if(y==1)
        			break;
        	}
        	if(x==1)
        		break;
        }
        output.addAll(temp);
        return output;
    }
	//This one took 1ms to execute
	public List<Integer> powerfulIntegers1(int x, int y, int bound) {
		List<Integer> output=new ArrayList<Integer>();
        Set<Integer> temp=new HashSet<Integer>();
        for(int i=1;i<bound;i*=x) {
        	for(int j=1;i+j<=bound;j*=y) {
        		temp.add(i+j);
        		if(y==1)
        			break;
        	}
        	if(x==1)
        		break;
        }
        output.addAll(temp);
        return output;
    }
}
