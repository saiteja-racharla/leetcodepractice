/*
 * Given an array and target sum. Prints dublets whose sum equals to target.
 * 2 sum problem
 * Time Complexity:O(n)
 * Space Complexity: O(n)
 */
package ds.arrays;

import java.util.HashMap;

public class TwoSumProblem {

	public static void main(String[] args) {
		int a[]={4,3,1,2,5};
		int target=7;
		HashMap<Integer,Integer> hm=new HashMap<Integer,Integer>();
		for(int i=0;i<a.length;i++){
			int temp=target-a[i];
			if(hm.containsKey(temp)){
				System.out.println("{"+a[i]+","+temp+"}");
			}
			hm.put(a[i],i);
		}
	}

}
