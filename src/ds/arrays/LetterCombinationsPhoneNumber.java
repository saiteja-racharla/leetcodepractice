package ds.arrays;

import java.util.ArrayList;
import java.util.List;

public class LetterCombinationsPhoneNumber {
    public static final String[] keys={"","","abc","def","ghi","jkl","mno","pqrs","tuv","wxyz"};
	public static void main(String[] args) {
		List<String> result=letterCombinations("23");
		for(String res:result){
			System.out.println(res);
		}
	}
	public static List<String> letterCombinations(String digits) {
		List<String> result=new ArrayList<String>();
		letterCombinationsUtil(result,digits,0,"");
		return result;
    }
	public static void letterCombinationsUtil(List<String> result,String digits,int length,String temp) {
		if(length>=digits.length()){
			result.add(temp);
			return;
		}
		String key=keys[digits.charAt(length)-'0'];
		for(int i=0;i<key.length();i++){
			letterCombinationsUtil(result,digits,length+1,temp+key.charAt(i)+"");
		}
    }
}
