/*
 * https://leetcode.com/problems/largest-number/discuss/53158/My-Java-Solution-to-share
 * Given a list of non negative integers, arrange them such that they form the largest number.
 * Example 1:
 * Input: [10,2]
 * Output: "210"
 * 
 * Example 2:
 * Input: [3,30,34,5,9]
 * Output: "9534330"
 */
package ds.arrays;

import java.util.Arrays;
import java.util.Comparator;

public class LargestNumber {
	public static void main(String[] args) {
		int nums[]={10,2};
		String numbers[]=new String[nums.length];
		for(int i=0;i<nums.length;i++){
			numbers[i]=String.valueOf(nums[i]);
		}
		
		Comparator<String> comp=new Comparator<String>() {
			public int compare(String o1, String o2) {
				String s1=o1+o2;
				String s2=o2+o1;
				return s2.compareTo(s1);
			}
		};
		
		Arrays.sort(numbers, comp);
		if(numbers[0].charAt(0) == '0')
            System.out.println("0");
		StringBuilder sb=new StringBuilder();
		for(String s:numbers){
			sb.append(s);
		}
		System.out.println(sb.toString());
	}
}
