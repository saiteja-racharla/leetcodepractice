/**
 * Given an unsorted array nums, reorder it such that nums[0] < nums[1] > nums[2] < nums[3]....

Example 1:

Input: nums = [1, 5, 1, 1, 6, 4]
Output: One possible answer is [1, 4, 1, 5, 1, 6].

Example 2:
Input: nums = [1, 3, 2, 2, 3, 1]
Output: One possible answer is [2, 3, 1, 3, 1, 2].

Note:
You may assume all input has valid answer.

Follow Up:
Can you do it in O(n) time and/or in-place with O(1) extra space?
https://leetcode.com/submissions/detail/292446649/
https://leetcode.com/problems/wiggle-sort-ii/discuss/77682/Step-by-step-explanation-of-index-mapping-in-Java
 */
package ds.arrays;

import java.util.Arrays;

public class WiggleSortII {

	public static void main(String[] args) {
		int[] nums= {5,8,2,4,6,1,3,7};
		wiggleSort(nums);
		for(int i:nums) {
			System.out.print(i+"\t");
		}
	}

	public static void wiggleSort(int[] nums) {
		int n=nums.length;
		Arrays.sort(nums);
		int[] copy=Arrays.copyOf(nums, n);
		int j=n-1,i=0;
		int mid=n/2;
		int count=(n%2==0)?mid:mid-1;
		while(i<n) {
			nums[i]=copy[count];
			if(i<n-1) {
				nums[i+1]=copy[j];
			}
			count--;
			j--;
			i+=2;
		}
	}
}
