/**
 * Given an array of non-negative integers, you are initially positioned at the first index of the array.

Each element in the array represents your maximum jump length at that position.

Your goal is to reach the last index in the minimum number of jumps.

Example:

Input: [2,3,1,1,4]
Output: 2
Explanation: The minimum number of jumps to reach the last index is 2.
    Jump 1 step from index 0 to 1, then 3 steps to the last index.
 */
package ds.arrays;

public class JumpGameII {

	public static void main(String[] args) {
		int[] A={2,1,1,0,4};
		System.out.println(jump(A));
		//System.out.println(jump1(A));
		int[] A1={2,3,1,1,4};
		System.out.println(jump(A1));
		//System.out.println(jump1(A1));
	}
	//this solution works only if there is solution.
	public static int jump(int[] A) {
		int jumps = 0, curEnd = 0, curFarthest = 0;
		for (int i = 0; i < A.length - 1; i++) {
			curFarthest = Math.max(curFarthest, i + A[i]);
			if (i == curEnd) {
				jumps++;
				curEnd = curFarthest;
			}
		}
		return jumps;
    }
	
	//tushar roy solution also does not work when there is no solution
	public static int jump1(int[] nums){
		if (nums.length == 1) {
            return 0;
        }
        int count = 0;
        int i = 0;
        while(i+nums[i]<nums.length-1){
        	int maxVal = 0;
            int maxValIndex = 0;
            for (int j = 1; j <= nums[i]; j++) {
                if (nums[j + i] + j > maxVal) {
                    maxVal = nums[j + i] + j;
                    maxValIndex = i + j;
                }
            }
            i = maxValIndex;
            count++;
        }
		return count+1;
	}

}
