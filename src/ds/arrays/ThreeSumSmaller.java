package ds.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThreeSumSmaller {

	public static void main(String[] args) {
		ThreeSumSmaller t=new ThreeSumSmaller();
		int[] arr={-2, 0, 1, 3};
		int target=2;
		System.out.println("Size of returned value is "+t.threeSumSmaller(arr, target));
	}
	public int threeSumSmaller(int[] arr,int target){
		Arrays.sort(arr);
		int result=0;
		for(int i=0;i<arr.length;i++){
			int temp=arr[i];
			int low=i+1;
			int high=arr.length-1;
			while(low<high){
				if(temp+arr[low]+arr[high]<target){
					result+=(high-low);
					low++;
				}
				else{
					high--;
				}
			}
		}
		return result;
	}
}
