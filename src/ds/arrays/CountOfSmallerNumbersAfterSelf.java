/**
 * You are given an integer array nums and you have to return a new counts array. The counts array has the property where counts[i] is the number of smaller elements to the right of nums[i].

Example:

Input: [5,2,6,1]
Output: [2,1,1,0] 
Explanation:
To the right of 5 there are 2 smaller elements (2 and 1).
To the right of 2 there is only 1 smaller element (1).
To the right of 6 there is 1 smaller element (1).
To the right of 1 there is 0 smaller element.
 */
package ds.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class CountOfSmallerNumbersAfterSelf {
	static int[] count;
	public static void main(String[] args) {
		int[] nums={6,1,4,3,2,0,5};
		List<Integer> result=countSmaller(nums);
		System.out.println(Arrays.toString(result.toArray()));
	}
	public static List<Integer> countSmaller(int[] nums) {
        List<Integer> res = new ArrayList<>();
        if(nums == null || nums.length == 0) return res;
        TreeNode root = new TreeNode(nums[nums.length - 1]);
        res.add(0);
        for(int i = nums.length - 2; i >= 0; i--) {
            int count = insertNode(root, nums[i]);
            res.add(count);
        }
        Collections.reverse(res);
        return res;
    }

    public static int insertNode(TreeNode root, int val) {
        int thisCount = 0;
        while(true) {
            if(val <= root.val) {
                root.count++;
                if(root.left == null) {
                    root.left = new TreeNode(val); break;
                } else {
                    root = root.left;
                }
            } else {
                thisCount += root.count;
                if(root.right == null) {
                    root.right = new TreeNode(val); break;
                } else {
                    root = root.right;
                }
            }
        }
        return thisCount;
    }
	/*public static List<Integer> countSmaller(int[] nums) {
	    List<Integer> res = new ArrayList<Integer>();     

	    count = new int[nums.length];
	    int[] indexes = new int[nums.length];
	    for(int i = 0; i < nums.length; i++){
	    	indexes[i] = i;
	    }
	    mergesort(nums, indexes, 0, nums.length - 1);
	    for(int i = 0; i < count.length; i++){
	    	res.add(count[i]);
	    }
	    return res;
	}
	private static void mergesort(int[] nums, int[] indexes, int start, int end){
		if(end <= start){
			return;
		}
		int mid = (start + end) / 2;
		mergesort(nums, indexes, start, mid);
		mergesort(nums, indexes, mid + 1, end);
		
		merge(nums, indexes, start, end);
	}
	private static void merge(int[] nums, int[] indexes, int start, int end){
		int mid = (start + end) / 2;
		int left_index = start;
		int right_index = mid+1;
		int rightcount = 0;    	
		int[] new_indexes = new int[end - start + 1];

		int sort_index = 0;
		while(left_index <= mid && right_index <= end){
			if(nums[indexes[right_index]] < nums[indexes[left_index]]){
				new_indexes[sort_index] = indexes[right_index];
				rightcount++;
				right_index++;
			}else{
				new_indexes[sort_index] = indexes[left_index];
				count[indexes[left_index]] += rightcount;
				left_index++;
			}
			sort_index++;
		}
		while(left_index <= mid){
			new_indexes[sort_index] = indexes[left_index];
			count[indexes[left_index]] += rightcount;
			left_index++;
			sort_index++;
		}
		while(right_index <= end){
			new_indexes[sort_index++] = indexes[right_index++];
		}
		for(int i = start; i <= end; i++){
			indexes[i] = new_indexes[i - start];
		}
	}*/
}
class TreeNode {
    TreeNode left; 
    TreeNode right;
    int val;
    int count = 1;
    public TreeNode(int val) {
        this.val = val;
    }
}

