package ds.arrays;

public class MaximumConsecutiveOnes {
	public static void main(String[] args) {
		
	}
	public static int findMaxConsecutiveOnes(int[] nums) {
		int count=0;
		int maxOnes=0;
		for(int i=0;i<nums.length;i++){
			if(nums[i]==1)
				count++;
			else{
				maxOnes=Math.max(maxOnes, count);
				count=0;
			}
		}
		maxOnes=Math.max(maxOnes, count);	//if the array has all 1's
        return maxOnes;
    }
}
