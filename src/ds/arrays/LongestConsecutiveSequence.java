/**
 * Given an unsorted array of integers, find the length of the longest consecutive elements sequence.

	Your algorithm should run in O(n) complexity.
	Input: [100, 4, 200, 1, 3, 2]
	Output: 4
	Explanation: The longest consecutive elements sequence is [1, 2, 3, 4]. Therefore its length is 4.
	
	We will use HashMap. The key thing is to keep track of the sequence length and store that in 
	the boundary points of the sequence. For example, as a result, for sequence {1, 2, 3, 4, 5}, 
	map.get(1) and map.get(5) should both return 5.

	Whenever a new element n is inserted into the map, do two things:

See if n - 1 and n + 1 exist in the map, and if so, it means there is an existing sequence next to n. 
Variables left and right will be the length of those two sequences, while 0 means there is no sequence
 and n will be the boundary point later. Store (left + right + 1) as the associated value to key n into 
 the map.
Use left and right to locate the other end of the sequences to the left and right of n respectively, 
and replace the value with the new length.
Everything inside the for loop is O(1) so the total time is O(n). Please comment if you see 
something wrong. Thanks.
 */
package ds.arrays;

import java.util.HashMap;

public class LongestConsecutiveSequence {
	public static void main(String[] args) {
		int[] num={100,4,200,1,3,2,5};
		int result=longestConsecutive(num);
		System.out.println(result);
	}
	public static int longestConsecutive(int[] num) {
		int result=0;
		HashMap<Integer, Integer> hm=new HashMap<Integer,Integer>();
		for(int i=0;i<num.length;i++){
			if(hm.containsKey(num[i])){
				continue;
			}
			else{
				int left=hm.containsKey(num[i]-1)?hm.get(num[i]-1):0;
				int right=hm.containsKey(num[i]+1)?hm.get(num[i]+1):0;
				int sum=left+right+1;
				hm.put(num[i], sum);
				result=Math.max(result, sum);

				hm.put(num[i]-left,sum);
				hm.put(num[i]+right,sum);
			}
		}
		return result;
	}
}
