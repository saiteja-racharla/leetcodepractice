package ds.arrays;

public class NonDecreasingArray {
	public static void main(String[] args){
		int possibleCount=0;
		int nums[]={4,2,3,1};
        for(int i=0;i<nums.length-2;i++){
            if(nums[i]>nums[i+1])
                possibleCount++;
        }
        if(possibleCount>1)
            System.out.println("false");
        else
            System.out.println("true");
	}
}
