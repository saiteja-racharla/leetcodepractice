/**
 * Given a paragraph and a list of banned words, return the most frequent word that is not in the list of banned words.  It is guaranteed there is at least one word that isn't banned, and that the answer is unique.
 * Words in the list of banned words are given in lowercase, and free of punctuation.  Words in the paragraph are not case sensitive.  The answer is in lowercase.
 * Input: 
 * paragraph = "Bob hit a ball, the hit BALL flew far after it was hit."
 * banned = ["hit"]
 * Output: "ball"
 * Explanation: 
 * "hit" occurs 3 times, but it is a banned word.
 * "ball" occurs twice (and no other word does), so it is the most frequent non-banned word in the paragraph. 
 * Note that words in the paragraph are not case sensitive,
 * that punctuation is ignored (even if adjacent to words, such as "ball,"), 
 * and that "hit" isn't the answer even though it occurs more because it is banned.
 */
package ds.arrays;

import java.util.HashMap;
import java.util.Map;

public class MostCommonWord {

	public static void main(String[] args) {
		String paragraph = "Bob. hIt, baLl";
		String[] banned= {"bob", "hit"};
		System.out.println("The most common word is "+mostCommonWord(paragraph,banned));
	}
	public static String mostCommonWord(String paragraph, String[] banned) {
        Map<String,Integer> hm=new HashMap<String,Integer>();
        String[] paras=paragraph.toLowerCase().split("[ !?',;.]");
        for(String word:paras) {
        	if(word != null && !word.isEmpty())
        		hm.put(word, hm.getOrDefault(word, 0)+1);
        }
        for(String word:banned) {
        	if(hm.containsKey(word))
        		hm.remove(word);
        }
        String res=null;
        for(String word : hm.keySet())
        	if(res == null || hm.get(word) > hm.get(res))
                res = word;
        return res;
    }

}
