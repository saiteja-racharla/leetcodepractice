package ds.arrays;

public class RangeSumQuery2D {
	static int[][] nums={
			{3,0,1,4,2},
			{5,6,3,2,1},
			{1,2,0,1,5},
			{4,1,0,1,7},
			{1,0,3,0,5}
	};
	static int[][] sums=new int[nums.length+1][nums[0].length+1];
	public static void main(String[] args) {
    	for(int i=1;i<sums.length;i++){
    		for(int j=1;j<sums[0].length;j++){
    			sums[i][j]=sums[i-1][j]+sums[i][j-1]+nums[i-1][j-1]-sums[i-1][j-1];
    		}
    	}
    	for(int i=0;i<sums.length;i++){
    		for(int j=0;j<sums[0].length;j++){
    			System.out.print(sums[i][j]+"\t");
    		}
    		System.out.println();
    	}
		System.out.println(sumRegion(2,2,4,4));
	}
    
    public static int sumRegion(int row1, int col1, int row2, int col2) {
    	return sums[row2+1][col2+1]-sums[row2+1][col1]-sums[row1][col2+1]+sums[row1][col1];
    }
}
