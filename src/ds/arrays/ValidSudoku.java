package ds.arrays;

import java.util.HashSet;

public class ValidSudoku {

	public static void main(String[] args) {
		char[][] arr={
				{'5','3','.','.','7','.','.','.','.'},
				{'6','.','.','1','9','5','.','.','.'},
				{'.','9','8','.','.','.','.','6','.'},
				{'8','.','.','.','6','.','.','.','3'},
				{'4','.','.','8','.','3','.','.','1'},
				{'7','.','.','.','2','.','.','.','6'},
				{'.','6','.','.','.','.','2','8','.'},
				{'.','.','.','4','1','9','.','.','5'},
				{'.','.','.','.','8','.','.','7','9'}	
		};
		ValidSudoku vs=new ValidSudoku();
		System.out.println("Is valid sudoku-"+vs.isValidSudoku(arr));
	}

	public boolean isValidSudoku(char[][] board) {
		for(int i=0;i<9;i++){
			HashSet<Character> rows=new HashSet<Character>();
			HashSet<Character> cols=new HashSet<Character>();
			HashSet<Character> box=new HashSet<Character>();
			for(int j=0;j<9;j++){
				if(board[i][j]!='.' && !rows.add(board[i][j]))
					return false;
				if(board[j][i]!='.' && !cols.add(board[j][i]))
					return false;
				int RowIndex = 3*(i/3);
	            int ColIndex = 3*(i%3);
	            if(board[RowIndex + j/3][ColIndex + j%3]!='.' && !box.add(board[RowIndex + j/3][ColIndex + j%3]))
	                return false;
			}
		}
		return true;
	}
}
