/**
 * Given an integer array arr, in one move you can select a palindromic subarray arr[i], 
 * arr[i+1], ..., arr[j] where i <= j, and remove that subarray from the given array. 
 * Note that after removing a subarray, the elements on the left and on the right of that subarray move to fill the gap left by the removal.
Return the minimum number of moves needed to remove all numbers from the array.

this would be of some help
https://www.youtube.com/watch?v=KxvTeK2nv28
 */
package ds.arrays;

public class PalindromeRemoval {

	public static void main(String[] args) {
		int[] arr= {1,3,4,1,5};
		PalindromeRemoval pr=new PalindromeRemoval();
		int moves=pr.minimumMoves(arr);
		System.out.println(moves);
	}
	public int minimumMoves(int[] arr) {
		int n=arr.length;
		int dp[][]=new int[n+1][n+1];
		for(int len=1;len<=n;len++) {
			for(int i=0,j=len-1;j<n;i++,j++) {
				if(len==1)
					dp[i][j]=1;
				else {
					dp[i][j]=1+dp[i+1][j];
					if(arr[i]==arr[i+1])
						dp[i][j]=Math.min(1+dp[i+2][j], dp[i][j]);
					for(int k=i+2;k<=j;k++) {
						if(arr[i]==arr[k])
							dp[i][j]=Math.min(dp[i+1][k-1]+dp[k+1][j], dp[i][j]);
					}
				}
			}
		}
        return dp[0][n-1];
    }
}
