/**
 * Jump game
 * Given an array of non-negative integers, you are initially positioned at the first index of the array.

Each element in the array represents your maximum jump length at that position.

Determine if you are able to reach the last index.

Example 1:

Input: [2,3,1,1,4]
Output: true
Explanation: Jump 1 step from index 0 to 1, then 3 steps to the last index.
Example 2:

Input: [3,2,1,0,4]
Output: false
Explanation: You will always arrive at index 3 no matter what. Its maximum
             jump length is 0, which makes it impossible to reach the last index.
 */
package ds.arrays;

public class JumpGame {
	public static void main(String[] args) {
		int nums[]={2,1,1,0,4};
		int nums1[]= {2,3,1,1,4};
		JumpGame j=new JumpGame();
		System.out.println("Can reach to end?? "+j.canJumpToEnd(nums));
		System.out.println("Can reach to end?? "+j.tryFromLast(nums));
		System.out.println("Can reach to end?? "+j.canJumpToEnd(nums1));
		System.out.println("Can reach to end?? "+j.tryFromLast(nums1));
	}
	public boolean canJumpToEnd(int[] nums){
		int i = 0;
		int n=nums.length;
	    for (int reach = 0; i < n && i <= reach; i++)
	        reach = Math.max(i + nums[i], reach);
	    return i == n;
	}
	//reaching from the last..this is an easy solution
	public boolean tryFromLast(int[] nums) {
		int last=nums.length-1;
		for(int i=last-1;i>=0;i--) {
			if(i+nums[i]>=last) {
				last=i;
			}
		}
		return last<=0;
	}
}
