/*
 * This program searches for an element in rotated sorted array
 */
package ds.arrays;

public class SearchRotatedSortedArray {
	public static void main(String[] args) {
		SearchRotatedSortedArray s=new SearchRotatedSortedArray();
		int[] nums={2,2,2,0,2,2};
		int target=0;
		System.out.println("Element found status?"+s.search(nums,target));
	}
	public boolean search(int[] nums, int target) {
		int left = 0, right =  nums.length-1, mid;
        
        while(left<=right)
        {
            mid = (left + right) >> 1;
            if(nums[mid] == target) return true;

            // the only difference from the first one, trickly case, just updat left and right
            if( (nums[left] == nums[mid]) && (nums[right] == nums[mid]) ) {++left; --right;}

            else if(nums[left] <= nums[mid])
            {
                if( (nums[left]<=target) && (nums[mid] > target) ) right = mid-1;
                else left = mid + 1; 
            }
            else
            {
                if((nums[mid] < target) &&  (nums[right] >= target) ) left = mid+1;
                else right = mid-1;
            }
        }
        return false;
    }
}
