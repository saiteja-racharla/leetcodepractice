/*This program finds the duplicate element in the array
 * Time complexity-O(n)
 *Space complexity-O(1)
 *This program does not work if there is an element 500, but there are no 500 elements
 * */
package ds.arrays;

public class FindDuplicates {
	public static void main(String[] args){
		findDuplicates();	//follow this one
		System.out.println("\nSecond way");
		findDuplicates1();
	}
	public static void findDuplicates(){
		int arr[]={1,2,3,1,3,6,6,7};
		for(int i=0;i<arr.length;i++){
			if(arr[Math.abs(arr[i])]>=0)
				arr[Math.abs(arr[i])] = -arr[Math.abs(arr[i])];
			else
				System.out.print(Math.abs(arr[i])+"\t");
		}
	}
	public static void findDuplicates1(){
		/*Given an array of n elements which contains elements from 0 to n-1, with any of these numbers 
		 * appearing any number of times. Find these repeating numbers in O(n) and using only constant 
		 * memory space.
		 */
		int arr[]={1,2,3,1,3,6,6,8,7,9};
		// First check all the values that are
		// present in an array then go to that
		// values as indexes and increment by
		// the size of array
		for (int i = 0; i < arr.length; i++)
		{
			int index = arr[i] % arr.length;
			arr[index] += arr.length;
		}

		// Now check which value exists more
		// than once by dividing with the size
		// of array
		for (int i = 0; i < arr.length; i++)
		{
			if ((arr[i]/arr.length) > 1)
				System.out.print(i+"\t");
		}
	}
}
