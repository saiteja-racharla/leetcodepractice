/**
 * https://leetcode.com/problems/snakes-and-ladders/description/
 */
package ds.arrays;

import java.util.LinkedList;
import java.util.Queue;

public class SnakesAndLadder {

	public static void main(String[] args) {
		
		  int[][] input= { {-1,-1,-1,-1,-1,-1}, {-1,-1,-1,-1,-1,-1},
		  {-1,-1,-1,-1,-1,-1}, {-1,35,-1,-1,13,-1}, {-1,-1,-1,-1,-1,-1},
		  {-1,15,-1,-1,-1,-1} };
		 
		/*
		 * int[][] input= { {-1,-1}, {-1,1} };
		 */
		System.out.println(snakesAndLadders(input));
	}
	public static int snakesAndLadders(int[][] board) {
		int n=board.length;
		boolean visited[]=new boolean[n*n+1];
		Queue<Integer> q=new LinkedList<Integer>();
		q.add(1);
		int move=0;
		while(!q.isEmpty()) {
			for(int size=q.size();size>0;size--) {
				int pop=q.poll();
				if (visited[pop]) 
					continue;
				visited[pop]=true;
				if(pop==n*n)
					return move;
				for(int i=1;i<=6 && pop+i<=n*n;i++) {
					int next=pop+i;
					int nextVal=getBoardValue(board,next);
					if(nextVal>0)
						next=nextVal;
					if(!visited[next])
						q.add(next);
				}
			}
			move++;
		}
        return -1;
    }

    private static int getBoardValue(int[][] board, int num) {
        int n = board.length;
        int r = (num - 1) / n;
        int x = n - 1 - r;
        int y = r % 2 == 0 ? num - 1 - r * n : n + r * n - num;
        return board[x][y];
    }

}
