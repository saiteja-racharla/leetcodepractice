/**
 * Find Words That Can Be Formed by Characters
 * You are given an array of strings words and a string chars.
A string is good if it can be formed by characters from chars (each character can only be used once).
Return the sum of lengths of all good strings in words.
 */
package ds.arrays;

import java.util.Arrays;

public class WordsThatCanBeFormedByCharacters {
	public static void main(String[] args) {
		String[] words= {"hello","world","leetcode"};
		String chars="welldonehoneyr";
		System.out.println(countCharacters(words, chars));
	}
	public static int countCharacters(String[] words, String chars) {
		int count=0;
		for(int i=0;i<words.length;i++) {
			boolean found=true;
			int[] charsCount=new int[26];
			for(int j=0;j<chars.length();j++) {
				charsCount[chars.charAt(j)-'a']++;
			}
			String temp=words[i];
			for(char c:temp.toCharArray()) {
				charsCount[c-'a']--;
				if(charsCount[c-'a']<0) {
					found=false;
					break;
				}
			}
			if(found)
				count=count+temp.length();
			
		}
        return count;
    }
}
