package ds.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;

public class FindKPairsWithSmallestSums {

	public static void main(String[] args) {
		FindKPairsWithSmallestSums f=new FindKPairsWithSmallestSums();
		int[] nums1= {1,2,2};
		int[] nums2= {1,1,3};
		int k=2;
		System.out.println(f.kSmallestPairs(nums1, nums2, k));
	}
	public List<List<Integer>> kSmallestPairs(int[] nums1, int[] nums2, int k) {
        PriorityQueue<int[]> que = new PriorityQueue<>((a,b)->a[0]+a[1]-b[0]-b[1]);
        List<List<Integer>> res = new ArrayList<>();
        if(nums1.length==0 || nums2.length==0 || k==0) return res;
        for(int i=0; i<nums1.length && i<k; i++) 
            que.offer(new int[]{nums1[i], nums2[0], 0});
        while(k-- > 0 && !que.isEmpty()){
            int[] cur = que.poll();
            res.add(new ArrayList<>(Arrays.asList(cur[0], cur[1])));
            if(cur[2] == nums2.length-1) 
                continue;
            que.offer(new int[]{cur[0],nums2[cur[2]+1], cur[2]+1});
        }
        return res;
    }
}
