/**
 * Given an array of integers and an integer k, you need to find the total number of continuous subarrays whose sum equals to k.
Example 1:
Input:nums = [1,1,1], k = 2
Output: 2
Note:
The length of the array is in range [1, 20,000].
The range of numbers in the array is [-1000, 1000] and the range of the integer k is [-1e7, 1e7].

this problem can be solved with sliding window. in this case-28,54,7,-70,22,65,-6
https://leetcode.com/problems/subarray-sums-divisible-by-k/description/
 */
package ds.arrays;

import java.util.HashMap;
import java.util.Map;

public class SubarraySumEqualsK {
	public static void main(String[] args) {
		SubarraySumEqualsK s=new SubarraySumEqualsK();
		int[] arr2= {1,2,3,1,3,2,4}; 
		int k=6; 
		System.out.println(s.subarraySum(arr2,k)); 
		k=2; 
		int[] arr1= {1,1,1}; 
		System.out.println(s.subarraySum(arr1, k));
		k=0; 
		int[] arr3= {1}; 
		System.out.println(s.subarraySum(arr3, k)); 
		k=2; 
		int[]
		arr4= {2,2}; 
		System.out.println(s.subarraySum(arr4, k));
		k=0;
		int[] arr5= {-1,-1,1};
		System.out.println(s.subarraySum(arr5, k));
		int[] arr6= {28,54,7,-70,22,65,-6};
		k=100;
		System.out.println(s.subarraySum(arr6, k));
	}
	public int subarraySum(int[] nums, int k) {
		Map<Integer, Integer> hm=new HashMap<Integer, Integer>();
		int count=0,sum=0;
		hm.put(0, 1);
		for(int i=0;i<nums.length;i++) {
			sum=sum+nums[i];
			if(hm.containsKey(sum-k)) {
				count+=hm.get(sum-k);
			}
			hm.put(sum, hm.getOrDefault(sum,0)+1);
		}
		return count;
    }

}
