/** Microsoft Online Assessment
 * Numbers with equal digit sum
 * Given an array A consisting of N integers, return the maximum sum of two numbers whose digits add up to an equal sum. if there are no two 
 * numbers whose digits add up to an equal sum, then function should return -1
 * Examples:
 * A=[51,71,17,42], function shld return 93. (51,42) and (17,71). The first pair pairs to maximum 93
 * A=[42,33,60], function shld return 102. (42,33) (42,60) (33,60). The second pair pairs to maximum 102
 * A=[51,32,43], function shld return -1;
 */

package ds.arrays;

import java.util.HashMap;
import java.util.Map;

public class NumbersWithEqualDigitSum {

	public static void main(String[] args) {
		int arr1[]= {51,71,17,42};
		int arr2[]= {42,33,60};
		int arr3[]= {51,32,43};
		System.out.println(maxSum(arr1));
		System.out.println(maxSum(arr2));
		System.out.println(maxSum(arr3));

	}
	public static int maxSum(int[] arr) {
		Map<Integer, Integer> hm=new HashMap<>();
		int res=-1;
		for(int i:arr) {
			int digitSum=calculateDigitSum(i);
			if(hm.containsKey(digitSum)) {
				/*if the sum is already present, then we get that number from map and add it to current number to get max*/
				res=Math.max(res, hm.get(digitSum)+i);
				/*we replace the map with this number if this number is larger than the one prsent in map.
				if map contains(5, 32), now we have number 41, the map becomes (5, 41). We replace with greater number if possible so we can 
				achieve maximum of result*/
				hm.put(digitSum, Math.max(i, hm.get(digitSum)));
			} else {
				hm.put(digitSum, i);
			}
		}
		return res;
	}
	public static int calculateDigitSum(int number) {
		int result = 0;
		while(number>0) {
			result=result+(number%10);
			number=number/10;
		}
		return result;
	}

}
