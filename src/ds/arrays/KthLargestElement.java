/**
 * This program finds out the kth largest element in the array
 * Time complexity-Best-O(n) worst-O(n2)
 * Space complexity-O(1)
 */
package ds.arrays;

public class KthLargestElement {

	public static void main(String[] args) {
		int[] arr={3,2,1,5,6,4};
		if(arr.length==0) System.out.println(0);
		System.out.println(quickSelect(arr,0,arr.length-1,2));
	}
	static int quickSelect(int a[],int startIndex, int endIndex, int k){
		int partitionIndex=partition(a,startIndex,endIndex);
		int m=endIndex-partitionIndex+1;
		if(m==k) return a[partitionIndex];
		else if(m<k) 
			return quickSelect(a, startIndex, partitionIndex-1, k-m);
		else 
			return quickSelect(a, partitionIndex+1, endIndex,k);
	}
	static int partition(int a[],int startIndex, int endIndex){
		int partitionIndex=0;
		int pivot=a[endIndex];
		partitionIndex=startIndex;
		for(int i=startIndex;i<=endIndex-1;i++){
			if(a[i]<=pivot){
				int temp=a[i];
				a[i]=a[partitionIndex];
				a[partitionIndex]=temp;
				partitionIndex+=1;
			}
		}
		int temp=a[partitionIndex];
		a[partitionIndex]=a[endIndex];
		a[endIndex]=temp;
		return partitionIndex;
	}
}
