/**
 * https://leetcode.com/problems/play-with-chips/description/
 * There are some chips, and the i-th chip is at position chips[i].
You can perform any of the two following types of moves any number of times (possibly zero) on any chip:
Move the i-th chip by 2 units to the left or to the right with a cost of 0.
Move the i-th chip by 1 unit to the left or to the right with a cost of 1.
There can be two or more chips at the same position initially.
Return the minimum cost needed to move all the chips to the same position (any position).
Example 1:
Input: chips = [1,2,3]
Output: 1
Explanation: Second chip will be moved to positon 3 with cost 1. First chip will be moved to position 3 with cost 0. Total cost is 1.

Example 2:
Input: chips = [2,2,2,3,3]
Output: 2
Explanation: Both fourth and fifth chip will be moved to position two with cost 1. Total minimum cost will be 2.


for [1, 2, 3], would look like:

position 1	position 2	position 3
chip 1	chip 2	chip 3
for [2, 2, 2, 3, 3], would look like:

position 2	position 3
chip 1	    chip 4
chip 2	    chip 5
chip 3	
 */
package ds.arrays;

public class PlaywithChips {

	public static void main(String[] args) {
		int[] chips1= {1,2,3};
		int[] chips2= {1,2,2,2,2};
		int[] chips3= {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30};
		int[] chips4= {2,2,2,3,3};
		PlaywithChips ps=new PlaywithChips();
		System.out.println(ps.minCostToMoveChips(chips1));
		System.out.println(ps.minCostToMoveChips(chips2));
		System.out.println(ps.minCostToMoveChips(chips3));
		System.out.println(ps.minCostToMoveChips(chips4));

	}
	public int minCostToMoveChips(int[] chips) {
        int odd=0,even=0;
        for(int i=0;i<chips.length;i++){
            if(chips[i]%2==0)
                even++;
            else
                odd++;
        }
        return Math.min(odd,even);
    }
}
