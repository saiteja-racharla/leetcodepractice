package ds.arrays;

import java.util.Arrays;

public class ThreeSumClosest {

	public static void main(String[] args) {
		ThreeSumClosest t=new ThreeSumClosest();
		int[] arr={-1,2,1,-4};
		int target=1;
		System.out.println("Sum closest to "+target+" is "+t.threeSumClosest(arr,target));
	}
	public int threeSumClosest(int[] arr,int target){
		Arrays.sort(arr);
		int ans=0;
		int sum=0;
		if(arr.length<3)
			return ans;
		ans=arr[0]+arr[1]+arr[2];
		for(int i=0;i<arr.length-1;i++){
			int temp=arr[i];
			int low=i+1;
			int high=arr.length-1;
			while(low<high){
				sum=temp+arr[low]+arr[high];
				if(Math.abs(target-ans)>Math.abs(target-sum)){
					ans=sum;
					if (ans == target) return ans;
				}
				if(sum>target){
					high--;
				}
				else{
					low++;
				}
			}
		}
		return ans;
		
	}

}
