
/**
 * Given an N x N grid containing only values 0 and 1, where 0 represents water and 1 represents land, find a water cell such that its distance to the nearest land cell is maximized and return the distance.
The distance used in this problem is the Manhattan distance: the distance between two cells (x0, y0) and (x1, y1) is |x0 - x1| + |y0 - y1|.
If no land or water exists in the grid, return -1.
https://leetcode.com/problems/as-far-from-land-as-possible/description/
Time complexity: O(n * n). We process an individual cell only once (or twice).
Space complexity: O(n) for the queue.
Solutions:
https://leetcode.com/problems/as-far-from-land-as-possible/discuss/360963/C++-with-picture-DFS-and-BFS
https://leetcode.com/problems/as-far-from-land-as-possible/discuss/360996/A-very-typical-O(v)-BFS-JAVA-17-ms-faster-than-100.00
 */
package ds.arrays;

import java.util.ArrayDeque;
import java.util.Queue;

public class AsFarFromLandAsPossible {
	static int[][] directions = new int[][] {{-1, 0},{1,0},{0,-1},{0,1}};
	public static void main(String[] args) {
		int[][] grid= {{1,0,0},
						{0,0,0},
						{0,0,0}};
		System.out.println(maxDistance(grid));
	}
	public static int maxDistance(int[][] grid) {
		if(grid == null || grid.length == 0 || grid[0].length == 0)return -1;
		boolean[][] visited = new boolean[grid.length][grid[0].length];
		Queue<int[]> queue = new ArrayDeque<>();
		for(int i=0;i<grid.length;i++) {
			for(int j=0;j<grid[0].length;j++) {
				if(grid[i][j]==1) {
					queue.offer(new int[] {i,j});
					visited[i][j]=true;
				}
					
			}
		}
		int level = -1;
		while(!queue.isEmpty()) {
			int size = queue.size();
			for(int i=0;i<size;i++) {
				int[] poppedEle=queue.poll();
				int x=poppedEle[0];
				int y=poppedEle[1];
				for(int[] dir:directions) {
					int newX = x + dir[0];
                    int newY = y + dir[1];
                    if(newX >= 0 && newX < grid.length && newY >= 0 && newY < grid[0].length 
                            && !visited[newX][newY] && grid[newX][newY] == 0) {
                             visited[newX][newY] = true;
                             queue.offer(new int[]{newX, newY});
                         }
				}
			}
			level++;
		}
		return level <= 0 ? -1 : level;
		
    }
}
