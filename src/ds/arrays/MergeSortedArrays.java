/*
 * Given two sorted integer arrays nums1 and nums2, merge nums2 into nums1 as one sorted array.

Note:
You may assume that nums1 has enough space (size that is greater or equal to m + n) to hold additional 
elements from nums2. The number of elements initialized in nums1 and nums2 are m and n respectively.
 */
package ds.arrays;

public class MergeSortedArrays {
	public static void main(String[] args) {
		MergeSortedArrays msr=new MergeSortedArrays();
		int[] nums1={8,9,10,0,0,0};
		int[] nums2={1,3,5};
		int m=5;
		int n=2;
		msr.merge(nums1, m, nums2, n);
	}
	public void merge(int[] nums1, int m, int[] nums2, int n) {
        int nums1Length=2;
        int nums2Length=2;
        int j=m;
        while(nums1Length>=0 && nums2Length>=0){
	        if(nums1[nums1Length]>nums2[nums2Length]){
	        	nums1[j--]=nums1[nums1Length--];
	        }
	        else{
	        	nums1[j--]=nums2[nums2Length--];
	        }
		}
        while(nums2Length>=0){
        	nums1[j--]=nums2[nums2Length--];
        }
    }
}
