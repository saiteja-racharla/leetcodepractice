package ds.arrays;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class JumpGameIII {

	public static void main(String[] args) {
		int[] arr1= {4,2,3,0,3,1,2};
		int start=5;
		System.out.println(canReach(arr1, start));
		int[] arr2= {4,2,3,0,3,1,2};
		start=0;
		System.out.println(canReach(arr2, start));
		int[] arr3= {3,0,2,1,2};
		start=2;
		System.out.println(canReach(arr3, start));
	}
	public static boolean canReach(int[] arr, int start) {
        Queue<Integer> queue=new LinkedList<Integer>();
        HashSet<Integer> visited=new HashSet<Integer>();;
        queue.add(start);
        while(!queue.isEmpty()) {
        	int index=queue.poll();
        	if(arr[index]==0)
        		return true;
        	if(visited.contains(index)) {
        		continue;
        	}
        	visited.add(index);
        	if(index+arr[index]<arr.length)
        		queue.add(index+arr[index]);
        	if(index-arr[index]>=0)
        		queue.add(index-arr[index]);
        }
        return false;
    }
}
