package ds.arrays;

public class SolveSudoku {
	public static void main(String[] args) {
		sudokuSolution s=new sudokuSolution();
		char[][] board={
				{'5','3','.','.','7','.','.','.','.'},
				{'6','.','.','1','9','5','.','.','.'},
				{'.','9','8','.','.','.','.','6','.'},
				{'8','.','.','.','6','.','.','.','3'},
				{'4','.','.','8','.','3','.','.','1'},
				{'7','.','.','.','2','.','.','.','6'},
				{'.','6','.','.','.','.','2','8','.'},
				{'.','.','.','4','1','9','.','.','5'},
				{'.','.','.','.','8','.','.','7','9'}	
		};
		System.out.println(s.solveSudoku(board));
	}
}

class sudokuSolution{
	public boolean solveSudoku(char[][] board) {
		for(int i = 0; i < board.length; i++){
            for(int j = 0; j < board[0].length; j++){
                if(board[i][j] == '.'){
                    for(char c = '1'; c <= '9'; c++){//trial. Try 1 through 9
                        if(isSafe(board, i, j, c-'0')){
                            board[i][j] = c; //Put c for this cell
                            
                            if(solveSudoku(board))
                                return true; //If it's the solution return true
                            else
                                board[i][j] = '.'; //Otherwise go back
                        }
                    }
                    
                    return false;
                }
            }
        }
        return true;
    }
	public boolean isSafe(char[][] board,int row,int col,int element){
		return !usedInRow(board,row,element) && !usedInCol(board,col,element) && !usedInBox(board,row,col,element);
	}
	public boolean usedInRow(char[][] board,int row,int element){
		for(int i=0;i<board.length;i++){
			if(board[row][i]==element+'0')
				return true;
		}
		return false;
	}
	public boolean usedInCol(char[][] board,int col,int element){
		for(int i=0;i<board[0].length;i++){
			if(board[i][col]==element+'0')
				return true;
		}
		return false;
	}
	public boolean usedInBox(char[][] board,int row,int col, int element){
		for(int j=0;j<9;j++){
			int rowIndex=3*(row/3);
			int colIndex=3*(col/3);
			if(board[rowIndex+(j/3)][colIndex+(j%3)]!='.' && board[rowIndex+(j/3)][colIndex+(j%3)]==element+'0')
				return true;
		}
		return false;
	}
	public void printBoard(char[][] board){
		for(int i=0;i<board.length;i++){
			for(int j=0;j<board[0].length;j++){
				System.out.print(board[i][j]+"\t");
			}
			System.out.println();
		}
	}
	public boolean findUnassignedLocation(char[][] board){
		for(int row=0;row<board.length;row++){
			for(int col=0;col<board[0].length;col++){
				if(board[row][col]=='.')
					return true;
			}
		}
		return false;
	}
}