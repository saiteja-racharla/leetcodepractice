/*
 * Given an array of size n, find the majority element. The majority element is the element that 
 * appears more than n/2 times.
 * 
 * You may assume that the array is non-empty and the majority element always exist in the array.
 * Example 1:
 * Input: [3,2,3]
 * Output: 3
 * 
 * Example 2:
 * Input: [2,2,1,1,1,2,2]
 * Output: 2
 * 
 * Solving using Boyer-Moore Voting Algorithm(look at solution of leetcode)
 * https://leetcode.com/problems/majority-element/solution/
 */
package ds.arrays;

public class MajorityElement {
	public static void main(String[] args){
		int[] nums={7, 7, 5, 7, 5, 1 ,5, 7 , 5, 5, 7, 7 , 5,5,5,5};
		int count=0;
		Integer candidate=null;
		for(int num:nums){
			if(count==0){
				candidate=num;
			}
			if(num==candidate){
				count++;
			}
			else{
				count--;
			}
		}
		System.out.println(candidate);
	}
}
