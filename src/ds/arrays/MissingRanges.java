/**
 * Given a sorted integer array nums, where the range of elements are in the inclusive range [lower, upper], return its missing ranges.

Example:
Input: nums = [0, 1, 3, 50, 75], lower = 0 and upper = 99,
Output: ["2", "4->49", "51->74", "76->99"]
 */
package ds.arrays;

import java.util.ArrayList;
import java.util.List;

public class MissingRanges {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	public List<String> findMissingRanges(int[] nums, int lower, int upper) {
		List<String> midRange=new ArrayList<>();
		if(nums==null || nums.length==0) {
			midRange.add(frameRange(lower, upper));
			return midRange;
		}
		int prev=nums[0];
		if(prev>lower) {
			midRange.add(frameRange(lower,prev-1));
		}
		for(int i=1;i<nums.length;i++) {
			if(nums[i] != prev && nums[i]>prev+1) {
				midRange.add(frameRange(prev+1,nums[i]-1));
			}
			prev=nums[i];
		}
		if(prev<upper) {
			midRange.add(frameRange(prev+1,upper));
		}
		return midRange;
    }
    public static String frameRange(int low, int high){
        return low == high ? String.valueOf(low) : (low + "->" + high);
    }
}
