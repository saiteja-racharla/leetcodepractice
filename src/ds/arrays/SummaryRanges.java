/*
 * Given a sorted integer array without duplicates, return the summary of its ranges.
 * Example 1
 * Input:  [0,1,2,4,5,7]
 * Output: ["0->2","4->5","7"]
 * Explanation: 0,1,2 form a continuous range; 4,5 form a continuous range.
 * 
 * Example 2
 * Input:  [0,2,3,4,6,8,9]
 * Output: ["0","2->4","6","8->9"]
 * Explanation: 2,3,4 form a continuous range; 8,9 form a continuous range.
 */
package ds.arrays;

import java.util.ArrayList;
import java.util.List;

public class SummaryRanges {

	public static void main(String[] args) {
		int[] nums={0,1,3,4,6,7,9};
		List<String> results = summaryRanges(nums);
		for(String result:results){
			System.out.println(result);
		}
	}
	
	public static List<String> summaryRanges(int[] nums) {
        List<String> result=new ArrayList<String>();
        if(nums.length==0)
        	return result;
        int start=nums[0];
        int end=0;
        for(int i=1;i<nums.length;i++){
        	if(nums[i]!=nums[i-1]+1){
        		end=nums[i-1];
        		if(start==end){
        			result.add(start+"");
        		}
        		else{
        			result.add(start+"->"+end);
        		}
        		start=nums[i];
        	}
        }
        if(start==nums[nums.length-1])
        	result.add(start+"");
        else
        	result.add(start+"->"+nums[nums.length-1]);
        return result;
    }
}
