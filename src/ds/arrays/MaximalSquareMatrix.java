/*
 * Given a 2D binary matrix filled with 0's and 1's, 
 * find the largest square containing only 1's and return its area.
 */
package ds.arrays;

public class MaximalSquareMatrix {
	public static void main(String[] args) {
//		int matrix[][]={
//				{1,0,1,0,0},
//				{1,0,1,1,1},
//				{1,1,1,1,1},
//				{1,0,0,1,0}
//		};
		int[][] matrix={};
		System.out.println("Maximum square area is "+maximalSquare(matrix));
	}
	public static int maximalSquare(int[][] matrix) {
		if(matrix.length==0 || matrix[0].length==0) return 0;
		int result=0;
		int[][] b=new int[matrix.length+1][matrix[0].length+1];
		for(int i=1;i<b.length;i++){
			for(int j=1;j<b.length;j++){
				if(matrix[i-1][j-1]==1){
					b[i][j]=Math.min(Math.min(b[i][j-1], b[i-1][j-1]), b[i-1][j])+1;
					result=Math.max(b[i][j], result);
				}
			}
		}
        return result*result;
    }

}
