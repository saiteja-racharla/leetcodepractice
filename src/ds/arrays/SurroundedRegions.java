/*
 * 1. First, check the four border of the matrix. If there is a element is 'O', alter it and all its neighbor 
  'O' elements to '*'.
  2. Then ,alter all the 'O' to 'X'
  3. At last,alter all the '*' to 'O'
 */
package ds.arrays;

public class SurroundedRegions {
	private static final int[][] DIRECTIONS = new int[][] {{-1,0},{1,0},{0,-1},{0,1}};
	public static void main(String[] args) {
		char[][] board={
				{'X','X','X','X'},
				{'X','X','O','X'},
				{'X','O','X','X'},
				{'X','O','X','X'}
		};
		solve(board);
		for(int i=0;i<board.length;i++){
			for(int j=0;j<board[0].length;j++){
				System.out.print(board[i][j]+"\t");
			}
			System.out.println();
		}
	}
	public static void solve(char[][] board) {
		if (board.length == 0 || board[0].length == 0)
			return;
		if (board.length < 2 || board[0].length < 2)
			return;
		int m=board.length;
		int n=board[0].length;
		//Any 'O' connected to boundary cannot turned to 'X'. So we convert 'O' to '*'
		for(int i=0;i<m;i++){
			if(board[i][0]=='O')
				boundaryDFS(board, i, 0);
			if(board[i][n-1]=='O')
				boundaryDFS(board, i, n-1);
		}
		//Any 'O' connected to boundary cannot turned to 'X'. So we convert 'O' to '*'
		for(int j=0;j<n;j++){
			if(board[0][j]=='O')
				boundaryDFS(board, 0, j);
			if(board[m-1][j]=='O')
				boundaryDFS(board, m-1, j);
		}
		//The remaining 'O's in the matrix are not connected to boundary, so we convert
		//them to 'X'
		for(int i=0;i<m;i++){
			for(int j=0;j<n;j++){
				if(board[i][j]=='O')
					board[i][j]='X';
				else if (board[i][j] == '*')
					board[i][j] = 'O';
			}
		}
	}
	private static void boundaryDFS(char[][] board, int i, int j) {
		if(i<0 || i>board.length-1 || j<0 || j>board[0].length-1) return;
		if(board[i][j]=='O')
			board[i][j]='*';
		for (int[] dir : DIRECTIONS) {
            int ii = i+dir[0];
            int jj = j+dir[1];
            if (ii>=0 && ii<board.length && jj>=0 && jj<board[0].length 
            		&& board[ii][jj]=='O') {
            	boundaryDFS(board, ii, jj);
            }
        }
	}
}
