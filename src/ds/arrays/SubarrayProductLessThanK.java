/*
 * Your are given an array of positive integers nums.

Count and print the number of (contiguous) subarrays where the product of all the elements in the 
subarray is less than k
 */
package ds.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SubarrayProductLessThanK {
	public static void main(String[] args) {
		int nums[] = {10,5,2,6};
		int target = 100;
		SubarrayProductLessThanK s=new SubarrayProductLessThanK();
		System.out.println(s.numSubarrayProductLessThanK(nums, target));
	}
	public int numSubarrayProductLessThanK(int[] nums, int k) {
		List<List<Integer>> result=new ArrayList<List<Integer>>();
		List<Integer> temp=new ArrayList<Integer>();
        if (k <= 1) return 0;
        int prod = 1, ans = 0, left = 0;
        for (int right = 0; right < nums.length; right++) {
            prod *= nums[right];
            while (prod >= k) 
            	prod /= nums[left++];
            ans += right - left + 1;
        }
        return ans;
    }
	/*public int numSubarrayProductLessThanK(int[] nums, int target) {
		int output=1;
		List<List<Integer>> result=new ArrayList<List<Integer>>();
		numSubarrayProductLessThanKUtil(nums,output,target,0, new ArrayList<Integer>(),result,true);
		return result.size();
        
    }
	public void numSubarrayProductLessThanKUtil(int[] nums, int output, int target, int start,List<Integer> temp, List<List<Integer>> result, boolean first){
		if(output<target){
			if(first){
				
			}
			else{
				result.add(new ArrayList<>(temp));
			}
		}
		else{
			return ;
		}
		for(int i=start;i<nums.length;i++){
			output=output*nums[i];
			temp.add(nums[i]);
			numSubarrayProductLessThanKUtil(nums, output, target, i+1, temp, result,false);
			int x=temp.remove(temp.size()-1);
			output=output/x;
		}
	}*/
}
