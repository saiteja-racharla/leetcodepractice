package ds.arrays;

public class HouseRobber {
	public static void main(String[] args) {
		HouseRobber hs=new HouseRobber();
		int nums[]={50,12,18,25,30,62};
		System.out.println("Maximum amount robberer can rob is "+hs.rob(nums));
		System.out.println("Maximum amount robberer can rob is "+hs.rob1(nums));
	}
	//this is DP solution. It uses space of O(n)
	public int rob(int[] nums) {
		if(nums.length==0) return 0;
        if(nums.length==1) return nums[0];
        //dp solution
		int[] dp=new int[nums.length];
		dp[0]=nums[0];
		dp[1]=Math.max(nums[0], nums[1]);
		for(int i=2;i<nums.length;i++){
			dp[i]=Math.max(dp[i-2]+nums[i], dp[i-1]);
		}
		return dp[nums.length-1];
    }
	//this is an enhancement of above solution. In this solution, we reduce space to O(1)
	public int rob1(int[] nums){
		if(nums.length==0) return 0;
        if(nums.length==1) return nums[0];
		int a=nums[0];
		int b=Math.max(nums[0], nums[1]);
		for(int i=2;i<nums.length;i++){
			int temp=b;
			b=Math.max(a+nums[i], b);
			a=temp;
		}
		return b;
	}
}
