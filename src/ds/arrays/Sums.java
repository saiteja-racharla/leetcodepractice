/**
 * Find if target number can be achieved by given numbers.The given numbers can be repeated any number
 * of times to get the target
 */
package ds.arrays;
import java.util.*;

public class Sums {
	public static boolean status=false;
	public static void main(String[] args){
		List<Integer> numbers=new ArrayList<Integer>();
		numbers.add(3);
		numbers.add(6);
		int[] nums={3,6};
		sum(0,5,numbers,0);
		System.out.println(status);
	}
	public static void sum(int startIndex,int target, List<Integer> numbers,int sum){
		if(sum==target)
			status=true;
		for(int i=startIndex;i<numbers.size();i++){
			if(sum+numbers.get(i)<=target){
				sum=sum+numbers.get(i);
				sum(i,target,numbers,sum);
				sum -= (Integer) numbers.get(i);
			}
		}
	}
	
}
