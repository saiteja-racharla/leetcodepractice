package ds.linkedlist;

public class ReverseLinkedList {
	public static void main(String[] args) {
		SinglyLinkedListNode n1 = new SinglyLinkedListNode(2,null);
		SinglyLinkedListNode n2 = new SinglyLinkedListNode(3,null);
		SinglyLinkedListNode n3 = new SinglyLinkedListNode(4,null);
		SinglyLinkedListNode n4 = new SinglyLinkedListNode(3,null);
		SinglyLinkedListNode n5 = new SinglyLinkedListNode(4,null);
		SinglyLinkedListNode n6 = new SinglyLinkedListNode(5,null);
 
		n1.next = n2;
		n2.next = n3;
		n3.next = n4;
		n4.next = n5;
		n5.next = n6;
		SinglyLinkedListNode newHead=reverseLinkedListNonRecursive(n1);
		SinglyLinkedListNode temp=newHead;
		while(temp!=null){
			System.out.print(temp.data+"->");
			temp=temp.next;
		}
	}
	public static void reverseLinkedListRecursive(SinglyLinkedListNode node,SinglyLinkedListNode head){
		if(node.next==null){
			head=node;
			return;
		}
		reverseLinkedListRecursive(node.next,head);
		SinglyLinkedListNode temp=node.next;
		temp.next=node;
		node.next=null;
	}
	public static SinglyLinkedListNode reverseLinkedListNonRecursive(SinglyLinkedListNode head){
		SinglyLinkedListNode temp=new SinglyLinkedListNode();
		SinglyLinkedListNode prev=null;
		while(head!=null){
			temp=head;
			head=head.next;
			temp.next=prev;
			prev=temp;
		}
		head=prev;
		return head;
		/*SinglyLinkedListNode prev=null;
		SinglyLinkedListNode next=null;
		while(head.next!=null){
			next=head.next;
			head.next=prev;
			prev=head;
			head=next;
		}
		head.next=prev;
		return head;*/
	}
}
