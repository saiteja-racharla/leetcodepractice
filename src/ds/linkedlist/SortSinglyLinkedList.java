/*
 * This program uses merge sort to sort the linked list
 * Time complexity-O(nlogn)
 */
package ds.linkedlist;

public class SortSinglyLinkedList {
	/*public static void main(String[] args) {
		SinglyLinkedListNode n1 = new SinglyLinkedListNode(2,null);
		SinglyLinkedListNode n2 = new SinglyLinkedListNode(3,null);
		SinglyLinkedListNode n3 = new SinglyLinkedListNode(4,null);
		SinglyLinkedListNode n4 = new SinglyLinkedListNode(3,null);
		SinglyLinkedListNode n5 = new SinglyLinkedListNode(4,null);
		SinglyLinkedListNode n6 = new SinglyLinkedListNode(5,null);
 
		n1.next = n2;
		n2.next = n3;
		n3.next = n4;
		n4.next = n5;
		n5.next = n6;
 
		n1 = mergeSortList(n1);
 
		PrintSinglyLinkedList.printSinglyLinkedList(n1);
	}
	public static SinglyLinkedListNode mergeSortList(SinglyLinkedListNode head) {
		 
		if (head == null || head.next == null)
			return head;
 
		// count total number of elements
		int count = 0;
		SinglyLinkedListNode p = head;
		while (p != null) {
			count++;
			p = p.next;
		}
 
		// break up to two list
		int middle = count / 2;
 
		SinglyLinkedListNode l = head, r = null;
		SinglyLinkedListNode p2 = head;
		int countHalf = 0;
		while (p2 != null) {
			countHalf++;
			SinglyLinkedListNode next = p2.next;
 
			if (countHalf == middle) {
				p2.next = null;
				r = next;
			}
			p2 = next;
		}
 
		// now we have two parts l and r, recursively sort them
		SinglyLinkedListNode h1 = mergeSortList(l);
		SinglyLinkedListNode h2 = mergeSortList(r);
 
		// merge together
		SinglyLinkedListNode merged = merge(h1, h2);
 
		return merged;
	}
	public static SinglyLinkedListNode merge(SinglyLinkedListNode l, SinglyLinkedListNode r) {
		SinglyLinkedListNode p1 = l;
		SinglyLinkedListNode p2 = r;
 
		SinglyLinkedListNode fakeHead = new SinglyLinkedListNode(100,null);
		SinglyLinkedListNode pNew = fakeHead;
 
		while (p1 != null || p2 != null) {
 
			if (p1 == null) {
				pNew.next = new SinglyLinkedListNode(p2.data,null);
				p2 = p2.next;
				pNew = pNew.next;
			} else if (p2 == null) {
				pNew.next = new SinglyLinkedListNode(p1.data, null);
				p1 = p1.next;
				pNew = pNew.next;
			} else {
				if (p1.data < p2.data) {
					// if(fakeHead)
					pNew.next = new SinglyLinkedListNode(p1.data,null);
					p1 = p1.next;
					pNew = pNew.next;
				} else if (p1.data == p2.data) {
					pNew.next = new SinglyLinkedListNode(p1.data,null);
					pNew.next.next = new SinglyLinkedListNode(p1.data,null);
					pNew = pNew.next.next;
					p1 = p1.next;
					p2 = p2.next;
 
				} else {
					pNew.next = new SinglyLinkedListNode(p2.data,null);
					p2 = p2.next;
					pNew = pNew.next;
				}
			}
		}
 
		// printList(fakeHead.next);
		return fakeHead.next;
	}*/
	public static void main(String[] args) {
		SinglyLinkedListNode n1 = new SinglyLinkedListNode(2,null);
		SinglyLinkedListNode n2 = new SinglyLinkedListNode(3,null);
		SinglyLinkedListNode n3 = new SinglyLinkedListNode(4,null);
		SinglyLinkedListNode n4 = new SinglyLinkedListNode(3,null);
		SinglyLinkedListNode n5 = new SinglyLinkedListNode(4,null);
		SinglyLinkedListNode n6 = new SinglyLinkedListNode(5,null);
 
		n1.next = n2;
		n2.next = n3;
		n3.next = n4;
		n4.next = n5;
		n5.next = n6;
 
		n1 = divide(n1);
 
		PrintSinglyLinkedList.printSinglyLinkedList(n1);
	}
	public static SinglyLinkedListNode divide(SinglyLinkedListNode head){
		if(head==null || head.next==null)
			return head;
		SinglyLinkedListNode a=head;		//slow pointer
		SinglyLinkedListNode b=head.next;	//fast pointer
		while(b!=null && b.next!=null){
			head=head.next;
			b=b.next.next;
		}
		b=head.next;
		head.next=null;
		return merge(divide(a),divide(b));
	}
	public static SinglyLinkedListNode merge(SinglyLinkedListNode low, SinglyLinkedListNode high){
		SinglyLinkedListNode temp=new SinglyLinkedListNode();
		SinglyLinkedListNode head=temp;
		SinglyLinkedListNode c=head;
		while(low!=null && high!=null){
			if(low.data<high.data){
				c.next=low;
				c=low;
				low=low.next;
			}
			else{
				c.next=high;
				c=high;
				high=high.next;
			}
		}
		c.next = (low == null) ? high : low;
        return head.next;
	}
}
