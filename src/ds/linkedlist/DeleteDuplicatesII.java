/*
 * Remove Duplicates from Sorted List II
 * delete duplicates
 */
package ds.linkedlist;

public class DeleteDuplicatesII {

	public static void main(String[] args) {
		SinglyLinkedListNode head=new SinglyLinkedListNode(1);
		LinkedListOperations.insertAtEnd(head);
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(2));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(3));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(3));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(3));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(4));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(4));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(5));
		System.out.println("Original List:");
		LinkedListOperations.display(head);
		DeleteDuplicatesII d=new DeleteDuplicatesII();
		SinglyLinkedListNode newHead = d.removeDeplicates(head);
		System.out.println("\nNew List:");
		LinkedListOperations.display(newHead);
	}
	public SinglyLinkedListNode removeDeplicates(SinglyLinkedListNode head){
		if(head==null || head.next==null)
			return head;
		SinglyLinkedListNode pre=new SinglyLinkedListNode(0);
		pre.next=head;
		SinglyLinkedListNode fake=pre;
		SinglyLinkedListNode curr=head;
		while(curr!=null){
			while(curr.next!=null && curr.data==curr.next.data){
				curr=curr.next;
			}
			pre.next=curr;
			pre=pre.next;
			curr=curr.next;
		}
		return fake.next;
	}

}
