
/*
 * This program covers the following features
 * insert
 * insert at position
 * insert at tail
 * remove from position
 * remove matched data
 * remove head
 * remove tail
 * Display Forward
 * Display backward
 */
package ds.linkedlist;

public class DoubleLinkedListOperations {
	static DoubleLinkedListNode head=null;
	static DoubleLinkedListNode tail=null;
	static int length=0;
	public static void main(String[] args){
		
	}
	
	public static int getPosition(int data){
		DoubleLinkedListNode temp=head;
		int position=1;
		while(temp!=null){
			if(temp.data==data)
				return position;
			position++;
			temp=temp.next;
		}
		return Integer.MIN_VALUE;
	}
	
	public static int getLength(){
		return length;
	}
	
	public static void insertAtFront(int data){
		if(head==null){
			head=new DoubleLinkedListNode(data);
			tail=head;
		}
		else{
			DoubleLinkedListNode node=new DoubleLinkedListNode(data);
			node.next=head;
			head.prev=node;
			head=node;
			length++;
		}
	}
	
	public static void insertAtPosition(int data,int position){
		DoubleLinkedListNode newNode=new DoubleLinkedListNode(data);
		if(position<1)
			position=1;
		if(position>length)
			position=length;
		if(head==null){
			insertAtFront(data);
		}
		else if(position==1){
			newNode.next=head;
			head.prev=newNode;
			head=newNode;
			length++;
		}
		else{
			DoubleLinkedListNode temp=head;
			for(int i=0;i<position-2;i++){
				temp=temp.next;
			}
			temp.next=newNode;
			newNode.prev=temp;
			newNode.next=temp.next;
			if(newNode.next!=null){
				newNode.next.prev=newNode;
			}
			length++;
		}
	}
	
	public static void insertTail(int data){
		DoubleLinkedListNode newNode=new DoubleLinkedListNode(data);
		newNode.prev=tail;
		tail.next=newNode;
		tail=newNode;
		length++;
	}
	
	public static void remove(int position){
		if(position<1)
			position=1;
		if(position>length)
			position=length;
		if(position==1){
			head=head.next;
			if(head==null)
				tail=null;	
		}
		else{
			DoubleLinkedListNode temp=head;
			for(int i=0;i<position-2;i++){
				temp=temp.next;
			}
			temp.next=temp.next.next;
			temp.next.prev=temp;
		}
	}
	
	public static void removeMatched(DoubleLinkedListNode node){
		if(head==null)
			return;
		if(node.equals(head)){	
			head=head.next;
			if(head==null)
				tail=null;
			return;
		
		}
		DoubleLinkedListNode temp=head;
		while(temp!=null){
			if(temp.data==node.data){
				temp.prev.next=temp.next;
				temp.next.prev=temp.prev;
				return;
			}
			temp=temp.next;
		}
	}
	
	//Removes the head and sends the next data value
	public int removeHead(){
		if(length==0){
			return Integer.MIN_VALUE;
		}
		DoubleLinkedListNode save=head.next;
		save.prev=null;
		head.next=null;
		head=save;
		length--;
		return save.data;
	}
	
	public int removeTail(){
		if(length==0)
			return Integer.MIN_VALUE;
		DoubleLinkedListNode save=tail.prev;
		save.next=null;
		tail=save;
		length--;
		return save.data;
	}
	
	public void displayFromFront(){
		DoubleLinkedListNode temp=head;
		while(temp!=null){
			System.out.print(temp.data+" ");
			temp=temp.next;
		}
		System.out.println();
	}
	public void displayFromBack(){
		DoubleLinkedListNode temp=tail;
		while(temp!=null){
			System.out.print(temp.data+" ");
			temp=temp.prev;
		}
		System.out.println();
	}
}
