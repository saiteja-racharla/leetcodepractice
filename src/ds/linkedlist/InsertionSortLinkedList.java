/**
 * This program uses insertion sort to sort the linked list
 */

package ds.linkedlist;

public class InsertionSortLinkedList {
	public static void main(String[] args) {
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(1));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(2));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(5));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(4));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(2));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(6));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(7));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(8));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(9));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(10));
		System.out.println("Before sorting");
		LinkedListOperations.display(LinkedListOperations.head);
		System.out.println("\n After sortin");
		LinkedListOperations.display(sort(LinkedListOperations.head));
	}
	public static SinglyLinkedListNode sort(SinglyLinkedListNode head){
		SinglyLinkedListNode newHead=new SinglyLinkedListNode(head.data);
		SinglyLinkedListNode pointer=head.next;
		SinglyLinkedListNode oldHead=null;
		SinglyLinkedListNode next=null;
		while(pointer!=null){
			SinglyLinkedListNode innerPointer=newHead;
			next=pointer.next;
			if(pointer.data<=newHead.data){
				oldHead=newHead;
				newHead=pointer;
				newHead.next=oldHead;
			}
			else{
				while(innerPointer.next!=null){
					if(innerPointer.data<pointer.data && innerPointer.next.data>=pointer.data){
						SinglyLinkedListNode temp=innerPointer.next;
						innerPointer.next=pointer;
						pointer.next=temp;
					}
					innerPointer=innerPointer.next;
				}
				if(innerPointer.next==null && pointer.data>innerPointer.data){
					innerPointer.next=pointer;
					pointer.next=null;
				}
			}
			pointer=next;
		}
		return newHead;
	}
}
