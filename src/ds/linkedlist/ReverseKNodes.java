package ds.linkedlist;

public class ReverseKNodes {
	public static void main(String[] args) {
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(1));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(2));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(5));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(4));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(2));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(6));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(7));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(8));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(9));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(10));
		LinkedListOperations.display(LinkedListOperations.head);
		System.out.println("After exchanging");
		reverseKNodes(4);
	}
	public static void reverseKNodes(int k){
		SinglyLinkedListNode curr=LinkedListOperations.head;
		SinglyLinkedListNode head=LinkedListOperations.head;
		SinglyLinkedListNode prevTail=null;
		SinglyLinkedListNode prevCurrent=LinkedListOperations.head;
		while(curr!=null){
			int count=k;
			SinglyLinkedListNode tail=null;
			while(curr!=null && count>0){
				SinglyLinkedListNode next=curr.next;
				curr.next=tail;
				tail=curr;
				curr=next;
				count--;
			}
			if(prevTail!=null){
				prevTail.next=tail;
			}
			else{
				head=tail;
			}
			prevTail=prevCurrent;
			prevCurrent=curr;
		}
		LinkedListOperations.display(head);
	}
}
