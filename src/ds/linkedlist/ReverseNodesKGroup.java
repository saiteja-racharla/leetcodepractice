/*
 * Given a linked list, reverse the nodes of a linked list k at a time and return its modified list.
 * reverse k nodes

k is a positive integer and is less than or equal to the length of the linked list. 
If the number of nodes is not a multiple of k then left-out nodes in the end should remain as it is.

Example:
Given this linked list: 1->2->3->4->5
For k = 2, you should return: 2->1->4->3->5
For k = 3, you should return: 3->2->1->4->5
 * 
 */
package ds.linkedlist;

public class ReverseNodesKGroup {
	public static void main(String[] args) {
		LinkedListNode node=new LinkedListNode(1);
		node.next=new LinkedListNode(2);
		node.next.next=new LinkedListNode(3);
		node.next.next.next=new LinkedListNode(4);
		node.next.next.next.next=new LinkedListNode(5);
		node.next.next.next.next.next=new LinkedListNode(6);
		node.next.next.next.next.next.next=new LinkedListNode(7);
		node.next.next.next.next.next.next.next=new LinkedListNode(8);
		System.out.println("Original List-");
		display(node);
		LinkedListNode modifiedNodes = reverseKGroups(node, 3);
		System.out.println("\nModified List-");
		display(modifiedNodes);
	}
	public static LinkedListNode reverseKGroups(LinkedListNode head, int k){
		LinkedListNode begin;
		if (head==null || head.next ==null || k==1)
	    	return head;
		LinkedListNode dummyhead = new LinkedListNode(-1);
	    dummyhead.next = head;
	    begin = dummyhead;
	    int i=0;
	    while(head!=null){
	    	i++;
	    	if(i%k==0){
	    		begin = reverseNodes(begin,head.next);
	    		head=begin.next;
	    	}
	    	else
	    		head=head.next;
	    }
	    return dummyhead.next;
	}
	/*
	 * Begin node is -1 and End node is 4(if k=3)
	 * Original list is (begin)-1->1->2->3->4(end)
	 * Modified list is returned as -1->3->2->1(first)->4
	 * -1->1->2->3->4->5
	 * converted to
	 * -1->3->2->1->4->5
	 */
	public static LinkedListNode reverseNodes(LinkedListNode begin,LinkedListNode end){
		LinkedListNode curr=begin.next;
		LinkedListNode prev=begin;
		LinkedListNode next=null;
		LinkedListNode first=null;
		first=curr;
		while(curr!=end){
			next=curr.next;
			curr.next=prev;
			prev=curr;
			curr=next;
		}
		begin.next=prev;
		first.next=curr;
		return first;
	}
	public static void display(LinkedListNode temp){
		while(temp!=null){
			System.out.print(temp.data+"\t");
			temp=temp.next;
		}
	}
}