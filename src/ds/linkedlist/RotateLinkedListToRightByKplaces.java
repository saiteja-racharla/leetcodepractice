/*
 * Rotates the linked list to k positions.
 * 1->2->3->4->5
 * if k=2,
 * then 4->5->1->2->3
 * Time complexity: O(n)
 * Space complexity: O(1)
 */
package ds.linkedlist;

public class RotateLinkedListToRightByKplaces {
	public static void main(String[] args) {
		SinglyLinkedListNode head = new SinglyLinkedListNode(1,null);
		SinglyLinkedListNode prev=head;
		SinglyLinkedListNode temp=null;
		for(int i=2;i<=5;i++){
			if(temp==null)
				temp=new SinglyLinkedListNode();
			temp.data=i;
			prev.next=temp;
			prev=temp;
			temp=temp.next;
		}
		temp=head;
		SinglyLinkedListNode head1=head;
		int k=3;
		for(int i=0;i<k;i++){
			temp=temp.next;
		}
		while(temp.next!=null){
			head1=head1.next;
			temp=temp.next;
		}
		temp.next=head;
		head=head1.next;
		head1.next=null;
		PrintSinglyLinkedList.printSinglyLinkedList(head);
	}
	public SinglyLinkedListNode rotateRight(SinglyLinkedListNode head, int k) {
		if(head==null || head.next==null) return head;
		SinglyLinkedListNode temp=new SinglyLinkedListNode(0);
        temp.next=head;
        SinglyLinkedListNode slow=temp,fast=temp;
        int i=0;
        //calculate length of list
        for(i=0;fast.next!=null;i++)
            fast=fast.next;
        //if k>length of list
        for(int j=i-k%i;j>0;j--)
            slow=slow.next;
        fast.next=temp.next;
        temp.next=slow.next;
        slow.next=null;
        return temp.next;
	}

}
