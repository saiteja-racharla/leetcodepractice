/**
 * This program checks if loop exists or not.
 * If loop exists, check the start node of loop
 */

package ds.linkedlist;

public class FindStartNodeInCycle {
	public static void main(String[] args){
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(1));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(2));
		SinglyLinkedListNode node3=new SinglyLinkedListNode(5);
		LinkedListOperations.insertAtEnd(node3);
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(4));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(2));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(6));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(7));
		SinglyLinkedListNode node8=new SinglyLinkedListNode(8);
		LinkedListOperations.insertAtEnd(node8);
		node8.next=node3;
		findStartNodeInCycleIfCycleExists();
	}
	
	public static void findStartNodeInCycleIfCycleExists(){
		if(LinkedListOperations.head==null) System.out.println("null head");;
		SinglyLinkedListNode fast=LinkedListOperations.head;
		SinglyLinkedListNode slow=LinkedListOperations.head;
		boolean cycleExists=false;
		while(fast!=null && fast.next!=null){
			slow=slow.next;
			fast=fast.next.next;
			if(slow==fast){
				cycleExists = true;
				break;
			}
		}
		slow=LinkedListOperations.head;
		if(cycleExists){
			System.out.println("loop exists");
			while(slow!=fast){
				slow=slow.next;
				fast=fast.next;
			}
			System.out.println("Intersection point of loop"+slow.data);
		}
	}
}
