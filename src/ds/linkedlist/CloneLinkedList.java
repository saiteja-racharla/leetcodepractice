package ds.linkedlist;

import java.util.HashMap;

class CloneLinkedListNode{
	int data;
	CloneLinkedListNode next,random;
	CloneLinkedListNode(int data){
		this.data=data;
	}
	CloneLinkedListNode(){
		
	}
}

public class CloneLinkedList {
	public static void cloneLinkedList(CloneLinkedListNode head){
		/* This method users hashmap and it consumes space complexity of O(N)  */
		HashMap<CloneLinkedListNode, CloneLinkedListNode> hm=new HashMap<CloneLinkedListNode, CloneLinkedListNode>();
		while(head!=null){
			CloneLinkedListNode temp1=new CloneLinkedListNode();
			temp1.data=head.data;
			temp1.next=null;
			temp1.random=null;
			hm.put(head, temp1);
			head=head.next;
		}
		while(head!=null){
			CloneLinkedListNode temp1=hm.get(head);
			temp1.next=hm.get(head.next);
			temp1.random=hm.get(head.random);
			head=head.next;
		}
		
		/* This solution does not need any extra space */
		// First round: make copy of each node,
		// and link them together side-by-side in a single list.
		CloneLinkedListNode iter = head, next;
		while (iter != null) {
			next = iter.next;

			CloneLinkedListNode copy = new CloneLinkedListNode(iter.data);
			iter.next = copy;
			copy.next = next;

			iter = next;
		}

		// Second round: assign random pointers for the copy nodes.
		iter = head;
		while (iter != null) {
			if (iter.random != null) {
				iter.next.random = iter.random.next;
			}
			iter = iter.next.next;
		}

		// Third round: restore the original list, and extract the copy list.
		iter = head;
		CloneLinkedListNode pseudoHead = new CloneLinkedListNode(0);
		CloneLinkedListNode copy, copyIter = pseudoHead;

		while (iter != null) {
			next = iter.next.next;

			// extract the copy
			copy = iter.next;
			copyIter.next = copy;
			copyIter = copy;

			// restore the original list
			iter.next = next;

			iter = next;
		}

		//pseudoHead.next;		//this line produces the new linked list
		
	}
}
