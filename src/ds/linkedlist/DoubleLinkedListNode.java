package ds.linkedlist;

public class DoubleLinkedListNode {
	int data;
	DoubleLinkedListNode prev,next;
	public DoubleLinkedListNode(int data) {
		this.data=data;
		prev=null;
		next=null;
	}
}
