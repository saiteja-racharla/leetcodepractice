/*
 * This program returns the nth node from the end of linked list
 */

package ds.linkedlist;

public class NthNodeFromEnd {
	public static void main(String[] args){
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(1));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(2));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(5));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(4));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(2));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(6));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(7));
		LinkedListOperations.display(LinkedListOperations.head);
		printNthNodeFromEnd(4);
	}
	public static void printNthNodeFromEnd(int n){
		SinglyLinkedListNode fast = LinkedListOperations.head;
		SinglyLinkedListNode slow = LinkedListOperations.head;
		for(int i=0;i<n;i++){
			fast=fast.next;
		}
		while(fast!=null){
			slow=slow.next;
			fast=fast.next;
		}
		System.out.println("\nLast "+n+" from end is "+slow.data);
	}
}
