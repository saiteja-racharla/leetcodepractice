/**
 * This program is used to add two linked lists.
 * Search terms-addition, add, linked links addition, adding linked lists
 * 3->4->3
 * 5->6->4
 * 
 * 8->0->8	-->Output
 */
package ds.linkedlist;

public class AdditionLinkedList {
	public static void main(String[] args) {
		SinglyLinkedListNode head1=new SinglyLinkedListNode(3);
		LinkedListOperations.insertAtEnd(head1);
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(4));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(3));
		SinglyLinkedListNode head2=new SinglyLinkedListNode(5);
		head2.next=new SinglyLinkedListNode(6);
		head2.next.next=new SinglyLinkedListNode(7);
		LinkedListOperations.display(addLinkedLists(head1, head2));
	}
	public static SinglyLinkedListNode addLinkedLists(SinglyLinkedListNode head1,SinglyLinkedListNode head2){
		int carry=0;
		int sum=0;
		SinglyLinkedListNode head=new SinglyLinkedListNode(0);
		SinglyLinkedListNode current=head;
		while(head1!=null && head2!=null){
			sum=head1.data+head2.data+carry;
			carry=sum/10;
			sum=sum%10;
			current.next=new SinglyLinkedListNode(sum);
			current=current.next;
			head1=head1.next;
			head2=head2.next;
		}
		if(head1!=null){
			if(carry!=0){
				current.next=new SinglyLinkedListNode(head.data+carry);
			}
			else{
				current.next=head1;
			}
		}
		else if (head2!=null){
			if(carry!=0){
				current.next=new SinglyLinkedListNode(head2.data+carry);
			}
			else{
				current.next=head2;
			}
		}
		else if(carry!=0){
			current.next=new SinglyLinkedListNode(carry);
		}
		return head.next;
	}
}
