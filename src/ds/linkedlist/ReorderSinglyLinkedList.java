/*
 * L1->L2->L3.....Ln-1->Ln 
 * 		change it to
 * L1->Ln->L2->Ln-1....
 */
package ds.linkedlist;

public class ReorderSinglyLinkedList {
	public static void main(String[] args) {
		SinglyLinkedListNode head = new SinglyLinkedListNode(1,null);
		SinglyLinkedListNode prev=head;
		SinglyLinkedListNode temp=null;
		for(int i=2;i<=6;i++){
			if(temp==null)
				temp=new SinglyLinkedListNode();
			temp.data=i;
			prev.next=temp;
			prev=temp;
			temp=temp.next;
		}
		System.out.println("Original data is ");
		PrintSinglyLinkedList.printSinglyLinkedList(head);
		SinglyLinkedListNode slowPointer=head,fastPointer=head;
		while(fastPointer!=null && fastPointer.next!=null){
			slowPointer=slowPointer.next;
			fastPointer=fastPointer.next.next;
		}
		SinglyLinkedListNode newHead=null;
		newHead=slowPointer.next;
		slowPointer.next=null;
		System.out.println("Created 2 childs");
		System.out.println("\nFirst child");
		PrintSinglyLinkedList.printSinglyLinkedList(head);
		System.out.println("\nSecond child");
		PrintSinglyLinkedList.printSinglyLinkedList(newHead);
		newHead=ReverseLinkedList.reverseLinkedListNonRecursive(newHead);
		System.out.println("\nNew reversed second list");
		PrintSinglyLinkedList.printSinglyLinkedList(newHead);
		SinglyLinkedListNode combinedHead=alternateNodes(head,newHead);
		System.out.println("\nCombined data is ");
		PrintSinglyLinkedList.printSinglyLinkedList(combinedHead);
		
	}
	public static SinglyLinkedListNode alternateNodes(SinglyLinkedListNode head1,SinglyLinkedListNode head2){
		SinglyLinkedListNode temp1,temp2;
		SinglyLinkedListNode combinedHead=head1;
		while(head1!=null && head2!=null){
			temp1=head1;
			temp2=head2;
			head1=head1.next;
			temp1.next=head2;
			head2=head2.next;
			temp2.next=head1;
		}
		return combinedHead;
	}

}
