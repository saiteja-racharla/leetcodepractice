/**
 * This program checks if the given linked list is palindrome or not
 */

package ds.linkedlist;

public class CheckLinkedListPalindrome {
	public static void main(String[] args) {
		SinglyLinkedListNode head=new SinglyLinkedListNode(1);
		LinkedListOperations.insertAtEnd(head);
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(2));
		SinglyLinkedListNode node3=new SinglyLinkedListNode(3);
		LinkedListOperations.insertAtEnd(node3);
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(2));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(1));
		System.out.println("Complete List");
		LinkedListOperations.display(head);
		boolean isPalindrome = CheckLinkedListPalindrome(head);
		if(isPalindrome){
			System.out.println("Palindrome");
		}
		else{
			System.out.println("Not a palindrome");
		}
	}
	public static boolean CheckLinkedListPalindrome(SinglyLinkedListNode head){
		SinglyLinkedListNode slow=head;
		SinglyLinkedListNode fast=head;
		SinglyLinkedListNode prev=null;
		SinglyLinkedListNode newHead=null;
		while(fast!=null && fast.next!=null){
			fast=fast.next.next;
			prev=slow;
			slow=slow.next;
		}
		boolean isPalindrome=true;
		if(fast==null){
			//even case
			prev.next=null;
			newHead=slow;
			newHead = ReverseLinkedList.reverseLinkedListNonRecursive(newHead);
			while(head!=null && newHead!=null){
				head=head.next;
				newHead=newHead.next;
				if(head.data==newHead.data){
					isPalindrome=true;
				}
				else{
					isPalindrome=false;break;
				}
			}
		}
		else{
			//odd case
			newHead=slow.next;
			slow.next=null;
			newHead = ReverseLinkedList.reverseLinkedListNonRecursive(newHead);
			while(newHead!=null){
				if(head.data==newHead.data){
					isPalindrome=true;
				}
				else{
					isPalindrome=false;break;
				}
				head=head.next;
				newHead=newHead.next;
			}
		}
		return isPalindrome;
	}

}
