package ds.linkedlist;

public class PrintSinglyLinkedList {
	public static void printSinglyLinkedList(SinglyLinkedListNode head){
		while(head!=null){
			System.out.print(head.data+" ");
			head=head.next;
		}
	}
}
