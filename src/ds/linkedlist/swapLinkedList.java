/*
 * pair wise swap in linked list
 * a->b->c->d 
 * 		to
 * b->a->d->c
 * Time complexity: O(n)
 */
package ds.linkedlist;

public class swapLinkedList {
		static SinglyLinkedListNode head=null;
		public static void insert(int data){
			SinglyLinkedListNode temp=new SinglyLinkedListNode(data,null);
			if(head==null){
				head=temp;
			}
			else{
				SinglyLinkedListNode current=head;
				while(current.next!=null){
					current=current.next;
				}
				current.next=temp;
			}
		}
		public static void display(){
			SinglyLinkedListNode temp=head;
			PrintSinglyLinkedList.printSinglyLinkedList(temp);
		}
		public static void swap(){
			SinglyLinkedListNode temp = head;
			 
	        /* Traverse only till there are atleast 2 nodes left */
	        while (temp != null && temp.next != null) {
	 
	            /* Swap the data */
	            int k = temp.data;
	            temp.data = temp.next.data;
	            temp.next.data = k;
	            temp = temp.next.next;
	         }
		}
		
		//pair wise swap by changing links
		public SinglyLinkedListNode swapPairs(SinglyLinkedListNode head){
			if(head==null || head.next==null)
	            return head;
			SinglyLinkedListNode prev=head;
			SinglyLinkedListNode curr=prev.next;
			SinglyLinkedListNode node=curr;
	        while(true){
	        	SinglyLinkedListNode next=curr.next;
	            curr.next=prev;
	            if(next==null || next.next==null){
	                prev.next=next;
	                break;
	            }
	            prev.next=next.next;
	            prev=next;
	            curr=prev.next;
	        }
	        return node;
		}
		//static void swap(){
			
		//}
		//recursive is not working. Its prototype.But will work for sure.
		static void recursivePairWiseSwap(){
			recursivePairWiseSwapUtil(head);
		}
		static void recursivePairWiseSwapUtil(SinglyLinkedListNode head)
		{
		  /* There must be at-least two nodes in the list */
		  //if (head != NULL && head.next != NULL)
		  {
		      /* Swap the node's data with data of next node */
		      //swap(head.data, head.next.data);
		    
		      /* Call pairWiseSwap() for rest of the list */
		      recursivePairWiseSwapUtil(head.next.next);
		  }  
		}
	public static void main(String[] args) {
		insert(1);
		insert(2);
		insert(3);
		insert(4);
		display();
		swap();
		System.out.println();
		display();
	}
}
