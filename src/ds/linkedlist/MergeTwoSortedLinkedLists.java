package ds.linkedlist;

public class MergeTwoSortedLinkedLists {
	public static void main(String[] args) {
		SinglyLinkedListNode head1=new SinglyLinkedListNode(1);
		head1.next=new SinglyLinkedListNode(3);
		head1.next.next=new SinglyLinkedListNode(5);
		head1.next.next.next=new SinglyLinkedListNode(7);
		System.out.println("First list");
		LinkedListOperations.display(head1);
		SinglyLinkedListNode head2=new SinglyLinkedListNode(2);
		head2.next=new SinglyLinkedListNode(4);
		head2.next.next=new SinglyLinkedListNode(6);
		head2.next.next.next=new SinglyLinkedListNode(8);
		System.out.println("\nSecond list");
		LinkedListOperations.display(head2);
		SinglyLinkedListNode head3=new SinglyLinkedListNode(0);
		SinglyLinkedListNode curr=head3;
		while(head1!=null && head2!=null){
			if(head1.data<=head2.data){
				curr.next=head1;
				head1=head1.next;
			}
			else{
				curr.next=head2;
				head2=head2.next;
			}
			curr=curr.next;
		}
		if(head1!=null){
			curr.next=head1;
		}
		else if(head2!=null){
			curr.next=head2;
		}
		System.out.println("\nMerged list is");
		LinkedListOperations.display(head3.next);
	}
}
