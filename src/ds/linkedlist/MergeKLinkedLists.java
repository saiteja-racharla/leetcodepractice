/**
 * https://leetcode.com/problems/merge-k-sorted-lists/description/
 * Merge k sorted linked lists and return it as one sorted list. Analyze and describe its complexity.
Example:
Input:
[
  1->4->5,
  1->3->4,
  2->6
]
Output: 1->1->2->3->4->4->5->6
 */
package ds.linkedlist;

public class MergeKLinkedLists {
	public static void main(String[] args) {
		LinkedListNode list1=new LinkedListNode(1);
		list1.next=new LinkedListNode(4);
		list1.next.next=new LinkedListNode(5);
		list1.next.next.next=null;
		
		LinkedListNode list2=new LinkedListNode(1);
		list2.next=new LinkedListNode(3);
		list2.next.next=new LinkedListNode(4);
		list2.next.next.next=null;

		LinkedListNode list3=new LinkedListNode(2);
		list3.next=new LinkedListNode(6);
		list3.next.next=null;
		
		LinkedListNode[] lists={list1,list2,list3};
		LinkedListNode mergedList= mergeKLists(lists);
		while(mergedList!=null){
			System.out.print(mergedList.data+"->");
			mergedList=mergedList.next;
		}
	}
	
	public static LinkedListNode mergeKLists(LinkedListNode[] lists){
	    return partition(lists,0,lists.length-1);
	}
	
	public static LinkedListNode partition(LinkedListNode[] lists,int start, int end){
		if(start==end) return lists[start];
		if(start<end){
		    int mid=start+(end-start)/2;
		    LinkedListNode l1=partition(lists,start,mid);
		    LinkedListNode l2=partition(lists,mid+1,end);
		    return merge(l1,l2);
		}
		else
			return null;
	}
	
	public static LinkedListNode merge(LinkedListNode list1,LinkedListNode list2){
		if(list1==null) return list2;
		if(list2==null) return list1;
		if(list1.data<list2.data){
			list1.next=merge(list1.next,list2);
			return list1;
		}
		else{
			list2.next=merge(list1,list2.next);
			return list2;
		}
	}
}
