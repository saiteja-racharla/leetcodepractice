/**
 * This program is used to split a linked list into two linked lists
 * 1->2->3->4->NULL is split to
 * 1->2->NULL 		and 	3->4->NULL
 */

package ds.linkedlist;

public class SplitLinkedList {
	public static void main(String[] args) {
		SinglyLinkedListNode head=new SinglyLinkedListNode(1);
		LinkedListOperations.insertAtEnd(head);
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(2));
		SinglyLinkedListNode node3=new SinglyLinkedListNode(3);
		LinkedListOperations.insertAtEnd(node3);
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(4));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(5));
		System.out.println("Complete List");
		LinkedListOperations.display(head);
		splitLinkedList(head);
	}
	
	public static void splitLinkedList(SinglyLinkedListNode head){
		SinglyLinkedListNode slow=head;
		SinglyLinkedListNode fast=head;
		SinglyLinkedListNode prev=null;
		SinglyLinkedListNode newHead=null;
		while(fast!=null && fast.next!=null){
			fast=fast.next.next;
			prev=slow;
			slow=slow.next;
		}
		if(fast==null){
			//even case
			prev.next=null;
			newHead=slow;
		}
		else{
			//odd case
			newHead=slow.next;
			slow.next=null;
		}
		System.out.println("\nFirst List");
		LinkedListOperations.display(head);
		System.out.println("\nSecond List");
		LinkedListOperations.display(newHead);
	}
}
