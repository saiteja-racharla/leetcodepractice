/*
 * Find the last element from the beginning whose n%k==0, where n is the number of elements
 * in the list and k is an integer constant.
 * Time complexity - O(n)
 */
package ds.linkedlist;

public class ModularNode {
	public static void main(String[] args) {
		SinglyLinkedListNode head=new SinglyLinkedListNode(1,null);
		SinglyLinkedListNode temp=null;
		SinglyLinkedListNode prev=head;
		for(int i=2;i<=19;i++){
			if(temp==null)
				temp=new SinglyLinkedListNode();
			temp.data=i;
			prev.next=temp;
			prev=temp;
			temp=temp.next;
		}
		PrintSinglyLinkedList.printSinglyLinkedList(head);
		System.out.println();
		SinglyLinkedListNode modularNode=null;
		int i=1,k=3;
		if(k<=0)
			System.out.println("null");
		for(;head!=null;head=head.next){
			if(i%k==0){
				modularNode=head;
			}
			i++;
		}
		System.out.println(modularNode.data);
	}
}
