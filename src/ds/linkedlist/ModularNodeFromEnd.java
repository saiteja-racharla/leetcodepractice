package ds.linkedlist;

public class ModularNodeFromEnd {
	public static void main(String[] args) {
		SinglyLinkedListNode head=new SinglyLinkedListNode(1);
		SinglyLinkedListNode prev=head;
		SinglyLinkedListNode temp=null;
		for(int i=2;i<=19;i++){
			if(temp==null)
				temp=new SinglyLinkedListNode();
			temp.data=i;
			prev.next=temp;
			prev=temp;
			temp=temp.next;
		}
		PrintSinglyLinkedList.printSinglyLinkedList(head);
		int i=0;
		int k=3;
		SinglyLinkedListNode modularNode = null;
		modularNode=head;
		//if you want second modularNode from end, do the loop as follows
		//for(i=0;i<2*k;i++)
		//for(i=0;i<3*k;i++)	//3rd modular node from end
		for(i=0;i<k;i++){
			if(head!=null)
				head=head.next;
			else
				System.out.println("null");
		}
		while(head!=null){
			modularNode = modularNode.next;
			head=head.next;
		}
		System.out.println("Modular Node from end is "+modularNode.data);
	}
}
