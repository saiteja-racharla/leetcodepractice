package ds.linkedlist;

public class ExchangeAdjacencyNodes {

	public static void main(String[] args) {
		SinglyLinkedListNode head=new SinglyLinkedListNode(1);
		LinkedListOperations.insertAtEnd(head);
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(2));
		SinglyLinkedListNode node3=new SinglyLinkedListNode(3);
		LinkedListOperations.insertAtEnd(node3);
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(4));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(5));
		SinglyLinkedListNode displayNode = exchangeAdjacentNodes(head);
		LinkedListOperations.display(displayNode);
		
	}
	public static SinglyLinkedListNode exchangeAdjacentNodes(SinglyLinkedListNode head){
		SinglyLinkedListNode temp=new SinglyLinkedListNode(0);
		temp.next=LinkedListOperations.head;
		SinglyLinkedListNode prev=temp, curr=head;
		while(curr!=null && curr.next!=null){
			SinglyLinkedListNode tmp=curr.next.next;
			curr.next.next=prev.next;
			prev.next=curr.next;
			curr.next=tmp;
			prev=curr;
			curr=prev.next;
		}
		return temp.next;
	}
}
