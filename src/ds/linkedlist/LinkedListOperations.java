/*
 * This program does the following operations
 * insertAtBegin
 * insertAtEnd
 * insert at given position
 * remove from beginning
 * remove matching node
 * remove from given position
 * get the position of the given data
 * display the data
 * Linked list middle element
 * Print list from the end
 * Check linked list is even or odd
 */

package ds.linkedlist;

public class LinkedListOperations {
	static SinglyLinkedListNode head=null;
	public static int length=0;
	public static void main(String[] args){
		SinglyLinkedListNode node=new SinglyLinkedListNode(2);
		insertAtPosition(node,1);
		display(head);
		System.out.println("\nAdded 3 at beginning");
		node=new SinglyLinkedListNode(3);
		insertAtBegin(node);
		display(head);
		System.out.println("\nAdded 4 at end");
		node=new SinglyLinkedListNode(4);
		insertAtEnd(node);
		insertAtEnd(new SinglyLinkedListNode(5));
		insertAtEnd(new SinglyLinkedListNode(6));		
		display(head);
		System.out.println("\nPosition of node 4 is "+getPosition(new SinglyLinkedListNode(5)));
		printMiddleElement();
		System.out.println("Linked list from end is\n");
		printLinkedListFromEnd(head);
		checkIfLinkedListIsEvenOrOdd();
		insertAtEnd(new SinglyLinkedListNode(8));
		display(head);
		checkIfLinkedListIsEvenOrOdd();		
	}
	
	/******************Insertion Operations**********************/
	/**********Insert at Beginning*******************/
	public static void insertAtBegin(SinglyLinkedListNode newNode){
		if(head==null){
			head=newNode;
		}
		else{
			newNode.next=head;
			head=newNode;
		}
		length++;
	}
	
	/*************Insert at End**************************/
	public static void insertAtEnd(SinglyLinkedListNode newNode){
		SinglyLinkedListNode temp=head;
		if(temp==null){
			head=newNode;
		}
		else{
			while(temp.next!=null){
				temp=temp.next;
			}
			temp.next=newNode;
		}
		length++;
	}
	
	/**************Insert at given position***************/
	public static void insertAtPosition(SinglyLinkedListNode newNode,int position){
		//fixing the position
		if(position<1)
			position=1;
		if(position>length+1){
			position=length;
			insertAtEnd(newNode);
		}
		if(head==null){
			head=newNode;
			return;
		}
		SinglyLinkedListNode temp=head;
		if(position==1)
			insertAtBegin(newNode);
		else{
			for(int i=0;i<position-2;i++){
				temp=temp.next;
			}
			newNode.next=temp.next;
			temp.next=newNode;
		}
		length++;
	}
	
	/*****************Deleting the nodes**********************/
	/***************Delete at the beginning******************/
	public static void removeFromBegin(){
		if(head!=null){
			SinglyLinkedListNode temp=head;
			head=head.next;
			temp.next=null;
			length--;
		}
	}
	
	/*****************Deleting a node at the end****************/	
	public static void removeFromEnd(){
		if(head!=null){
			SinglyLinkedListNode temp=head;
			SinglyLinkedListNode prev=null;
			while(temp.next!=null){
				prev=temp;
				temp=temp.next;
			}
			prev.next=null;
			length--;
		}
	}
	
	/******************Deleting a node with the given value*************/
	public void removeMatchingNode(SinglyLinkedListNode node){
		if(head==null)
			return;
		if(node.equals(head))
			removeFromBegin();
		else{
			SinglyLinkedListNode temp=head;
			SinglyLinkedListNode prev=null;
			while(temp.next!=null){
				prev=temp;
				temp=temp.next;
				if(temp.equals(node)){
					break;
				}
			}
			prev.next=temp.next;
			temp.next=null;
		}
	}
	
	/******************Deleting the value at a given position****************/
	public void removePosition(int position){
		if(position==1){
			removeFromBegin();
		}
		else{
			SinglyLinkedListNode temp=head;
			for(int i=0;i<position-2;i++){
				temp=temp.next;
			}
			SinglyLinkedListNode nextNode=temp.next;
			temp.next=nextNode.next;
			nextNode.next=null;
			length--;
			
		}
	}
	
	/******************Get Position of given node**********************/
	public static int getPosition(SinglyLinkedListNode node){
		SinglyLinkedListNode temp=head;
		int position=1;
		while(temp!=null){
			if(node.data==temp.data){
				return position;
			}
			position++;
			temp=temp.next;
		}
		return Integer.MIN_VALUE;
	}
	/***************Display Linked list**********************/
	public static void display(SinglyLinkedListNode temp){
		while(temp!=null){
			System.out.print(temp.data+"\t");
			temp=temp.next;
		}
	}
	
	public static void printMiddleElement(){
		SinglyLinkedListNode slow=head;
		SinglyLinkedListNode fast=head;
		while(fast.next!=null){
			slow=slow.next;
			fast=fast.next.next;
		}
		System.out.println("Middle element is "+slow.data);
	}
	
	public static void printLinkedListFromEnd(SinglyLinkedListNode node){
		if(node==null)
			return;
		printLinkedListFromEnd(node.next);
		System.out.print(node.data+"\t");
	}
	
	public static void checkIfLinkedListIsEvenOrOdd(){
		SinglyLinkedListNode fast=head;
		SinglyLinkedListNode slow=head;
		while(fast!=null && fast.next!=null){
			fast=fast.next.next;
			slow=slow.next;
		}
		if(fast==null){
			System.out.println("Even");
		}
		else{
			System.out.println("Odd");
		}
	}
}
