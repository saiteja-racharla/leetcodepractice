/**
 * Find the length of the loop
 */

package ds.linkedlist;

public class FindLengthOfLoop {
	public static void main(String[] args) {
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(1));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(2));
		SinglyLinkedListNode node3=new SinglyLinkedListNode(5);
		LinkedListOperations.insertAtEnd(node3);
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(4));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(2));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(6));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(7));
		SinglyLinkedListNode node8=new SinglyLinkedListNode(8);
		LinkedListOperations.insertAtEnd(node8);
		node8.next=node3;
		findLengthOfLoop();
	}
	public static void findLengthOfLoop(){
		SinglyLinkedListNode fast=LinkedListOperations.head;
		SinglyLinkedListNode slow=LinkedListOperations.head;
		boolean cycleExists=false;
		while(fast!=null && fast.next!=null){
			slow=slow.next;
			fast=fast.next.next;
			if(slow==fast){
				cycleExists = true;
				break;
			}
		}
		slow=LinkedListOperations.head;
		if(cycleExists){
			System.out.println("loop exists");
			while(slow!=fast){
				slow=slow.next;
				fast=fast.next;
			}
			System.out.println("Intersection point of loop"+slow.data);
		}
		int count=1;
		SinglyLinkedListNode temp=slow;
		slow=slow.next;
		while(temp!=slow){
			slow=slow.next;
			count++;
		}
		System.out.println("Loop count"+count);
	}
}
