package ds.linkedlist;

public class LinkedListNode {
	public int data;
	public LinkedListNode next;
	LinkedListNode(int data){
		this.data=data;
		next=null;
	}
}
