package ds.linkedlist;

public class SinglyLinkedListNode {
	int data;
	SinglyLinkedListNode next;
	public SinglyLinkedListNode(int data, SinglyLinkedListNode next){
		this.data=data;
		this.next=next;
	}
	public SinglyLinkedListNode(){
		this.data=0;
		this.next=null;
	}
	public SinglyLinkedListNode(int data){
		this.data=data;
		this.next=null;
	}
}
