package ds.linkedlist;

class CircularLinkedListNode{
	int data;
	CircularLinkedListNode next;
	public CircularLinkedListNode(int data) {
		this.next=null;
		this.data=data;
	}
}

public class CircularLinkedListOperations {
	static CircularLinkedListNode tail=null;
	static int length=0;
	public static void main(String[] args) {
	
	}
	public static void add(int data){
		addToHead(data);
	}
	
	public int peek(){
		return tail.next.data;
	}
	
	public int tailPeek(){
		return tail.data;
	}
	
	public static void addToHead(int data){
		CircularLinkedListNode temp=new CircularLinkedListNode(data);
		if(tail==null){
			tail=temp;
			tail.next=tail;
		}
		else{
			temp.next=tail.next;
			tail.next=temp;
		}
		length++;
	}
	public static void addToTail(int data){
		addToHead(data);
		tail=tail.next;
	}
	
	public static void removeFromHead(){
		CircularLinkedListNode temp=tail.next;
		if(tail==tail.next){
			tail=null;
		}
		else{
			tail.next=temp.next;
			temp.next=null;
		}
		length--;
	}
	
	public static void removeFromTail(){
		CircularLinkedListNode temp=tail;
		while(temp.next!=tail){
			temp=temp.next;
		}
		temp.next=tail.next;
		tail=temp;
		tail.next=null;
		length--;
	}
	
	public int remove(int data){
		if(tail==null)
			return Integer.MIN_VALUE;
		CircularLinkedListNode temp=tail.next;
		CircularLinkedListNode prev=tail;
		int compares=0;
		for(compares=0;compares<length && (!(temp.data==data));compares++){
			prev=temp;
			temp=temp.next;
		}
		if(temp.data==data){
			if(tail==tail.next)
				tail=null;
			else{
				if(temp==tail)
					tail=prev;
				prev.next=temp.next;
			}
			temp.next=null;
			length--;
			return temp.data;
		}
		return Integer.MIN_VALUE;
	}
	
	public static void display(){
		if(tail==null){
			System.out.println("No Elements");
		}
		CircularLinkedListNode temp=tail.next;
		while(temp!=tail){
			System.out.println(temp.data+"\t");
			temp=temp.next;
		}
	}
}
