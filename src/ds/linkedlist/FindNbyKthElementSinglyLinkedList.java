/*
 * This program finds the n/kth element in the linked list
 */
package ds.linkedlist;

public class FindNbyKthElementSinglyLinkedList {
	public static void main(String[] args) {
		SinglyLinkedListNode head = new SinglyLinkedListNode(1,null);
		SinglyLinkedListNode prev=head;
		SinglyLinkedListNode temp=null;
		for(int i=2;i<=18;i++){
			if(temp==null)
				temp=new SinglyLinkedListNode();
			temp.data=i;
			prev.next=temp;
			prev=temp;
			temp=temp.next;
		}
		PrintSinglyLinkedList.printSinglyLinkedList(head);
		SinglyLinkedListNode fractionalNode=null;
		int i=0;
		int k=3;
		if(k<=0)
			System.out.println("null");
		for(;head!=null;head=head.next){
			if(i%k==0){
				if(fractionalNode==null)
					fractionalNode=head;
				else
					fractionalNode=fractionalNode.next;
			}
			i++;
		}
		System.out.println("\nnode is " +fractionalNode.data);
	}
}
