package ds.linkedlist;

public class CheckIfCycleExists {
	public static void main(String[] args){
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(1));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(2));
		SinglyLinkedListNode node3=new SinglyLinkedListNode(5);
		LinkedListOperations.insertAtEnd(node3);
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(4));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(2));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(6));
		LinkedListOperations.insertAtEnd(new SinglyLinkedListNode(7));
		SinglyLinkedListNode node8=new SinglyLinkedListNode(8);
		LinkedListOperations.insertAtEnd(node8);
		node8.next=node3;
		boolean cycleExists = CheckIfCycleExistsOrNot();
		if(cycleExists)
			System.out.println("Cycle exists");
		else
			System.out.println("No cycle exists");
	}
	
	public static boolean CheckIfCycleExistsOrNot(){
		if(LinkedListOperations.head==null) return false;
		SinglyLinkedListNode fast=LinkedListOperations.head;
		SinglyLinkedListNode slow=LinkedListOperations.head;
		while(fast!=null && fast.next!=null){
			slow=slow.next;
			fast=fast.next.next;
			if(slow==fast){
				return true;
			}
		}
		return false;
	}
}
