package ds.bitManipulations;

import ds.stacks.MaxRectangularAreaInHistogram;;

public class MaximumRectangleAreaInMatrix {

	public static void main(String[] args) {
		int input[][] = {{1,1,1,0},
                {1,1,1,1},
                {0,1,1,0},
                {0,1,1,1},
                {1,0,0,1},
                {1,1,1,1}};
		maximumRectangleAreaInMatrix(input);
	}
	public static void maximumRectangleAreaInMatrix(int[][] input){
		int area=0;
		int maxArea=0;
		int temp[] = new int[input[0].length];
		for(int i=0;i<input.length;i++){
			for(int j=0;j<input[0].length;j++){
				if(input[i][j]==0)
					temp[j]=0;
				else
					temp[j]=temp[j]+input[i][j];
			}
			area=MaxRectangularAreaInHistogram.findMaxAreaInHistogram(temp);
			if(area>maxArea)
				maxArea=area;
		}
		System.out.println("Area of rectangle is "+maxArea);
	}

}
