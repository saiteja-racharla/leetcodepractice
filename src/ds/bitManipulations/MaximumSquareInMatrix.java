package ds.bitManipulations;

public class MaximumSquareInMatrix {
	public static void main(String[] args) {
		int M[][] = {{0, 1, 1, 0, 1}, 
				{1, 1, 0, 1, 0}, 
				{0, 1, 1, 1, 0},
				{1, 1, 1, 1, 0},
				{1, 1, 1, 1, 1},
				{0, 0, 0, 0, 0}};
		printMaxSubSquare(M);
	}
	public static void printMaxSubSquare(int arr[][]){
		int max=0;
		int[][] result=new int[arr.length][arr[0].length];
		for(int i=0; i < arr.length; i++){
            result[i][0] = arr[i][0];
            if (result[i][0] == 1)
            {
                max = 1;
            }
        }
		for(int i=0; i < arr[0].length; i++){
            result[0][i] = arr[0][i];
            if (result[0][i] == 1)
            {
                max = 1;
            }
            
        }
		for(int i=1; i < arr.length; i++){
            for(int j=1; j < arr[i].length; j++){
                if(arr[i][j] == 0){
                    continue;
                }
                int t = Math.min(result[i-1][j],Math.min(result[i-1][j-1],result[i][j-1]));
                result[i][j] =  t +1;
                if(result[i][j] > max){
                    max = result[i][j];
                }
            }
        }
		System.out.println(max);
	}
}
