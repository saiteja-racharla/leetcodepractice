package ds.bitManipulations;

public class ConvertIntegerToBinary {

	public static void main(String[] args) {
		ConvertIntegerToBinary c=new ConvertIntegerToBinary();
		int number=12;
		c.printBinaryRepresentation(number);
	}
	public void printBinaryRepresentation(int number){
		int mask = 1 << 31;
		for(int i=1; i<=32; i++) {
			if( (mask & number) != 0 )
				System.out.print(1);
	        else
	            System.out.print(0);
			if(i%4==0)
				System.out.print("\t");
	        mask = mask >> 1;
		}
	}

}
