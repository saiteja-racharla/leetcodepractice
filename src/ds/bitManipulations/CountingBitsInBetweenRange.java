/*
 * Given a non negative integer number num. For every numbers i in the range 0 ≤ i ≤ num 
 * calculate the number of 1's in their binary representation and return them as an array.

Example:
For num = 5 you should return [0,1,1,2,1,2].
 */
package ds.bitManipulations;

public class CountingBitsInBetweenRange {

	public static void main(String[] args) {
		int num=5;
		int output[]={};
        output=new int[num+1];
        output[0]=0;
        int offset=1;
        for(int i=1;i<=num;i++){
            if(offset*2==i){
                offset=offset*2;
            }
            output[i]=output[i-offset]+1;
        }
        for(int i=0;i<=num;i++){
        	System.out.print(output[i]+",");
        }
	}

}
