/*
 * https://www.geeksforgeeks.org/add-two-numbers-without-using-arithmetic-operators/
 */
/*
 * Sum of two bits can be obtained by performing XOR (^) of the two bits. Carry bit can be 
 * obtained by performing AND (&) of two bits.
Above is simple Half Adder logic that can be used to add 2 single bits. 
We can extend this logic for integers. If x and y don�t have set bits at same position(s), 
then bitwise XOR (^) of x and y gives the sum of x and y. To incorporate common set bits also, 
bitwise AND (&) is used. Bitwise AND of x and y gives all carry bits. We calculate (x & y) << 1 and 
add it to x ^ y to get the required result.
XOR-Same '0', different '1'
 */
package ds.bitManipulations;

public class SumOfTwoNumbers {

	public static void main(String[] args) {
		int a=11;
		int b=15;
		System.out.println(getsum(a,b));
		System.out.println(getSumIterative(a,b));
	}
	
	public static int getsum(int a, int b){
		if(b==0) return a;
		int carry=(a&b)<<1;			//<<-left shift
		int sum=a^b;
		return getsum(sum,carry);
	}
	
	public static int getSumIterative(int a, int b){
		while(b!=0){
			int carry=a&b;		
			a=a^b;
			b=carry<<1;
		}
		return a;
	}

}
