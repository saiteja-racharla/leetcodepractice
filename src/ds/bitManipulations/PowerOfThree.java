package ds.bitManipulations;

public class PowerOfThree {
	public static void main(String[] args) {
		// 1162261467 is 3^19,  3^20 is bigger than int  
		int n=81;
		System.out.println(n>0 && 1162261467%n==0);
	}
}
