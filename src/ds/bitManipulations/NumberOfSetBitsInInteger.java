/*
 * Can be done in two approaches.
 * 1. time-complexity-O(logn)
 * 		space-complexity-O(1)
 * 2. time-complexity-O((logn)/k)
 * 		best-case-O(1)
 * 		Space complexity-O(2 power k)
 */
package ds.bitManipulations;

public class NumberOfSetBitsInInteger {

	public static void main(String[] args) {
		int countOfSetBits=0;
		int integer=255;
		System.out.println("Using first approach. Count of set bits in "+integer +" is "+countSetBits1(integer));
		System.out.println("Using second approach. Count of set bits in "+integer +" is "+countSetBits2(integer));
	}
	public static int countSetBits1(int integer){
		int count=0;
		while(integer!=0){
			integer=integer&(integer-1);
			count++;
		}
		return count;
	}
	public static int countSetBits2(int integer){
		int count=0;
		int table[]={0,1,1,2,1,2,2,3,1,2,2,3,2,3,3,4};
		while(integer!=0){
			//with below statement,integer & 0x0F you get the last 4 indices using which we can search in above table
			count=count+table[integer & 0x0F];
			integer=integer>>4;
		}
		return count;
	}

}
