/*
 * Given an array of numbers nums, in which exactly two elements appear only once and all the 
 * other elements appear exactly twice. Find the two elements that appear only once.
 * For example:
 * 
 * Given nums = [1, 2, 1, 3, 2, 5], return [3, 5].
 */
package ds.bitManipulations;

public class TwoElementsOccuringOnce {

	public static void main(String[] args) {
		int arr[]={2,4,7,9,2,4};
		int set_bit_no;
		int x = 0,y = 0;
		int xor=arr[0];
		for(int i=0;i<arr.length;i++){
			xor=xor^arr[i];
		}
		set_bit_no = xor & ~(xor-1);
		for(int i=0;i<arr.length;i++){
			if((arr[i] & set_bit_no) == 0)
				x=x^arr[i];
			else
				y=y^arr[i];
		}
		System.out.println("Unique numbers are "+x+" and "+y);
	}

}
