package ds.bitManipulations;

public class ConvertIntegerToHex {
	public static void main(String[] args) {
		ConvertIntegerToHex c=new ConvertIntegerToHex();
		int number=-2;
		System.out.println(c.printHex(number));
	}
	
	public String printHex(int number){
		char[] map={'0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f'};
		if(number==0)
			return "0";
		String result="";
		while(number!=0){
			result=map[(number&0x0F)]+result;
			number=(number>>>4);
		}
		return result;
	}
}
