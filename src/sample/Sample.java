package sample;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;



class Result {

	/*
	 * Complete the 'closestStraightCity' function below.
	 *
	 * The function is expected to return a STRING_ARRAY.
	 * The function accepts following parameters:
	 *  1. STRING_ARRAY c
	 *  2. INTEGER_ARRAY x
	 *  3. INTEGER_ARRAY y
	 *  4. STRING_ARRAY q
	 */

	public static List<String> closestStraightCity(List<String> c, List<Integer> x, List<Integer> y, List<String> q) {
		List<Double> result = new ArrayList< >();
		List<Double> dislist = new ArrayList< >();
		List<String> res = new ArrayList<String>();
		int temp=0,dis=0;
		int[] tempAr=new int[1000];
		for(int i=0;i<c.size();i++)
		{
			temp=0;
			for(int j=0;j<x.size();j++)
			{
				if(i==j)
				{

				}
				else{
					if(x.get(i)==x.get(j)||y.get(i)==y.get(j))
					{
						dislist.add(dis(x.get(i),y.get(i),x.get(j),y.get(j))); 
						System.out.println(c.get(i)+" dis "+ dis);
						temp=1;
					}
					else{
						dislist.add((double) 0); 
					}
				}

			}

			if(temp==0)
			{
				res.add("None");
				//  System.out.println(c.get(i));
			}
			else{
				List<Double> disTemplist = new ArrayList<Double>(dislist);
				Collections.sort(disTemplist);
				int ind=disTemplist.size();   
				//result.add(c.get(dislist.indexOf(ind)));
			}

		}
		return res;
	}

	public static double dis(int x1, int y1, int x2, int y2) 
	{       
		double x = Math.sqrt((y2 - y1) * (y2 - y1) + (x2 - x1) * (x2 - x1));
		return x;
	}
}

	public class Sample {
		public static void main(String[] args) throws IOException {
			List<String> c = new ArrayList<>(Arrays.asList("fastcity", "bigbanana", "xyz"));

			List<Integer> x = new ArrayList<>(Arrays.asList(23,23,23));

			List<Integer> y = new ArrayList<>(Arrays.asList(1,10,20));

			List<String> q = new ArrayList<>(Arrays.asList("fastcity", "bigbanana", "xyz"));

			List<String> result = Result.closestStraightCity(c, x, y, q);

			for (int i = 0; i < result.size(); i++) {
				System.out.println(result.get(i));

				if (i != result.size() - 1) {
					System.out.println("\n");
				}
			}
		}
	}