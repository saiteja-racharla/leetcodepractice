/*
 * Count all possible paths from top left to bottom right of a mXn matrix
 */
package sample;

public class PathsFromMtoN {

	public static int numberOfPaths(int m,int n){
		if (m == 1 || n == 1)
			return 1;
		return numberOfPaths(m-1, n) + numberOfPaths(m, n-1);
	}
	public static int numberOfPathsDP(int m,int n){
		if (m == 1 || n == 1)
			return 1;
		int count[][]=new int[m][n];
		for(int i=0;i<m;i++)
			count[i][0]=1;
		for(int i=0;i<n;i++)
			count[0][i]=1;
		for(int i=1;i<m;i++){
			for(int j=1;j<n;j++){
				count[i][j]=count[i-1][j]+count[i][j-1];
			}
		}
		return count[m-1][n-1];
	}
	public static void main(String[] args) {
		System.out.println(numberOfPaths(3,7));
		System.out.println(numberOfPathsDP(3,7));
	}

}
