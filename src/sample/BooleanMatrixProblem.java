package sample;

import java.util.Scanner;

public class BooleanMatrixProblem {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter rows and cols");
		String row[]=sc.nextLine().split(" ");
		System.out.println("Enter numbers");
		String numbers[]=sc.nextLine().split(" ");
		int rows=Integer.parseInt(row[0]);
		int cols=Integer.parseInt(row[1]);
		int data[][]=new int[rows][cols];
		int output[][]=new int[rows][cols];
		int a=0;
		for(int i=0;i<rows;i++){
			for(int j=0;j<cols;j++){
				data[i][j]=Integer.parseInt(numbers[a]);
				a++;
				if(data[i][j]==1){
					for(int k=0;k<cols;k++){
						output[i][k]=1;
					}
					for(int k=0;k<rows;k++){
						output[k][j]=1;
					}
				}
			}
		}
		for(int k=0;k<rows;k++){
			for(int h=0;h<cols;h++){
				System.out.print(output[k][h]+" ");
			}
		}
	}
}
