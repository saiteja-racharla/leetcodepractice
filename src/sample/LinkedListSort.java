package sample;

import java.util.Iterator;
import java.util.TreeSet;

class LinkedListNode{
	int data;
	LinkedListNode next;
	public LinkedListNode(int data){
		this.data=data;
	}
}

class LinkedListOperations{
	LinkedListNode root=null;
	public LinkedListNode insert(int data){
		LinkedListNode node=new LinkedListNode(data);
		if(root==null)
			root=node;
		else{
			LinkedListNode temp=root;
			while(temp.next!=null){
				temp=temp.next;
			}
			temp.next=node;
		}
		return root;
	}
	public LinkedListNode sort(LinkedListNode node){
		TreeSet<Integer> ts=new TreeSet<Integer>();
		while(node!=null){
			ts.add(node.data);
			node=node.next;
		}
		Iterator<Integer> it=ts.iterator();
		LinkedListNode innerNode=null,root=null;
		while(it.hasNext()){
			innerNode=new LinkedListNode(it.next());
			if(root==null)
				root=innerNode;
			else{
				LinkedListNode temp=root;
				while(temp.next!=null){
					temp=temp.next;
				}
				temp.next=innerNode;
			}
		}
		return root;
	}
}

public class LinkedListSort {
	public static void main(String[] args) {
		LinkedListNode node;
		LinkedListOperations ls=new LinkedListOperations();
		node=ls.insert(45);
		node=ls.insert(30);
		node=ls.insert(35);
		node=ls.insert(20);
		node=ls.sort(node);
		while(node!=null){
			System.out.print(node.data+" ");
			node=node.next;
		}
	}

}
